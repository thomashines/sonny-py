class GCode:

	def __init__(self):
		self.home_x = 0.0
		self.home_y = 0.0
		self.lift_height = 0.0
		self.rate_lift = 0.0
		self.rate_move = 0.0
		self.rate_draw = 0.0
		self.rate_xy = 0.0

		self.position = [0.0, 0.0, 0.0]
		self.lifted = False
		self.lift_with_servo = True
		self.servo_id = 0

		self.actions = []

	def echo_gcode(self):
		print("G21 ; Set units to millimeters")
		print("G90 ; Set to absolute coordinates")
		print("G0 F60 ; Set feed rate\n")

		for description, action in self.actions:
			print("%-35s ; %-16s" % (
				action,
				description))

		print("\nM84 ; disable motors")

	def move_to(self, description, x, y, z, rate):
		self.actions.append([
			description,
			"G0 X%06.2f Y%06.2f Z%06.2f F%07.2f" % (
				x,
				y,
				z,
				60.0*rate)])
		self.position = [x, y, z]

	def move_abs_xy(self, x, y):
		self.move_to(
			"move_abs_xy",
			x,
			y,
			self.position[2],
			self.rate_xy)
		# print("; move_abs_xy x", x, "y", y)

	def move_rel_xy(self, dx, dy):
		self.move_to(
			"move_rel_xy",
			self.position[0] + dx,
			self.position[1] + dy,
			self.position[2],
			self.rate_xy)

	def home_xy(self):
		self.actions.append([
			"home_xy",
			"G28 X Y"])
		self.position[0] = self.home_x
		self.position[1] = self.home_y

	def home_z(self):
		self.actions.append([
			"home_z",
			"G28 Z"])
		self.position[2] = 0

	def servo_set(self, angle):
		self.wait_on_actions()
		self.actions.append([
			"servo_set",
			"M280 P%d S%06.2f" % (
				self.servo_id,
				angle)])
		self.dwell(200.0)

	def lift(self):
		if not self.lifted:
			self.servo_set(45.0)
			self.lifted = True

	def lower(self):
		if self.lifted:
			self.servo_set(135.0)
			self.lifted = False

	def dwell(self, milliseconds):
		self.actions.append([
			"dwell",
			"G4 P%06.2f" % milliseconds])

	def wait_on_actions(self):
		self.actions.append([
			"wait_on_actions",
			"M400"])

	def start(self):
		self.home_z()
		self.lift()
		self.home_xy()

	def finish(self):
		self.lift()
		self.home_xy()
		if self.lift_with_servo:
			self.lower()
