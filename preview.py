#!/bin/python2

import pygame
import sys

if __name__ == "__main__":
	print("Sonny G-code previewer.")

	print("START")

	# Open file
	print("GCode file:", sys.argv[1])
	with open(sys.argv[1], "r") as gcode_file:

		scale = 2.0

		# Start pygame
		pygame.init()
		screen_size = [int(190*scale), int(280*scale)]
		screen = pygame.display.set_mode(screen_size)
		screen.fill((0, 0, 0))

		# top left
		translate = [10.0*scale, 10.0*scale]

		draw = False
		position = [translate[0], translate[1]]

		# top left
		pygame.draw.line(screen, (255, 0, 0), [0,0], translate)
		# left
		pygame.draw.line(screen, (255, 0, 0), translate, [translate[0], screen_size[1] - translate[1]])
		# top
		pygame.draw.line(screen, (255, 0, 0), translate, [screen_size[0] - translate[0], translate[1]])
		# right
		pygame.draw.line(screen, (255, 0, 0), [screen_size[0] - translate[0], translate[1]], [screen_size[0] - translate[0], screen_size[1] - translate[1]])
		# bottom
		pygame.draw.line(screen, (255, 0, 0), [translate[0], screen_size[1] - translate[1]], [screen_size[0] - translate[0], screen_size[1] - translate[1]])

		# x center
		pygame.draw.line(screen, (255, 0, 0), [screen_size[0]/2, 0], [screen_size[0]/2, screen_size[1]])
		# y center
		pygame.draw.line(screen, (255, 0, 0), [0, screen_size[1]/2], [screen_size[0], screen_size[1]/2])

		for line in gcode_file:
			line_split = line.split(" ")
			if len(line_split) > 0:
				if line_split[0] == "G0":
					move_to = [position[0], position[1]]
					for word in line_split:
						if len(word) > 2:
							if word[0] == "X":
								move_to[0] = float(word[1:])*scale + translate[0]
							elif word[0] == "Y":
								move_to[1] = screen_size[1] - (float(word[1:])*scale + translate[1])
					if draw:
						pygame.draw.line(screen, (255, 255, 255), position, move_to)
					position = move_to
				elif line_split[0] == "M280":
					for word in line_split:
						if len(word) > 2:
							if word[0] == "S":
								servo = float(word[1:])
								if servo > 90:
									draw = True
								else:
									draw = False

		# Update screen
		pygame.display.update()

		print("END")

		# Wait for quit
		while (True):
			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					pygame.quit()
					sys.exit()
				if event.type == pygame.KEYDOWN:
					if event.key == pygame.K_ESCAPE:
						pygame.quit()
						sys.exit()
