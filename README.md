Sonny is a 2D G-code generator intended to make sketches of image files using pens.  
The original goal was to sketch bitmap images in a style similar to Sonny's in that one scene from I, Robot. Now, however, I want it to print SVG images.

To generate a path:
$ ./sonny.py test/template.svg > test/template.gcode

To preview the Gcode:
$ ./preview.py test/template.gcode

Inkscape fonts will render as outlines. Use the Eggbot Hershey text extension to render engraving strokes if this is not desired.

Process for LyX: (doesn't work yet)

1. Export from LyX to PDF
2. Convert PDF2SVG
3. Use Sonny to convert SVG to GCode
4. Print