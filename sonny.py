#!/bin/python3

import sys
from math import sqrt

# import pysvg
# from pysvg import parser
import svg.path
from svg.path import parse_path
from lxml import etree

from gcode import GCode

# bed actual: X0-210, Y0-270, Z0-300

dpmm = 3.5433  # pixels/mm
segment_length = 0.5  # mm
min_x = 0.0
max_x = 170.0
min_y = 0.0
max_y = 260.0


def recurse_paths(gc, root, translate_x, translate_y):
	transform = root.get("transform")
	if transform:
		transform = root.get("transform").split('(')
		if transform[0] == "translate":
			coords = transform[1][:-1].split(',')
			translate_x += float(coords[0])
			translate_y += float(coords[1])
	print(";", "\tTranslate X", translate_x, "Y", translate_y)
	if "path" in root.tag:
		path = parse_path(root.get("d"))
		end = None
		for part in path:
			print(";", "\t\t", part)
			# print(";", "\t\t", part.start, part.end)
			if part.length()/dpmm > segment_length:
				if end:
					# If start is far from previous end, lift
					distance = end - part.start
					if sqrt(distance.real**2 + distance.imag**2) > dpmm*segment_length:
						gc.lift()
				gc.move_abs_xy(
					(part.start.real + translate_x)/dpmm,
					max_y - (part.start.imag + translate_y)/dpmm)
				gc.lower()
				if type(part) is svg.path.path.CubicBezier:
					segment_count = int(part.length()/(dpmm*segment_length))
					for i in range(1, segment_count):
						point = part.point(i*1.0/segment_count)
						gc.move_abs_xy(
							(point.real + translate_x)/dpmm,
							max_y - (point.imag + translate_y)/dpmm)
				elif type(part) is svg.path.path.Line:
					pass
				else:
					# print("; Other path:", type(part))
					pass
				gc.move_abs_xy(
					(part.end.real + translate_x)/dpmm,
					max_y - (part.end.imag + translate_y)/dpmm)
				end = part.end
		gc.lift()
	for child in root:
		recurse_paths(gc, child, translate_x, translate_y)

if __name__ == "__main__":
	print("; Sonny G-code generator.\n")
	print("; START\n")

	# Set variables
	gc = GCode()
	gc.home_x = 0.0
	gc.home_y = 270.0
	gc.lift_height = 5.0
	gc.rate_lift = 3.0
	gc.rate_move = 100
	gc.rate_draw = 100
	gc.rate_xy = 100
	gc.lift_with_servo = True
	gc.servo_id = 0

	# Home
	gc.start()

	# Load image
	# circle_svg = parser.parse('./test/circle.svg')
	# recurse_paths(gc, circle_svg)
	# tree = etree.parse("./test/circle.svg")
	tree = etree.parse(sys.argv[1])
	recurse_paths(gc, tree.getroot(), 0, 0)

	# Draw rectangle
	# gc.move_rel_xy(20, -20)  # To position
	# gc.lower()
	# gc.move_rel_xy(10, 0)  # Right
	# gc.move_rel_xy(0, -10)  # Down
	# gc.move_rel_xy(-10, 0)  # Left
	# gc.move_rel_xy(0, 10)  # Up

	# Home
	gc.finish()
	gc.echo_gcode()
	print("\n; END\n")
