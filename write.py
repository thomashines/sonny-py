#!/bin/python2

import os
import pygame
import sys


black = (0, 0, 0)
red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
white = (255, 255, 255)

mouse_left = 0
mouse_right = 2


def write(save_directory):
	# Start pygame
	pygame.init()
	screen_size = [200, 200]
	screen = pygame.display.set_mode(screen_size)

	# Set up base screen
	screen.fill(white)
	pygame.display.update()

	# Get next filename
	# Setup variables
	mouse_down = pygame.mouse.get_pressed()[mouse_left]
	tracks = []
	track = []
	# Start loop
	while (True):
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				sys.exit()
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_ESCAPE:
					pygame.quit()
					sys.exit()
				elif event.key == pygame.K_s:
					print("Saving to")
					screen.fill(white)
					pygame.display.update()
					tracks = []
					track = []
			if event.type in (pygame.MOUSEBUTTONDOWN, pygame.MOUSEBUTTONUP):
				mouse_new = pygame.mouse.get_pressed()[mouse_left]
				if mouse_down and not mouse_new:
					tracks.append(track)
					print("Tracks", len(tracks))
				if not mouse_down and mouse_new:
					track = [pygame.mouse.get_pos()]
				mouse_down = mouse_new
			if event.type == pygame.MOUSEMOTION:
				if mouse_down:
					track.append(event.pos)
					# Draw track
					if len(track) >= 2:
						pygame.draw.line(screen, black, track[-2], track[-1])
						pygame.display.update()

		# Redraw all tracks
		# screen.fill(white)
		# for t in tracks:
		# 	for i in range(len(t) - 1):
		# 		pygame.draw.line(screen, black, t[i], t[i + 1])
		# pygame.display.update()

if __name__ == "__main__":
	print("Sonny G-code writing pad.")

	print("START")

	# Check arguments
	if len(sys.argv) != 2:
		print("Usage: ./write.py save_directory")
	else:
		save_directory = sys.argv[1]
		if not os.access(save_directory, os.W_OK):
			print("Path", save_directory, "not writable.")
		else:
			write(save_directory)

	print("END")

