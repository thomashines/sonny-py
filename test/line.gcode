; Sonny G-code generator.

; START

; 	 [0, 0]
; 	 [0, 0]
; 	 [0, 0]
; 	 [0, 0]
; 	 [0, 0]
; 	 [0, 0]
; 	 [0, 0]
; 	 [0, 0]
; 	 [0, 0]
; 	 [0.0, -131.10236]
; 	 [0.0, -131.10236]
; 		 Line(start=(1.147386+131.85078j), end=(601.21482+1051.6137j))
G21 ; Set units to millimeters
G90 ; Set to absolute coordinates
G0 F60 ; Set feed rate

G28 Z                               ; home_z          
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G28 X Y                             ; home_xy         
G0 X000.32 Y259.79 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X169.68 Y000.21 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G28 X Y                             ; home_xy         
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           

M84 ; disable motors

; END

