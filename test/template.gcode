; Sonny G-code generator.

; START

; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0.0 Y -131.10236
; 	Translate X 0.0 Y -131.10236
; 	Translate X 233.7075 Y 462.12991999999997
; 	Translate X 243.7075 Y 462.12991999999997
; 		 Line(start=(7-9j), end=(5-11j))
; 		 Line(start=(5-11j), end=(2-12j))
; 		 Line(start=(2-12j), end=(-2-12j))
; 		 Line(start=(-2-12j), end=(-5-11j))
; 		 Line(start=(-5-11j), end=(-7-9j))
; 		 Line(start=(-7-9j), end=(-7-7j))
; 		 Line(start=(-7-7j), end=(-6-5j))
; 		 Line(start=(-6-5j), end=(-5-4j))
; 		 Line(start=(-5-4j), end=(-3-3j))
; 		 Line(start=(-3-3j), end=(3-1j))
; 		 Line(start=(3-1j), end=(5+0j))
; 		 Line(start=(5+0j), end=(6+1j))
; 		 Line(start=(6+1j), end=(7+3j))
; 		 Line(start=(7+3j), end=(7+6j))
; 		 Line(start=(7+6j), end=(5+8j))
; 		 Line(start=(5+8j), end=(2+9j))
; 		 Line(start=(2+9j), end=(-2+9j))
; 		 Line(start=(-2+9j), end=(-5+8j))
; 		 Line(start=(-5+8j), end=(-7+6j))
; 	Translate X 264.7075 Y 462.12991999999997
; 		 Line(start=(-2-12j), end=(-4-11j))
; 		 Line(start=(-4-11j), end=(-6-9j))
; 		 Line(start=(-6-9j), end=(-7-7j))
; 		 Line(start=(-7-7j), end=(-8-4j))
; 		 Line(start=(-8-4j), end=(-8+1j))
; 		 Line(start=(-8+1j), end=(-7+4j))
; 		 Line(start=(-7+4j), end=(-6+6j))
; 		 Line(start=(-6+6j), end=(-4+8j))
; 		 Line(start=(-4+8j), end=(-2+9j))
; 		 Line(start=(-2+9j), end=(2+9j))
; 		 Line(start=(2+9j), end=(4+8j))
; 		 Line(start=(4+8j), end=(6+6j))
; 		 Line(start=(6+6j), end=(7+4j))
; 		 Line(start=(7+4j), end=(8+1j))
; 		 Line(start=(8+1j), end=(8-4j))
; 		 Line(start=(8-4j), end=(7-7j))
; 		 Line(start=(7-7j), end=(6-9j))
; 		 Line(start=(6-9j), end=(4-11j))
; 		 Line(start=(4-11j), end=(2-12j))
; 		 Line(start=(2-12j), end=(-2-12j))
; 	Translate X 287.7075 Y 462.12991999999997
; 		 Line(start=(-8-12j), end=(-8+9j))
; 		 Line(start=(-8-12j), end=9j)
; 		 Line(start=(8-12j), end=9j)
; 		 Line(start=(8-12j), end=(8+9j))
; 	Translate X 310.7075 Y 462.12991999999997
; 		 Line(start=(-7-12j), end=(-7+9j))
; 		 Line(start=(-7-12j), end=(7+9j))
; 		 Line(start=(7-12j), end=(7+9j))
; 	Translate X 325.7075 Y 462.12991999999997
; 		 Line(start=-12j, end=9j)
; 	Translate X 338.7075 Y 462.12991999999997
; 		 Line(start=-12j, end=(-8+9j))
; 		 Line(start=-12j, end=(8+9j))
; 		 Line(start=(-5+2j), end=(5+2j))
; 	Translate X 357.7075 Y 462.12991999999997
; 		 Line(start=(8-7j), end=(7-9j))
; 		 Line(start=(7-9j), end=(5-11j))
; 		 Line(start=(5-11j), end=(3-12j))
; 		 Line(start=(3-12j), end=(-1-12j))
; 		 Line(start=(-1-12j), end=(-3-11j))
; 		 Line(start=(-3-11j), end=(-5-9j))
; 		 Line(start=(-5-9j), end=(-6-7j))
; 		 Line(start=(-6-7j), end=(-7-4j))
; 		 Line(start=(-7-4j), end=(-7+1j))
; 		 Line(start=(-7+1j), end=(-6+4j))
; 		 Line(start=(-6+4j), end=(-5+6j))
; 		 Line(start=(-5+6j), end=(-3+8j))
; 		 Line(start=(-3+8j), end=(-1+9j))
; 		 Line(start=(-1+9j), end=(3+9j))
; 		 Line(start=(3+9j), end=(5+8j))
; 		 Line(start=(5+8j), end=(7+6j))
; 		 Line(start=(7+6j), end=(8+4j))
G21 ; Set units to millimeters
G90 ; Set to absolute coordinates
G0 F60 ; Set feed rate

G28 Z                               ; home_z          
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G28 X Y                             ; home_xy         
G0 X070.76 Y132.12 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X070.19 Y132.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X070.19 Y132.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X069.34 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X069.34 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X068.22 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X068.22 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X067.37 Y132.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X067.37 Y132.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X066.80 Y132.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X066.80 Y132.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X066.80 Y131.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X066.80 Y131.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X067.09 Y130.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X067.37 Y130.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X067.93 Y130.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X067.93 Y130.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X069.63 Y129.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X069.63 Y129.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X070.19 Y129.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X070.47 Y129.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X070.76 Y128.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X070.76 Y128.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X070.76 Y127.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X070.76 Y127.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X070.19 Y127.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X070.19 Y127.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X069.34 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X069.34 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X068.22 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X068.22 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X067.37 Y127.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X067.37 Y127.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X066.80 Y127.88 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X074.14 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X073.58 Y132.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X073.58 Y132.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X073.01 Y132.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X073.01 Y132.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X072.73 Y131.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X072.73 Y131.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X072.45 Y130.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X072.45 Y130.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X072.45 Y129.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X072.45 Y129.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X072.73 Y128.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X072.73 Y128.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X073.01 Y127.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X073.01 Y127.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X073.58 Y127.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X073.58 Y127.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X074.14 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X074.14 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X075.27 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X075.27 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X075.84 Y127.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X075.84 Y127.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X076.40 Y127.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X076.40 Y127.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X076.68 Y128.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X076.68 Y128.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X076.96 Y129.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X076.96 Y129.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X076.96 Y130.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X076.96 Y130.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X076.68 Y131.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X076.68 Y131.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X076.40 Y132.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X076.40 Y132.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X075.84 Y132.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X075.84 Y132.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X075.27 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X075.27 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X074.14 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X078.94 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X078.94 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X078.94 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X081.20 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X083.46 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X081.20 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X083.46 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X083.46 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X085.71 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X085.71 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X085.71 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X089.66 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X089.66 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X089.66 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X091.92 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X091.92 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X095.59 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X093.33 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X095.59 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X097.85 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X094.18 Y129.01 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X097.00 Y129.01 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X103.21 Y131.55 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X102.93 Y132.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X102.93 Y132.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X102.36 Y132.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X102.36 Y132.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X101.80 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X101.80 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X100.67 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X100.67 Y132.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X100.11 Y132.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X100.11 Y132.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X099.54 Y132.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X099.54 Y132.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X099.26 Y131.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X099.26 Y131.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X098.98 Y130.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X098.98 Y130.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X098.98 Y129.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X098.98 Y129.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X099.26 Y128.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X099.26 Y128.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X099.54 Y127.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X099.54 Y127.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X100.11 Y127.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X100.11 Y127.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X100.67 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X100.67 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X101.80 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X101.80 Y127.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X102.36 Y127.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X102.36 Y127.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X102.93 Y127.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X102.93 Y127.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X103.21 Y128.45 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G28 X Y                             ; home_xy         
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           

M84 ; disable motors

; END

