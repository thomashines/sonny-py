; Sonny G-code generator.

; START

; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.78125-5.328125j), control1=(2.0625-5.875j), control2=(2.65625-6.34375j), end=(3.3125-6.34375j))
; 		 CubicBezier(start=(3.3125-6.34375j), control1=(3.921875-6.34375j), control2=(4.375-5.921875j), end=(4.375-5.109375j))
; 		 CubicBezier(start=(4.375-5.109375j), control1=(4.375-4.09375j), control2=(3.703125-3.3125j), end=(3.046875-2.640625j))
; 		 CubicBezier(start=(3.046875-2.640625j), control1=(2.359375-1.953125j), control2=(1.640625-1.3125j), end=(0.9375-0.640625j))
; 		 CubicBezier(start=(0.9375-0.640625j), control1=(0.703125-0.4375j), control2=(0.53125-0.34375j), end=(0.5-0.015625j))
; 		 Line(start=(0.5-0.015625j), end=(4.09375-0.015625j))
; 		 Line(start=(4.09375-0.015625j), end=(4.1875-0.03125j))
; 		 Line(start=(4.1875-0.03125j), end=(4.765625-1.75j))
; 		 Line(start=(4.765625-1.75j), end=(4.609375-1.75j))
; 		 CubicBezier(start=(4.609375-1.75j), control1=(4.421875-1.75j), control2=(4.296875-0.921875j), end=(4.09375-0.84375j))
; 		 CubicBezier(start=(4.09375-0.84375j), control1=(3.953125-0.78125j), control2=(3.75-0.78125j), end=(3.546875-0.78125j))
; 		 CubicBezier(start=(3.546875-0.78125j), control1=(3.34375-0.78125j), control2=(3.140625-0.78125j), end=(3.015625-0.78125j))
; 		 Line(start=(3.015625-0.78125j), end=(1.421875-0.78125j))
; 		 CubicBezier(start=(1.421875-0.78125j), control1=(2.078125-1.390625j), control2=(2.796875-1.9375j), end=(3.515625-2.484375j))
; 		 CubicBezier(start=(3.515625-2.484375j), control1=(4.328125-3.125j), control2=(5.28125-3.859375j), end=(5.28125-5.03125j))
; 		 CubicBezier(start=(5.28125-5.03125j), control1=(5.28125-5.515625j), control2=(5.078125-6.046875j), end=(4.609375-6.328125j))
; 		 CubicBezier(start=(4.609375-6.328125j), control1=(4.3125-6.53125j), control2=(3.953125-6.625j), end=(3.578125-6.65625j))
; 		 Line(start=(3.578125-6.65625j), end=(3.453125-6.65625j))
; 		 CubicBezier(start=(3.453125-6.65625j), control1=(2.640625-6.65625j), control2=(1.859375-6.15625j), end=(1.5-5.421875j))
; 		 CubicBezier(start=(1.5-5.421875j), control1=(1.390625-5.21875j), control2=(1.296875-4.984375j), end=(1.296875-4.75j))
; 		 CubicBezier(start=(1.296875-4.75j), control1=(1.296875-4.5j), control2=(1.4375-4.296875j), end=(1.75-4.296875j))
; 		 CubicBezier(start=(1.75-4.296875j), control1=(2.078125-4.296875j), control2=(2.359375-4.546875j), end=(2.359375-4.90625j))
; 		 CubicBezier(start=(2.359375-4.90625j), control1=(2.359375-5.109375j), control2=(2.21875-5.328125j), end=(1.90625-5.328125j))
; 		 CubicBezier(start=(1.90625-5.328125j), control1=(1.859375-5.328125j), control2=(1.828125-5.328125j), end=(1.78125-5.328125j))
; 		 Line(start=(1.78125-5.328125j), end=(1.78125-5.328125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.109375-4.515625j), end=(1.265625-4.515625j))
; 		 CubicBezier(start=(1.265625-4.515625j), control1=(1.40625-4.515625j), control2=(1.421875-4.734375j), end=(1.453125-4.859375j))
; 		 CubicBezier(start=(1.453125-4.859375j), control1=(1.5625-5.25j), control2=(1.671875-5.71875j), end=(1.953125-6.03125j))
; 		 CubicBezier(start=(1.953125-6.03125j), control1=(2.34375-6.4375j), control2=(2.9375-6.453125j), end=(3.453125-6.453125j))
; 		 Line(start=(3.453125-6.453125j), end=(3.8125-6.453125j))
; 		 CubicBezier(start=(3.8125-6.453125j), control1=(3.984375-6.453125j), control2=(4.171875-6.4375j), end=(4.171875-6.234375j))
; 		 CubicBezier(start=(4.171875-6.234375j), control1=(4.171875-6.109375j), control2=(4.140625-5.984375j), end=(4.125-5.875j))
; 		 Line(start=(4.125-5.875j), end=(3.296875-0.84375j))
; 		 CubicBezier(start=(3.296875-0.84375j), control1=(3.1875-0.375j), control2=(2.953125-0.359375j), end=(2.640625-0.34375j))
; 		 CubicBezier(start=(2.640625-0.34375j), control1=(2.34375-0.3125j), control2=(2.046875-0.3125j), end=(1.75-0.3125j))
; 		 Line(start=(1.75-0.3125j), end=(1.703125-0.015625j))
; 		 Line(start=(1.703125-0.015625j), end=(5.484375-0.015625j))
; 		 Line(start=(5.484375-0.015625j), end=(5.53125-0.3125j))
; 		 Line(start=(5.53125-0.3125j), end=(4.890625-0.3125j))
; 		 CubicBezier(start=(4.890625-0.3125j), control1=(4.53125-0.3125j), control2=(4.140625-0.34375j), end=(4.140625-0.640625j))
; 		 CubicBezier(start=(4.140625-0.640625j), control1=(4.140625-0.71875j), control2=(4.171875-0.8125j), end=(4.1875-0.890625j))
; 		 Line(start=(4.1875-0.890625j), end=(5.03125-5.984375j))
; 		 CubicBezier(start=(5.03125-5.984375j), control1=(5.078125-6.28125j), control2=(5.0625-6.453125j), end=(5.6875-6.453125j))
; 		 Line(start=(5.6875-6.453125j), end=(5.96875-6.453125j))
; 		 CubicBezier(start=(5.96875-6.453125j), control1=(6.625-6.453125j), control2=(7.375-6.453125j), end=(7.375-5.296875j))
; 		 CubicBezier(start=(7.375-5.296875j), control1=(7.375-5.046875j), control2=(7.359375-4.765625j), end=(7.328125-4.515625j))
; 		 Line(start=(7.328125-4.515625j), end=(7.5-4.515625j))
; 		 Line(start=(7.5-4.515625j), end=(7.578125-4.546875j))
; 		 Line(start=(7.578125-4.546875j), end=(7.765625-6.765625j))
; 		 Line(start=(7.765625-6.765625j), end=(1.8125-6.765625j))
; 		 Line(start=(1.8125-6.765625j), end=(1.6875-6.734375j))
; 		 Line(start=(1.6875-6.734375j), end=(1.53125-6.15625j))
; 		 Line(start=(1.53125-6.15625j), end=(1.109375-4.515625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.46875-6.796875j), end=(1.421875-6.515625j))
; 		 CubicBezier(start=(1.421875-6.515625j), control1=(1.53125-6.515625j), control2=(1.65625-6.515625j), end=(1.78125-6.515625j))
; 		 CubicBezier(start=(1.78125-6.515625j), control1=(2.078125-6.515625j), control2=(2.375-6.484375j), end=(2.375-6.21875j))
; 		 CubicBezier(start=(2.375-6.21875j), control1=(2.375-6.125j), control2=(2.34375-6.015625j), end=(2.328125-5.921875j))
; 		 Line(start=(2.328125-5.921875j), end=(1.5-0.9375j))
; 		 CubicBezier(start=(1.5-0.9375j), control1=(1.453125-0.609375j), control2=(1.421875-0.40625j), end=(1.046875-0.34375j))
; 		 CubicBezier(start=(1.046875-0.34375j), control1=(0.828125-0.3125j), control2=(0.59375-0.3125j), end=(0.375-0.3125j))
; 		 Line(start=(0.375-0.3125j), end=(0.328125-0.015625j))
; 		 Line(start=(0.328125-0.015625j), end=(3.265625-0.015625j))
; 		 Line(start=(3.265625-0.015625j), end=(3.3125-0.3125j))
; 		 Line(start=(3.3125-0.3125j), end=(2.96875-0.3125j))
; 		 CubicBezier(start=(2.96875-0.3125j), control1=(2.640625-0.3125j), control2=(2.359375-0.34375j), end=(2.359375-0.59375j))
; 		 CubicBezier(start=(2.359375-0.59375j), control1=(2.359375-0.640625j), control2=(2.359375-0.671875j), end=(2.359375-0.71875j))
; 		 Line(start=(2.359375-0.71875j), end=(2.453125-1.21875j))
; 		 CubicBezier(start=(2.453125-1.21875j), control1=(2.578125-1.9375j), control2=(2.703125-2.671875j), end=(2.8125-3.40625j))
; 		 Line(start=(2.8125-3.40625j), end=(5.78125-3.40625j))
; 		 CubicBezier(start=(5.78125-3.40625j), control1=(5.75-3.15625j), control2=(5.703125-2.90625j), end=(5.65625-2.65625j))
; 		 Line(start=(5.65625-2.65625j), end=(5.34375-0.78125j))
; 		 CubicBezier(start=(5.34375-0.78125j), control1=(5.28125-0.34375j), control2=(4.9375-0.3125j), end=(4.4375-0.3125j))
; 		 Line(start=(4.4375-0.3125j), end=(4.25-0.3125j))
; 		 Line(start=(4.25-0.3125j), end=(4.203125-0.015625j))
; 		 Line(start=(4.203125-0.015625j), end=(7.140625-0.015625j))
; 		 Line(start=(7.140625-0.015625j), end=(7.1875-0.3125j))
; 		 CubicBezier(start=(7.1875-0.3125j), control1=(7.0625-0.3125j), control2=(6.9375-0.3125j), end=(6.8125-0.3125j))
; 		 CubicBezier(start=(6.8125-0.3125j), control1=(6.5-0.3125j), control2=(6.21875-0.34375j), end=(6.21875-0.625j))
; 		 CubicBezier(start=(6.21875-0.625j), control1=(6.21875-0.734375j), control2=(6.25-0.828125j), end=(6.265625-0.921875j))
; 		 Line(start=(6.265625-0.921875j), end=(7.125-6.0625j))
; 		 CubicBezier(start=(7.125-6.0625j), control1=(7.203125-6.484375j), control2=(7.5-6.515625j), end=(7.8125-6.515625j))
; 		 Line(start=(7.8125-6.515625j), end=(8.109375-6.515625j))
; 		 CubicBezier(start=(8.109375-6.515625j), control1=(8.25-6.515625j), control2=(8.265625-6.703125j), end=(8.265625-6.8125j))
; 		 Line(start=(8.265625-6.8125j), end=(5.328125-6.796875j))
; 		 Line(start=(5.328125-6.796875j), end=(5.28125-6.515625j))
; 		 CubicBezier(start=(5.28125-6.515625j), control1=(5.390625-6.515625j), control2=(5.53125-6.515625j), end=(5.65625-6.515625j))
; 		 CubicBezier(start=(5.65625-6.515625j), control1=(5.96875-6.515625j), control2=(6.25-6.484375j), end=(6.25-6.21875j))
; 		 CubicBezier(start=(6.25-6.21875j), control1=(6.25-6.046875j), control2=(6.1875-5.84375j), end=(6.15625-5.671875j))
; 		 Line(start=(6.15625-5.671875j), end=(5.953125-4.390625j))
; 		 CubicBezier(start=(5.953125-4.390625j), control1=(5.90625-4.171875j), control2=(5.859375-3.9375j), end=(5.84375-3.703125j))
; 		 Line(start=(5.84375-3.703125j), end=(2.859375-3.703125j))
; 		 CubicBezier(start=(2.859375-3.703125j), control1=(2.90625-3.921875j), control2=(2.9375-4.125j), end=(2.96875-4.34375j))
; 		 Line(start=(2.96875-4.34375j), end=(3.25-6j))
; 		 CubicBezier(start=(3.25-6j), control1=(3.328125-6.484375j), control2=(3.578125-6.515625j), end=(3.90625-6.515625j))
; 		 Line(start=(3.90625-6.515625j), end=(4.25-6.515625j))
; 		 CubicBezier(start=(4.25-6.515625j), control1=(4.390625-6.515625j), control2=(4.390625-6.703125j), end=(4.390625-6.8125j))
; 		 Line(start=(4.390625-6.8125j), end=(2.609375-6.796875j))
; 		 Line(start=(2.609375-6.796875j), end=(1.46875-6.796875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.421875-6.484375j), end=(1.734375-6.484375j))
; 		 CubicBezier(start=(1.734375-6.484375j), control1=(2.0625-6.484375j), control2=(2.375-6.453125j), end=(2.375-6.1875j))
; 		 CubicBezier(start=(2.375-6.1875j), control1=(2.375-6.078125j), control2=(2.34375-5.96875j), end=(2.328125-5.875j))
; 		 Line(start=(2.328125-5.875j), end=(1.46875-0.765625j))
; 		 CubicBezier(start=(1.46875-0.765625j), control1=(1.40625-0.34375j), control2=(1.109375-0.3125j), end=(0.796875-0.3125j))
; 		 Line(start=(0.796875-0.3125j), end=(0.484375-0.3125j))
; 		 CubicBezier(start=(0.484375-0.3125j), control1=(0.34375-0.3125j), control2=(0.34375-0.125j), end=(0.34375-0.015625j))
; 		 Line(start=(0.34375-0.015625j), end=(5.9375-0.015625j))
; 		 Line(start=(5.9375-0.015625j), end=(6.0625-0.03125j))
; 		 Line(start=(6.0625-0.03125j), end=(6.921875-2.578125j))
; 		 Line(start=(6.921875-2.578125j), end=(6.765625-2.578125j))
; 		 CubicBezier(start=(6.765625-2.578125j), control1=(6.625-2.578125j), control2=(6.609375-2.421875j), end=(6.578125-2.328125j))
; 		 CubicBezier(start=(6.578125-2.328125j), control1=(6.25-1.328125j), control2=(5.890625-0.390625j), end=(4.328125-0.328125j))
; 		 CubicBezier(start=(4.328125-0.328125j), control1=(4.109375-0.3125j), control2=(3.921875-0.3125j), end=(3.703125-0.3125j))
; 		 Line(start=(3.703125-0.3125j), end=(2.6875-0.3125j))
; 		 CubicBezier(start=(2.6875-0.3125j), control1=(2.515625-0.3125j), control2=(2.328125-0.34375j), end=(2.328125-0.53125j))
; 		 CubicBezier(start=(2.328125-0.53125j), control1=(2.328125-0.671875j), control2=(2.390625-0.859375j), end=(2.40625-1j))
; 		 Line(start=(2.40625-1j), end=(2.78125-3.203125j))
; 		 CubicBezier(start=(2.78125-3.203125j), control1=(2.796875-3.28125j), control2=(2.765625-3.375j), end=(2.90625-3.375j))
; 		 Line(start=(2.90625-3.375j), end=(3.703125-3.375j))
; 		 CubicBezier(start=(3.703125-3.375j), control1=(4.109375-3.375j), control2=(4.640625-3.375j), end=(4.640625-2.734375j))
; 		 CubicBezier(start=(4.640625-2.734375j), control1=(4.640625-2.5625j), control2=(4.609375-2.375j), end=(4.578125-2.21875j))
; 		 Line(start=(4.578125-2.21875j), end=(4.828125-2.21875j))
; 		 Line(start=(4.828125-2.21875j), end=(5.28125-4.859375j))
; 		 Line(start=(5.28125-4.859375j), end=(5.03125-4.859375j))
; 		 CubicBezier(start=(5.03125-4.859375j), control1=(4.96875-4.5625j), control2=(4.921875-4.25j), end=(4.734375-4j))
; 		 CubicBezier(start=(4.734375-4j), control1=(4.5-3.71875j), control2=(4.09375-3.6875j), end=(3.703125-3.6875j))
; 		 Line(start=(3.703125-3.6875j), end=(2.859375-3.6875j))
; 		 CubicBezier(start=(2.859375-3.6875j), control1=(2.953125-4.34375j), control2=(3.078125-5j), end=(3.1875-5.65625j))
; 		 Line(start=(3.1875-5.65625j), end=(3.265625-6.125j))
; 		 CubicBezier(start=(3.265625-6.125j), control1=(3.296875-6.453125j), control2=(3.515625-6.484375j), end=(3.75-6.484375j))
; 		 Line(start=(3.75-6.484375j), end=(5.1875-6.484375j))
; 		 CubicBezier(start=(5.1875-6.484375j), control1=(5.6875-6.484375j), control2=(6.3125-6.40625j), end=(6.578125-5.90625j))
; 		 CubicBezier(start=(6.578125-5.90625j), control1=(6.703125-5.65625j), control2=(6.734375-5.375j), end=(6.734375-5.078125j))
; 		 CubicBezier(start=(6.734375-5.078125j), control1=(6.734375-4.90625j), control2=(6.734375-4.71875j), end=(6.734375-4.546875j))
; 		 Line(start=(6.734375-4.546875j), end=(6.984375-4.546875j))
; 		 Line(start=(6.984375-4.546875j), end=(6.984375-6.0625j))
; 		 Line(start=(6.984375-6.0625j), end=(7.0625-6.78125j))
; 		 Line(start=(7.0625-6.78125j), end=(1.578125-6.78125j))
; 		 Line(start=(1.578125-6.78125j), end=(1.46875-6.765625j))
; 		 Line(start=(1.46875-6.765625j), end=(1.453125-6.6875j))
; 		 Line(start=(1.453125-6.6875j), end=(1.421875-6.484375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(4.875-7.03125j), control1=(2.734375-6.796875j), control2=(1.0625-4.90625j), end=(1.0625-2.765625j))
; 		 CubicBezier(start=(1.0625-2.765625j), control1=(1.0625-1.46875j), control2=(1.6875-0.28125j), end=(3.1875+0.140625j))
; 		 CubicBezier(start=(3.1875+0.140625j), control1=(3.40625+0.1875j), control2=(3.625+0.203125j), end=(3.84375+0.203125j))
; 		 CubicBezier(start=(3.84375+0.203125j), control1=(5.984375+0.203125j), control2=(7.8125-1.90625j), end=(7.8125-4.03125j))
; 		 CubicBezier(start=(7.8125-4.03125j), control1=(7.8125-5.46875j), control2=(6.953125-7.046875j), end=(5.03125-7.046875j))
; 		 CubicBezier(start=(5.03125-7.046875j), control1=(4.984375-7.046875j), control2=(4.921875-7.03125j), end=(4.875-7.03125j))
; 		 Line(start=(4.875-7.03125j), end=(4.875-7.03125j))
; 		 CubicBezier(start=(6.828125-4.171875j), control1=(6.75-2.6875j), control2=(6.09375-0.703125j), end=(4.46875-0.15625j))
; 		 CubicBezier(start=(4.46875-0.15625j), control1=(4.28125-0.09375j), control2=(4.0625-0.046875j), end=(3.859375-0.046875j))
; 		 CubicBezier(start=(3.859375-0.046875j), control1=(2.515625-0.046875j), control2=(2.0625-1.46875j), end=(2.0625-2.515625j))
; 		 CubicBezier(start=(2.0625-2.515625j), control1=(2.0625-4.109375j), control2=(2.734375-6.25j), end=(4.53125-6.734375j))
; 		 CubicBezier(start=(4.53125-6.734375j), control1=(4.671875-6.765625j), control2=(4.828125-6.796875j), end=(5-6.796875j))
; 		 CubicBezier(start=(5-6.796875j), control1=(6.296875-6.796875j), control2=(6.84375-5.546875j), end=(6.84375-4.5j))
; 		 CubicBezier(start=(6.84375-4.5j), control1=(6.84375-4.390625j), control2=(6.84375-4.28125j), end=(6.828125-4.171875j))
; 		 Line(start=(6.828125-4.171875j), end=(6.828125-4.171875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.46875-6.8125j), end=(1.421875-6.515625j))
; 		 CubicBezier(start=(1.421875-6.515625j), control1=(1.53125-6.515625j), control2=(1.671875-6.515625j), end=(1.78125-6.515625j))
; 		 CubicBezier(start=(1.78125-6.515625j), control1=(2.09375-6.515625j), control2=(2.390625-6.484375j), end=(2.390625-6.21875j))
; 		 CubicBezier(start=(2.390625-6.21875j), control1=(2.390625-6.125j), control2=(2.359375-6.015625j), end=(2.34375-5.921875j))
; 		 Line(start=(2.34375-5.921875j), end=(1.515625-0.9375j))
; 		 CubicBezier(start=(1.515625-0.9375j), control1=(1.46875-0.609375j), control2=(1.4375-0.40625j), end=(1.0625-0.34375j))
; 		 CubicBezier(start=(1.0625-0.34375j), control1=(0.84375-0.3125j), control2=(0.609375-0.3125j), end=(0.390625-0.3125j))
; 		 Line(start=(0.390625-0.3125j), end=(0.34375-0.015625j))
; 		 Line(start=(0.34375-0.015625j), end=(3.25-0.015625j))
; 		 Line(start=(3.25-0.015625j), end=(3.296875-0.3125j))
; 		 Line(start=(3.296875-0.3125j), end=(2.953125-0.3125j))
; 		 CubicBezier(start=(2.953125-0.3125j), control1=(2.625-0.3125j), control2=(2.328125-0.34375j), end=(2.328125-0.59375j))
; 		 CubicBezier(start=(2.328125-0.59375j), control1=(2.328125-0.640625j), control2=(2.34375-0.671875j), end=(2.34375-0.71875j))
; 		 Line(start=(2.34375-0.71875j), end=(2.734375-3.03125j))
; 		 CubicBezier(start=(2.734375-3.03125j), control1=(2.734375-3.125j), control2=(2.765625-3.21875j), end=(2.765625-3.3125j))
; 		 Line(start=(2.765625-3.3125j), end=(3.453125-3.3125j))
; 		 CubicBezier(start=(3.453125-3.3125j), control1=(3.578125-3.3125j), control2=(3.703125-3.3125j), end=(3.84375-3.3125j))
; 		 CubicBezier(start=(3.84375-3.3125j), control1=(4.3125-3.3125j), control2=(5.046875-3.1875j), end=(5.046875-2.296875j))
; 		 CubicBezier(start=(5.046875-2.296875j), control1=(5.046875-2.171875j), control2=(5.03125-2.046875j), end=(5-1.90625j))
; 		 Line(start=(5-1.90625j), end=(4.921875-1.421875j))
; 		 CubicBezier(start=(4.921875-1.421875j), control1=(4.890625-1.25j), control2=(4.859375-1.09375j), end=(4.859375-0.953125j))
; 		 CubicBezier(start=(4.859375-0.953125j), control1=(4.859375-0.25j), control2=(5.46875+0.203125j), end=(6.375+0.203125j))
; 		 Line(start=(6.375+0.203125j), end=(6.453125+0.203125j))
; 		 CubicBezier(start=(6.453125+0.203125j), control1=(6.953125+0.140625j), control2=(7.28125-0.296875j), end=(7.421875-0.75j))
; 		 CubicBezier(start=(7.421875-0.75j), control1=(7.421875-0.796875j), control2=(7.453125-0.875j), end=(7.453125-0.921875j))
; 		 CubicBezier(start=(7.453125-0.921875j), control1=(7.453125-0.984375j), control2=(7.421875-1.0625j), end=(7.328125-1.0625j))
; 		 CubicBezier(start=(7.328125-1.0625j), control1=(7.171875-1.0625j), control2=(7.171875-0.8125j), end=(7.140625-0.703125j))
; 		 CubicBezier(start=(7.140625-0.703125j), control1=(7.015625-0.375j), control2=(6.75-0.015625j), end=(6.375-0.015625j))
; 		 CubicBezier(start=(6.375-0.015625j), control1=(5.96875-0.015625j), control2=(5.90625-0.515625j), end=(5.90625-0.84375j))
; 		 CubicBezier(start=(5.90625-0.84375j), control1=(5.90625-1.265625j), control2=(5.921875-1.5j), end=(5.921875-1.859375j))
; 		 Line(start=(5.921875-1.859375j), end=(5.921875-2.046875j))
; 		 CubicBezier(start=(5.921875-2.046875j), control1=(5.921875-2.75j), control2=(5.5625-3.09375j), end=(4.890625-3.40625j))
; 		 CubicBezier(start=(4.890625-3.40625j), control1=(5.796875-3.625j), control2=(6.921875-4.140625j), end=(6.921875-5.28125j))
; 		 CubicBezier(start=(6.921875-5.28125j), control1=(6.921875-5.875j), control2=(6.421875-6.328125j), end=(5.90625-6.5625j))
; 		 CubicBezier(start=(5.90625-6.5625j), control1=(5.390625-6.796875j), control2=(4.8125-6.8125j), end=(4.25-6.8125j))
; 		 Line(start=(4.25-6.8125j), end=(1.46875-6.8125j))
; 		 CubicBezier(start=(2.8125-3.53125j), control1=(2.859375-3.765625j), control2=(2.890625-4j), end=(2.9375-4.25j))
; 		 Line(start=(2.9375-4.25j), end=(3.25-6.125j))
; 		 CubicBezier(start=(3.25-6.125j), control1=(3.28125-6.390625j), control2=(3.34375-6.515625j), end=(3.796875-6.515625j))
; 		 Line(start=(3.796875-6.515625j), end=(4.28125-6.515625j))
; 		 CubicBezier(start=(4.28125-6.515625j), control1=(4.953125-6.515625j), control2=(5.9375-6.421875j), end=(5.9375-5.421875j))
; 		 CubicBezier(start=(5.9375-5.421875j), control1=(5.9375-4.828125j), control2=(5.71875-4.21875j), end=(5.265625-3.890625j))
; 		 CubicBezier(start=(5.265625-3.890625j), control1=(4.921875-3.65625j), control2=(4.46875-3.546875j), end=(4.0625-3.53125j))
; 		 Line(start=(4.0625-3.53125j), end=(2.8125-3.53125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.25-6.8125j), end=(1.203125-6.515625j))
; 		 Line(start=(1.203125-6.515625j), end=(1.453125-6.515625j))
; 		 CubicBezier(start=(1.453125-6.515625j), control1=(1.6875-6.515625j), control2=(1.9375-6.5j), end=(2.0625-6.375j))
; 		 CubicBezier(start=(2.0625-6.375j), control1=(2.203125-6.25j), control2=(2.265625-6.078125j), end=(2.328125-5.90625j))
; 		 Line(start=(2.328125-5.90625j), end=(2.71875-5.03125j))
; 		 CubicBezier(start=(2.71875-5.03125j), control1=(2.953125-4.515625j), control2=(3.1875-3.984375j), end=(3.421875-3.46875j))
; 		 Line(start=(3.421875-3.46875j), end=(3.640625-2.984375j))
; 		 CubicBezier(start=(3.640625-2.984375j), control1=(3.671875-2.90625j), control2=(3.75-2.796875j), end=(3.75-2.703125j))
; 		 CubicBezier(start=(3.75-2.703125j), control1=(3.75-2.65625j), control2=(3.734375-2.609375j), end=(3.71875-2.546875j))
; 		 Line(start=(3.71875-2.546875j), end=(3.453125-0.984375j))
; 		 CubicBezier(start=(3.453125-0.984375j), control1=(3.40625-0.6875j), control2=(3.421875-0.375j), end=(2.953125-0.34375j))
; 		 CubicBezier(start=(2.953125-0.34375j), control1=(2.75-0.3125j), control2=(2.53125-0.3125j), end=(2.328125-0.3125j))
; 		 Line(start=(2.328125-0.3125j), end=(2.265625-0.015625j))
; 		 Line(start=(2.265625-0.015625j), end=(5.1875-0.015625j))
; 		 Line(start=(5.1875-0.015625j), end=(5.234375-0.3125j))
; 		 CubicBezier(start=(5.234375-0.3125j), control1=(5.125-0.3125j), control2=(5-0.3125j), end=(4.890625-0.3125j))
; 		 CubicBezier(start=(4.890625-0.3125j), control1=(4.5625-0.3125j), control2=(4.265625-0.34375j), end=(4.265625-0.625j))
; 		 CubicBezier(start=(4.265625-0.625j), control1=(4.265625-0.703125j), control2=(4.28125-0.8125j), end=(4.3125-0.890625j))
; 		 Line(start=(4.3125-0.890625j), end=(4.578125-2.5j))
; 		 CubicBezier(start=(4.578125-2.5j), control1=(4.59375-2.671875j), control2=(4.625-2.75j), end=(4.734375-2.875j))
; 		 Line(start=(4.734375-2.875j), end=(4.921875-3.125j))
; 		 CubicBezier(start=(4.921875-3.125j), control1=(5.453125-3.8125j), control2=(6-4.5j), end=(6.53125-5.1875j))
; 		 CubicBezier(start=(6.53125-5.1875j), control1=(7.015625-5.796875j), control2=(7.359375-6.40625j), end=(8.28125-6.5j))
; 		 CubicBezier(start=(8.28125-6.5j), control1=(8.34375-6.515625j), control2=(8.40625-6.46875j), end=(8.453125-6.609375j))
; 		 Line(start=(8.453125-6.609375j), end=(8.484375-6.8125j))
; 		 Line(start=(8.484375-6.8125j), end=(6.375-6.8125j))
; 		 Line(start=(6.375-6.8125j), end=(6.3125-6.515625j))
; 		 CubicBezier(start=(6.3125-6.515625j), control1=(6.515625-6.515625j), control2=(6.828125-6.421875j), end=(6.828125-6.171875j))
; 		 Line(start=(6.828125-6.171875j), end=(6.828125-6.15625j))
; 		 CubicBezier(start=(6.828125-6.15625j), control1=(6.78125-5.9375j), control2=(6.625-5.78125j), end=(6.5-5.609375j))
; 		 CubicBezier(start=(6.5-5.609375j), control1=(6.015625-5j), control2=(5.515625-4.390625j), end=(5.046875-3.765625j))
; 		 Line(start=(5.046875-3.765625j), end=(4.75-3.390625j))
; 		 CubicBezier(start=(4.75-3.390625j), control1=(4.6875-3.3125j), control2=(4.640625-3.234375j), end=(4.5625-3.15625j))
; 		 CubicBezier(start=(4.5625-3.15625j), control1=(4.53125-3.296875j), control2=(4.46875-3.40625j), end=(4.40625-3.53125j))
; 		 Line(start=(4.40625-3.53125j), end=(4.203125-4j))
; 		 CubicBezier(start=(4.203125-4j), control1=(3.984375-4.484375j), control2=(3.765625-4.96875j), end=(3.5625-5.4375j))
; 		 Line(start=(3.5625-5.4375j), end=(3.296875-6.015625j))
; 		 CubicBezier(start=(3.296875-6.015625j), control1=(3.265625-6.078125j), control2=(3.21875-6.15625j), end=(3.21875-6.234375j))
; 		 CubicBezier(start=(3.21875-6.234375j), control1=(3.21875-6.515625j), control2=(3.703125-6.515625j), end=(3.953125-6.515625j))
; 		 Line(start=(3.953125-6.515625j), end=(4-6.8125j))
; 		 Line(start=(4-6.8125j), end=(1.25-6.8125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.734375-1.109375j), control1=(2.59375-0.171875j), control2=(3.609375+0.265625j), end=(4.875+0.265625j))
; 		 CubicBezier(start=(4.875+0.265625j), control1=(6.734375+0.265625j), control2=(8.078125-1.34375j), end=(8.078125-3.140625j))
; 		 CubicBezier(start=(8.078125-3.140625j), control1=(8.078125-4.421875j), control2=(7.40625-5.703125j), end=(6.203125-6.25j))
; 		 CubicBezier(start=(6.203125-6.25j), control1=(5.0625-6.765625j), control2=(3.6875-6.703125j), end=(2.703125-7.390625j))
; 		 CubicBezier(start=(2.703125-7.390625j), control1=(2.078125-7.828125j), control2=(1.71875-8.5625j), end=(1.71875-9.296875j))
; 		 CubicBezier(start=(1.71875-9.296875j), control1=(1.71875-10.640625j), control2=(2.859375-11.640625j), end=(4.15625-11.640625j))
; 		 CubicBezier(start=(4.15625-11.640625j), control1=(5.953125-11.640625j), control2=(7.0625-10.125j), end=(7.28125-8.28125j))
; 		 CubicBezier(start=(7.28125-8.28125j), control1=(7.296875-8.125j), control2=(7.28125-7.875j), end=(7.453125-7.875j))
; 		 CubicBezier(start=(7.453125-7.875j), control1=(7.5625-7.875j), control2=(7.625-7.921875j), end=(7.625-8.046875j))
; 		 Line(start=(7.625-8.046875j), end=(7.625-11.875j))
; 		 CubicBezier(start=(7.625-11.875j), control1=(7.625-11.96875j), control2=(7.609375-12.03125j), end=(7.5-12.03125j))
; 		 CubicBezier(start=(7.5-12.03125j), control1=(7.296875-12.03125j), control2=(7.015625-11.125j), end=(6.765625-10.671875j))
; 		 CubicBezier(start=(6.765625-10.671875j), control1=(6.265625-11.578125j), control2=(5.109375-12.046875j), end=(4.09375-12.046875j))
; 		 CubicBezier(start=(4.09375-12.046875j), control1=(2.328125-12.046875j), control2=(0.890625-10.6875j), end=(0.890625-8.90625j))
; 		 CubicBezier(start=(0.890625-8.90625j), control1=(0.890625-7.234375j), control2=(1.984375-6.203125j), end=(3.59375-5.75j))
; 		 CubicBezier(start=(3.59375-5.75j), control1=(5.015625-5.375j), control2=(6.59375-5.328125j), end=(7.140625-3.53125j))
; 		 CubicBezier(start=(7.140625-3.53125j), control1=(7.203125-3.28125j), control2=(7.25-3.03125j), end=(7.25-2.765625j))
; 		 Line(start=(7.25-2.765625j), end=(7.25-2.65625j))
; 		 CubicBezier(start=(7.25-2.65625j), control1=(7.15625-1.375j), control2=(6.265625-0.1875j), end=(4.796875-0.1875j))
; 		 CubicBezier(start=(4.796875-0.1875j), control1=(3.671875-0.1875j), control2=(2.5-0.6875j), end=(1.828125-1.640625j))
; 		 CubicBezier(start=(1.828125-1.640625j), control1=(1.453125-2.203125j), control2=(1.28125-2.84375j), end=(1.21875-3.515625j))
; 		 CubicBezier(start=(1.21875-3.515625j), control1=(1.203125-3.65625j), control2=(1.25-3.921875j), end=(1.046875-3.921875j))
; 		 CubicBezier(start=(1.046875-3.921875j), control1=(0.90625-3.921875j), control2=(0.890625-3.828125j), end=(0.890625-3.703125j))
; 		 Line(start=(0.890625-3.703125j), end=(0.890625+0.046875j))
; 		 CubicBezier(start=(0.890625+0.046875j), control1=(0.890625+0.125j), control2=(0.890625+0.203125j), end=(0.96875+0.234375j))
; 		 Line(start=(0.96875+0.234375j), end=(1.015625+0.234375j))
; 		 CubicBezier(start=(1.015625+0.234375j), control1=(1.1875+0.234375j), control2=(1.546875-0.671875j), end=(1.734375-1.109375j))
; 		 Line(start=(1.734375-1.109375j), end=(1.734375-1.109375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.65625-7.375j), end=(0.65625-6.921875j))
; 		 Line(start=(0.65625-6.921875j), end=(0.875-6.921875j))
; 		 CubicBezier(start=(0.875-6.921875j), control1=(1.390625-6.921875j), control2=(1.8125-6.859375j), end=(1.8125-6.078125j))
; 		 Line(start=(1.8125-6.078125j), end=(1.8125+1.8125j))
; 		 CubicBezier(start=(1.8125+1.8125j), control1=(1.8125+1.921875j), control2=(1.828125+2.0625j), end=(1.828125+2.203125j))
; 		 CubicBezier(start=(1.828125+2.203125j), control1=(1.828125+2.84375j), control2=(1.359375+2.875j), end=(0.859375+2.875j))
; 		 Line(start=(0.859375+2.875j), end=(0.65625+2.875j))
; 		 Line(start=(0.65625+2.875j), end=(0.65625+3.328125j))
; 		 Line(start=(0.65625+3.328125j), end=(3.875+3.328125j))
; 		 Line(start=(3.875+3.328125j), end=(3.875+2.875j))
; 		 Line(start=(3.875+2.875j), end=(3.578125+2.875j))
; 		 CubicBezier(start=(3.578125+2.875j), control1=(3.109375+2.875j), control2=(2.71875+2.828125j), end=(2.71875+2.25j))
; 		 Line(start=(2.71875+2.25j), end=(2.734375-1.0625j))
; 		 CubicBezier(start=(2.734375-1.0625j), control1=(3.109375-0.28125j), control2=(4.109375+0.125j), end=(4.859375+0.125j))
; 		 CubicBezier(start=(4.859375+0.125j), control1=(6.921875+0.125j), control2=(8.3125-1.8125j), end=(8.3125-3.734375j))
; 		 CubicBezier(start=(8.3125-3.734375j), control1=(8.3125-5.25j), control2=(7.59375-6.734375j), end=(6.046875-7.390625j))
; 		 CubicBezier(start=(6.046875-7.390625j), control1=(5.71875-7.5j), control2=(5.390625-7.5625j), end=(5.0625-7.5625j))
; 		 CubicBezier(start=(5.0625-7.5625j), control1=(4.171875-7.5625j), control2=(3.078125-7.0625j), end=(2.703125-6.296875j))
; 		 Line(start=(2.703125-6.296875j), end=(2.6875-7.5625j))
; 		 Line(start=(2.6875-7.5625j), end=(0.65625-7.375j))
; 		 CubicBezier(start=(7.1875-3.40625j), control1=(7.09375-2.171875j), control2=(6.421875-0.15625j), end=(4.71875-0.15625j))
; 		 CubicBezier(start=(4.71875-0.15625j), control1=(3.765625-0.15625j), control2=(2.71875-1.125j), end=(2.71875-1.953125j))
; 		 Line(start=(2.71875-1.953125j), end=(2.71875-4.75j))
; 		 CubicBezier(start=(2.71875-4.75j), control1=(2.71875-4.9375j), control2=(2.703125-5.140625j), end=(2.703125-5.359375j))
; 		 CubicBezier(start=(2.703125-5.359375j), control1=(2.703125-6.359375j), control2=(3.890625-7.25j), end=(4.890625-7.25j))
; 		 CubicBezier(start=(4.890625-7.25j), control1=(6.515625-7.25j), control2=(7.203125-5.078125j), end=(7.203125-3.703125j))
; 		 CubicBezier(start=(7.203125-3.703125j), control1=(7.203125-3.59375j), control2=(7.203125-3.5j), end=(7.1875-3.40625j))
; 		 Line(start=(7.1875-3.40625j), end=(7.1875-3.40625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.6875-4.0625j), end=(6.515625-4.0625j))
; 		 CubicBezier(start=(6.515625-4.0625j), control1=(6.65625-4.0625j), control2=(6.734375-4.09375j), end=(6.734375-4.265625j))
; 		 CubicBezier(start=(6.734375-4.265625j), control1=(6.734375-5.953125j), control2=(5.84375-7.625j), end=(3.890625-7.625j))
; 		 CubicBezier(start=(3.890625-7.625j), control1=(1.859375-7.625j), control2=(0.5625-5.6875j), end=(0.5625-3.8125j))
; 		 CubicBezier(start=(0.5625-3.8125j), control1=(0.5625-2j), control2=(1.796875+0.015625j), end=(3.859375+0.125j))
; 		 Line(start=(3.859375+0.125j), end=(4+0.125j))
; 		 CubicBezier(start=(4+0.125j), control1=(5.1875+0.125j), control2=(6.34375-0.65625j), end=(6.6875-1.875j))
; 		 CubicBezier(start=(6.6875-1.875j), control1=(6.703125-1.921875j), control2=(6.71875-1.984375j), end=(6.71875-2.046875j))
; 		 CubicBezier(start=(6.71875-2.046875j), control1=(6.71875-2.140625j), control2=(6.65625-2.1875j), end=(6.59375-2.1875j))
; 		 CubicBezier(start=(6.59375-2.1875j), control1=(6.375-2.1875j), control2=(6.296875-1.71875j), end=(6.203125-1.515625j))
; 		 CubicBezier(start=(6.203125-1.515625j), control1=(5.8125-0.78125j), control2=(5.046875-0.1875j), end=(4.1875-0.1875j))
; 		 CubicBezier(start=(4.1875-0.1875j), control1=(3.6875-0.1875j), control2=(3.140625-0.375j), end=(2.75-0.71875j))
; 		 CubicBezier(start=(2.75-0.71875j), control1=(1.78125-1.546875j), control2=(1.6875-2.875j), end=(1.6875-4.0625j))
; 		 Line(start=(1.6875-4.0625j), end=(1.6875-4.0625j))
; 		 CubicBezier(start=(1.703125-4.34375j), control1=(1.734375-5.6875j), control2=(2.40625-7.328125j), end=(3.859375-7.328125j))
; 		 CubicBezier(start=(3.859375-7.328125j), control1=(5.125-7.328125j), control2=(5.625-6.015625j), end=(5.734375-4.96875j))
; 		 CubicBezier(start=(5.734375-4.96875j), control1=(5.765625-4.765625j), control2=(5.75-4.546875j), end=(5.78125-4.34375j))
; 		 Line(start=(5.78125-4.34375j), end=(1.703125-4.34375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(6.125-6.046875j), control1=(6.0625-6.0625j), control2=(5.96875-6.078125j), end=(5.90625-6.078125j))
; 		 CubicBezier(start=(5.90625-6.078125j), control1=(5.546875-6.078125j), control2=(5.296875-5.78125j), end=(5.296875-5.4375j))
; 		 CubicBezier(start=(5.296875-5.4375j), control1=(5.296875-5.078125j), control2=(5.59375-4.828125j), end=(5.9375-4.828125j))
; 		 CubicBezier(start=(5.9375-4.828125j), control1=(6.28125-4.828125j), control2=(6.578125-5.078125j), end=(6.578125-5.515625j))
; 		 CubicBezier(start=(6.578125-5.515625j), control1=(6.578125-6.796875j), control2=(5.21875-7.625j), end=(4.0625-7.625j))
; 		 CubicBezier(start=(4.0625-7.625j), control1=(1.890625-7.625j), control2=(0.65625-5.546875j), end=(0.65625-3.703125j))
; 		 CubicBezier(start=(0.65625-3.703125j), control1=(0.65625-1.6875j), control2=(2.0625+0.125j), end=(4.203125+0.125j))
; 		 CubicBezier(start=(4.203125+0.125j), control1=(5.34375+0.125j), control2=(6.359375-0.734375j), end=(6.65625-1.828125j))
; 		 CubicBezier(start=(6.65625-1.828125j), control1=(6.703125-1.875j), control2=(6.71875-1.96875j), end=(6.71875-2.03125j))
; 		 Line(start=(6.71875-2.03125j), end=(6.71875-2.078125j))
; 		 CubicBezier(start=(6.71875-2.078125j), control1=(6.71875-2.15625j), control2=(6.640625-2.171875j), end=(6.59375-2.171875j))
; 		 CubicBezier(start=(6.59375-2.171875j), control1=(6.375-2.171875j), control2=(6.375-1.90625j), end=(6.3125-1.75j))
; 		 CubicBezier(start=(6.3125-1.75j), control1=(5.9375-0.859375j), control2=(5.1875-0.1875j), end=(4.21875-0.1875j))
; 		 CubicBezier(start=(4.21875-0.1875j), control1=(3.875-0.1875j), control2=(3.515625-0.28125j), end=(3.203125-0.484375j))
; 		 CubicBezier(start=(3.203125-0.484375j), control1=(2.078125-1.140625j), control2=(1.78125-2.5625j), end=(1.78125-3.765625j))
; 		 CubicBezier(start=(1.78125-3.765625j), control1=(1.78125-5.109375j), control2=(2.21875-7.3125j), end=(4.109375-7.3125j))
; 		 CubicBezier(start=(4.109375-7.3125j), control1=(4.953125-7.3125j), control2=(5.8125-6.8125j), end=(6.125-6.046875j))
; 		 Line(start=(6.125-6.046875j), end=(6.125-6.046875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(2.375-10.609375j), control1=(2.375-9.59375j), control2=(2.078125-7.25j), end=(0.296875-7.25j))
; 		 Line(start=(0.296875-7.25j), end=(0.296875-6.96875j))
; 		 Line(start=(0.296875-6.96875j), end=(1.78125-6.96875j))
; 		 Line(start=(1.78125-6.96875j), end=(1.78125-3.03125j))
; 		 CubicBezier(start=(1.78125-3.03125j), control1=(1.78125-2.796875j), control2=(1.75-2.53125j), end=(1.75-2.296875j))
; 		 CubicBezier(start=(1.75-2.296875j), control1=(1.75-0.953125j), control2=(2.359375+0.125j), end=(3.75+0.125j))
; 		 CubicBezier(start=(3.75+0.125j), control1=(5.078125+0.125j), control2=(5.375-1.296875j), end=(5.375-2.375j))
; 		 Line(start=(5.375-2.375j), end=(5.375-3.140625j))
; 		 Line(start=(5.375-3.140625j), end=(5.0625-3.140625j))
; 		 CubicBezier(start=(5.0625-3.140625j), control1=(5.0625-2.875j), control2=(5.078125-2.625j), end=(5.078125-2.34375j))
; 		 CubicBezier(start=(5.078125-2.34375j), control1=(5.078125-1.59375j), control2=(4.90625-0.1875j), end=(3.890625-0.1875j))
; 		 CubicBezier(start=(3.890625-0.1875j), control1=(2.921875-0.1875j), control2=(2.6875-1.3125j), end=(2.6875-2.21875j))
; 		 Line(start=(2.6875-2.21875j), end=(2.6875-6.96875j))
; 		 Line(start=(2.6875-6.96875j), end=(5.09375-6.96875j))
; 		 Line(start=(5.09375-6.96875j), end=(5.09375-7.421875j))
; 		 Line(start=(5.09375-7.421875j), end=(2.6875-7.421875j))
; 		 Line(start=(2.6875-7.421875j), end=(2.6875-10.609375j))
; 		 Line(start=(2.6875-10.609375j), end=(2.375-10.609375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.640625-7.375j), end=(0.640625-6.921875j))
; 		 Line(start=(0.640625-6.921875j), end=(0.890625-6.921875j))
; 		 CubicBezier(start=(0.890625-6.921875j), control1=(1.46875-6.921875j), control2=(1.796875-6.765625j), end=(1.796875-6.0625j))
; 		 Line(start=(1.796875-6.0625j), end=(1.796875-1.171875j))
; 		 CubicBezier(start=(1.796875-1.171875j), control1=(1.796875-0.5625j), control2=(1.578125-0.46875j), end=(0.640625-0.46875j))
; 		 Line(start=(0.640625-0.46875j), end=(0.640625-0.015625j))
; 		 Line(start=(0.640625-0.015625j), end=(4.109375-0.015625j))
; 		 Line(start=(4.109375-0.015625j), end=(4.109375-0.46875j))
; 		 Line(start=(4.109375-0.46875j), end=(3.609375-0.46875j))
; 		 CubicBezier(start=(3.609375-0.46875j), control1=(3.09375-0.46875j), control2=(2.671875-0.53125j), end=(2.671875-1.1875j))
; 		 Line(start=(2.671875-1.1875j), end=(2.671875-3.59375j))
; 		 CubicBezier(start=(2.671875-3.59375j), control1=(2.671875-4.734375j), control2=(2.734375-6.046875j), end=(3.65625-6.890625j))
; 		 CubicBezier(start=(3.65625-6.890625j), control1=(3.921875-7.125j), control2=(4.3125-7.28125j), end=(4.671875-7.28125j))
; 		 CubicBezier(start=(4.671875-7.28125j), control1=(4.796875-7.28125j), control2=(4.921875-7.265625j), end=(5.0625-7.21875j))
; 		 CubicBezier(start=(5.0625-7.21875j), control1=(4.84375-7.09375j), control2=(4.65625-6.953125j), end=(4.65625-6.65625j))
; 		 CubicBezier(start=(4.65625-6.65625j), control1=(4.65625-6.375j), control2=(4.859375-6.09375j), end=(5.1875-6.0625j))
; 		 CubicBezier(start=(5.1875-6.0625j), control1=(5.609375-6.0625j), control2=(5.84375-6.3125j), end=(5.84375-6.640625j))
; 		 CubicBezier(start=(5.84375-6.640625j), control1=(5.84375-7.28125j), control2=(5.203125-7.5625j), end=(4.65625-7.5625j))
; 		 CubicBezier(start=(4.65625-7.5625j), control1=(3.609375-7.5625j), control2=(2.8125-6.578125j), end=(2.640625-5.640625j))
; 		 Line(start=(2.640625-5.640625j), end=(2.625-7.5625j))
; 		 Line(start=(2.625-7.5625j), end=(0.640625-7.375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(3.8125-7.625j), control1=(1.734375-7.375j), control2=(0.5625-5.546875j), end=(0.5625-3.703125j))
; 		 CubicBezier(start=(0.5625-3.703125j), control1=(0.5625-1.890625j), control2=(1.78125+0.125j), end=(4.09375+0.125j))
; 		 CubicBezier(start=(4.09375+0.125j), control1=(6.140625+0.125j), control2=(7.5-1.828125j), end=(7.5-3.71875j))
; 		 CubicBezier(start=(7.5-3.71875j), control1=(7.5-5.5625j), control2=(6.25-7.625j), end=(4.015625-7.625j))
; 		 Line(start=(4.015625-7.625j), end=(3.8125-7.625j))
; 		 CubicBezier(start=(1.6875-3.375j), control1=(1.6875-3.515625j), control2=(1.671875-3.65625j), end=(1.671875-3.78125j))
; 		 CubicBezier(start=(1.671875-3.78125j), control1=(1.671875-5.03125j), control2=(1.859375-6.59375j), end=(3.234375-7.1875j))
; 		 CubicBezier(start=(3.234375-7.1875j), control1=(3.5-7.28125j), control2=(3.765625-7.359375j), end=(4.03125-7.359375j))
; 		 CubicBezier(start=(4.03125-7.359375j), control1=(5.4375-7.359375j), control2=(6.21875-6.046875j), end=(6.359375-4.75j))
; 		 CubicBezier(start=(6.359375-4.75j), control1=(6.375-4.453125j), control2=(6.390625-4.15625j), end=(6.390625-3.84375j))
; 		 CubicBezier(start=(6.390625-3.84375j), control1=(6.390625-2.890625j), control2=(6.3125-1.8125j), end=(5.6875-1.03125j))
; 		 CubicBezier(start=(5.6875-1.03125j), control1=(5.265625-0.515625j), control2=(4.65625-0.1875j), end=(4.015625-0.1875j))
; 		 CubicBezier(start=(4.015625-0.1875j), control1=(2.640625-0.1875j), control2=(1.890625-1.515625j), end=(1.75-2.703125j))
; 		 CubicBezier(start=(1.75-2.703125j), control1=(1.71875-2.921875j), control2=(1.71875-3.15625j), end=(1.6875-3.375j))
; 		 Line(start=(1.6875-3.375j), end=(1.6875-3.375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.296875-0.796875j), end=(1.328125-0.796875j))
; 		 CubicBezier(start=(1.328125-0.796875j), control1=(1.6875-0.171875j), control2=(2.484375+0.125j), end=(3.171875+0.125j))
; 		 CubicBezier(start=(3.171875+0.125j), control1=(4.359375+0.125j), control2=(5.8125-0.34375j), end=(5.8125-2.140625j))
; 		 CubicBezier(start=(5.8125-2.140625j), control1=(5.8125-2.890625j), control2=(5.453125-3.671875j), end=(4.53125-4.15625j))
; 		 CubicBezier(start=(4.53125-4.15625j), control1=(3.546875-4.671875j), control2=(1.359375-4.34375j), end=(1.15625-5.890625j))
; 		 Line(start=(1.15625-5.890625j), end=(1.15625-5.96875j))
; 		 CubicBezier(start=(1.15625-5.96875j), control1=(1.15625-6.9375j), control2=(2.125-7.390625j), end=(3.078125-7.390625j))
; 		 CubicBezier(start=(3.078125-7.390625j), control1=(3.953125-7.390625j), control2=(5.09375-6.890625j), end=(5.09375-5.25j))
; 		 CubicBezier(start=(5.09375-5.25j), control1=(5.09375-5.109375j), control2=(5.09375-5.015625j), end=(5.265625-5.015625j))
; 		 CubicBezier(start=(5.265625-5.015625j), control1=(5.390625-5.015625j), control2=(5.40625-5.09375j), end=(5.40625-5.21875j))
; 		 Line(start=(5.40625-5.21875j), end=(5.40625-7.453125j))
; 		 CubicBezier(start=(5.40625-7.453125j), control1=(5.40625-7.53125j), control2=(5.375-7.625j), end=(5.28125-7.625j))
; 		 CubicBezier(start=(5.28125-7.625j), control1=(5.125-7.625j), control2=(4.875-7.234375j), end=(4.75-7.03125j))
; 		 CubicBezier(start=(4.75-7.03125j), control1=(4.25-7.4375j), control2=(3.78125-7.625j), end=(3.140625-7.625j))
; 		 CubicBezier(start=(3.140625-7.625j), control1=(2.015625-7.625j), control2=(0.53125-7.25j), end=(0.53125-5.59375j))
; 		 CubicBezier(start=(0.53125-5.59375j), control1=(0.53125-4.6875j), control2=(1.3125-4.03125j), end=(2.125-3.765625j))
; 		 CubicBezier(start=(2.125-3.765625j), control1=(2.6875-3.59375j), control2=(3.265625-3.546875j), end=(3.84375-3.390625j))
; 		 CubicBezier(start=(3.84375-3.390625j), control1=(4.515625-3.203125j), control2=(5.171875-2.625j), end=(5.203125-1.875j))
; 		 Line(start=(5.203125-1.875j), end=(5.203125-1.8125j))
; 		 CubicBezier(start=(5.203125-1.8125j), control1=(5.203125-0.703125j), control2=(4.265625-0.15625j), end=(3.1875-0.15625j))
; 		 CubicBezier(start=(3.1875-0.15625j), control1=(1.75-0.15625j), control2=(1.21875-1.359375j), end=(0.9375-2.515625j))
; 		 CubicBezier(start=(0.9375-2.515625j), control1=(0.890625-2.703125j), control2=(0.9375-3.0625j), end=(0.6875-3.0625j))
; 		 CubicBezier(start=(0.6875-3.0625j), control1=(0.546875-3.0625j), control2=(0.53125-3j), end=(0.53125-2.890625j))
; 		 Line(start=(0.53125-2.890625j), end=(0.53125-0.046875j))
; 		 CubicBezier(start=(0.53125-0.046875j), control1=(0.53125+0.015625j), control2=(0.5625+0.125j), end=(0.65625+0.125j))
; 		 CubicBezier(start=(0.65625+0.125j), control1=(0.828125+0.125j), control2=(0.96875-0.234375j), end=(1.0625-0.40625j))
; 		 CubicBezier(start=(1.0625-0.40625j), control1=(1.15625-0.53125j), control2=(1.234375-0.65625j), end=(1.296875-0.796875j))
; 		 Line(start=(1.296875-0.796875j), end=(1.296875-0.796875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.28125-7.421875j), end=(0.28125-6.96875j))
; 		 Line(start=(0.28125-6.96875j), end=(0.515625-6.96875j))
; 		 CubicBezier(start=(0.515625-6.96875j), control1=(1.40625-6.96875j), control2=(1.53125-6.390625j), end=(1.96875-5.25j))
; 		 CubicBezier(start=(1.96875-5.25j), control1=(2.5625-3.65625j), control2=(3.28125-2.078125j), end=(3.90625-0.484375j))
; 		 CubicBezier(start=(3.90625-0.484375j), control1=(3.9375-0.359375j), control2=(4.0625-0.1875j), end=(4.0625-0.03125j))
; 		 CubicBezier(start=(4.0625-0.03125j), control1=(4.0625+0.265625j), control2=(3.78125+0.734375j), end=(3.65625+1.046875j))
; 		 CubicBezier(start=(3.65625+1.046875j), control1=(3.328125+1.921875j), control2=(2.84375+3.1875j), end=(1.75+3.1875j))
; 		 CubicBezier(start=(1.75+3.1875j), control1=(1.40625+3.1875j), control2=(1.125+3.046875j), end=(0.859375+2.828125j))
; 		 CubicBezier(start=(0.859375+2.828125j), control1=(1.1875+2.796875j), control2=(1.453125+2.625j), end=(1.453125+2.265625j))
; 		 CubicBezier(start=(1.453125+2.265625j), control1=(1.453125+1.953125j), control2=(1.203125+1.6875j), end=(0.875+1.6875j))
; 		 CubicBezier(start=(0.875+1.6875j), control1=(0.546875+1.6875j), control2=(0.3125+1.921875j), end=(0.3125+2.265625j))
; 		 CubicBezier(start=(0.3125+2.265625j), control1=(0.3125+3.03125j), control2=(1.078125+3.46875j), end=(1.75+3.46875j))
; 		 CubicBezier(start=(1.75+3.46875j), control1=(2.984375+3.46875j), control2=(3.546875+2.1875j), end=(3.9375+1.203125j))
; 		 Line(start=(3.9375+1.203125j), end=(4.453125-0.09375j))
; 		 CubicBezier(start=(4.453125-0.09375j), control1=(5.1875-1.90625j), control2=(5.984375-3.71875j), end=(6.6875-5.578125j))
; 		 CubicBezier(start=(6.6875-5.578125j), control1=(7.03125-6.28125j), control2=(7.28125-6.96875j), end=(8.25-6.96875j))
; 		 Line(start=(8.25-6.96875j), end=(8.25-7.421875j))
; 		 Line(start=(8.25-7.421875j), end=(5.90625-7.421875j))
; 		 Line(start=(5.90625-7.421875j), end=(5.90625-6.96875j))
; 		 CubicBezier(start=(5.90625-6.96875j), control1=(6.265625-6.96875j), control2=(6.5625-6.65625j), end=(6.5625-6.34375j))
; 		 CubicBezier(start=(6.5625-6.34375j), control1=(6.5625-6.0625j), control2=(6.421875-5.75j), end=(6.3125-5.515625j))
; 		 CubicBezier(start=(6.3125-5.515625j), control1=(5.8125-4.25j), control2=(5.28125-3.03125j), end=(4.796875-1.75j))
; 		 CubicBezier(start=(4.796875-1.75j), control1=(4.71875-1.5625j), control2=(4.625-1.375j), end=(4.578125-1.1875j))
; 		 Line(start=(4.578125-1.1875j), end=(4.5625-1.1875j))
; 		 CubicBezier(start=(4.5625-1.1875j), control1=(4.53125-1.359375j), control2=(4.4375-1.53125j), end=(4.375-1.6875j))
; 		 Line(start=(4.375-1.6875j), end=(4.03125-2.53125j))
; 		 CubicBezier(start=(4.03125-2.53125j), control1=(3.5625-3.703125j), control2=(3.0625-4.90625j), end=(2.59375-6.078125j))
; 		 CubicBezier(start=(2.59375-6.078125j), control1=(2.53125-6.21875j), control2=(2.4375-6.40625j), end=(2.4375-6.5625j))
; 		 Line(start=(2.4375-6.5625j), end=(2.4375-6.59375j))
; 		 CubicBezier(start=(2.4375-6.59375j), control1=(2.484375-6.953125j), control2=(2.96875-6.96875j), end=(3.234375-6.96875j))
; 		 Line(start=(3.234375-6.96875j), end=(3.234375-7.421875j))
; 		 Line(start=(3.234375-7.421875j), end=(0.28125-7.421875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.171875-4.09375j), end=(0.171875-3.34375j))
; 		 Line(start=(0.171875-3.34375j), end=(4.484375-3.34375j))
; 		 Line(start=(4.484375-3.34375j), end=(4.484375-4.09375j))
; 		 Line(start=(4.484375-4.09375j), end=(0.171875-4.09375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.984375-11.78125j), end=(0.984375-11.328125j))
; 		 Line(start=(0.984375-11.328125j), end=(1.390625-11.328125j))
; 		 CubicBezier(start=(1.390625-11.328125j), control1=(1.921875-11.328125j), control2=(2.375-11.265625j), end=(2.375-10.609375j))
; 		 CubicBezier(start=(2.375-10.609375j), control1=(2.375-10.46875j), control2=(2.359375-10.34375j), end=(2.359375-10.234375j))
; 		 Line(start=(2.359375-10.234375j), end=(2.359375-1.515625j))
; 		 CubicBezier(start=(2.359375-1.515625j), control1=(2.359375-1.40625j), control2=(2.375-1.28125j), end=(2.375-1.15625j))
; 		 CubicBezier(start=(2.375-1.15625j), control1=(2.375-0.515625j), control2=(1.859375-0.46875j), end=(1.328125-0.46875j))
; 		 Line(start=(1.328125-0.46875j), end=(0.984375-0.46875j))
; 		 Line(start=(0.984375-0.46875j), end=(0.984375-0.015625j))
; 		 Line(start=(0.984375-0.015625j), end=(5.234375-0.015625j))
; 		 CubicBezier(start=(5.234375-0.015625j), control1=(5.5625-0.015625j), control2=(5.90625+0j), end=(6.234375+0j))
; 		 CubicBezier(start=(6.234375+0j), control1=(9.546875+0j), control2=(11.453125-2.921875j), end=(11.453125-5.8125j))
; 		 CubicBezier(start=(11.453125-5.8125j), control1=(11.453125-8.3125j), control2=(10.015625-11.1875j), end=(7.3125-11.6875j))
; 		 CubicBezier(start=(7.3125-11.6875j), control1=(6.90625-11.78125j), control2=(6.484375-11.796875j), end=(6.0625-11.796875j))
; 		 CubicBezier(start=(6.0625-11.796875j), control1=(5.75-11.796875j), control2=(5.421875-11.781245j), end=(5.109375-11.781245j))
; 		 Line(start=(5.109375-11.781245j), end=(0.984375-11.78125j))
; 		 CubicBezier(start=(10.15625-5.5625j), control1=(10.14062-5.4375j), control2=(10.14062-5.34375j), end=(10.14062-5.234375j))
; 		 Line(start=(10.14062-5.234375j), end=(10.14062-4.90625j))
; 		 CubicBezier(start=(10.14062-4.90625j), control1=(9.96875-3.015625j), control2=(9.15625-1.1875j), end=(7.21875-0.625j))
; 		 CubicBezier(start=(7.21875-0.625j), control1=(6.703125-0.46875j), control2=(6.1875-0.46875j), end=(5.640625-0.46875j))
; 		 Line(start=(5.640625-0.46875j), end=(4.03125-0.46875j))
; 		 CubicBezier(start=(4.03125-0.46875j), control1=(3.671875-0.46875j), control2=(3.4375-0.578125j), end=(3.4375-1.015625j))
; 		 CubicBezier(start=(3.4375-1.015625j), control1=(3.4375-1.15625j), control2=(3.46875-1.296875j), end=(3.46875-1.390625j))
; 		 Line(start=(3.46875-1.390625j), end=(3.46875-10.078125j))
; 		 CubicBezier(start=(3.46875-10.078125j), control1=(3.46875-10.25j), control2=(3.4375-10.40625j), end=(3.4375-10.578125j))
; 		 CubicBezier(start=(3.4375-10.578125j), control1=(3.4375-11.03125j), control2=(3.4375-11.328125j), end=(4.171875-11.328125j))
; 		 Line(start=(4.171875-11.328125j), end=(6.359375-11.328125j))
; 		 CubicBezier(start=(6.359375-11.328125j), control1=(9.34375-11.109375j), control2=(10.171875-8.09375j), end=(10.171875-5.84375j))
; 		 CubicBezier(start=(10.171875-5.84375j), control1=(10.171875-5.75j), control2=(10.156245-5.640625j), end=(10.156245-5.5625j))
; 		 Line(start=(10.156245-5.5625j), end=(10.15625-5.5625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.734375-7.375j), end=(0.734375-6.921875j))
; 		 Line(start=(0.734375-6.921875j), end=(0.984375-6.921875j))
; 		 CubicBezier(start=(0.984375-6.921875j), control1=(1.53125-6.921875j), control2=(1.84375-6.765625j), end=(1.84375-6.046875j))
; 		 Line(start=(1.84375-6.046875j), end=(1.84375-1.171875j))
; 		 CubicBezier(start=(1.84375-1.171875j), control1=(1.84375-0.5625j), control2=(1.640625-0.46875j), end=(0.6875-0.46875j))
; 		 Line(start=(0.6875-0.46875j), end=(0.6875-0.015625j))
; 		 Line(start=(0.6875-0.015625j), end=(3.75-0.015625j))
; 		 Line(start=(3.75-0.015625j), end=(3.75-0.46875j))
; 		 Line(start=(3.75-0.46875j), end=(3.515625-0.46875j))
; 		 CubicBezier(start=(3.515625-0.46875j), control1=(3.109375-0.46875j), control2=(2.71875-0.515625j), end=(2.71875-1.0625j))
; 		 Line(start=(2.71875-1.0625j), end=(2.71875-7.5625j))
; 		 Line(start=(2.71875-7.5625j), end=(0.734375-7.375j))
; 		 CubicBezier(start=(1.921875-11.3125j), control1=(1.546875-11.25j), control2=(1.34375-10.921875j), end=(1.34375-10.625j))
; 		 CubicBezier(start=(1.34375-10.625j), control1=(1.34375-10.296875j), control2=(1.625-9.921875j), end=(2.03125-9.921875j))
; 		 CubicBezier(start=(2.03125-9.921875j), control1=(2.46875-9.921875j), control2=(2.75-10.265625j), end=(2.75-10.625j))
; 		 CubicBezier(start=(2.75-10.625j), control1=(2.75-10.9375j), control2=(2.484375-11.328125j), end=(2.0625-11.328125j))
; 		 CubicBezier(start=(2.0625-11.328125j), control1=(2.015625-11.328125j), control2=(1.96875-11.312495j), end=(1.921875-11.312495j))
; 		 Line(start=(1.921875-11.312495j), end=(1.921875-11.3125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.8125-7.421875j), end=(0.4375-7.421875j))
; 		 Line(start=(0.4375-7.421875j), end=(0.4375-6.96875j))
; 		 Line(start=(0.4375-6.96875j), end=(1.8125-6.96875j))
; 		 Line(start=(1.8125-6.96875j), end=(1.8125-1.078125j))
; 		 CubicBezier(start=(1.8125-1.078125j), control1=(1.8125-0.484375j), control2=(1.359375-0.46875j), end=(0.75-0.46875j))
; 		 Line(start=(0.75-0.46875j), end=(0.65625-0.46875j))
; 		 Line(start=(0.65625-0.46875j), end=(0.65625-0.015625j))
; 		 Line(start=(0.65625-0.015625j), end=(3.84375-0.015625j))
; 		 Line(start=(3.84375-0.015625j), end=(3.84375-0.46875j))
; 		 Line(start=(3.84375-0.46875j), end=(3.53125-0.46875j))
; 		 CubicBezier(start=(3.53125-0.46875j), control1=(3.09375-0.46875j), control2=(2.6875-0.515625j), end=(2.6875-1.078125j))
; 		 Line(start=(2.6875-1.078125j), end=(2.6875-6.96875j))
; 		 Line(start=(2.6875-6.96875j), end=(6.28125-6.96875j))
; 		 Line(start=(6.28125-6.96875j), end=(6.28125-1.078125j))
; 		 CubicBezier(start=(6.28125-1.078125j), control1=(6.28125-0.484375j), control2=(5.84375-0.46875j), end=(5.234375-0.46875j))
; 		 Line(start=(5.234375-0.46875j), end=(5.125-0.46875j))
; 		 Line(start=(5.125-0.46875j), end=(5.125-0.015625j))
; 		 Line(start=(5.125-0.015625j), end=(8.609375-0.015625j))
; 		 Line(start=(8.609375-0.015625j), end=(8.609375-0.46875j))
; 		 Line(start=(8.609375-0.46875j), end=(8.125-0.46875j))
; 		 CubicBezier(start=(8.125-0.46875j), control1=(7.609375-0.46875j), control2=(7.15625-0.53125j), end=(7.15625-1.140625j))
; 		 Line(start=(7.15625-1.140625j), end=(7.15625-6.96875j))
; 		 Line(start=(7.15625-6.96875j), end=(9.125-6.96875j))
; 		 Line(start=(9.125-6.96875j), end=(9.125-7.421875j))
; 		 Line(start=(9.125-7.421875j), end=(7.125-7.421875j))
; 		 Line(start=(7.125-7.421875j), end=(7.125-9.75j))
; 		 CubicBezier(start=(7.125-9.75j), control1=(7.1875-10.578125j), control2=(7.59375-11.71875j), end=(8.5-11.828125j))
; 		 Line(start=(8.5-11.828125j), end=(8.578125-11.828125j))
; 		 CubicBezier(start=(8.578125-11.828125j), control1=(8.78125-11.828125j), control2=(9.109375-11.781255j), end=(9.265625-11.625j))
; 		 CubicBezier(start=(9.265625-11.625j), control1=(8.9375-11.625j), control2=(8.75-11.296875j), end=(8.75-11.046875j))
; 		 CubicBezier(start=(8.75-11.046875j), control1=(8.75-10.78125j), control2=(8.921875-10.5j), end=(9.234375-10.453125j))
; 		 CubicBezier(start=(9.234375-10.453125j), control1=(9.6875-10.453125j), control2=(9.9375-10.671875j), end=(9.9375-11.046875j))
; 		 CubicBezier(start=(9.9375-11.046875j), control1=(9.9375-11.765625j), control2=(9.234375-12.109375j), end=(8.640625-12.109375j))
; 		 CubicBezier(start=(8.640625-12.109375j), control1=(7.921875-12.109375j), control2=(7.46875-11.78125j), end=(6.953125-11.359375j))
; 		 CubicBezier(start=(6.953125-11.359375j), control1=(6.609375-11.96875j), control2=(5.734375-12.109375j), end=(5.171875-12.109375j))
; 		 CubicBezier(start=(5.171875-12.109375j), control1=(3.78125-12.109375j), control2=(1.96875-11.34375j), end=(1.8125-9.734375j))
; 		 Line(start=(1.8125-9.734375j), end=(1.8125-7.421875j))
; 		 CubicBezier(start=(6.375-10.234375j), control1=(6.28125-9.9375j), control2=(6.28125-9.65625j), end=(6.28125-9.359375j))
; 		 Line(start=(6.28125-9.359375j), end=(6.28125-7.421875j))
; 		 Line(start=(6.28125-7.421875j), end=(2.65625-7.421875j))
; 		 Line(start=(2.65625-7.421875j), end=(2.65625-9.28125j))
; 		 CubicBezier(start=(2.65625-9.28125j), control1=(2.65625-10.859375j), control2=(3.765625-11.828125j), end=(5.140625-11.828125j))
; 		 CubicBezier(start=(5.140625-11.828125j), control1=(5.609375-11.828125j), control2=(6.140625-11.703125j), end=(6.53125-11.40625j))
; 		 CubicBezier(start=(6.53125-11.40625j), control1=(6.203125-11.40625j), control2=(5.921875-11.140625j), end=(5.921875-10.796875j))
; 		 CubicBezier(start=(5.921875-10.796875j), control1=(5.921875-10.515625j), control2=(6.109375-10.296875j), end=(6.375-10.234375j))
; 		 Line(start=(6.375-10.234375j), end=(6.375-10.234375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.671875-5.96875j), control1=(1.921875-6.78125j), control2=(2.734375-7.359375j), end=(3.5625-7.359375j))
; 		 CubicBezier(start=(3.5625-7.359375j), control1=(4.765625-7.359375j), control2=(5.375-6.140625j), end=(5.375-5.015625j))
; 		 Line(start=(5.375-5.015625j), end=(5.375-4.390625j))
; 		 CubicBezier(start=(5.375-4.390625j), control1=(3.8125-4.390625j), control2=(1.921875-4.046875j), end=(1.078125-2.671875j))
; 		 CubicBezier(start=(1.078125-2.671875j), control1=(0.890625-2.359375j), control2=(0.796875-1.953125j), end=(0.796875-1.578125j))
; 		 Line(start=(0.796875-1.578125j), end=(0.796875-1.453125j))
; 		 CubicBezier(start=(0.796875-1.453125j), control1=(0.890625-0.328125j), control2=(2.203125+0.125j), end=(3.171875+0.125j))
; 		 CubicBezier(start=(3.171875+0.125j), control1=(4.078125+0.125j), control2=(5.140625-0.359375j), end=(5.4375-1.359375j))
; 		 Line(start=(5.4375-1.359375j), end=(5.453125-1.359375j))
; 		 CubicBezier(start=(5.453125-1.359375j), control1=(5.46875-0.65625j), control2=(5.96875+0.046875j), end=(6.71875+0.046875j))
; 		 Line(start=(6.71875+0.046875j), end=(6.828125+0.046875j))
; 		 CubicBezier(start=(6.828125+0.046875j), control1=(7.484375-0.03125j), control2=(7.921875-0.625j), end=(7.984375-1.25j))
; 		 Line(start=(7.984375-1.25j), end=(7.984375-2.515625j))
; 		 Line(start=(7.984375-2.515625j), end=(7.671875-2.515625j))
; 		 Line(start=(7.671875-2.515625j), end=(7.671875-1.65625j))
; 		 CubicBezier(start=(7.671875-1.65625j), control1=(7.671875-1.125j), control2=(7.59375-0.359375j), end=(6.953125-0.359375j))
; 		 CubicBezier(start=(6.953125-0.359375j), control1=(6.3125-0.359375j), control2=(6.28125-1.21875j), end=(6.28125-1.65625j))
; 		 Line(start=(6.28125-1.65625j), end=(6.28125-4.90625j))
; 		 CubicBezier(start=(6.28125-4.90625j), control1=(6.28125-5.40625j), control2=(6.265625-5.890625j), end=(6.015625-6.3125j))
; 		 CubicBezier(start=(6.015625-6.3125j), control1=(5.53125-7.140625j), control2=(4.5625-7.625j), end=(3.609375-7.625j))
; 		 CubicBezier(start=(3.609375-7.625j), control1=(2.71875-7.625j), control2=(1.796875-7.15625j), end=(1.40625-6.203125j))
; 		 CubicBezier(start=(1.40625-6.203125j), control1=(1.328125-5.984375j), control2=(1.234375-5.71875j), end=(1.234375-5.453125j))
; 		 CubicBezier(start=(1.234375-5.453125j), control1=(1.234375-5.03125j), control2=(1.5-4.75j), end=(1.875-4.75j))
; 		 CubicBezier(start=(1.875-4.75j), control1=(2.234375-4.75j), control2=(2.515625-5j), end=(2.515625-5.375j))
; 		 CubicBezier(start=(2.515625-5.375j), control1=(2.515625-5.734375j), control2=(2.234375-6.015625j), end=(1.890625-6.015625j))
; 		 CubicBezier(start=(1.890625-6.015625j), control1=(1.828125-6.015625j), control2=(1.734375-5.984375j), end=(1.671875-5.96875j))
; 		 Line(start=(1.671875-5.96875j), end=(1.671875-5.96875j))
; 		 Line(start=(5.375-4.109375j), end=(5.375-2.421875j))
; 		 CubicBezier(start=(5.375-2.421875j), control1=(5.375-1.3125j), control2=(4.671875-0.234375j), end=(3.484375-0.15625j))
; 		 Line(start=(3.484375-0.15625j), end=(3.375-0.15625j))
; 		 CubicBezier(start=(3.375-0.15625j), control1=(2.59375-0.15625j), control2=(1.859375-0.75j), end=(1.859375-1.59375j))
; 		 CubicBezier(start=(1.859375-1.59375j), control1=(1.859375-3.375j), control2=(3.84375-4.109375j), end=(5.375-4.109375j))
; 		 Line(start=(5.375-4.109375j), end=(5.375-4.109375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.6875-7.375j), end=(0.6875-6.921875j))
; 		 Line(start=(0.6875-6.921875j), end=(0.953125-6.921875j))
; 		 CubicBezier(start=(0.953125-6.921875j), control1=(1.515625-6.921875j), control2=(1.84375-6.765625j), end=(1.84375-6.0625j))
; 		 Line(start=(1.84375-6.0625j), end=(1.84375-1.171875j))
; 		 CubicBezier(start=(1.84375-1.171875j), control1=(1.84375-0.578125j), control2=(1.640625-0.46875j), end=(0.6875-0.46875j))
; 		 Line(start=(0.6875-0.46875j), end=(0.6875-0.015625j))
; 		 Line(start=(0.6875-0.015625j), end=(3.90625-0.015625j))
; 		 Line(start=(3.90625-0.015625j), end=(3.90625-0.46875j))
; 		 Line(start=(3.90625-0.46875j), end=(3.59375-0.46875j))
; 		 CubicBezier(start=(3.59375-0.46875j), control1=(3.1875-0.46875j), control2=(2.75-0.515625j), end=(2.75-1.046875j))
; 		 Line(start=(2.75-1.046875j), end=(2.75-4.34375j))
; 		 CubicBezier(start=(2.75-4.34375j), control1=(2.75-5.515625j), control2=(3.234375-7.28125j), end=(4.953125-7.28125j))
; 		 CubicBezier(start=(4.953125-7.28125j), control1=(6.0625-7.28125j), control2=(6.3125-6.25j), end=(6.3125-5.25j))
; 		 Line(start=(6.3125-5.25j), end=(6.3125-1.078125j))
; 		 CubicBezier(start=(6.3125-1.078125j), control1=(6.3125-0.5j), control2=(5.84375-0.46875j), end=(5.328125-0.46875j))
; 		 Line(start=(5.328125-0.46875j), end=(5.171875-0.46875j))
; 		 Line(start=(5.171875-0.46875j), end=(5.171875-0.015625j))
; 		 Line(start=(5.171875-0.015625j), end=(8.390625-0.015625j))
; 		 Line(start=(8.390625-0.015625j), end=(8.390625-0.46875j))
; 		 Line(start=(8.390625-0.46875j), end=(8.109375-0.46875j))
; 		 CubicBezier(start=(8.109375-0.46875j), control1=(7.65625-0.46875j), control2=(7.234375-0.515625j), end=(7.234375-1.0625j))
; 		 Line(start=(7.234375-1.0625j), end=(7.234375-5.0625j))
; 		 CubicBezier(start=(7.234375-5.0625j), control1=(7.234375-5.703125j), control2=(7.21875-6.40625j), end=(6.78125-6.90625j))
; 		 CubicBezier(start=(6.78125-6.90625j), control1=(6.34375-7.421875j), control2=(5.671875-7.5625j), end=(5-7.5625j))
; 		 CubicBezier(start=(5-7.5625j), control1=(4-7.5625j), control2=(2.9375-6.765625j), end=(2.71875-5.796875j))
; 		 Line(start=(2.71875-5.796875j), end=(2.703125-7.5625j))
; 		 Line(start=(2.703125-7.5625j), end=(0.6875-7.375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(5.125-11.78125j), end=(5.125-11.328125j))
; 		 Line(start=(5.125-11.328125j), end=(5.40625-11.328125j))
; 		 CubicBezier(start=(5.40625-11.328125j), control1=(5.921875-11.328125j), control2=(6.28125-11.1875j), end=(6.28125-10.53125j))
; 		 Line(start=(6.28125-10.53125j), end=(6.265625-6.375j))
; 		 CubicBezier(start=(6.265625-6.375j), control1=(5.8125-7.125j), control2=(4.96875-7.5625j), end=(4.109375-7.5625j))
; 		 CubicBezier(start=(4.109375-7.5625j), control1=(2-7.5625j), control2=(0.65625-5.578125j), end=(0.65625-3.671875j))
; 		 CubicBezier(start=(0.65625-3.671875j), control1=(0.65625-1.828125j), control2=(1.953125+0.125j), end=(3.953125+0.125j))
; 		 CubicBezier(start=(3.953125+0.125j), control1=(4.828125+0.125j), control2=(5.78125-0.359375j), end=(6.234375-1.140625j))
; 		 Line(start=(6.234375-1.140625j), end=(6.25+0.125j))
; 		 Line(start=(6.25+0.125j), end=(8.3125-0.015625j))
; 		 Line(start=(8.3125-0.015625j), end=(8.3125-0.46875j))
; 		 Line(start=(8.3125-0.46875j), end=(8.078125-0.46875j))
; 		 CubicBezier(start=(8.078125-0.46875j), control1=(7.453125-0.46875j), control2=(7.15625-0.65625j), end=(7.15625-1.296875j))
; 		 Line(start=(7.15625-1.296875j), end=(7.15625-11.96875j))
; 		 Line(start=(7.15625-11.96875j), end=(5.125-11.78125j))
; 		 Line(start=(1.78125-3.28125j), end=(1.78125-3.546875j))
; 		 CubicBezier(start=(1.78125-3.546875j), control1=(1.78125-4.96875j), control2=(2.171875-7.28125j), end=(4.234375-7.28125j))
; 		 CubicBezier(start=(4.234375-7.28125j), control1=(5.28125-7.28125j), control2=(6.265625-6.296875j), end=(6.265625-5.421875j))
; 		 CubicBezier(start=(6.265625-5.421875j), control1=(6.265625-5.21875j), control2=(6.25-5.015625j), end=(6.25-4.796875j))
; 		 Line(start=(6.25-4.796875j), end=(6.25-2j))
; 		 CubicBezier(start=(6.25-2j), control1=(6.25-1.109375j), control2=(5.03125-0.15625j), end=(4.046875-0.15625j))
; 		 CubicBezier(start=(4.046875-0.15625j), control1=(2.828125-0.15625j), control2=(2.03125-1.359375j), end=(1.859375-2.515625j))
; 		 CubicBezier(start=(1.859375-2.515625j), control1=(1.8125-2.765625j), control2=(1.8125-3.03125j), end=(1.78125-3.28125j))
; 		 Line(start=(1.78125-3.28125j), end=(1.78125-3.28125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.84375-7.421875j), end=(0.484375-7.421875j))
; 		 Line(start=(0.484375-7.421875j), end=(0.484375-6.96875j))
; 		 Line(start=(0.484375-6.96875j), end=(1.84375-6.96875j))
; 		 Line(start=(1.84375-6.96875j), end=(1.84375-1.078125j))
; 		 CubicBezier(start=(1.84375-1.078125j), control1=(1.84375-0.484375j), control2=(1.390625-0.46875j), end=(0.796875-0.46875j))
; 		 Line(start=(0.796875-0.46875j), end=(0.6875-0.46875j))
; 		 Line(start=(0.6875-0.46875j), end=(0.6875-0.015625j))
; 		 Line(start=(0.6875-0.015625j), end=(4.171875-0.015625j))
; 		 Line(start=(4.171875-0.015625j), end=(4.171875-0.46875j))
; 		 Line(start=(4.171875-0.46875j), end=(3.6875-0.46875j))
; 		 CubicBezier(start=(3.6875-0.46875j), control1=(3.171875-0.46875j), control2=(2.71875-0.53125j), end=(2.71875-1.140625j))
; 		 Line(start=(2.71875-1.140625j), end=(2.71875-6.96875j))
; 		 Line(start=(2.71875-6.96875j), end=(4.6875-6.96875j))
; 		 Line(start=(4.6875-6.96875j), end=(4.6875-7.421875j))
; 		 Line(start=(4.6875-7.421875j), end=(2.6875-7.421875j))
; 		 Line(start=(2.6875-7.421875j), end=(2.6875-9.734375j))
; 		 CubicBezier(start=(2.6875-9.734375j), control1=(2.75-10.578125j), control2=(3.171875-11.796875j), end=(4.15625-11.828125j))
; 		 CubicBezier(start=(4.15625-11.828125j), control1=(4.390625-11.828125j), control2=(4.671875-11.765625j), end=(4.84375-11.625j))
; 		 CubicBezier(start=(4.84375-11.625j), control1=(4.515625-11.625j), control2=(4.328125-11.296875j), end=(4.328125-11.046875j))
; 		 CubicBezier(start=(4.328125-11.046875j), control1=(4.328125-10.765625j), control2=(4.515625-10.46875j), end=(4.859375-10.453125j))
; 		 CubicBezier(start=(4.859375-10.453125j), control1=(5.296875-10.453125j), control2=(5.515625-10.703125j), end=(5.515625-11.078125j))
; 		 CubicBezier(start=(5.515625-11.078125j), control1=(5.515625-11.78125j), control2=(4.75-12.109375j), end=(4.171875-12.109375j))
; 		 CubicBezier(start=(4.171875-12.109375j), control1=(3.1875-12.109375j), control2=(2.34375-11.421875j), end=(2.015625-10.53125j))
; 		 CubicBezier(start=(2.015625-10.53125j), control1=(1.84375-10.0625j), control2=(1.84375-9.5625j), end=(1.84375-9.0625j))
; 		 Line(start=(1.84375-9.0625j), end=(1.84375-7.421875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.6875-11.78125j), end=(0.6875-11.328125j))
; 		 Line(start=(0.6875-11.328125j), end=(0.84375-11.328125j))
; 		 CubicBezier(start=(0.84375-11.328125j), control1=(1.40625-11.328125j), control2=(1.84375-11.281255j), end=(1.84375-10.4375j))
; 		 Line(start=(1.84375-10.4375j), end=(1.84375-1.171875j))
; 		 CubicBezier(start=(1.84375-1.171875j), control1=(1.84375-0.546875j), control2=(1.546875-0.46875j), end=(0.6875-0.46875j))
; 		 Line(start=(0.6875-0.46875j), end=(0.6875-0.015625j))
; 		 Line(start=(0.6875-0.015625j), end=(3.875-0.015625j))
; 		 Line(start=(3.875-0.015625j), end=(3.875-0.46875j))
; 		 Line(start=(3.875-0.46875j), end=(3.515625-0.46875j))
; 		 CubicBezier(start=(3.515625-0.46875j), control1=(3.09375-0.46875j), control2=(2.71875-0.515625j), end=(2.71875-1.078125j))
; 		 Line(start=(2.71875-1.078125j), end=(2.71875-11.96875j))
; 		 Line(start=(2.71875-11.96875j), end=(0.6875-11.78125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.796875-3.3125j), end=(1.796875-3.28125j))
; 		 CubicBezier(start=(1.796875-3.28125j), control1=(1.421875-2.96875j), control2=(1.296875-2.375j), end=(1.296875-1.890625j))
; 		 CubicBezier(start=(1.296875-1.890625j), control1=(1.296875-1.3125j), control2=(1.515625-0.71875j), end=(2.03125-0.453125j))
; 		 Line(start=(2.03125-0.453125j), end=(2.03125-0.4375j))
; 		 CubicBezier(start=(2.03125-0.4375j), control1=(1.1875-0.28125j), control2=(0.453125+0.5j), end=(0.453125+1.34375j))
; 		 CubicBezier(start=(0.453125+1.34375j), control1=(0.453125+2.90625j), control2=(2.65625+3.484375j), end=(4.046875+3.484375j))
; 		 CubicBezier(start=(4.046875+3.484375j), control1=(5.453125+3.484375j), control2=(7.640625+2.9375j), end=(7.640625+1.25j))
; 		 CubicBezier(start=(7.640625+1.25j), control1=(7.640625+0.171875j), control2=(6.890625-0.53125j), end=(5.921875-0.828125j))
; 		 CubicBezier(start=(5.921875-0.828125j), control1=(5.28125-1j), control2=(4.65625-1.015625j), end=(4-1.015625j))
; 		 Line(start=(4-1.015625j), end=(2.96875-1.015625j))
; 		 CubicBezier(start=(2.96875-1.015625j), control1=(2.09375-1.015625j), control2=(1.671875-1.515625j), end=(1.671875-2.203125j))
; 		 CubicBezier(start=(1.671875-2.203125j), control1=(1.671875-2.5625j), control2=(1.84375-2.8125j), end=(1.984375-3.109375j))
; 		 CubicBezier(start=(1.984375-3.109375j), control1=(2.5-2.765625j), control2=(2.96875-2.5625j), end=(3.578125-2.5625j))
; 		 CubicBezier(start=(3.578125-2.5625j), control1=(5.03125-2.5625j), control2=(6.125-3.75j), end=(6.125-5.09375j))
; 		 CubicBezier(start=(6.125-5.09375j), control1=(6.125-5.671875j), control2=(5.921875-6.34375j), end=(5.46875-6.734375j))
; 		 Line(start=(5.46875-6.734375j), end=(5.46875-6.75j))
; 		 CubicBezier(start=(5.46875-6.75j), control1=(5.953125-7.09375j), control2=(6.421875-7.421875j), end=(7.03125-7.421875j))
; 		 CubicBezier(start=(7.03125-7.421875j), control1=(7.109375-7.421875j), control2=(7.203125-7.40625j), end=(7.296875-7.375j))
; 		 CubicBezier(start=(7.296875-7.375j), control1=(7.1875-7.28125j), control2=(7.0625-7.203125j), end=(7.0625-7.015625j))
; 		 CubicBezier(start=(7.0625-7.015625j), control1=(7.0625-6.796875j), control2=(7.25-6.609375j), end=(7.46875-6.609375j))
; 		 Line(start=(7.46875-6.609375j), end=(7.484375-6.609375j))
; 		 CubicBezier(start=(7.484375-6.609375j), control1=(7.75-6.625j), control2=(7.859375-6.8125j), end=(7.859375-7.03125j))
; 		 CubicBezier(start=(7.859375-7.03125j), control1=(7.859375-7.484375j), control2=(7.40625-7.703125j), end=(7.0625-7.703125j))
; 		 CubicBezier(start=(7.0625-7.703125j), control1=(6.46875-7.703125j), control2=(5.890625-7.453125j), end=(5.453125-7.09375j))
; 		 CubicBezier(start=(5.453125-7.09375j), control1=(5.40625-7.046875j), control2=(5.359375-6.96875j), end=(5.28125-6.96875j))
; 		 CubicBezier(start=(5.28125-6.96875j), control1=(5.171875-6.96875j), control2=(4.859375-7.25j), end=(4.609375-7.359375j))
; 		 CubicBezier(start=(4.609375-7.359375j), control1=(4.28125-7.484375j), control2=(3.9375-7.5625j), end=(3.59375-7.5625j))
; 		 CubicBezier(start=(3.59375-7.5625j), control1=(2.21875-7.5625j), control2=(1.046875-6.46875j), end=(1.046875-5.0625j))
; 		 CubicBezier(start=(1.046875-5.0625j), control1=(1.046875-4.4375j), control2=(1.3125-3.6875j), end=(1.796875-3.3125j))
; 		 Line(start=(1.796875-3.3125j), end=(1.796875-3.3125j))
; 		 Line(start=(1.140625+1.46875j), end=(1.140625+1.328125j))
; 		 CubicBezier(start=(1.140625+1.328125j), control1=(1.140625+0.34375j), control2=(1.984375-0.265625j), end=(2.96875-0.265625j))
; 		 Line(start=(2.96875-0.265625j), end=(4.515625-0.265625j))
; 		 CubicBezier(start=(4.515625-0.265625j), control1=(5.421875-0.265625j), control2=(6.9375+0j), end=(6.9375+1.34375j))
; 		 CubicBezier(start=(6.9375+1.34375j), control1=(6.9375+2.671875j), control2=(5.109375+3.1875j), end=(4.046875+3.1875j))
; 		 CubicBezier(start=(4.046875+3.1875j), control1=(2.9375+3.1875j), control2=(1.328125+2.71875j), end=(1.140625+1.46875j))
; 		 Line(start=(1.140625+1.46875j), end=(1.140625+1.46875j))
; 		 Line(start=(2.078125-4.703125j), end=(2.078125-5.140625j))
; 		 CubicBezier(start=(2.078125-5.140625j), control1=(2.078125-6.078125j), control2=(2.421875-7.25j), end=(3.59375-7.25j))
; 		 CubicBezier(start=(3.59375-7.25j), control1=(4.828125-7.25j), control2=(5.109375-5.953125j), end=(5.109375-5j))
; 		 CubicBezier(start=(5.109375-5j), control1=(5.109375-4.03125j), control2=(4.734375-2.875j), end=(3.59375-2.875j))
; 		 CubicBezier(start=(3.59375-2.875j), control1=(2.578125-2.875j), control2=(2.1875-3.78125j), end=(2.078125-4.703125j))
; 		 Line(start=(2.078125-4.703125j), end=(2.078125-4.703125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.6875-11.78125j), end=(0.6875-11.328125j))
; 		 Line(start=(0.6875-11.328125j), end=(0.84375-11.328125j))
; 		 CubicBezier(start=(0.84375-11.328125j), control1=(1.40625-11.328125j), control2=(1.84375-11.281255j), end=(1.84375-10.4375j))
; 		 Line(start=(1.84375-10.4375j), end=(1.84375-1.171875j))
; 		 CubicBezier(start=(1.84375-1.171875j), control1=(1.84375-0.515625j), control2=(1.5-0.46875j), end=(0.6875-0.46875j))
; 		 Line(start=(0.6875-0.46875j), end=(0.6875-0.015625j))
; 		 Line(start=(0.6875-0.015625j), end=(3.90625-0.015625j))
; 		 Line(start=(3.90625-0.015625j), end=(3.90625-0.46875j))
; 		 Line(start=(3.90625-0.46875j), end=(3.59375-0.46875j))
; 		 CubicBezier(start=(3.59375-0.46875j), control1=(3.1875-0.46875j), control2=(2.75-0.515625j), end=(2.75-1.046875j))
; 		 Line(start=(2.75-1.046875j), end=(2.75-4.34375j))
; 		 CubicBezier(start=(2.75-4.34375j), control1=(2.75-5.515625j), control2=(3.234375-7.28125j), end=(4.953125-7.28125j))
; 		 CubicBezier(start=(4.953125-7.28125j), control1=(6.0625-7.28125j), control2=(6.3125-6.25j), end=(6.3125-5.25j))
; 		 Line(start=(6.3125-5.25j), end=(6.3125-1.078125j))
; 		 CubicBezier(start=(6.3125-1.078125j), control1=(6.3125-0.5j), control2=(5.84375-0.46875j), end=(5.328125-0.46875j))
; 		 Line(start=(5.328125-0.46875j), end=(5.171875-0.46875j))
; 		 Line(start=(5.171875-0.46875j), end=(5.171875-0.015625j))
; 		 Line(start=(5.171875-0.015625j), end=(8.390625-0.015625j))
; 		 Line(start=(8.390625-0.015625j), end=(8.390625-0.46875j))
; 		 Line(start=(8.390625-0.46875j), end=(8.078125-0.46875j))
; 		 CubicBezier(start=(8.078125-0.46875j), control1=(7.671875-0.46875j), control2=(7.296875-0.5j), end=(7.234375-0.9375j))
; 		 Line(start=(7.234375-0.9375j), end=(7.234375-5.046875j))
; 		 CubicBezier(start=(7.234375-5.046875j), control1=(7.234375-6.453125j), control2=(6.78125-7.5625j), end=(5.0625-7.5625j))
; 		 CubicBezier(start=(5.0625-7.5625j), control1=(4-7.5625j), control2=(3.046875-6.859375j), end=(2.734375-5.859375j))
; 		 Line(start=(2.734375-5.859375j), end=(2.71875-11.96875j))
; 		 Line(start=(2.71875-11.96875j), end=(0.6875-11.78125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.640625-8.109375j), end=(0.421875-5.453125j))
; 		 Line(start=(0.421875-5.453125j), end=(0.6875-5.453125j))
; 		 CubicBezier(start=(0.6875-5.453125j), control1=(0.734375-6.046875j), control2=(0.75-6.890625j), end=(1.09375-7.28125j))
; 		 CubicBezier(start=(1.09375-7.28125j), control1=(1.46875-7.734375j), control2=(2.15625-7.78125j), end=(2.71875-7.78125j))
; 		 Line(start=(2.71875-7.78125j), end=(3.25-7.78125j))
; 		 CubicBezier(start=(3.25-7.78125j), control1=(3.484375-7.78125j), control2=(3.734375-7.75j), end=(3.734375-7.359375j))
; 		 Line(start=(3.734375-7.359375j), end=(3.734375-0.90625j))
; 		 CubicBezier(start=(3.734375-0.90625j), control1=(3.734375-0.421875j), control2=(3.296875-0.34375j), end=(2.875-0.34375j))
; 		 CubicBezier(start=(2.875-0.34375j), control1=(2.75-0.34375j), control2=(2.609375-0.359375j), end=(2.5-0.359375j))
; 		 Line(start=(2.5-0.359375j), end=(2.140625-0.359375j))
; 		 Line(start=(2.140625-0.359375j), end=(2.140625-0.015625j))
; 		 Line(start=(2.140625-0.015625j), end=(6.3125-0.015625j))
; 		 Line(start=(6.3125-0.015625j), end=(6.3125-0.359375j))
; 		 Line(start=(6.3125-0.359375j), end=(5.59375-0.359375j))
; 		 CubicBezier(start=(5.59375-0.359375j), control1=(5.140625-0.359375j), control2=(4.703125-0.40625j), end=(4.703125-0.890625j))
; 		 Line(start=(4.703125-0.890625j), end=(4.703125-7.15625j))
; 		 CubicBezier(start=(4.703125-7.15625j), control1=(4.703125-7.234375j), control2=(4.703125-7.296875j), end=(4.703125-7.375j))
; 		 CubicBezier(start=(4.703125-7.375j), control1=(4.703125-7.71875j), control2=(4.90625-7.78125j), end=(5.3125-7.78125j))
; 		 Line(start=(5.3125-7.78125j), end=(5.6875-7.78125j))
; 		 CubicBezier(start=(5.6875-7.78125j), control1=(6.5-7.78125j), control2=(7.34375-7.703125j), end=(7.609375-6.6875j))
; 		 CubicBezier(start=(7.609375-6.6875j), control1=(7.703125-6.28125j), control2=(7.71875-5.859375j), end=(7.75-5.453125j))
; 		 Line(start=(7.75-5.453125j), end=(8.015625-5.453125j))
; 		 Line(start=(8.015625-5.453125j), end=(7.8125-8.109375j))
; 		 Line(start=(7.8125-8.109375j), end=(0.640625-8.109375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.375-8.171875j), end=(0.375-7.828125j))
; 		 Line(start=(0.375-7.828125j), end=(0.59375-7.828125j))
; 		 CubicBezier(start=(0.59375-7.828125j), control1=(0.984375-7.828125j), control2=(1.296875-7.78125j), end=(1.296875-7.234375j))
; 		 Line(start=(1.296875-7.234375j), end=(1.296875-0.84375j))
; 		 CubicBezier(start=(1.296875-0.84375j), control1=(1.296875-0.40625j), control2=(0.984375-0.359375j), end=(0.640625-0.359375j))
; 		 Line(start=(0.640625-0.359375j), end=(0.375-0.359375j))
; 		 Line(start=(0.375-0.359375j), end=(0.375-0.015625j))
; 		 Line(start=(0.375-0.015625j), end=(2.96875-0.015625j))
; 		 Line(start=(2.96875-0.015625j), end=(2.96875-0.359375j))
; 		 Line(start=(2.96875-0.359375j), end=(2.703125-0.359375j))
; 		 CubicBezier(start=(2.703125-0.359375j), control1=(2.359375-0.359375j), control2=(2.0625-0.40625j), end=(2.0625-0.84375j))
; 		 Line(start=(2.0625-0.84375j), end=(2.0625-3.03125j))
; 		 CubicBezier(start=(2.0625-3.03125j), control1=(2.0625-3.859375j), control2=(2.484375-5.03125j), end=(3.65625-5.03125j))
; 		 CubicBezier(start=(3.65625-5.03125j), control1=(4.390625-5.03125j), control2=(4.546875-4.328125j), end=(4.546875-3.625j))
; 		 Line(start=(4.546875-3.625j), end=(4.546875-0.8125j))
; 		 CubicBezier(start=(4.546875-0.8125j), control1=(4.546875-0.390625j), control2=(4.203125-0.359375j), end=(3.84375-0.359375j))
; 		 Line(start=(3.84375-0.359375j), end=(3.640625-0.359375j))
; 		 Line(start=(3.640625-0.359375j), end=(3.640625-0.015625j))
; 		 Line(start=(3.640625-0.015625j), end=(6.234375-0.015625j))
; 		 Line(start=(6.234375-0.015625j), end=(6.234375-0.359375j))
; 		 Line(start=(6.234375-0.359375j), end=(5.953125-0.359375j))
; 		 CubicBezier(start=(5.953125-0.359375j), control1=(5.625-0.359375j), control2=(5.3125-0.375j), end=(5.3125-0.828125j))
; 		 Line(start=(5.3125-0.828125j), end=(5.3125-3.53125j))
; 		 CubicBezier(start=(5.3125-3.53125j), control1=(5.3125-3.921875j), control2=(5.3125-4.296875j), end=(5.109375-4.640625j))
; 		 CubicBezier(start=(5.109375-4.640625j), control1=(4.828125-5.109375j), control2=(4.265625-5.265625j), end=(3.734375-5.265625j))
; 		 CubicBezier(start=(3.734375-5.265625j), control1=(3.015625-5.265625j), control2=(2.265625-4.828125j), end=(2.0625-4.125j))
; 		 Line(start=(2.0625-4.125j), end=(2.046875-8.3125j))
; 		 Line(start=(2.046875-8.3125j), end=(0.375-8.171875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(2.75-5.328125j), control1=(1.46875-5.21875j), control2=(0.34375-4.09375j), end=(0.34375-2.578125j))
; 		 CubicBezier(start=(0.34375-2.578125j), control1=(0.34375-1.21875j), control2=(1.34375+0.109375j), end=(2.921875+0.109375j))
; 		 CubicBezier(start=(2.921875+0.109375j), control1=(4.359375+0.109375j), control2=(5.484375-1.15625j), end=(5.484375-2.5625j))
; 		 CubicBezier(start=(5.484375-2.5625j), control1=(5.484375-3.890625j), control2=(4.5-5.34375j), end=(2.90625-5.34375j))
; 		 CubicBezier(start=(2.90625-5.34375j), control1=(2.859375-5.34375j), control2=(2.796875-5.328125j), end=(2.75-5.328125j))
; 		 Line(start=(2.75-5.328125j), end=(2.75-5.328125j))
; 		 CubicBezier(start=(1.265625-2.3125j), control1=(1.25-2.40625j), control2=(1.25-2.5j), end=(1.25-2.59375j))
; 		 CubicBezier(start=(1.25-2.59375j), control1=(1.25-3.421875j), control2=(1.34375-4.5j), end=(2.265625-4.9375j))
; 		 CubicBezier(start=(2.265625-4.9375j), control1=(2.46875-5.03125j), control2=(2.6875-5.078125j), end=(2.90625-5.078125j))
; 		 CubicBezier(start=(2.90625-5.078125j), control1=(3.359375-5.078125j), control2=(3.796875-4.875j), end=(4.09375-4.515625j))
; 		 CubicBezier(start=(4.09375-4.515625j), control1=(4.515625-4.03125j), control2=(4.5625-3.359375j), end=(4.5625-2.734375j))
; 		 CubicBezier(start=(4.5625-2.734375j), control1=(4.5625-2.0625j), control2=(4.546875-1.296875j), end=(4.09375-0.734375j))
; 		 CubicBezier(start=(4.09375-0.734375j), control1=(3.796875-0.375j), control2=(3.359375-0.15625j), end=(2.90625-0.15625j))
; 		 CubicBezier(start=(2.90625-0.15625j), control1=(1.90625-0.15625j), control2=(1.40625-1.046875j), end=(1.296875-1.9375j))
; 		 CubicBezier(start=(1.296875-1.9375j), control1=(1.28125-2.0625j), control2=(1.28125-2.1875j), end=(1.265625-2.3125j))
; 		 Line(start=(1.265625-2.3125j), end=(1.265625-2.3125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.375-5.140625j), end=(0.375-4.796875j))
; 		 Line(start=(0.375-4.796875j), end=(0.609375-4.796875j))
; 		 CubicBezier(start=(0.609375-4.796875j), control1=(0.953125-4.796875j), control2=(1.296875-4.75j), end=(1.296875-4.203125j))
; 		 Line(start=(1.296875-4.203125j), end=(1.296875-0.859375j))
; 		 CubicBezier(start=(1.296875-0.859375j), control1=(1.296875-0.421875j), control2=(1.0625-0.359375j), end=(0.375-0.359375j))
; 		 Line(start=(0.375-0.359375j), end=(0.375-0.015625j))
; 		 Line(start=(0.375-0.015625j), end=(2.96875-0.015625j))
; 		 Line(start=(2.96875-0.015625j), end=(2.96875-0.359375j))
; 		 Line(start=(2.96875-0.359375j), end=(2.703125-0.359375j))
; 		 CubicBezier(start=(2.703125-0.359375j), control1=(2.359375-0.359375j), control2=(2.0625-0.40625j), end=(2.0625-0.84375j))
; 		 Line(start=(2.0625-0.84375j), end=(2.0625-3.03125j))
; 		 CubicBezier(start=(2.0625-3.03125j), control1=(2.0625-3.859375j), control2=(2.484375-5.03125j), end=(3.65625-5.03125j))
; 		 CubicBezier(start=(3.65625-5.03125j), control1=(4.390625-5.03125j), control2=(4.546875-4.328125j), end=(4.546875-3.625j))
; 		 Line(start=(4.546875-3.625j), end=(4.546875-0.8125j))
; 		 CubicBezier(start=(4.546875-0.8125j), control1=(4.546875-0.390625j), control2=(4.203125-0.359375j), end=(3.84375-0.359375j))
; 		 Line(start=(3.84375-0.359375j), end=(3.640625-0.359375j))
; 		 Line(start=(3.640625-0.359375j), end=(3.640625-0.015625j))
; 		 Line(start=(3.640625-0.015625j), end=(6.234375-0.015625j))
; 		 Line(start=(6.234375-0.015625j), end=(6.234375-0.359375j))
; 		 Line(start=(6.234375-0.359375j), end=(5.953125-0.359375j))
; 		 CubicBezier(start=(5.953125-0.359375j), control1=(5.609375-0.359375j), control2=(5.3125-0.40625j), end=(5.3125-0.84375j))
; 		 Line(start=(5.3125-0.84375j), end=(5.3125-3.03125j))
; 		 CubicBezier(start=(5.3125-3.03125j), control1=(5.3125-3.859375j), control2=(5.734375-5.03125j), end=(6.90625-5.03125j))
; 		 CubicBezier(start=(6.90625-5.03125j), control1=(7.640625-5.03125j), control2=(7.796875-4.328125j), end=(7.796875-3.625j))
; 		 Line(start=(7.796875-3.625j), end=(7.796875-0.8125j))
; 		 CubicBezier(start=(7.796875-0.8125j), control1=(7.796875-0.390625j), control2=(7.453125-0.359375j), end=(7.09375-0.359375j))
; 		 Line(start=(7.09375-0.359375j), end=(6.890625-0.359375j))
; 		 Line(start=(6.890625-0.359375j), end=(6.890625-0.015625j))
; 		 Line(start=(6.890625-0.015625j), end=(9.484375-0.015625j))
; 		 Line(start=(9.484375-0.015625j), end=(9.484375-0.359375j))
; 		 Line(start=(9.484375-0.359375j), end=(9.203125-0.359375j))
; 		 CubicBezier(start=(9.203125-0.359375j), control1=(8.875-0.359375j), control2=(8.578125-0.375j), end=(8.578125-0.828125j))
; 		 Line(start=(8.578125-0.828125j), end=(8.578125-3.53125j))
; 		 CubicBezier(start=(8.578125-3.53125j), control1=(8.578125-3.90625j), control2=(8.5625-4.28125j), end=(8.359375-4.609375j))
; 		 CubicBezier(start=(8.359375-4.609375j), control1=(8.09375-5.109375j), control2=(7.53125-5.265625j), end=(7-5.265625j))
; 		 CubicBezier(start=(7-5.265625j), control1=(6.265625-5.265625j), control2=(5.515625-4.8125j), end=(5.296875-4.09375j))
; 		 Line(start=(5.296875-4.09375j), end=(5.28125-4.09375j))
; 		 CubicBezier(start=(5.28125-4.09375j), control1=(5.203125-4.921875j), control2=(4.46875-5.265625j), end=(3.6875-5.265625j))
; 		 CubicBezier(start=(3.6875-5.265625j), control1=(2.9375-5.265625j), control2=(2.21875-4.71875j), end=(2.015625-4.046875j))
; 		 Line(start=(2.015625-4.046875j), end=(2.015625-5.265625j))
; 		 Line(start=(2.015625-5.265625j), end=(0.375-5.140625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.25-4.40625j), control1=(1.546875-4.859375j), control2=(2.0625-5.09375j), end=(2.59375-5.09375j))
; 		 CubicBezier(start=(2.59375-5.09375j), control1=(3.28125-5.09375j), control2=(3.828125-4.40625j), end=(3.828125-3.734375j))
; 		 Line(start=(3.828125-3.734375j), end=(3.828125-3.09375j))
; 		 CubicBezier(start=(3.828125-3.09375j), control1=(2.625-3.09375j), control2=(0.515625-2.765625j), end=(0.515625-1.125j))
; 		 Line(start=(0.515625-1.125j), end=(0.515625-1j))
; 		 CubicBezier(start=(0.515625-1j), control1=(0.625-0.1875j), control2=(1.609375+0.125j), end=(2.3125+0.125j))
; 		 CubicBezier(start=(2.3125+0.125j), control1=(2.9375+0.125j), control2=(3.71875-0.265625j), end=(3.890625-0.921875j))
; 		 Line(start=(3.890625-0.921875j), end=(3.90625-0.921875j))
; 		 CubicBezier(start=(3.90625-0.921875j), control1=(3.9375-0.359375j), control2=(4.34375+0.046875j), end=(4.828125+0.046875j))
; 		 CubicBezier(start=(4.828125+0.046875j), control1=(5.328125+0.046875j), control2=(5.71875-0.375j), end=(5.765625-0.875j))
; 		 Line(start=(5.765625-0.875j), end=(5.765625-1.75j))
; 		 Line(start=(5.765625-1.75j), end=(5.515625-1.75j))
; 		 Line(start=(5.515625-1.75j), end=(5.515625-1.15625j))
; 		 CubicBezier(start=(5.515625-1.15625j), control1=(5.515625-0.8125j), control2=(5.453125-0.296875j), end=(5.0625-0.296875j))
; 		 CubicBezier(start=(5.0625-0.296875j), control1=(4.671875-0.296875j), control2=(4.59375-0.8125j), end=(4.59375-1.109375j))
; 		 CubicBezier(start=(4.59375-1.109375j), control1=(4.59375-1.25j), control2=(4.609375-1.375j), end=(4.609375-1.5j))
; 		 Line(start=(4.609375-1.5j), end=(4.609375-3.171875j))
; 		 CubicBezier(start=(4.609375-3.171875j), control1=(4.609375-3.296875j), control2=(4.609375-3.4375j), end=(4.609375-3.578125j))
; 		 CubicBezier(start=(4.609375-3.578125j), control1=(4.609375-4.703125j), control2=(3.578125-5.328125j), end=(2.59375-5.328125j))
; 		 CubicBezier(start=(2.59375-5.328125j), control1=(1.828125-5.328125j), control2=(0.859375-4.875j), end=(0.859375-3.9375j))
; 		 CubicBezier(start=(0.859375-3.9375j), control1=(0.859375-3.59375j), control2=(1.09375-3.40625j), end=(1.375-3.40625j))
; 		 CubicBezier(start=(1.375-3.40625j), control1=(1.65625-3.40625j), control2=(1.859375-3.640625j), end=(1.859375-3.90625j))
; 		 CubicBezier(start=(1.859375-3.90625j), control1=(1.859375-4.203125j), control2=(1.640625-4.40625j), end=(1.34375-4.40625j))
; 		 Line(start=(1.34375-4.40625j), end=(1.25-4.40625j))
; 		 Line(start=(3.828125-2.875j), end=(3.828125-1.75j))
; 		 CubicBezier(start=(3.828125-1.75j), control1=(3.828125-0.890625j), control2=(3.28125-0.125j), end=(2.4375-0.125j))
; 		 CubicBezier(start=(2.4375-0.125j), control1=(1.984375-0.125j), control2=(1.375-0.484375j), end=(1.375-1.09375j))
; 		 CubicBezier(start=(1.375-1.09375j), control1=(1.375-2.15625j), control2=(2.328125-2.6875j), end=(3.328125-2.828125j))
; 		 CubicBezier(start=(3.328125-2.828125j), control1=(3.484375-2.859375j), control2=(3.65625-2.84375j), end=(3.828125-2.875j))
; 		 Line(start=(3.828125-2.875j), end=(3.828125-2.875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(3.390625-4.953125j), control1=(3.0625-5.234375j), control2=(2.671875-5.328125j), end=(2.25-5.328125j))
; 		 CubicBezier(start=(2.25-5.328125j), control1=(1.390625-5.328125j), control2=(0.390625-5j), end=(0.390625-3.84375j))
; 		 Line(start=(0.390625-3.84375j), end=(0.390625-3.734375j))
; 		 CubicBezier(start=(0.390625-3.734375j), control1=(0.84375-1.734375j), control2=(3.671875-2.96875j), end=(3.671875-1.203125j))
; 		 CubicBezier(start=(3.671875-1.203125j), control1=(3.671875-0.46875j), control2=(3-0.125j), end=(2.359375-0.125j))
; 		 CubicBezier(start=(2.359375-0.125j), control1=(1.171875-0.125j), control2=(0.875-1.09375j), end=(0.671875-1.921875j))
; 		 CubicBezier(start=(0.671875-1.921875j), control1=(0.640625-2j), control2=(0.609375-2.0625j), end=(0.53125-2.0625j))
; 		 CubicBezier(start=(0.53125-2.0625j), control1=(0.421875-2.0625j), control2=(0.390625-1.96875j), end=(0.390625-1.890625j))
; 		 Line(start=(0.390625-1.890625j), end=(0.390625-0.03125j))
; 		 CubicBezier(start=(0.390625-0.03125j), control1=(0.390625+0.03125j), control2=(0.421875+0.109375j), end=(0.515625+0.109375j))
; 		 CubicBezier(start=(0.515625+0.109375j), control1=(0.71875+0.109375j), control2=(0.890625-0.40625j), end=(1.015625-0.40625j))
; 		 Line(start=(1.015625-0.40625j), end=(1.03125-0.40625j))
; 		 CubicBezier(start=(1.03125-0.40625j), control1=(1.109375-0.375j), control2=(1.171875-0.28125j), end=(1.25-0.234375j))
; 		 CubicBezier(start=(1.25-0.234375j), control1=(1.5625+0.015625j), control2=(1.953125+0.109375j), end=(2.359375+0.109375j))
; 		 CubicBezier(start=(2.359375+0.109375j), control1=(3.234375+0.109375j), control2=(4.21875-0.328125j), end=(4.21875-1.53125j))
; 		 CubicBezier(start=(4.21875-1.53125j), control1=(4.21875-2.234375j), control2=(3.65625-2.875j), end=(3-3.078125j))
; 		 CubicBezier(start=(3-3.078125j), control1=(2.359375-3.28125j), control2=(0.9375-3.234375j), end=(0.9375-4.203125j))
; 		 CubicBezier(start=(0.9375-4.203125j), control1=(0.9375-4.875j), control2=(1.65625-5.109375j), end=(2.21875-5.109375j))
; 		 CubicBezier(start=(2.21875-5.109375j), control1=(2.90625-5.109375j), control2=(3.65625-4.75j), end=(3.65625-3.734375j))
; 		 CubicBezier(start=(3.65625-3.734375j), control1=(3.65625-3.625j), control2=(3.671875-3.53125j), end=(3.796875-3.53125j))
; 		 CubicBezier(start=(3.796875-3.53125j), control1=(3.890625-3.53125j), control2=(3.921875-3.609375j), end=(3.921875-3.6875j))
; 		 Line(start=(3.921875-3.6875j), end=(3.921875-5.1875j))
; 		 CubicBezier(start=(3.921875-5.1875j), control1=(3.921875-5.25j), control2=(3.890625-5.328125j), end=(3.8125-5.328125j))
; 		 CubicBezier(start=(3.8125-5.328125j), control1=(3.65625-5.328125j), control2=(3.46875-5.046875j), end=(3.390625-4.953125j))
; 		 Line(start=(3.390625-4.953125j), end=(3.390625-4.953125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.484375-8.171875j), end=(0.484375-7.828125j))
; 		 Line(start=(0.484375-7.828125j), end=(0.875-7.828125j))
; 		 CubicBezier(start=(0.875-7.828125j), control1=(1.265625-7.828125j), control2=(1.640625-7.78125j), end=(1.640625-7.328125j))
; 		 CubicBezier(start=(1.640625-7.328125j), control1=(1.640625-7.25j), control2=(1.625-7.171875j), end=(1.625-7.09375j))
; 		 Line(start=(1.625-7.09375j), end=(1.625-0.890625j))
; 		 CubicBezier(start=(1.625-0.890625j), control1=(1.625-0.421875j), control2=(1.265625-0.34375j), end=(0.90625-0.34375j))
; 		 CubicBezier(start=(0.90625-0.34375j), control1=(0.765625-0.34375j), control2=(0.625-0.359375j), end=(0.484375-0.359375j))
; 		 Line(start=(0.484375-0.359375j), end=(0.484375-0.015625j))
; 		 Line(start=(0.484375-0.015625j), end=(3.734375-0.015625j))
; 		 Line(start=(3.734375-0.015625j), end=(3.734375-0.359375j))
; 		 CubicBezier(start=(3.734375-0.359375j), control1=(3.578125-0.359375j), control2=(3.4375-0.34375j), end=(3.296875-0.34375j))
; 		 CubicBezier(start=(3.296875-0.34375j), control1=(2.921875-0.34375j), control2=(2.59375-0.421875j), end=(2.59375-0.890625j))
; 		 Line(start=(2.59375-0.890625j), end=(2.59375-4.09375j))
; 		 Line(start=(2.59375-4.09375j), end=(6.171875-4.09375j))
; 		 Line(start=(6.171875-4.09375j), end=(6.171875-0.890625j))
; 		 CubicBezier(start=(6.171875-0.890625j), control1=(6.171875-0.375j), control2=(5.859375-0.359375j), end=(5.03125-0.359375j))
; 		 Line(start=(5.03125-0.359375j), end=(5.03125-0.015625j))
; 		 Line(start=(5.03125-0.015625j), end=(8.265625-0.015625j))
; 		 Line(start=(8.265625-0.015625j), end=(8.265625-0.359375j))
; 		 Line(start=(8.265625-0.359375j), end=(7.890625-0.359375j))
; 		 CubicBezier(start=(7.890625-0.359375j), control1=(7.5-0.359375j), control2=(7.125-0.40625j), end=(7.125-0.859375j))
; 		 CubicBezier(start=(7.125-0.859375j), control1=(7.125-0.9375j), control2=(7.140625-1.015625j), end=(7.140625-1.09375j))
; 		 Line(start=(7.140625-1.09375j), end=(7.140625-7.296875j))
; 		 CubicBezier(start=(7.140625-7.296875j), control1=(7.140625-7.765625j), control2=(7.5-7.84375j), end=(7.859375-7.84375j))
; 		 CubicBezier(start=(7.859375-7.84375j), control1=(8-7.84375j), control2=(8.140625-7.828125j), end=(8.265625-7.828125j))
; 		 Line(start=(8.265625-7.828125j), end=(8.265625-8.171875j))
; 		 Line(start=(8.265625-8.171875j), end=(5.03125-8.171875j))
; 		 Line(start=(5.03125-8.171875j), end=(5.03125-7.828125j))
; 		 CubicBezier(start=(5.03125-7.828125j), control1=(5.171875-7.828125j), control2=(5.3125-7.84375j), end=(5.453125-7.84375j))
; 		 CubicBezier(start=(5.453125-7.84375j), control1=(5.84375-7.84375j), control2=(6.171875-7.78125j), end=(6.171875-7.3125j))
; 		 Line(start=(6.171875-7.3125j), end=(6.171875-4.4375j))
; 		 Line(start=(6.171875-4.4375j), end=(2.59375-4.4375j))
; 		 Line(start=(2.59375-4.4375j), end=(2.59375-7.3125j))
; 		 CubicBezier(start=(2.59375-7.3125j), control1=(2.59375-7.8125j), control2=(2.984375-7.828125j), end=(3.515625-7.828125j))
; 		 Line(start=(3.515625-7.828125j), end=(3.734375-7.828125j))
; 		 Line(start=(3.734375-7.828125j), end=(3.734375-8.171875j))
; 		 Line(start=(3.734375-8.171875j), end=(0.484375-8.171875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.4375-5.140625j), end=(0.4375-4.796875j))
; 		 Line(start=(0.4375-4.796875j), end=(0.640625-4.796875j))
; 		 CubicBezier(start=(0.640625-4.796875j), control1=(1.015625-4.796875j), control2=(1.296875-4.734375j), end=(1.296875-4.203125j))
; 		 Line(start=(1.296875-4.203125j), end=(1.296875-0.859375j))
; 		 CubicBezier(start=(1.296875-0.859375j), control1=(1.296875-0.421875j), control2=(1.078125-0.359375j), end=(0.390625-0.359375j))
; 		 Line(start=(0.390625-0.359375j), end=(0.390625-0.015625j))
; 		 Line(start=(0.390625-0.015625j), end=(2.875-0.015625j))
; 		 Line(start=(2.875-0.015625j), end=(2.875-0.359375j))
; 		 Line(start=(2.875-0.359375j), end=(2.703125-0.359375j))
; 		 CubicBezier(start=(2.703125-0.359375j), control1=(2.359375-0.359375j), control2=(2.0625-0.375j), end=(2.0625-0.78125j))
; 		 Line(start=(2.0625-0.78125j), end=(2.0625-5.265625j))
; 		 Line(start=(2.0625-5.265625j), end=(0.4375-5.140625j))
; 		 CubicBezier(start=(1.40625-7.9375j), control1=(1.09375-7.890625j), control2=(0.921875-7.625j), end=(0.921875-7.375j))
; 		 CubicBezier(start=(0.921875-7.375j), control1=(0.921875-7.125j), control2=(1.109375-6.796875j), end=(1.484375-6.796875j))
; 		 CubicBezier(start=(1.484375-6.796875j), control1=(1.859375-6.796875j), control2=(2.078125-7.078125j), end=(2.078125-7.359375j))
; 		 CubicBezier(start=(2.078125-7.359375j), control1=(2.078125-7.609375j), control2=(1.890625-7.953125j), end=(1.515625-7.953125j))
; 		 CubicBezier(start=(1.515625-7.953125j), control1=(1.484375-7.953125j), control2=(1.453125-7.9375j), end=(1.40625-7.9375j))
; 		 Line(start=(1.40625-7.9375j), end=(1.40625-7.9375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.375-5.140625j), end=(0.375-4.796875j))
; 		 Line(start=(0.375-4.796875j), end=(0.609375-4.796875j))
; 		 CubicBezier(start=(0.609375-4.796875j), control1=(0.953125-4.796875j), control2=(1.296875-4.75j), end=(1.296875-4.203125j))
; 		 Line(start=(1.296875-4.203125j), end=(1.296875-0.859375j))
; 		 CubicBezier(start=(1.296875-0.859375j), control1=(1.296875-0.421875j), control2=(1.0625-0.359375j), end=(0.375-0.359375j))
; 		 Line(start=(0.375-0.359375j), end=(0.375-0.015625j))
; 		 Line(start=(0.375-0.015625j), end=(2.96875-0.015625j))
; 		 Line(start=(2.96875-0.015625j), end=(2.96875-0.359375j))
; 		 Line(start=(2.96875-0.359375j), end=(2.703125-0.359375j))
; 		 CubicBezier(start=(2.703125-0.359375j), control1=(2.359375-0.359375j), control2=(2.0625-0.40625j), end=(2.0625-0.84375j))
; 		 Line(start=(2.0625-0.84375j), end=(2.0625-3.03125j))
; 		 CubicBezier(start=(2.0625-3.03125j), control1=(2.0625-3.859375j), control2=(2.484375-5.03125j), end=(3.65625-5.03125j))
; 		 CubicBezier(start=(3.65625-5.03125j), control1=(4.390625-5.03125j), control2=(4.546875-4.328125j), end=(4.546875-3.625j))
; 		 Line(start=(4.546875-3.625j), end=(4.546875-0.8125j))
; 		 CubicBezier(start=(4.546875-0.8125j), control1=(4.546875-0.390625j), control2=(4.203125-0.359375j), end=(3.84375-0.359375j))
; 		 Line(start=(3.84375-0.359375j), end=(3.640625-0.359375j))
; 		 Line(start=(3.640625-0.359375j), end=(3.640625-0.015625j))
; 		 Line(start=(3.640625-0.015625j), end=(6.234375-0.015625j))
; 		 Line(start=(6.234375-0.015625j), end=(6.234375-0.359375j))
; 		 Line(start=(6.234375-0.359375j), end=(5.953125-0.359375j))
; 		 CubicBezier(start=(5.953125-0.359375j), control1=(5.625-0.359375j), control2=(5.3125-0.375j), end=(5.3125-0.828125j))
; 		 Line(start=(5.3125-0.828125j), end=(5.3125-3.53125j))
; 		 CubicBezier(start=(5.3125-3.53125j), control1=(5.3125-3.921875j), control2=(5.3125-4.296875j), end=(5.109375-4.625j))
; 		 CubicBezier(start=(5.109375-4.625j), control1=(4.84375-5.109375j), control2=(4.265625-5.265625j), end=(3.75-5.265625j))
; 		 CubicBezier(start=(3.75-5.265625j), control1=(3.171875-5.265625j), control2=(2.671875-5.0625j), end=(2.265625-4.5j))
; 		 CubicBezier(start=(2.265625-4.5j), control1=(2.15625-4.34375j), control2=(2.0625-4.203125j), end=(2.015625-4.046875j))
; 		 Line(start=(2.015625-4.046875j), end=(2.015625-5.265625j))
; 		 Line(start=(2.015625-5.265625j), end=(0.375-5.140625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.265625-2.78125j), end=(4.6875-2.78125j))
; 		 CubicBezier(start=(4.6875-2.78125j), control1=(4.8125-2.78125j), control2=(4.859375-2.828125j), end=(4.859375-2.953125j))
; 		 CubicBezier(start=(4.859375-2.953125j), control1=(4.859375-4.1875j), control2=(4.203125-5.328125j), end=(2.796875-5.328125j))
; 		 CubicBezier(start=(2.796875-5.328125j), control1=(1.359375-5.328125j), control2=(0.34375-4.03125j), end=(0.34375-2.640625j))
; 		 CubicBezier(start=(0.34375-2.640625j), control1=(0.34375-1.59375j), control2=(0.90625-0.53125j), end=(1.953125-0.09375j))
; 		 CubicBezier(start=(1.953125-0.09375j), control1=(2.21875+0.03125j), control2=(2.515625+0.109375j), end=(2.828125+0.109375j))
; 		 Line(start=(2.828125+0.109375j), end=(2.859375+0.109375j))
; 		 CubicBezier(start=(2.859375+0.109375j), control1=(3.734375+0.109375j), control2=(4.578125-0.421875j), end=(4.828125-1.296875j))
; 		 CubicBezier(start=(4.828125-1.296875j), control1=(4.84375-1.34375j), control2=(4.859375-1.390625j), end=(4.859375-1.4375j))
; 		 CubicBezier(start=(4.859375-1.4375j), control1=(4.859375-1.5j), control2=(4.796875-1.546875j), end=(4.734375-1.546875j))
; 		 CubicBezier(start=(4.734375-1.546875j), control1=(4.59375-1.546875j), control2=(4.53125-1.234375j), end=(4.453125-1.078125j))
; 		 CubicBezier(start=(4.453125-1.078125j), control1=(4.140625-0.53125j), control2=(3.578125-0.15625j), end=(2.9375-0.15625j))
; 		 CubicBezier(start=(2.9375-0.15625j), control1=(2.484375-0.15625j), control2=(2.078125-0.40625j), end=(1.796875-0.734375j))
; 		 CubicBezier(start=(1.796875-0.734375j), control1=(1.3125-1.3125j), control2=(1.265625-2.0625j), end=(1.265625-2.78125j))
; 		 Line(start=(1.265625-2.78125j), end=(1.265625-2.78125j))
; 		 CubicBezier(start=(1.28125-3.015625j), control1=(1.28125-3.921875j), control2=(1.78125-5.09375j), end=(2.765625-5.09375j))
; 		 CubicBezier(start=(2.765625-5.09375j), control1=(3.828125-5.09375j), control2=(4.109375-3.859375j), end=(4.109375-3.015625j))
; 		 Line(start=(4.109375-3.015625j), end=(1.28125-3.015625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(2.078125-0.21875j), control1=(2.09375-0.09375j), control2=(2.09375+0.03125j), end=(2.09375+0.140625j))
; 		 CubicBezier(start=(2.09375+0.140625j), control1=(2.0625+0.8125j), control2=(1.796875+1.515625j), end=(1.3125+2j))
; 		 CubicBezier(start=(1.3125+2j), control1=(1.265625+2.046875j), control2=(1.1875+2.109375j), end=(1.1875+2.1875j))
; 		 Line(start=(1.1875+2.1875j), end=(1.1875+2.21875j))
; 		 CubicBezier(start=(1.1875+2.21875j), control1=(1.21875+2.265625j), control2=(1.265625+2.296875j), end=(1.296875+2.296875j))
; 		 CubicBezier(start=(1.296875+2.296875j), control1=(1.46875+2.296875j), control2=(1.75+1.875j), end=(1.890625+1.65625j))
; 		 CubicBezier(start=(1.890625+1.65625j), control1=(2.15625+1.1875j), control2=(2.34375+0.609375j), end=(2.34375+0.046875j))
; 		 CubicBezier(start=(2.34375+0.046875j), control1=(2.34375-0.40625j), control2=(2.21875-1.171875j), end=(1.609375-1.171875j))
; 		 CubicBezier(start=(1.609375-1.171875j), control1=(1.25-1.171875j), control2=(1.046875-0.875j), end=(1.046875-0.578125j))
; 		 CubicBezier(start=(1.046875-0.578125j), control1=(1.046875-0.234375j), control2=(1.296875-0.015625j), end=(1.609375-0.015625j))
; 		 CubicBezier(start=(1.609375-0.015625j), control1=(1.8125-0.015625j), control2=(1.953125-0.09375j), end=(2.078125-0.21875j))
; 		 Line(start=(2.078125-0.21875j), end=(2.078125-0.21875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.328125-2.015625j), end=(3.46875-2.015625j))
; 		 Line(start=(3.46875-2.015625j), end=(3.46875-0.875j))
; 		 CubicBezier(start=(3.46875-0.875j), control1=(3.46875-0.375j), control2=(3.125-0.359375j), end=(2.328125-0.359375j))
; 		 Line(start=(2.328125-0.359375j), end=(2.328125-0.015625j))
; 		 Line(start=(2.328125-0.015625j), end=(5.453125-0.015625j))
; 		 Line(start=(5.453125-0.015625j), end=(5.453125-0.359375j))
; 		 CubicBezier(start=(5.453125-0.359375j), control1=(5.3125-0.359375j), control2=(5.15625-0.34375j), end=(5.03125-0.34375j))
; 		 CubicBezier(start=(5.03125-0.34375j), control1=(4.640625-0.34375j), control2=(4.3125-0.40625j), end=(4.3125-0.875j))
; 		 Line(start=(4.3125-0.875j), end=(4.3125-2.015625j))
; 		 Line(start=(4.3125-2.015625j), end=(5.515625-2.015625j))
; 		 Line(start=(5.515625-2.015625j), end=(5.515625-2.359375j))
; 		 Line(start=(5.515625-2.359375j), end=(4.3125-2.359375j))
; 		 Line(start=(4.3125-2.359375j), end=(4.3125-7.921875j))
; 		 CubicBezier(start=(4.3125-7.921875j), control1=(4.3125-8.03125j), control2=(4.25-8.078125j), end=(4.15625-8.078125j))
; 		 CubicBezier(start=(4.15625-8.078125j), control1=(3.90625-8.078125j), control2=(3.453125-7.203125j), end=(3.171875-6.78125j))
; 		 CubicBezier(start=(3.171875-6.78125j), control1=(2.3125-5.4375j), control2=(1.4375-4.109375j), end=(0.578125-2.765625j))
; 		 Line(start=(0.578125-2.765625j), end=(0.4375-2.53125j))
; 		 CubicBezier(start=(0.4375-2.53125j), control1=(0.359375-2.421875j), control2=(0.3125-2.34375j), end=(0.3125-2.21875j))
; 		 CubicBezier(start=(0.3125-2.21875j), control1=(0.3125-2.140625j), control2=(0.328125-2.0625j), end=(0.328125-2.015625j))
; 		 Line(start=(0.328125-2.015625j), end=(0.328125-2.015625j))
; 		 Line(start=(0.625-2.359375j), end=(3.515625-6.84375j))
; 		 Line(start=(3.515625-6.84375j), end=(3.515625-2.359375j))
; 		 Line(start=(3.515625-2.359375j), end=(0.625-2.359375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.21875-6.875j), control1=(1.515625-7.390625j), control2=(2.1875-7.671875j), end=(2.796875-7.671875j))
; 		 CubicBezier(start=(2.796875-7.671875j), control1=(3.5-7.671875j), control2=(4.015625-7.21875j), end=(4.015625-6.28125j))
; 		 CubicBezier(start=(4.015625-6.28125j), control1=(4.015625-5.5625j), control2=(3.71875-4.625j), end=(2.96875-4.40625j))
; 		 CubicBezier(start=(2.96875-4.40625j), control1=(2.734375-4.3125j), control2=(2.46875-4.375j), end=(2.25-4.296875j))
; 		 Line(start=(2.25-4.296875j), end=(2.203125-4.296875j))
; 		 CubicBezier(start=(2.203125-4.296875j), control1=(2.09375-4.296875j), control2=(1.953125-4.296875j), end=(1.953125-4.1875j))
; 		 CubicBezier(start=(1.953125-4.1875j), control1=(1.953125-4.078125j), control2=(2.03125-4.046875j), end=(2.140625-4.046875j))
; 		 Line(start=(2.140625-4.046875j), end=(2.265625-4.046875j))
; 		 CubicBezier(start=(2.265625-4.046875j), control1=(2.421875-4.046875j), control2=(2.5625-4.0625j), end=(2.71875-4.0625j))
; 		 CubicBezier(start=(2.71875-4.0625j), control1=(3.84375-4.0625j), control2=(4.234375-3.0625j), end=(4.234375-2.171875j))
; 		 CubicBezier(start=(4.234375-2.171875j), control1=(4.234375-1.40625j), control2=(4.046875-0.078125j), end=(2.859375-0.078125j))
; 		 CubicBezier(start=(2.859375-0.078125j), control1=(2.09375-0.078125j), control2=(1.375-0.421875j), end=(0.9375-1.140625j))
; 		 Line(start=(0.9375-1.140625j), end=(1.046875-1.140625j))
; 		 CubicBezier(start=(1.046875-1.140625j), control1=(1.421875-1.140625j), control2=(1.6875-1.359375j), end=(1.6875-1.734375j))
; 		 CubicBezier(start=(1.6875-1.734375j), control1=(1.6875-2.09375j), control2=(1.40625-2.3125j), end=(1.046875-2.3125j))
; 		 Line(start=(1.046875-2.3125j), end=(1-2.3125j))
; 		 CubicBezier(start=(1-2.3125j), control1=(0.6875-2.28125j), control2=(0.484375-2.015625j), end=(0.484375-1.71875j))
; 		 CubicBezier(start=(0.484375-1.71875j), control1=(0.484375-0.484375j), control2=(1.71875+0.234375j), end=(2.859375+0.234375j))
; 		 CubicBezier(start=(2.859375+0.234375j), control1=(4.140625+0.234375j), control2=(5.34375-0.71875j), end=(5.34375-2.0625j))
; 		 CubicBezier(start=(5.34375-2.0625j), control1=(5.34375-3.125j), control2=(4.40625-4.125j), end=(3.3125-4.203125j))
; 		 CubicBezier(start=(3.3125-4.203125j), control1=(4.328125-4.609375j), control2=(5.015625-5.296875j), end=(5.015625-6.328125j))
; 		 CubicBezier(start=(5.015625-6.328125j), control1=(5.015625-7.453125j), control2=(3.765625-7.953125j), end=(2.859375-7.953125j))
; 		 CubicBezier(start=(2.859375-7.953125j), control1=(1.953125-7.953125j), control2=(0.8125-7.453125j), end=(0.8125-6.390625j))
; 		 CubicBezier(start=(0.8125-6.390625j), control1=(0.8125-6.125j), control2=(0.9375-5.84375j), end=(1.28125-5.78125j))
; 		 Line(start=(1.28125-5.78125j), end=(1.359375-5.78125j))
; 		 CubicBezier(start=(1.359375-5.78125j), control1=(1.671875-5.78125j), control2=(1.90625-6.03125j), end=(1.90625-6.328125j))
; 		 CubicBezier(start=(1.90625-6.328125j), control1=(1.90625-6.640625j), control2=(1.703125-6.890625j), end=(1.34375-6.890625j))
; 		 CubicBezier(start=(1.34375-6.890625j), control1=(1.296875-6.890625j), control2=(1.265625-6.890625j), end=(1.21875-6.875j))
; 		 Line(start=(1.21875-6.875j), end=(1.21875-6.875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.09375-7.203125j), end=(1.09375-6.84375j))
; 		 CubicBezier(start=(1.09375-6.84375j), control1=(1.625-6.84375j), control2=(2.078125-6.9375j), end=(2.609375-7.15625j))
; 		 Line(start=(2.609375-7.15625j), end=(2.609375-0.890625j))
; 		 CubicBezier(start=(2.609375-0.890625j), control1=(2.609375-0.421875j), control2=(2.140625-0.34375j), end=(1.671875-0.34375j))
; 		 CubicBezier(start=(1.671875-0.34375j), control1=(1.484375-0.34375j), control2=(1.296875-0.359375j), end=(1.140625-0.359375j))
; 		 Line(start=(1.140625-0.359375j), end=(1.140625-0.015625j))
; 		 Line(start=(1.140625-0.015625j), end=(4.890625-0.015625j))
; 		 Line(start=(4.890625-0.015625j), end=(4.890625-0.359375j))
; 		 CubicBezier(start=(4.890625-0.359375j), control1=(4.75-0.359375j), control2=(4.546875-0.34375j), end=(4.375-0.34375j))
; 		 CubicBezier(start=(4.375-0.34375j), control1=(3.78125-0.34375j), control2=(3.4375-0.40625j), end=(3.4375-0.921875j))
; 		 Line(start=(3.4375-0.921875j), end=(3.4375-7.78125j))
; 		 CubicBezier(start=(3.4375-7.78125j), control1=(3.4375-7.859375j), control2=(3.40625-7.953125j), end=(3.28125-7.953125j))
; 		 CubicBezier(start=(3.28125-7.953125j), control1=(3.125-7.953125j), control2=(2.96875-7.734375j), end=(2.828125-7.65625j))
; 		 CubicBezier(start=(2.828125-7.65625j), control1=(2.3125-7.296875j), control2=(1.703125-7.203125j), end=(1.09375-7.203125j))
; 		 Line(start=(1.09375-7.203125j), end=(1.09375-7.203125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.015625-8.09375j), end=(0.640625-5.703125j))
; 		 Line(start=(0.640625-5.703125j), end=(0.90625-5.703125j))
; 		 CubicBezier(start=(0.90625-5.703125j), control1=(0.96875-6.0625j), control2=(1.03125-6.640625j), end=(1.203125-6.78125j))
; 		 CubicBezier(start=(1.203125-6.78125j), control1=(1.28125-6.84375j), control2=(1.453125-6.84375j), end=(1.640625-6.84375j))
; 		 Line(start=(1.640625-6.84375j), end=(2.109375-6.84375j))
; 		 CubicBezier(start=(2.109375-6.84375j), control1=(2.28125-6.859375j), control2=(2.46875-6.859375j), end=(2.625-6.859375j))
; 		 Line(start=(2.625-6.859375j), end=(4.890625-6.859375j))
; 		 CubicBezier(start=(4.890625-6.859375j), control1=(4.640625-6.53125j), control2=(4.40625-6.1875j), end=(4.171875-5.828125j))
; 		 CubicBezier(start=(4.171875-5.828125j), control1=(3.890625-5.4375j), control2=(3.609375-5.046875j), end=(3.359375-4.625j))
; 		 CubicBezier(start=(3.359375-4.625j), control1=(2.578125-3.328125j), control2=(2.09375-1.859375j), end=(2.09375-0.328125j))
; 		 Line(start=(2.09375-0.328125j), end=(2.09375-0.203125j))
; 		 CubicBezier(start=(2.09375-0.203125j), control1=(2.125+0.046875j), control2=(2.34375+0.234375j), end=(2.59375+0.234375j))
; 		 Line(start=(2.59375+0.234375j), end=(2.625+0.234375j))
; 		 CubicBezier(start=(2.625+0.234375j), control1=(3.03125+0.234375j), control2=(3.09375-0.125j), end=(3.09375-0.390625j))
; 		 Line(start=(3.09375-0.390625j), end=(3.09375-0.9375j))
; 		 CubicBezier(start=(3.09375-0.9375j), control1=(3.09375-2.265625j), control2=(3.1875-3.75j), end=(3.90625-4.90625j))
; 		 CubicBezier(start=(3.90625-4.90625j), control1=(4.078125-5.171875j), control2=(4.265625-5.421875j), end=(4.453125-5.6875j))
; 		 Line(start=(4.453125-5.6875j), end=(5.484375-7.1875j))
; 		 CubicBezier(start=(5.484375-7.1875j), control1=(5.578125-7.3125j), control2=(5.671875-7.390625j), end=(5.671875-7.5625j))
; 		 Line(start=(5.671875-7.5625j), end=(5.671875-7.703125j))
; 		 Line(start=(5.671875-7.703125j), end=(3.984375-7.703125j))
; 		 CubicBezier(start=(3.984375-7.703125j), control1=(3.671875-7.703125j), control2=(3.171875-7.671875j), end=(2.703125-7.671875j))
; 		 CubicBezier(start=(2.703125-7.671875j), control1=(1.984375-7.671875j), control2=(1.296875-7.75j), end=(1.28125-8.09375j))
; 		 Line(start=(1.28125-8.09375j), end=(1.015625-8.09375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(0.984375-6.25j), control1=(1.21875-7j), control2=(1.8125-7.609375j), end=(2.625-7.609375j))
; 		 CubicBezier(start=(2.625-7.609375j), control1=(3.6875-7.609375j), control2=(4.25-6.609375j), end=(4.25-5.671875j))
; 		 CubicBezier(start=(4.25-5.671875j), control1=(4.25-4.453125j), control2=(3.375-3.46875j), end=(2.625-2.609375j))
; 		 Line(start=(2.625-2.609375j), end=(1.765625-1.640625j))
; 		 CubicBezier(start=(1.765625-1.640625j), control1=(1.515625-1.359375j), control2=(1.265625-1.078125j), end=(1.015625-0.78125j))
; 		 Line(start=(1.015625-0.78125j), end=(0.734375-0.484375j))
; 		 CubicBezier(start=(0.734375-0.484375j), control1=(0.671875-0.375j), control2=(0.578125-0.328125j), end=(0.578125-0.171875j))
; 		 CubicBezier(start=(0.578125-0.171875j), control1=(0.578125-0.125j), control2=(0.578125-0.0625j), end=(0.578125-0.015625j))
; 		 Line(start=(0.578125-0.015625j), end=(4.9375-0.015625j))
; 		 Line(start=(4.9375-0.015625j), end=(5.25-2.015625j))
; 		 Line(start=(5.25-2.015625j), end=(4.984375-2.015625j))
; 		 CubicBezier(start=(4.984375-2.015625j), control1=(4.890625-1.453125j), control2=(4.921875-0.890625j), end=(4.40625-0.890625j))
; 		 CubicBezier(start=(4.40625-0.890625j), control1=(4.078125-0.890625j), control2=(3.734375-0.859375j), end=(3.390625-0.859375j))
; 		 Line(start=(3.390625-0.859375j), end=(1.40625-0.859375j))
; 		 CubicBezier(start=(1.40625-0.859375j), control1=(2.03125-1.484375j), control2=(2.625-2.09375j), end=(3.28125-2.671875j))
; 		 CubicBezier(start=(3.28125-2.671875j), control1=(4.109375-3.4375j), control2=(5.25-4.34375j), end=(5.25-5.59375j))
; 		 CubicBezier(start=(5.25-5.59375j), control1=(5.25-7.09375j), control2=(4.09375-7.953125j), end=(2.796875-7.953125j))
; 		 CubicBezier(start=(2.796875-7.953125j), control1=(1.625-7.953125j), control2=(0.578125-7j), end=(0.578125-5.796875j))
; 		 CubicBezier(start=(0.578125-5.796875j), control1=(0.578125-5.46875j), control2=(0.6875-5.21875j), end=(1.125-5.140625j))
; 		 CubicBezier(start=(1.125-5.140625j), control1=(1.515625-5.140625j), control2=(1.71875-5.421875j), end=(1.71875-5.71875j))
; 		 CubicBezier(start=(1.71875-5.71875j), control1=(1.71875-5.984375j), control2=(1.546875-6.21875j), end=(1.21875-6.28125j))
; 		 CubicBezier(start=(1.21875-6.28125j), control1=(1.140625-6.28125j), control2=(1.0625-6.28125j), end=(0.984375-6.25j))
; 		 Line(start=(0.984375-6.25j), end=(0.984375-6.25j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.484375-3.875j), end=(1.46875-3.875j))
; 		 CubicBezier(start=(1.46875-3.875j), control1=(1.46875-5.03125j), control2=(1.515625-6.34375j), end=(2.4375-7.203125j))
; 		 CubicBezier(start=(2.4375-7.203125j), control1=(2.765625-7.5j), control2=(3.171875-7.671875j), end=(3.609375-7.671875j))
; 		 CubicBezier(start=(3.609375-7.671875j), control1=(4-7.671875j), control2=(4.453125-7.546875j), end=(4.65625-7.1875j))
; 		 CubicBezier(start=(4.65625-7.1875j), control1=(4.625-7.203125j), control2=(4.59375-7.203125j), end=(4.546875-7.203125j))
; 		 CubicBezier(start=(4.546875-7.203125j), control1=(4.265625-7.203125j), control2=(4.046875-6.984375j), end=(4.046875-6.6875j))
; 		 CubicBezier(start=(4.046875-6.6875j), control1=(4.046875-6.421875j), control2=(4.25-6.1875j), end=(4.546875-6.1875j))
; 		 CubicBezier(start=(4.546875-6.1875j), control1=(4.828125-6.1875j), control2=(5.0625-6.390625j), end=(5.0625-6.75j))
; 		 CubicBezier(start=(5.0625-6.75j), control1=(5.0625-7.578125j), control2=(4.28125-7.953125j), end=(3.5625-7.953125j))
; 		 CubicBezier(start=(3.5625-7.953125j), control1=(1.546875-7.953125j), control2=(0.484375-5.65625j), end=(0.484375-3.90625j))
; 		 CubicBezier(start=(0.484375-3.90625j), control1=(0.484375-2.28125j), control2=(0.8125+0.234375j), end=(2.96875+0.234375j))
; 		 CubicBezier(start=(2.96875+0.234375j), control1=(4.046875+0.234375j), control2=(5-0.625j), end=(5.25-1.640625j))
; 		 CubicBezier(start=(5.25-1.640625j), control1=(5.3125-1.90625j), control2=(5.34375-2.203125j), end=(5.34375-2.484375j))
; 		 CubicBezier(start=(5.34375-2.484375j), control1=(5.34375-3.71875j), control2=(4.5-5.109375j), end=(3.0625-5.109375j))
; 		 CubicBezier(start=(3.0625-5.109375j), control1=(2.421875-5.109375j), control2=(1.671875-4.6875j), end=(1.484375-3.875j))
; 		 Line(start=(1.484375-3.875j), end=(1.484375-3.875j))
; 		 Line(start=(1.5-2.59375j), end=(1.5-2.71875j))
; 		 CubicBezier(start=(1.5-2.71875j), control1=(1.5-3.578125j), control2=(1.859375-4.859375j), end=(2.984375-4.859375j))
; 		 Line(start=(2.984375-4.859375j), end=(3.0625-4.859375j))
; 		 CubicBezier(start=(3.0625-4.859375j), control1=(4.3125-4.75j), control2=(4.359375-3.34375j), end=(4.359375-2.53125j))
; 		 Line(start=(4.359375-2.53125j), end=(4.359375-2.328125j))
; 		 CubicBezier(start=(4.359375-2.328125j), control1=(4.359375-1.59375j), control2=(4.34375-0.734375j), end=(3.640625-0.28125j))
; 		 CubicBezier(start=(3.640625-0.28125j), control1=(3.4375-0.140625j), control2=(3.203125-0.078125j), end=(2.96875-0.078125j))
; 		 CubicBezier(start=(2.96875-0.078125j), control1=(2.5-0.078125j), control2=(2.140625-0.328125j), end=(1.875-0.71875j))
; 		 CubicBezier(start=(1.875-0.71875j), control1=(1.515625-1.28125j), control2=(1.546875-1.953125j), end=(1.5-2.59375j))
; 		 Line(start=(1.5-2.59375j), end=(1.5-2.59375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.75-7.359375j), control1=(1.75-6.59375j), control2=(1.4375-5.046875j), end=(0.21875-5.046875j))
; 		 Line(start=(0.21875-5.046875j), end=(0.21875-4.8125j))
; 		 Line(start=(0.21875-4.8125j), end=(1.234375-4.8125j))
; 		 Line(start=(1.234375-4.8125j), end=(1.234375-2.109375j))
; 		 CubicBezier(start=(1.234375-2.109375j), control1=(1.234375-1.9375j), control2=(1.21875-1.765625j), end=(1.21875-1.609375j))
; 		 CubicBezier(start=(1.21875-1.609375j), control1=(1.21875-0.53125j), control2=(1.765625+0.109375j), end=(2.765625+0.109375j))
; 		 CubicBezier(start=(2.765625+0.109375j), control1=(3.6875+0.109375j), control2=(3.890625-0.921875j), end=(3.890625-1.65625j))
; 		 Line(start=(3.890625-1.65625j), end=(3.890625-2.171875j))
; 		 Line(start=(3.890625-2.171875j), end=(3.640625-2.171875j))
; 		 CubicBezier(start=(3.640625-2.171875j), control1=(3.640625-2j), control2=(3.640625-1.8125j), end=(3.640625-1.640625j))
; 		 CubicBezier(start=(3.640625-1.640625j), control1=(3.640625-1.125j), control2=(3.546875-0.15625j), end=(2.828125-0.15625j))
; 		 CubicBezier(start=(2.828125-0.15625j), control1=(2.140625-0.15625j), control2=(2.015625-0.984375j), end=(2.015625-1.5625j))
; 		 Line(start=(2.015625-1.5625j), end=(2.015625-4.8125j))
; 		 Line(start=(2.015625-4.8125j), end=(3.6875-4.8125j))
; 		 Line(start=(3.6875-4.8125j), end=(3.6875-5.15625j))
; 		 Line(start=(3.6875-5.15625j), end=(2.015625-5.15625j))
; 		 Line(start=(2.015625-5.15625j), end=(2.015625-7.359375j))
; 		 Line(start=(2.015625-7.359375j), end=(1.75-7.359375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.53125-1.15625j), control1=(1.28125-1.109375j), control2=(1.046875-0.890625j), end=(1.046875-0.59375j))
; 		 CubicBezier(start=(1.046875-0.59375j), control1=(1.046875-0.28125j), control2=(1.296875-0.015625j), end=(1.609375-0.015625j))
; 		 CubicBezier(start=(1.609375-0.015625j), control1=(1.921875-0.015625j), control2=(2.203125-0.25j), end=(2.203125-0.578125j))
; 		 CubicBezier(start=(2.203125-0.578125j), control1=(2.203125-0.890625j), control2=(1.9375-1.171875j), end=(1.625-1.171875j))
; 		 CubicBezier(start=(1.625-1.171875j), control1=(1.59375-1.171875j), control2=(1.5625-1.15625j), end=(1.53125-1.15625j))
; 		 Line(start=(1.53125-1.15625j), end=(1.53125-1.15625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(6.078125-2.421875j), control1=(6.15625-2.25j), control2=(6.171875-2.078125j), end=(6.3125-1.9375j))
; 		 CubicBezier(start=(6.3125-1.9375j), control1=(6.53125-1.703125j), control2=(6.890625-1.546875j), end=(7.234375-1.546875j))
; 		 CubicBezier(start=(7.234375-1.546875j), control1=(8.328125-1.546875j), control2=(8.453125-2.96875j), end=(8.453125-4.015625j))
; 		 CubicBezier(start=(8.453125-4.015625j), control1=(8.453125-5.765625j), control2=(7.546875-7.640625j), end=(5.703125-8.234375j))
; 		 CubicBezier(start=(5.703125-8.234375j), control1=(5.359375-8.34375j), control2=(5.015625-8.421875j), end=(4.65625-8.421875j))
; 		 CubicBezier(start=(4.65625-8.421875j), control1=(2.40625-8.421875j), control2=(0.640625-6.59375j), end=(0.640625-4.1875j))
; 		 CubicBezier(start=(0.640625-4.1875j), control1=(0.640625-2.03125j), control2=(2.109375+0.109375j), end=(4.671875+0.109375j))
; 		 CubicBezier(start=(4.671875+0.109375j), control1=(5.875+0.109375j), control2=(7.171875-0.1875j), end=(8.25-0.6875j))
; 		 CubicBezier(start=(8.25-0.6875j), control1=(8.3125-0.703125j), control2=(8.40625-0.734375j), end=(8.421875-0.796875j))
; 		 Line(start=(8.421875-0.796875j), end=(8.421875-0.84375j))
; 		 CubicBezier(start=(8.421875-0.84375j), control1=(8.421875-0.890625j), control2=(8.375-0.921875j), end=(8.328125-0.9375j))
; 		 Line(start=(8.328125-0.9375j), end=(7.984375-0.9375j))
; 		 CubicBezier(start=(7.984375-0.9375j), control1=(7.75-0.9375j), control2=(7.453125-0.71875j), end=(7.1875-0.625j))
; 		 CubicBezier(start=(7.1875-0.625j), control1=(6.328125-0.3125j), control2=(5.4375-0.125j), end=(4.546875-0.125j))
; 		 Line(start=(4.546875-0.125j), end=(4.40625-0.125j))
; 		 CubicBezier(start=(4.40625-0.125j), control1=(2.265625-0.28125j), control2=(0.90625-2.21875j), end=(0.90625-4.171875j))
; 		 CubicBezier(start=(0.90625-4.171875j), control1=(0.90625-6.046875j), control2=(2.265625-8.1875j), end=(4.515625-8.1875j))
; 		 CubicBezier(start=(4.515625-8.1875j), control1=(6.78125-8.1875j), control2=(8.171875-6.109375j), end=(8.171875-4.140625j))
; 		 Line(start=(8.171875-4.140625j), end=(8.171875-3.875j))
; 		 CubicBezier(start=(8.171875-3.875j), control1=(8.171875-3.140625j), control2=(8.171875-1.796875j), end=(7.328125-1.796875j))
; 		 CubicBezier(start=(7.328125-1.796875j), control1=(6.875-1.796875j), control2=(6.859375-2.40625j), end=(6.859375-2.671875j))
; 		 CubicBezier(start=(6.859375-2.671875j), control1=(6.859375-2.859375j), control2=(6.875-3.03125j), end=(6.875-3.203125j))
; 		 Line(start=(6.875-3.203125j), end=(6.875-5.375j))
; 		 CubicBezier(start=(6.875-5.375j), control1=(6.875-5.421875j), control2=(6.890625-5.484375j), end=(6.890625-5.546875j))
; 		 CubicBezier(start=(6.890625-5.546875j), control1=(6.890625-5.71875j), control2=(6.796875-5.75j), end=(6.671875-5.75j))
; 		 Line(start=(6.671875-5.75j), end=(6.28125-5.75j))
; 		 CubicBezier(start=(6.28125-5.75j), control1=(6.125-5.75j), control2=(6.078125-5.921875j), end=(6-6.03125j))
; 		 CubicBezier(start=(6-6.03125j), control1=(5.625-6.46875j), control2=(5.109375-6.765625j), end=(4.53125-6.765625j))
; 		 CubicBezier(start=(4.53125-6.765625j), control1=(3.140625-6.765625j), control2=(2.21875-5.453125j), end=(2.21875-4.15625j))
; 		 CubicBezier(start=(2.21875-4.15625j), control1=(2.21875-2.875j), control2=(3.125-1.546875j), end=(4.53125-1.546875j))
; 		 CubicBezier(start=(4.53125-1.546875j), control1=(5.109375-1.546875j), control2=(5.828125-1.921875j), end=(6.078125-2.421875j))
; 		 Line(start=(6.078125-2.421875j), end=(6.078125-2.421875j))
; 		 Line(start=(2.984375-3.96875j), end=(2.984375-4.09375j))
; 		 CubicBezier(start=(2.984375-4.09375j), control1=(2.984375-5.015625j), control2=(3.359375-6.53125j), end=(4.5625-6.53125j))
; 		 CubicBezier(start=(4.5625-6.53125j), control1=(5.1875-6.53125j), control2=(5.765625-6j), end=(6-5.546875j))
; 		 CubicBezier(start=(6-5.546875j), control1=(6.046875-5.46875j), control2=(6.078125-5.421875j), end=(6.09375-5.328125j))
; 		 Line(start=(6.09375-5.328125j), end=(6.09375-3.078125j))
; 		 CubicBezier(start=(6.09375-3.078125j), control1=(6.09375-2.515625j), control2=(5.359375-2j), end=(4.984375-1.859375j))
; 		 CubicBezier(start=(4.984375-1.859375j), control1=(4.859375-1.828125j), control2=(4.703125-1.796875j), end=(4.578125-1.796875j))
; 		 Line(start=(4.578125-1.796875j), end=(4.546875-1.796875j))
; 		 CubicBezier(start=(4.546875-1.796875j), control1=(3.515625-1.796875j), control2=(3.078125-3.078125j), end=(2.984375-3.96875j))
; 		 Line(start=(2.984375-3.96875j), end=(2.984375-3.96875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.375-5.140625j), end=(0.375-4.796875j))
; 		 Line(start=(0.375-4.796875j), end=(0.53125-4.796875j))
; 		 CubicBezier(start=(0.53125-4.796875j), control1=(1.046875-4.796875j), control2=(1.296875-4.75j), end=(1.296875-4.03125j))
; 		 Line(start=(1.296875-4.03125j), end=(1.296875-1.640625j))
; 		 CubicBezier(start=(1.296875-1.640625j), control1=(1.296875-1.265625j), control2=(1.296875-0.890625j), end=(1.5-0.5625j))
; 		 CubicBezier(start=(1.5-0.5625j), control1=(1.8125-0.03125j), control2=(2.46875+0.125j), end=(3.03125+0.125j))
; 		 CubicBezier(start=(3.03125+0.125j), control1=(3.671875+0.125j), control2=(4.375-0.296875j), end=(4.546875-0.953125j))
; 		 Line(start=(4.546875-0.953125j), end=(4.5625+0.109375j))
; 		 Line(start=(4.5625+0.109375j), end=(6.234375-0.015625j))
; 		 Line(start=(6.234375-0.015625j), end=(6.234375-0.359375j))
; 		 Line(start=(6.234375-0.359375j), end=(6-0.359375j))
; 		 CubicBezier(start=(6-0.359375j), control1=(5.65625-0.359375j), control2=(5.3125-0.40625j), end=(5.3125-0.9375j))
; 		 Line(start=(5.3125-0.9375j), end=(5.3125-5.265625j))
; 		 Line(start=(5.3125-5.265625j), end=(3.640625-5.140625j))
; 		 Line(start=(3.640625-5.140625j), end=(3.640625-4.796875j))
; 		 Line(start=(3.640625-4.796875j), end=(3.78125-4.796875j))
; 		 CubicBezier(start=(3.78125-4.796875j), control1=(4.1875-4.796875j), control2=(4.546875-4.765625j), end=(4.546875-4.203125j))
; 		 Line(start=(4.546875-4.203125j), end=(4.546875-1.828125j))
; 		 CubicBezier(start=(4.546875-1.828125j), control1=(4.484375-0.984375j), control2=(4.015625-0.140625j), end=(3.078125-0.140625j))
; 		 CubicBezier(start=(3.078125-0.140625j), control1=(2.640625-0.140625j), control2=(2.21875-0.25j), end=(2.09375-0.890625j))
; 		 CubicBezier(start=(2.09375-0.890625j), control1=(2.0625-1.109375j), control2=(2.0625-1.34375j), end=(2.0625-1.578125j))
; 		 Line(start=(2.0625-1.578125j), end=(2.0625-5.265625j))
; 		 Line(start=(2.0625-5.265625j), end=(0.375-5.140625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(4.453125-0.78125j), end=(4.46875+1.5j))
; 		 CubicBezier(start=(4.46875+1.5j), control1=(4.46875+1.90625j), control2=(4.234375+1.953125j), end=(3.5625+1.953125j))
; 		 Line(start=(3.5625+1.953125j), end=(3.5625+2.3125j))
; 		 Line(start=(3.5625+2.3125j), end=(6.15625+2.3125j))
; 		 Line(start=(6.15625+2.3125j), end=(6.15625+1.953125j))
; 		 Line(start=(6.15625+1.953125j), end=(5.921875+1.953125j))
; 		 CubicBezier(start=(5.921875+1.953125j), control1=(5.5625+1.953125j), control2=(5.25+1.921875j), end=(5.25+1.46875j))
; 		 Line(start=(5.25+1.46875j), end=(5.25-5.265625j))
; 		 Line(start=(5.25-5.265625j), end=(5.015625-5.25j))
; 		 Line(start=(5.015625-5.25j), end=(4.5625-4.15625j))
; 		 CubicBezier(start=(4.5625-4.15625j), control1=(4.28125-4.75j), control2=(3.765625-5.265625j), end=(3-5.265625j))
; 		 CubicBezier(start=(3-5.265625j), control1=(1.515625-5.265625j), control2=(0.421875-4j), end=(0.421875-2.5625j))
; 		 CubicBezier(start=(0.421875-2.5625j), control1=(0.421875-1.21875j), control2=(1.4375+0.109375j), end=(2.859375+0.109375j))
; 		 CubicBezier(start=(2.859375+0.109375j), control1=(3.46875+0.109375j), control2=(4.140625-0.234375j), end=(4.453125-0.78125j))
; 		 Line(start=(4.453125-0.78125j), end=(4.453125-0.78125j))
; 		 Line(start=(1.34375-2.390625j), end=(1.34375-2.515625j))
; 		 CubicBezier(start=(1.34375-2.515625j), control1=(1.34375-3.421875j), control2=(1.703125-4.671875j), end=(2.703125-4.953125j))
; 		 CubicBezier(start=(2.703125-4.953125j), control1=(2.796875-4.984375j), control2=(2.90625-5.015625j), end=(3.015625-5.015625j))
; 		 Line(start=(3.015625-5.015625j), end=(3.0625-5.015625j))
; 		 CubicBezier(start=(3.0625-5.015625j), control1=(3.796875-5.015625j), control2=(4.40625-4.25j), end=(4.5-3.546875j))
; 		 Line(start=(4.5-3.546875j), end=(4.5-1.59375j))
; 		 CubicBezier(start=(4.5-1.59375j), control1=(4.5-0.90625j), control2=(3.796875-0.328125j), end=(3.296875-0.171875j))
; 		 CubicBezier(start=(3.296875-0.171875j), control1=(3.171875-0.140625j), control2=(3.03125-0.125j), end=(2.890625-0.125j))
; 		 CubicBezier(start=(2.890625-0.125j), control1=(1.78125-0.171875j), control2=(1.421875-1.625j), end=(1.34375-2.390625j))
; 		 Line(start=(1.34375-2.390625j), end=(1.34375-2.390625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(4.3125-4.453125j), control1=(4.28125-4.453125j), control2=(4.25-4.453125j), end=(4.21875-4.453125j))
; 		 CubicBezier(start=(4.21875-4.453125j), control1=(3.9375-4.453125j), control2=(3.734375-4.234375j), end=(3.734375-3.953125j))
; 		 CubicBezier(start=(3.734375-3.953125j), control1=(3.734375-3.65625j), control2=(3.953125-3.453125j), end=(4.21875-3.453125j))
; 		 CubicBezier(start=(4.21875-3.453125j), control1=(4.5-3.453125j), control2=(4.734375-3.640625j), end=(4.734375-3.96875j))
; 		 CubicBezier(start=(4.734375-3.96875j), control1=(4.734375-4.953125j), control2=(3.65625-5.328125j), end=(2.890625-5.328125j))
; 		 CubicBezier(start=(2.890625-5.328125j), control1=(1.453125-5.328125j), control2=(0.421875-3.984375j), end=(0.421875-2.59375j))
; 		 CubicBezier(start=(0.421875-2.59375j), control1=(0.421875-1.21875j), control2=(1.453125+0.109375j), end=(2.921875+0.109375j))
; 		 CubicBezier(start=(2.921875+0.109375j), control1=(3.734375+0.109375j), control2=(4.4375-0.328125j), end=(4.78125-1.140625j))
; 		 CubicBezier(start=(4.78125-1.140625j), control1=(4.8125-1.21875j), control2=(4.859375-1.328125j), end=(4.859375-1.421875j))
; 		 Line(start=(4.859375-1.421875j), end=(4.859375-1.46875j))
; 		 CubicBezier(start=(4.859375-1.46875j), control1=(4.84375-1.515625j), control2=(4.796875-1.546875j), end=(4.75-1.546875j))
; 		 CubicBezier(start=(4.75-1.546875j), control1=(4.5625-1.546875j), control2=(4.5625-1.359375j), end=(4.53125-1.25j))
; 		 CubicBezier(start=(4.53125-1.25j), control1=(4.296875-0.609375j), control2=(3.6875-0.15625j), end=(3.03125-0.15625j))
; 		 CubicBezier(start=(3.03125-0.15625j), control1=(1.75-0.15625j), control2=(1.34375-1.578125j), end=(1.34375-2.5625j))
; 		 CubicBezier(start=(1.34375-2.5625j), control1=(1.34375-3.546875j), control2=(1.65625-5.0625j), end=(2.984375-5.0625j))
; 		 CubicBezier(start=(2.984375-5.0625j), control1=(3.46875-5.0625j), control2=(4.03125-4.859375j), end=(4.3125-4.453125j))
; 		 Line(start=(4.3125-4.453125j), end=(4.3125-4.453125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(3.578125-8.171875j), end=(3.578125-7.828125j))
; 		 Line(start=(3.578125-7.828125j), end=(3.734375-7.828125j))
; 		 CubicBezier(start=(3.734375-7.828125j), control1=(4.203125-7.828125j), control2=(4.5-7.8125j), end=(4.5-7.09375j))
; 		 Line(start=(4.5-7.09375j), end=(4.5-4.515625j))
; 		 CubicBezier(start=(4.5-4.515625j), control1=(4.109375-4.984375j), control2=(3.59375-5.265625j), end=(2.984375-5.265625j))
; 		 CubicBezier(start=(2.984375-5.265625j), control1=(1.53125-5.265625j), control2=(0.421875-4j), end=(0.421875-2.5625j))
; 		 CubicBezier(start=(0.421875-2.5625j), control1=(0.421875-1.296875j), control2=(1.328125+0.109375j), end=(2.875+0.109375j))
; 		 CubicBezier(start=(2.875+0.109375j), control1=(3.53125+0.109375j), control2=(3.984375-0.171875j), end=(4.46875-0.703125j))
; 		 Line(start=(4.46875-0.703125j), end=(4.46875+0.109375j))
; 		 Line(start=(4.46875+0.109375j), end=(6.15625-0.015625j))
; 		 Line(start=(6.15625-0.015625j), end=(6.15625-0.359375j))
; 		 Line(start=(6.15625-0.359375j), end=(5.96875-0.359375j))
; 		 CubicBezier(start=(5.96875-0.359375j), control1=(5.609375-0.359375j), control2=(5.25-0.390625j), end=(5.25-0.953125j))
; 		 Line(start=(5.25-0.953125j), end=(5.25-8.3125j))
; 		 Line(start=(5.25-8.3125j), end=(3.578125-8.171875j))
; 		 Line(start=(1.34375-2.296875j), end=(1.34375-2.46875j))
; 		 CubicBezier(start=(1.34375-2.46875j), control1=(1.34375-3.34375j), control2=(1.453125-4.25j), end=(2.265625-4.8125j))
; 		 CubicBezier(start=(2.265625-4.8125j), control1=(2.515625-4.953125j), control2=(2.765625-5.015625j), end=(3.03125-5.015625j))
; 		 CubicBezier(start=(3.03125-5.015625j), control1=(3.640625-5.015625j), control2=(4.484375-4.546875j), end=(4.484375-3.796875j))
; 		 CubicBezier(start=(4.484375-3.796875j), control1=(4.484375-3.640625j), control2=(4.46875-3.484375j), end=(4.46875-3.328125j))
; 		 Line(start=(4.46875-3.328125j), end=(4.46875-1.34375j))
; 		 CubicBezier(start=(4.46875-1.34375j), control1=(4.46875-0.671875j), control2=(3.53125-0.125j), end=(2.9375-0.125j))
; 		 CubicBezier(start=(2.9375-0.125j), control1=(2.0625-0.125j), control2=(1.5-0.96875j), end=(1.390625-1.828125j))
; 		 CubicBezier(start=(1.390625-1.828125j), control1=(1.359375-1.984375j), control2=(1.359375-2.140625j), end=(1.34375-2.296875j))
; 		 Line(start=(1.34375-2.296875j), end=(1.34375-2.296875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(2.234375-4.046875j), end=(2.234375-4.03125j))
; 		 CubicBezier(start=(2.234375-4.03125j), control1=(1.390625-3.640625j), control2=(0.546875-2.9375j), end=(0.484375-1.9375j))
; 		 Line(start=(0.484375-1.9375j), end=(0.484375-1.8125j))
; 		 CubicBezier(start=(0.484375-1.8125j), control1=(0.484375-0.546875j), control2=(1.671875+0.203125j), end=(2.78125+0.234375j))
; 		 Line(start=(2.78125+0.234375j), end=(2.890625+0.234375j))
; 		 CubicBezier(start=(2.890625+0.234375j), control1=(4.09375+0.234375j), control2=(5.34375-0.59375j), end=(5.34375-2j))
; 		 CubicBezier(start=(5.34375-2j), control1=(5.34375-2.578125j), control2=(5.078125-3.15625j), end=(4.65625-3.5625j))
; 		 CubicBezier(start=(4.65625-3.5625j), control1=(4.484375-3.734375j), control2=(4.28125-3.84375j), end=(4.078125-4j))
; 		 CubicBezier(start=(4.078125-4j), control1=(3.90625-4.09375j), control2=(3.734375-4.234375j), end=(3.5625-4.3125j))
; 		 Line(start=(3.5625-4.3125j), end=(3.5625-4.34375j))
; 		 CubicBezier(start=(3.5625-4.34375j), control1=(4.25-4.609375j), control2=(4.953125-5.28125j), end=(5.015625-6.0625j))
; 		 Line(start=(5.015625-6.0625j), end=(5.015625-6.171875j))
; 		 CubicBezier(start=(5.015625-6.171875j), control1=(5.015625-7.09375j), control2=(4.21875-7.953125j), end=(2.953125-7.953125j))
; 		 CubicBezier(start=(2.953125-7.953125j), control1=(1.9375-7.953125j), control2=(0.8125-7.25j), end=(0.8125-6.03125j))
; 		 CubicBezier(start=(0.8125-6.03125j), control1=(0.8125-5.140625j), control2=(1.296875-4.609375j), end=(2.234375-4.046875j))
; 		 Line(start=(2.234375-4.046875j), end=(2.234375-4.046875j))
; 		 CubicBezier(start=(1.046875-1.703125j), control1=(1.046875-1.75j), control2=(1.03125-1.8125j), end=(1.03125-1.859375j))
; 		 CubicBezier(start=(1.03125-1.859375j), control1=(1.03125-2.6875j), control2=(1.65625-3.34375j), end=(2.3125-3.765625j))
; 		 CubicBezier(start=(2.3125-3.765625j), control1=(2.359375-3.796875j), control2=(2.421875-3.84375j), end=(2.484375-3.84375j))
; 		 CubicBezier(start=(2.484375-3.84375j), control1=(2.625-3.84375j), control2=(2.96875-3.546875j), end=(3.1875-3.40625j))
; 		 CubicBezier(start=(3.1875-3.40625j), control1=(3.78125-3j), control2=(4.796875-2.546875j), end=(4.796875-1.703125j))
; 		 CubicBezier(start=(4.796875-1.703125j), control1=(4.796875-0.671875j), control2=(3.84375-0.078125j), end=(2.9375-0.078125j))
; 		 CubicBezier(start=(2.9375-0.078125j), control1=(2.03125-0.078125j), control2=(1.15625-0.671875j), end=(1.046875-1.703125j))
; 		 Line(start=(1.046875-1.703125j), end=(1.046875-1.703125j))
; 		 CubicBezier(start=(1.296875-6.328125j), control1=(1.296875-7.203125j), control2=(2.109375-7.65625j), end=(2.890625-7.65625j))
; 		 CubicBezier(start=(2.890625-7.65625j), control1=(3.65625-7.65625j), control2=(4.546875-7.140625j), end=(4.546875-6.1875j))
; 		 CubicBezier(start=(4.546875-6.1875j), control1=(4.546875-5.5j), control2=(4-4.9375j), end=(3.484375-4.59375j))
; 		 CubicBezier(start=(3.484375-4.59375j), control1=(3.4375-4.5625j), control2=(3.375-4.515625j), end=(3.328125-4.515625j))
; 		 Line(start=(3.328125-4.515625j), end=(3.3125-4.515625j))
; 		 CubicBezier(start=(3.3125-4.515625j), control1=(3.1875-4.515625j), control2=(2.90625-4.75j), end=(2.71875-4.875j))
; 		 Line(start=(2.71875-4.875j), end=(2.125-5.265625j))
; 		 CubicBezier(start=(2.125-5.265625j), control1=(1.734375-5.515625j), control2=(1.359375-5.828125j), end=(1.296875-6.328125j))
; 		 Line(start=(1.296875-6.328125j), end=(1.296875-6.328125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(4.34375-8.421875j), control1=(2.34375-8.234375j), control2=(0.640625-6.53125j), end=(0.640625-4.0625j))
; 		 CubicBezier(start=(0.640625-4.0625j), control1=(0.640625-2.109375j), control2=(1.984375+0.234375j), end=(4.5625+0.234375j))
; 		 CubicBezier(start=(4.5625+0.234375j), control1=(6.890625+0.234375j), control2=(8.421875-1.921875j), end=(8.421875-4.078125j))
; 		 CubicBezier(start=(8.421875-4.078125j), control1=(8.421875-6.21875j), control2=(6.90625-8.4375j), end=(4.53125-8.4375j))
; 		 CubicBezier(start=(4.53125-8.4375j), control1=(4.453125-8.4375j), control2=(4.40625-8.421875j), end=(4.34375-8.421875j))
; 		 Line(start=(4.34375-8.421875j), end=(4.34375-8.421875j))
; 		 Line(start=(1.75-3.9375j), end=(1.75-4.140625j))
; 		 CubicBezier(start=(1.75-4.140625j), control1=(1.75-5.546875j), control2=(2.0625-7.09375j), end=(3.46875-7.890625j))
; 		 CubicBezier(start=(3.46875-7.890625j), control1=(3.796875-8.0625j), control2=(4.140625-8.15625j), end=(4.515625-8.15625j))
; 		 CubicBezier(start=(4.515625-8.15625j), control1=(5.578125-8.15625j), control2=(6.421875-7.453125j), end=(6.890625-6.484375j))
; 		 CubicBezier(start=(6.890625-6.484375j), control1=(7.21875-5.8125j), control2=(7.328125-5.015625j), end=(7.328125-4.296875j))
; 		 CubicBezier(start=(7.328125-4.296875j), control1=(7.328125-2.75j), control2=(6.90625-0.5625j), end=(5.03125-0.09375j))
; 		 CubicBezier(start=(5.03125-0.09375j), control1=(4.875-0.0625j), control2=(4.703125-0.03125j), end=(4.546875-0.03125j))
; 		 CubicBezier(start=(4.546875-0.03125j), control1=(2.875-0.03125j), control2=(2-1.8125j), end=(1.8125-3.234375j))
; 		 CubicBezier(start=(1.8125-3.234375j), control1=(1.796875-3.46875j), control2=(1.78125-3.703125j), end=(1.75-3.9375j))
; 		 Line(start=(1.75-3.9375j), end=(1.75-3.9375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.328125-8.171875j), end=(0.328125-7.828125j))
; 		 Line(start=(0.328125-7.828125j), end=(0.53125-7.828125j))
; 		 CubicBezier(start=(0.53125-7.828125j), control1=(0.890625-7.828125j), control2=(1.25-7.796875j), end=(1.25-7.234375j))
; 		 Line(start=(1.25-7.234375j), end=(1.25-0.015625j))
; 		 Line(start=(1.25-0.015625j), end=(1.5-0.03125j))
; 		 Line(start=(1.5-0.03125j), end=(1.640625-0.234375j))
; 		 Line(start=(1.640625-0.234375j), end=(1.921875-0.78125j))
; 		 CubicBezier(start=(1.921875-0.78125j), control1=(2.328125-0.28125j), control2=(2.796875+0.109375j), end=(3.5+0.109375j))
; 		 CubicBezier(start=(3.5+0.109375j), control1=(4.90625+0.109375j), control2=(6.078125-1.125j), end=(6.078125-2.59375j))
; 		 CubicBezier(start=(6.078125-2.59375j), control1=(6.078125-3.984375j), control2=(5.015625-5.265625j), end=(3.640625-5.265625j))
; 		 CubicBezier(start=(3.640625-5.265625j), control1=(3.03125-5.265625j), control2=(2.28125-4.984375j), end=(2.015625-4.46875j))
; 		 Line(start=(2.015625-4.46875j), end=(2-8.3125j))
; 		 Line(start=(2-8.3125j), end=(0.328125-8.171875j))
; 		 CubicBezier(start=(5.140625-2.3125j), control1=(5.09375-1.734375j), control2=(5.03125-1.21875j), end=(4.6875-0.78125j))
; 		 CubicBezier(start=(4.6875-0.78125j), control1=(4.375-0.375j), control2=(3.90625-0.125j), end=(3.4375-0.125j))
; 		 CubicBezier(start=(3.4375-0.125j), control1=(2.9375-0.125j), control2=(2.484375-0.421875j), end=(2.203125-0.84375j))
; 		 CubicBezier(start=(2.203125-0.84375j), control1=(2.109375-0.984375j), control2=(2.015625-1.109375j), end=(2.015625-1.296875j))
; 		 Line(start=(2.015625-1.296875j), end=(2.015625-3.28125j))
; 		 CubicBezier(start=(2.015625-3.28125j), control1=(2.015625-3.4375j), control2=(2.015625-3.59375j), end=(2.015625-3.75j))
; 		 CubicBezier(start=(2.015625-3.75j), control1=(2.015625-4.46875j), control2=(2.84375-5.015625j), end=(3.546875-5.015625j))
; 		 CubicBezier(start=(3.546875-5.015625j), control1=(4.859375-5.015625j), control2=(5.15625-3.5625j), end=(5.15625-2.5625j))
; 		 CubicBezier(start=(5.15625-2.5625j), control1=(5.15625-2.484375j), control2=(5.15625-2.390625j), end=(5.140625-2.3125j))
; 		 Line(start=(5.140625-2.3125j), end=(5.140625-2.3125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.328125-5.140625j), end=(0.328125-4.796875j))
; 		 Line(start=(0.328125-4.796875j), end=(0.53125-4.796875j))
; 		 CubicBezier(start=(0.53125-4.796875j), control1=(0.890625-4.796875j), control2=(1.25-4.75j), end=(1.25-4.203125j))
; 		 Line(start=(1.25-4.203125j), end=(1.25-0.859375j))
; 		 CubicBezier(start=(1.25-0.859375j), control1=(1.25-0.421875j), control2=(1.015625-0.359375j), end=(0.328125-0.359375j))
; 		 Line(start=(0.328125-0.359375j), end=(0.328125-0.015625j))
; 		 Line(start=(0.328125-0.015625j), end=(3.125-0.015625j))
; 		 Line(start=(3.125-0.015625j), end=(3.125-0.359375j))
; 		 CubicBezier(start=(3.125-0.359375j), control1=(3-0.359375j), control2=(2.859375-0.34375j), end=(2.71875-0.34375j))
; 		 CubicBezier(start=(2.71875-0.34375j), control1=(2.328125-0.34375j), control2=(2-0.40625j), end=(2-0.84375j))
; 		 Line(start=(2-0.84375j), end=(2-2.46875j))
; 		 CubicBezier(start=(2-2.46875j), control1=(2-3.390625j), control2=(2.140625-4.703125j), end=(3.125-5j))
; 		 CubicBezier(start=(3.125-5j), control1=(3.21875-5.015625j), control2=(3.296875-5.03125j), end=(3.390625-5.03125j))
; 		 CubicBezier(start=(3.390625-5.03125j), control1=(3.453125-5.03125j), control2=(3.5-5.015625j), end=(3.5625-5.015625j))
; 		 CubicBezier(start=(3.5625-5.015625j), control1=(3.375-4.9375j), control2=(3.28125-4.75j), end=(3.28125-4.5625j))
; 		 CubicBezier(start=(3.28125-4.5625j), control1=(3.28125-4.296875j), control2=(3.5-4.09375j), end=(3.734375-4.09375j))
; 		 CubicBezier(start=(3.734375-4.09375j), control1=(4.046875-4.09375j), control2=(4.21875-4.34375j), end=(4.21875-4.578125j))
; 		 CubicBezier(start=(4.21875-4.578125j), control1=(4.21875-5.015625j), control2=(3.796875-5.265625j), end=(3.390625-5.265625j))
; 		 CubicBezier(start=(3.390625-5.265625j), control1=(2.765625-5.265625j), control2=(2.296875-4.8125j), end=(2.0625-4.296875j))
; 		 CubicBezier(start=(2.0625-4.296875j), control1=(2.015625-4.1875j), control2=(1.953125-4.0625j), end=(1.953125-3.9375j))
; 		 Line(start=(1.953125-3.9375j), end=(1.9375-5.265625j))
; 		 Line(start=(1.9375-5.265625j), end=(0.328125-5.140625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(2.765625-7.953125j), control1=(0.484375-7.75j), control2=(0.484375-4.90625j), end=(0.484375-3.765625j))
; 		 CubicBezier(start=(0.484375-3.765625j), control1=(0.484375-2.125j), control2=(0.8125+0.234375j), end=(2.921875+0.234375j))
; 		 Line(start=(2.921875+0.234375j), end=(3.0625+0.234375j))
; 		 CubicBezier(start=(3.0625+0.234375j), control1=(5.328125+0.03125j), control2=(5.359375-2.75j), end=(5.359375-3.921875j))
; 		 CubicBezier(start=(5.359375-3.921875j), control1=(5.359375-5.546875j), control2=(5.015625-7.96875j), end=(2.921875-7.96875j))
; 		 CubicBezier(start=(2.921875-7.96875j), control1=(2.875-7.96875j), control2=(2.828125-7.953125j), end=(2.765625-7.953125j))
; 		 Line(start=(2.765625-7.953125j), end=(2.765625-7.953125j))
; 		 CubicBezier(start=(1.40625-3.125j), control1=(1.390625-3.21875j), control2=(1.390625-3.328125j), end=(1.390625-3.421875j))
; 		 CubicBezier(start=(1.390625-3.421875j), control1=(1.390625-4.15625j), control2=(1.390625-4.921875j), end=(1.453125-5.671875j))
; 		 CubicBezier(start=(1.453125-5.671875j), control1=(1.484375-6.140625j), control2=(1.546875-6.671875j), end=(1.8125-7.078125j))
; 		 CubicBezier(start=(1.8125-7.078125j), control1=(2.0625-7.453125j), control2=(2.484375-7.71875j), end=(2.921875-7.71875j))
; 		 CubicBezier(start=(2.921875-7.71875j), control1=(4.546875-7.71875j), control2=(4.453125-5.25j), end=(4.453125-4.125j))
; 		 Line(start=(4.453125-4.125j), end=(4.453125-3.234375j))
; 		 CubicBezier(start=(4.453125-3.234375j), control1=(4.453125-2.359375j), control2=(4.40625-1.40625j), end=(4.078125-0.78125j))
; 		 CubicBezier(start=(4.078125-0.78125j), control1=(3.859375-0.359375j), control2=(3.421875+0j), end=(2.921875+0j))
; 		 Line(start=(2.921875+0j), end=(2.84375+0j))
; 		 CubicBezier(start=(2.84375+0j), control1=(1.40625-0.125j), control2=(1.5-2.03125j), end=(1.40625-3.125j))
; 		 Line(start=(1.40625-3.125j), end=(1.40625-3.125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(3.71875-8.96875j), control1=(3.296875-8.8125j), control2=(2.515625-7.765625j), end=(2.140625-7.09375j))
; 		 CubicBezier(start=(2.140625-7.09375j), control1=(1.453125-5.84375j), control2=(1.1875-4.40625j), end=(1.1875-2.984375j))
; 		 CubicBezier(start=(1.1875-2.984375j), control1=(1.1875-0.78125j), control2=(1.859375+1.40625j), end=(3.578125+2.890625j))
; 		 CubicBezier(start=(3.578125+2.890625j), control1=(3.640625+2.921875j), control2=(3.71875+2.984375j), end=(3.796875+2.984375j))
; 		 CubicBezier(start=(3.796875+2.984375j), control1=(3.84375+2.984375j), control2=(3.890625+2.921875j), end=(3.890625+2.890625j))
; 		 CubicBezier(start=(3.890625+2.890625j), control1=(3.890625+2.765625j), control2=(3.546875+2.5j), end=(3.4375+2.34375j))
; 		 CubicBezier(start=(3.4375+2.34375j), control1=(2.234375+0.921875j), control2=(1.828125-1.046875j), end=(1.828125-2.890625j))
; 		 CubicBezier(start=(1.828125-2.890625j), control1=(1.828125-4.890625j), control2=(2.21875-7.140625j), end=(3.703125-8.640625j))
; 		 CubicBezier(start=(3.703125-8.640625j), control1=(3.78125-8.71875j), control2=(3.890625-8.796875j), end=(3.890625-8.875j))
; 		 CubicBezier(start=(3.890625-8.875j), control1=(3.890625-8.9375j), control2=(3.828125-8.984375j), end=(3.765625-8.984375j))
; 		 CubicBezier(start=(3.765625-8.984375j), control1=(3.734375-8.984375j), control2=(3.734375-8.984375j), end=(3.71875-8.96875j))
; 		 Line(start=(3.71875-8.96875j), end=(3.71875-8.96875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.484375-8.15625j), end=(0.484375-7.8125j))
; 		 Line(start=(0.484375-7.8125j), end=(0.84375-7.8125j))
; 		 CubicBezier(start=(0.84375-7.8125j), control1=(1.25-7.8125j), control2=(1.625-7.75j), end=(1.625-7.328125j))
; 		 CubicBezier(start=(1.625-7.328125j), control1=(1.625-7.25j), control2=(1.609375-7.171875j), end=(1.609375-7.09375j))
; 		 Line(start=(1.609375-7.09375j), end=(1.609375-0.890625j))
; 		 CubicBezier(start=(1.609375-0.890625j), control1=(1.609375-0.40625j), control2=(1.25-0.359375j), end=(0.84375-0.359375j))
; 		 Line(start=(0.84375-0.359375j), end=(0.484375-0.359375j))
; 		 Line(start=(0.484375-0.359375j), end=(0.484375-0.015625j))
; 		 Line(start=(0.484375-0.015625j), end=(7.140625-0.015625j))
; 		 Line(start=(7.140625-0.015625j), end=(7.625-3.078125j))
; 		 Line(start=(7.625-3.078125j), end=(7.359375-3.078125j))
; 		 CubicBezier(start=(7.359375-3.078125j), control1=(7.234375-2.34375j), control2=(7.15625-1.46875j), end=(6.640625-0.9375j))
; 		 CubicBezier(start=(6.640625-0.9375j), control1=(6.125-0.375j), control2=(5.28125-0.359375j), end=(4.5625-0.359375j))
; 		 Line(start=(4.5625-0.359375j), end=(3.078125-0.359375j))
; 		 CubicBezier(start=(3.078125-0.359375j), control1=(2.796875-0.359375j), control2=(2.578125-0.390625j), end=(2.578125-0.796875j))
; 		 Line(start=(2.578125-0.796875j), end=(2.578125-4.0625j))
; 		 Line(start=(2.578125-4.0625j), end=(3.703125-4.0625j))
; 		 CubicBezier(start=(3.703125-4.0625j), control1=(4.109375-4.0625j), control2=(4.546875-4.046875j), end=(4.78125-3.703125j))
; 		 CubicBezier(start=(4.78125-3.703125j), control1=(4.953125-3.4375j), control2=(4.953125-3.09375j), end=(4.953125-2.78125j))
; 		 Line(start=(4.953125-2.78125j), end=(4.953125-2.671875j))
; 		 Line(start=(4.953125-2.671875j), end=(5.21875-2.671875j))
; 		 Line(start=(5.21875-2.671875j), end=(5.21875-5.8125j))
; 		 Line(start=(5.21875-5.8125j), end=(4.953125-5.8125j))
; 		 Line(start=(4.953125-5.8125j), end=(4.953125-5.6875j))
; 		 CubicBezier(start=(4.953125-5.6875j), control1=(4.953125-5.078125j), control2=(4.859375-4.546875j), end=(4.09375-4.4375j))
; 		 CubicBezier(start=(4.09375-4.4375j), control1=(3.875-4.40625j), control2=(3.65625-4.40625j), end=(3.4375-4.40625j))
; 		 Line(start=(3.4375-4.40625j), end=(2.578125-4.40625j))
; 		 Line(start=(2.578125-4.40625j), end=(2.578125-7.40625j))
; 		 CubicBezier(start=(2.578125-7.40625j), control1=(2.578125-7.765625j), control2=(2.8125-7.8125j), end=(3.078125-7.8125j))
; 		 Line(start=(3.078125-7.8125j), end=(4.59375-7.8125j))
; 		 CubicBezier(start=(4.59375-7.8125j), control1=(5.234375-7.8125j), control2=(6.03125-7.796875j), end=(6.484375-7.28125j))
; 		 CubicBezier(start=(6.484375-7.28125j), control1=(6.921875-6.796875j), control2=(6.9375-6.09375j), end=(7.046875-5.484375j))
; 		 Line(start=(7.046875-5.484375j), end=(7.296875-5.484375j))
; 		 Line(start=(7.296875-5.484375j), end=(7-8.15625j))
; 		 Line(start=(7-8.15625j), end=(0.484375-8.15625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.171875-5.15625j), end=(0.171875-4.8125j))
; 		 Line(start=(0.171875-4.8125j), end=(0.40625-4.8125j))
; 		 CubicBezier(start=(0.40625-4.8125j), control1=(1.09375-4.8125j), control2=(1.25-4.46875j), end=(1.546875-4.09375j))
; 		 CubicBezier(start=(1.546875-4.09375j), control1=(1.828125-3.734375j), control2=(2.09375-3.375j), end=(2.359375-3.015625j))
; 		 CubicBezier(start=(2.359375-3.015625j), control1=(2.5-2.84375j), control2=(2.609375-2.65625j), end=(2.765625-2.515625j))
; 		 Line(start=(2.765625-2.515625j), end=(2.765625-2.5j))
; 		 CubicBezier(start=(2.765625-2.5j), control1=(2.625-2.375j), control2=(2.53125-2.21875j), end=(2.421875-2.09375j))
; 		 CubicBezier(start=(2.421875-2.09375j), control1=(1.828125-1.328125j), control2=(1.296875-0.359375j), end=(0.125-0.359375j))
; 		 Line(start=(0.125-0.359375j), end=(0.125-0.015625j))
; 		 Line(start=(0.125-0.015625j), end=(2.1875-0.015625j))
; 		 Line(start=(2.1875-0.015625j), end=(2.1875-0.359375j))
; 		 CubicBezier(start=(2.1875-0.359375j), control1=(1.953125-0.421875j), control2=(1.859375-0.53125j), end=(1.859375-0.734375j))
; 		 CubicBezier(start=(1.859375-0.734375j), control1=(1.859375-0.984375j), control2=(2.0625-1.1875j), end=(2.203125-1.375j))
; 		 CubicBezier(start=(2.203125-1.375j), control1=(2.4375-1.671875j), control2=(2.703125-1.953125j), end=(2.921875-2.28125j))
; 		 CubicBezier(start=(2.921875-2.28125j), control1=(3.21875-1.8125j), control2=(3.609375-1.328125j), end=(3.953125-0.921875j))
; 		 CubicBezier(start=(3.953125-0.921875j), control1=(4.015625-0.84375j), control2=(4.09375-0.75j), end=(4.09375-0.640625j))
; 		 CubicBezier(start=(4.09375-0.640625j), control1=(4.09375-0.46875j), control2=(3.84375-0.375j), end=(3.6875-0.359375j))
; 		 Line(start=(3.6875-0.359375j), end=(3.6875-0.015625j))
; 		 Line(start=(3.6875-0.015625j), end=(6.046875-0.015625j))
; 		 Line(start=(6.046875-0.015625j), end=(6.046875-0.359375j))
; 		 Line(start=(6.046875-0.359375j), end=(5.828125-0.359375j))
; 		 CubicBezier(start=(5.828125-0.359375j), control1=(5.25-0.359375j), control2=(5.0625-0.5625j), end=(4.609375-1.171875j))
; 		 CubicBezier(start=(4.609375-1.171875j), control1=(4.34375-1.515625j), control2=(4.09375-1.859375j), end=(3.828125-2.203125j))
; 		 CubicBezier(start=(3.828125-2.203125j), control1=(3.671875-2.40625j), control2=(3.53125-2.640625j), end=(3.34375-2.828125j))
; 		 CubicBezier(start=(3.34375-2.828125j), control1=(4.09375-3.578125j), control2=(4.484375-4.8125j), end=(5.859375-4.8125j))
; 		 Line(start=(5.859375-4.8125j), end=(5.859375-5.15625j))
; 		 Line(start=(5.859375-5.15625j), end=(3.78125-5.15625j))
; 		 Line(start=(3.78125-5.15625j), end=(3.78125-4.8125j))
; 		 CubicBezier(start=(3.78125-4.8125j), control1=(4-4.8125j), control2=(4.109375-4.609375j), end=(4.109375-4.4375j))
; 		 CubicBezier(start=(4.109375-4.4375j), control1=(4.109375-4.046875j), control2=(3.640625-3.640625j), end=(3.40625-3.328125j))
; 		 CubicBezier(start=(3.40625-3.328125j), control1=(3.328125-3.234375j), control2=(3.25-3.140625j), end=(3.1875-3.046875j))
; 		 Line(start=(3.1875-3.046875j), end=(3.171875-3.046875j))
; 		 CubicBezier(start=(3.171875-3.046875j), control1=(2.875-3.53125j), control2=(2.4375-3.9375j), end=(2.15625-4.421875j))
; 		 CubicBezier(start=(2.15625-4.421875j), control1=(2.140625-4.453125j), control2=(2.140625-4.484375j), end=(2.140625-4.5j))
; 		 CubicBezier(start=(2.140625-4.5j), control1=(2.140625-4.6875j), control2=(2.390625-4.796875j), end=(2.5625-4.8125j))
; 		 Line(start=(2.5625-4.8125j), end=(2.5625-5.15625j))
; 		 Line(start=(2.5625-5.15625j), end=(0.171875-5.15625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.328125-5.140625j), end=(0.328125-4.796875j))
; 		 Line(start=(0.328125-4.796875j), end=(0.546875-4.796875j))
; 		 CubicBezier(start=(0.546875-4.796875j), control1=(0.90625-4.796875j), control2=(1.25-4.75j), end=(1.25-4.25j))
; 		 Line(start=(1.25-4.25j), end=(1.25+1.25j))
; 		 CubicBezier(start=(1.25+1.25j), control1=(1.25+1.3125j), control2=(1.25+1.40625j), end=(1.25+1.484375j))
; 		 CubicBezier(start=(1.25+1.484375j), control1=(1.25+1.9375j), control2=(0.890625+1.953125j), end=(0.5+1.953125j))
; 		 Line(start=(0.5+1.953125j), end=(0.328125+1.953125j))
; 		 Line(start=(0.328125+1.953125j), end=(0.328125+2.3125j))
; 		 Line(start=(0.328125+2.3125j), end=(2.921875+2.3125j))
; 		 Line(start=(2.921875+2.3125j), end=(2.921875+1.953125j))
; 		 Line(start=(2.921875+1.953125j), end=(2.671875+1.953125j))
; 		 CubicBezier(start=(2.671875+1.953125j), control1=(2.328125+1.953125j), control2=(2.015625+1.90625j), end=(2.015625+1.484375j))
; 		 Line(start=(2.015625+1.484375j), end=(2.03125-0.65625j))
; 		 CubicBezier(start=(2.03125-0.65625j), control1=(2.328125-0.15625j), control2=(2.984375+0.109375j), end=(3.53125+0.109375j))
; 		 CubicBezier(start=(3.53125+0.109375j), control1=(4.984375+0.109375j), control2=(6.078125-1.1875j), end=(6.078125-2.609375j))
; 		 CubicBezier(start=(6.078125-2.609375j), control1=(6.078125-3.9375j), control2=(5.0625-5.265625j), end=(3.625-5.265625j))
; 		 CubicBezier(start=(3.625-5.265625j), control1=(2.96875-5.265625j), control2=(2.421875-4.953125j), end=(2-4.46875j))
; 		 Line(start=(2-4.46875j), end=(2-5.265625j))
; 		 Line(start=(2-5.265625j), end=(0.328125-5.140625j))
; 		 CubicBezier(start=(5.140625-2.390625j), control1=(5.078125-1.59375j), control2=(4.796875-0.6875j), end=(4-0.265625j))
; 		 CubicBezier(start=(4-0.265625j), control1=(3.8125-0.171875j), control2=(3.625-0.125j), end=(3.421875-0.125j))
; 		 CubicBezier(start=(3.421875-0.125j), control1=(2.859375-0.125j), control2=(2.015625-0.6875j), end=(2.015625-1.296875j))
; 		 Line(start=(2.015625-1.296875j), end=(2.015625-3.28125j))
; 		 CubicBezier(start=(2.015625-3.28125j), control1=(2.015625-3.4375j), control2=(2.015625-3.578125j), end=(2.015625-3.734375j))
; 		 CubicBezier(start=(2.015625-3.734375j), control1=(2.015625-4.40625j), control2=(2.828125-5.015625j), end=(3.53125-5.015625j))
; 		 CubicBezier(start=(3.53125-5.015625j), control1=(4.734375-5.015625j), control2=(5.15625-3.4375j), end=(5.15625-2.609375j))
; 		 CubicBezier(start=(5.15625-2.609375j), control1=(5.15625-2.53125j), control2=(5.15625-2.46875j), end=(5.140625-2.390625j))
; 		 Line(start=(5.140625-2.390625j), end=(5.140625-2.390625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(0.703125-8.96875j), control1=(0.671875-8.953125j), control2=(0.65625-8.921875j), end=(0.65625-8.890625j))
; 		 CubicBezier(start=(0.65625-8.890625j), control1=(0.65625-8.734375j), control2=(0.96875-8.515625j), end=(1.09375-8.359375j))
; 		 CubicBezier(start=(1.09375-8.359375j), control1=(2.328125-6.859375j), control2=(2.71875-4.84375j), end=(2.71875-2.921875j))
; 		 CubicBezier(start=(2.71875-2.921875j), control1=(2.71875-1.109375j), control2=(2.328125+0.84375j), end=(1.171875+2.28125j))
; 		 Line(start=(1.171875+2.28125j), end=(0.890625+2.609375j))
; 		 CubicBezier(start=(0.890625+2.609375j), control1=(0.796875+2.6875j), control2=(0.65625+2.78125j), end=(0.65625+2.890625j))
; 		 CubicBezier(start=(0.65625+2.890625j), control1=(0.65625+2.9375j), control2=(0.703125+2.96875j), end=(0.765625+2.96875j))
; 		 Line(start=(0.765625+2.96875j), end=(0.796875+2.96875j))
; 		 CubicBezier(start=(0.796875+2.96875j), control1=(0.9375+2.953125j), control2=(1.15625+2.71875j), end=(1.3125+2.5625j))
; 		 CubicBezier(start=(1.3125+2.5625j), control1=(2.765625+1.09375j), control2=(3.359375-0.953125j), end=(3.359375-2.96875j))
; 		 CubicBezier(start=(3.359375-2.96875j), control1=(3.359375-5.171875j), control2=(2.6875-7.40625j), end=(0.9375-8.875j))
; 		 CubicBezier(start=(0.9375-8.875j), control1=(0.890625-8.921875j), control2=(0.84375-8.984375j), end=(0.765625-8.984375j))
; 		 CubicBezier(start=(0.765625-8.984375j), control1=(0.734375-8.984375j), control2=(0.734375-8.984375j), end=(0.703125-8.96875j))
; 		 Line(start=(0.703125-8.96875j), end=(0.703125-8.96875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.34375-6.8125j), end=(0.34375-6.515625j))
; 		 CubicBezier(start=(0.34375-6.515625j), control1=(0.46875-6.515625j), control2=(0.59375-6.515625j), end=(0.71875-6.515625j))
; 		 CubicBezier(start=(0.71875-6.515625j), control1=(1.078125-6.515625j), control2=(1.359375-6.46875j), end=(1.359375-6.078125j))
; 		 Line(start=(1.359375-6.078125j), end=(1.359375-0.75j))
; 		 CubicBezier(start=(1.359375-0.75j), control1=(1.359375-0.375j), control2=(1.03125-0.3125j), end=(0.703125-0.3125j))
; 		 CubicBezier(start=(0.703125-0.3125j), control1=(0.5625-0.3125j), control2=(0.453125-0.3125j), end=(0.34375-0.3125j))
; 		 Line(start=(0.34375-0.3125j), end=(0.34375-0.015625j))
; 		 Line(start=(0.34375-0.015625j), end=(3.28125-0.015625j))
; 		 Line(start=(3.28125-0.015625j), end=(3.28125-0.3125j))
; 		 Line(start=(3.28125-0.3125j), end=(2.9375-0.3125j))
; 		 CubicBezier(start=(2.9375-0.3125j), control1=(2.515625-0.3125j), control2=(2.25-0.34375j), end=(2.25-0.75j))
; 		 Line(start=(2.25-0.75j), end=(2.25-3.15625j))
; 		 Line(start=(2.25-3.15625j), end=(4.09375-3.15625j))
; 		 CubicBezier(start=(4.09375-3.15625j), control1=(5.03125-3.234375j), control2=(6.21875-3.8125j), end=(6.21875-4.953125j))
; 		 CubicBezier(start=(6.21875-4.953125j), control1=(6.21875-6.015625j), control2=(5.171875-6.703125j), end=(4.171875-6.8125j))
; 		 CubicBezier(start=(4.171875-6.8125j), control1=(4.015625-6.8125j), control2=(3.875-6.828125j), end=(3.734375-6.828125j))
; 		 CubicBezier(start=(3.734375-6.828125j), control1=(3.53125-6.828125j), control2=(3.328125-6.8125j), end=(3.140625-6.8125j))
; 		 Line(start=(3.140625-6.8125j), end=(0.34375-6.8125j))
; 		 Line(start=(2.21875-3.421875j), end=(2.21875-6.1875j))
; 		 CubicBezier(start=(2.21875-6.1875j), control1=(2.21875-6.453125j), control2=(2.390625-6.484375j), end=(2.578125-6.515625j))
; 		 Line(start=(2.578125-6.515625j), end=(3.328125-6.515625j))
; 		 CubicBezier(start=(3.328125-6.515625j), control1=(3.890625-6.515625j), control2=(4.53125-6.484375j), end=(4.921875-6.015625j))
; 		 CubicBezier(start=(4.921875-6.015625j), control1=(5.15625-5.734375j), control2=(5.1875-5.328125j), end=(5.1875-4.96875j))
; 		 CubicBezier(start=(5.1875-4.96875j), control1=(5.1875-4.609375j), control2=(5.15625-4.234375j), end=(4.9375-3.9375j))
; 		 CubicBezier(start=(4.9375-3.9375j), control1=(4.609375-3.515625j), control2=(4.03125-3.421875j), end=(3.515625-3.421875j))
; 		 Line(start=(3.515625-3.421875j), end=(2.21875-3.421875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.328125-6.8125j), end=(0.328125-6.515625j))
; 		 CubicBezier(start=(0.328125-6.515625j), control1=(0.453125-6.515625j), control2=(0.59375-6.515625j), end=(0.703125-6.515625j))
; 		 CubicBezier(start=(0.703125-6.515625j), control1=(1.0625-6.515625j), control2=(1.359375-6.46875j), end=(1.359375-6.078125j))
; 		 Line(start=(1.359375-6.078125j), end=(1.359375-0.75j))
; 		 CubicBezier(start=(1.359375-0.75j), control1=(1.359375-0.375j), control2=(1.03125-0.3125j), end=(0.6875-0.3125j))
; 		 CubicBezier(start=(0.6875-0.3125j), control1=(0.5625-0.3125j), control2=(0.4375-0.3125j), end=(0.328125-0.3125j))
; 		 Line(start=(0.328125-0.3125j), end=(0.328125-0.015625j))
; 		 Line(start=(0.328125-0.015625j), end=(3.265625-0.015625j))
; 		 Line(start=(3.265625-0.015625j), end=(3.265625-0.3125j))
; 		 Line(start=(3.265625-0.3125j), end=(2.9375-0.3125j))
; 		 CubicBezier(start=(2.9375-0.3125j), control1=(2.515625-0.3125j), control2=(2.25-0.34375j), end=(2.25-0.75j))
; 		 Line(start=(2.25-0.75j), end=(2.25-3.40625j))
; 		 Line(start=(2.25-3.40625j), end=(5.21875-3.40625j))
; 		 Line(start=(5.21875-3.40625j), end=(5.21875-0.75j))
; 		 CubicBezier(start=(5.21875-0.75j), control1=(5.21875-0.359375j), control2=(4.890625-0.3125j), end=(4.53125-0.3125j))
; 		 Line(start=(4.53125-0.3125j), end=(4.203125-0.3125j))
; 		 Line(start=(4.203125-0.3125j), end=(4.203125-0.015625j))
; 		 Line(start=(4.203125-0.015625j), end=(7.140625-0.015625j))
; 		 Line(start=(7.140625-0.015625j), end=(7.140625-0.3125j))
; 		 CubicBezier(start=(7.140625-0.3125j), control1=(7.015625-0.3125j), control2=(6.890625-0.3125j), end=(6.765625-0.3125j))
; 		 CubicBezier(start=(6.765625-0.3125j), control1=(6.421875-0.3125j), control2=(6.109375-0.359375j), end=(6.109375-0.75j))
; 		 Line(start=(6.109375-0.75j), end=(6.109375-6.078125j))
; 		 CubicBezier(start=(6.109375-6.078125j), control1=(6.109375-6.453125j), control2=(6.4375-6.515625j), end=(6.765625-6.515625j))
; 		 CubicBezier(start=(6.765625-6.515625j), control1=(6.890625-6.515625j), control2=(7.03125-6.515625j), end=(7.140625-6.515625j))
; 		 Line(start=(7.140625-6.515625j), end=(7.140625-6.8125j))
; 		 Line(start=(7.140625-6.8125j), end=(4.203125-6.8125j))
; 		 Line(start=(4.203125-6.8125j), end=(4.203125-6.515625j))
; 		 Line(start=(4.203125-6.515625j), end=(4.53125-6.515625j))
; 		 CubicBezier(start=(4.53125-6.515625j), control1=(4.953125-6.515625j), control2=(5.21875-6.484375j), end=(5.21875-6.078125j))
; 		 Line(start=(5.21875-6.078125j), end=(5.21875-3.703125j))
; 		 Line(start=(5.21875-3.703125j), end=(2.25-3.703125j))
; 		 Line(start=(2.25-3.703125j), end=(2.25-6.078125j))
; 		 CubicBezier(start=(2.25-6.078125j), control1=(2.25-6.46875j), control2=(2.578125-6.515625j), end=(2.9375-6.515625j))
; 		 Line(start=(2.9375-6.515625j), end=(3.265625-6.515625j))
; 		 Line(start=(3.265625-6.515625j), end=(3.265625-6.8125j))
; 		 Line(start=(3.265625-6.8125j), end=(0.328125-6.8125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.109375-6.8125j), end=(0.109375-6.515625j))
; 		 Line(start=(0.109375-6.515625j), end=(0.328125-6.515625j))
; 		 CubicBezier(start=(0.328125-6.515625j), control1=(0.78125-6.515625j), control2=(1.015625-6.453125j), end=(1.25-6.0625j))
; 		 Line(start=(1.25-6.0625j), end=(1.53125-5.625j))
; 		 CubicBezier(start=(1.53125-5.625j), control1=(2.046875-4.765625j), control2=(2.5625-3.921875j), end=(3.09375-3.078125j))
; 		 CubicBezier(start=(3.09375-3.078125j), control1=(3.171875-2.953125j), control2=(3.296875-2.796875j), end=(3.296875-2.640625j))
; 		 Line(start=(3.296875-2.640625j), end=(3.296875-1j))
; 		 CubicBezier(start=(3.296875-1j), control1=(3.296875-0.9375j), control2=(3.3125-0.859375j), end=(3.3125-0.765625j))
; 		 CubicBezier(start=(3.3125-0.765625j), control1=(3.3125-0.359375j), control2=(2.953125-0.3125j), end=(2.578125-0.3125j))
; 		 Line(start=(2.578125-0.3125j), end=(2.265625-0.3125j))
; 		 Line(start=(2.265625-0.3125j), end=(2.265625-0.015625j))
; 		 Line(start=(2.265625-0.015625j), end=(5.1875-0.015625j))
; 		 Line(start=(5.1875-0.015625j), end=(5.1875-0.3125j))
; 		 Line(start=(5.1875-0.3125j), end=(4.875-0.3125j))
; 		 CubicBezier(start=(4.875-0.3125j), control1=(4.421875-0.3125j), control2=(4.15625-0.34375j), end=(4.15625-0.78125j))
; 		 Line(start=(4.15625-0.78125j), end=(4.15625-2.390625j))
; 		 CubicBezier(start=(4.15625-2.390625j), control1=(4.15625-2.453125j), control2=(4.140625-2.515625j), end=(4.140625-2.578125j))
; 		 CubicBezier(start=(4.140625-2.578125j), control1=(4.140625-2.765625j), control2=(4.28125-2.90625j), end=(4.359375-3.078125j))
; 		 CubicBezier(start=(4.359375-3.078125j), control1=(4.921875-3.9375j), control2=(5.46875-4.828125j), end=(6-5.71875j))
; 		 CubicBezier(start=(6-5.71875j), control1=(6.34375-6.3125j), control2=(6.78125-6.5j), end=(7.359375-6.515625j))
; 		 Line(start=(7.359375-6.515625j), end=(7.359375-6.8125j))
; 		 Line(start=(7.359375-6.8125j), end=(5.234375-6.8125j))
; 		 Line(start=(5.234375-6.8125j), end=(5.234375-6.515625j))
; 		 CubicBezier(start=(5.234375-6.515625j), control1=(5.5-6.515625j), control2=(5.796875-6.421875j), end=(5.796875-6.078125j))
; 		 CubicBezier(start=(5.796875-6.078125j), control1=(5.765625-5.9375j), control2=(5.6875-5.8125j), end=(5.609375-5.6875j))
; 		 Line(start=(5.609375-5.6875j), end=(5.359375-5.296875j))
; 		 CubicBezier(start=(5.359375-5.296875j), control1=(5.078125-4.828125j), control2=(4.78125-4.34375j), end=(4.5-3.875j))
; 		 Line(start=(4.5-3.875j), end=(4.21875-3.421875j))
; 		 CubicBezier(start=(4.21875-3.421875j), control1=(4.171875-3.34375j), control2=(4.09375-3.25j), end=(4.0625-3.15625j))
; 		 Line(start=(4.0625-3.15625j), end=(4.046875-3.15625j))
; 		 CubicBezier(start=(4.046875-3.15625j), control1=(4-3.25j), control2=(3.953125-3.328125j), end=(3.890625-3.421875j))
; 		 Line(start=(3.890625-3.421875j), end=(3.59375-3.921875j))
; 		 CubicBezier(start=(3.59375-3.921875j), control1=(3.15625-4.625j), control2=(2.6875-5.3125j), end=(2.296875-6.03125j))
; 		 CubicBezier(start=(2.296875-6.03125j), control1=(2.25-6.09375j), control2=(2.1875-6.1875j), end=(2.1875-6.265625j))
; 		 Line(start=(2.1875-6.265625j), end=(2.1875-6.296875j))
; 		 CubicBezier(start=(2.1875-6.296875j), control1=(2.234375-6.5j), control2=(2.578125-6.515625j), end=(2.8125-6.515625j))
; 		 Line(start=(2.8125-6.515625j), end=(2.875-6.515625j))
; 		 Line(start=(2.875-6.515625j), end=(2.875-6.8125j))
; 		 Line(start=(2.875-6.8125j), end=(0.109375-6.8125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.15625-0.484375j), control1=(1.78125+0j), control2=(2.265625+0.203125j), end=(3.046875+0.203125j))
; 		 CubicBezier(start=(3.046875+0.203125j), control1=(4.234375+0.203125j), control2=(4.96875-0.859375j), end=(4.96875-1.875j))
; 		 CubicBezier(start=(4.96875-1.875j), control1=(4.96875-2.703125j), control2=(4.5-3.46875j), end=(3.703125-3.796875j))
; 		 CubicBezier(start=(3.703125-3.796875j), control1=(3.09375-4.046875j), control2=(2.34375-4.03125j), end=(1.8125-4.34375j))
; 		 CubicBezier(start=(1.8125-4.34375j), control1=(1.421875-4.59375j), control2=(1.1875-5.03125j), end=(1.1875-5.46875j))
; 		 CubicBezier(start=(1.1875-5.46875j), control1=(1.1875-6.25j), control2=(1.859375-6.765625j), end=(2.5625-6.765625j))
; 		 CubicBezier(start=(2.5625-6.765625j), control1=(3.546875-6.765625j), control2=(4.234375-5.96875j), end=(4.390625-4.96875j))
; 		 CubicBezier(start=(4.390625-4.96875j), control1=(4.421875-4.828125j), control2=(4.390625-4.5625j), end=(4.578125-4.5625j))
; 		 CubicBezier(start=(4.578125-4.5625j), control1=(4.6875-4.5625j), control2=(4.6875-4.640625j), end=(4.6875-4.734375j))
; 		 Line(start=(4.6875-4.734375j), end=(4.6875-6.890625j))
; 		 CubicBezier(start=(4.6875-6.890625j), control1=(4.6875-6.96875j), control2=(4.671875-7.03125j), end=(4.59375-7.03125j))
; 		 CubicBezier(start=(4.59375-7.03125j), control1=(4.453125-7.03125j), control2=(4.34375-6.765625j), end=(4.25-6.625j))
; 		 CubicBezier(start=(4.25-6.625j), control1=(4.203125-6.546875j), control2=(4.125-6.4375j), end=(4.09375-6.34375j))
; 		 CubicBezier(start=(4.09375-6.34375j), control1=(3.671875-6.75j), control2=(3.203125-7.03125j), end=(2.546875-7.03125j))
; 		 CubicBezier(start=(2.546875-7.03125j), control1=(1.5625-7.03125j), control2=(0.546875-6.359375j), end=(0.546875-5.09375j))
; 		 CubicBezier(start=(0.546875-5.09375j), control1=(0.546875-4.0625j), control2=(1.3125-3.375j), end=(2.28125-3.171875j))
; 		 CubicBezier(start=(2.28125-3.171875j), control1=(2.5-3.125j), control2=(2.6875-3.0625j), end=(2.90625-3.015625j))
; 		 CubicBezier(start=(2.90625-3.015625j), control1=(3.453125-2.875j), control2=(4.015625-2.734375j), end=(4.25-2.03125j))
; 		 CubicBezier(start=(4.25-2.03125j), control1=(4.3125-1.875j), control2=(4.34375-1.71875j), end=(4.34375-1.5625j))
; 		 CubicBezier(start=(4.34375-1.5625j), control1=(4.34375-0.8125j), control2=(3.828125-0.09375j), end=(2.984375-0.09375j))
; 		 CubicBezier(start=(2.984375-0.09375j), control1=(2-0.09375j), control2=(1.03125-0.625j), end=(0.828125-1.78125j))
; 		 CubicBezier(start=(0.828125-1.78125j), control1=(0.8125-1.859375j), control2=(0.8125-1.96875j), end=(0.796875-2.046875j))
; 		 Line(start=(0.796875-2.046875j), end=(0.796875-2.125j))
; 		 CubicBezier(start=(0.796875-2.125j), control1=(0.796875-2.21875j), control2=(0.75-2.265625j), end=(0.671875-2.265625j))
; 		 CubicBezier(start=(0.671875-2.265625j), control1=(0.5625-2.265625j), control2=(0.546875-2.1875j), end=(0.546875-2.125j))
; 		 Line(start=(0.546875-2.125j), end=(0.546875+0.0625j))
; 		 CubicBezier(start=(0.546875+0.0625j), control1=(0.546875+0.140625j), control2=(0.5625+0.203125j), end=(0.640625+0.203125j))
; 		 CubicBezier(start=(0.640625+0.203125j), control1=(0.796875+0.203125j), control2=(0.90625-0.0625j), end=(0.984375-0.203125j))
; 		 CubicBezier(start=(0.984375-0.203125j), control1=(1.046875-0.296875j), control2=(1.109375-0.390625j), end=(1.15625-0.484375j))
; 		 Line(start=(1.15625-0.484375j), end=(1.15625-0.484375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.890625-6.015625j), end=(0.890625-5.703125j))
; 		 CubicBezier(start=(0.890625-5.703125j), control1=(1.328125-5.703125j), control2=(1.796875-5.75j), end=(2.1875-5.96875j))
; 		 Line(start=(2.1875-5.96875j), end=(2.1875-0.75j))
; 		 CubicBezier(start=(2.1875-0.75j), control1=(2.1875-0.375j), control2=(1.8125-0.3125j), end=(1.421875-0.3125j))
; 		 CubicBezier(start=(1.421875-0.3125j), control1=(1.25-0.3125j), control2=(1.09375-0.3125j), end=(0.953125-0.3125j))
; 		 Line(start=(0.953125-0.3125j), end=(0.953125-0.015625j))
; 		 Line(start=(0.953125-0.015625j), end=(4.1875-0.015625j))
; 		 Line(start=(4.1875-0.015625j), end=(4.1875-0.3125j))
; 		 CubicBezier(start=(4.1875-0.3125j), control1=(4.015625-0.3125j), control2=(3.84375-0.3125j), end=(3.6875-0.3125j))
; 		 CubicBezier(start=(3.6875-0.3125j), control1=(3.296875-0.3125j), control2=(2.9375-0.375j), end=(2.9375-0.75j))
; 		 Line(start=(2.9375-0.75j), end=(2.9375-6.484375j))
; 		 CubicBezier(start=(2.9375-6.484375j), control1=(2.9375-6.578125j), control2=(2.90625-6.65625j), end=(2.796875-6.65625j))
; 		 CubicBezier(start=(2.796875-6.65625j), control1=(2.65625-6.65625j), control2=(2.5-6.453125j), end=(2.375-6.375j))
; 		 CubicBezier(start=(2.375-6.375j), control1=(1.9375-6.09375j), control2=(1.40625-6.015625j), end=(0.890625-6.015625j))
; 		 Line(start=(0.890625-6.015625j), end=(0.890625-6.015625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(2.359375-6.65625j), control1=(0.421875-6.484375j), control2=(0.390625-4.125j), end=(0.390625-3.15625j))
; 		 CubicBezier(start=(0.390625-3.15625j), control1=(0.390625-2.21875j), control2=(0.46875-1.140625j), end=(1.09375-0.40625j))
; 		 CubicBezier(start=(1.09375-0.40625j), control1=(1.421875-0.015625j), control2=(1.96875+0.21875j), end=(2.46875+0.21875j))
; 		 CubicBezier(start=(2.46875+0.21875j), control1=(3.3125+0.21875j), control2=(3.984375-0.328125j), end=(4.28125-1.09375j))
; 		 CubicBezier(start=(4.28125-1.09375j), control1=(4.5625-1.78125j), control2=(4.578125-2.546875j), end=(4.578125-3.265625j))
; 		 CubicBezier(start=(4.578125-3.265625j), control1=(4.578125-4.203125j), control2=(4.484375-5.34375j), end=(3.875-6.03125j))
; 		 CubicBezier(start=(3.875-6.03125j), control1=(3.53125-6.421875j), control2=(3-6.65625j), end=(2.5-6.65625j))
; 		 CubicBezier(start=(2.5-6.65625j), control1=(2.4375-6.65625j), control2=(2.40625-6.65625j), end=(2.359375-6.65625j))
; 		 Line(start=(2.359375-6.65625j), end=(2.359375-6.65625j))
; 		 CubicBezier(start=(1.21875-2.578125j), control1=(1.21875-2.671875j), control2=(1.21875-2.75j), end=(1.21875-2.84375j))
; 		 Line(start=(1.21875-2.84375j), end=(1.21875-3.3125j))
; 		 CubicBezier(start=(1.21875-3.3125j), control1=(1.21875-3.796875j), control2=(1.21875-4.265625j), end=(1.265625-4.75j))
; 		 CubicBezier(start=(1.265625-4.75j), control1=(1.296875-5.125j), control2=(1.328125-5.546875j), end=(1.546875-5.875j))
; 		 CubicBezier(start=(1.546875-5.875j), control1=(1.765625-6.203125j), control2=(2.109375-6.4375j), end=(2.5-6.4375j))
; 		 CubicBezier(start=(2.5-6.4375j), control1=(3.671875-6.4375j), control2=(3.75-4.96875j), end=(3.75-3.984375j))
; 		 Line(start=(3.75-3.984375j), end=(3.75-3.515625j))
; 		 CubicBezier(start=(3.75-3.515625j), control1=(3.75-3.25j), control2=(3.765625-2.953125j), end=(3.765625-2.65625j))
; 		 CubicBezier(start=(3.765625-2.65625j), control1=(3.765625-1.390625j), control2=(3.59375-0.015625j), end=(2.5-0.015625j))
; 		 CubicBezier(start=(2.5-0.015625j), control1=(1.15625-0.015625j), control2=(1.28125-1.9375j), end=(1.21875-2.578125j))
; 		 Line(start=(1.21875-2.578125j), end=(1.21875-2.578125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(0.890625-5.328125j), control1=(1.125-5.828125j), control2=(1.5-6.34375j), end=(2.21875-6.34375j))
; 		 CubicBezier(start=(2.21875-6.34375j), control1=(3.15625-6.34375j), control2=(3.5625-5.484375j), end=(3.5625-4.703125j))
; 		 CubicBezier(start=(3.5625-4.703125j), control1=(3.5625-3.78125j), control2=(2.953125-3.03125j), end=(2.375-2.375j))
; 		 CubicBezier(start=(2.375-2.375j), control1=(1.8125-1.703125j), control2=(1.203125-1.0625j), end=(0.625-0.40625j))
; 		 CubicBezier(start=(0.625-0.40625j), control1=(0.53125-0.3125j), control2=(0.5-0.265625j), end=(0.5-0.125j))
; 		 Line(start=(0.5-0.125j), end=(0.5-0.015625j))
; 		 Line(start=(0.5-0.015625j), end=(4.203125-0.015625j))
; 		 Line(start=(4.203125-0.015625j), end=(4.46875-1.75j))
; 		 Line(start=(4.46875-1.75j), end=(4.234375-1.75j))
; 		 CubicBezier(start=(4.234375-1.75j), control1=(4.140625-1.296875j), control2=(4.203125-0.796875j), end=(3.6875-0.796875j))
; 		 Line(start=(3.6875-0.796875j), end=(3.296875-0.796875j))
; 		 CubicBezier(start=(3.296875-0.796875j), control1=(3-0.78125j), control2=(2.71875-0.78125j), end=(2.421875-0.78125j))
; 		 CubicBezier(start=(2.421875-0.78125j), control1=(2.03125-0.78125j), control2=(1.65625-0.78125j), end=(1.28125-0.78125j))
; 		 Line(start=(1.28125-0.78125j), end=(1.96875-1.453125j))
; 		 CubicBezier(start=(1.96875-1.453125j), control1=(2.25-1.71875j), control2=(2.515625-1.984375j), end=(2.796875-2.234375j))
; 		 CubicBezier(start=(2.796875-2.234375j), control1=(3.53125-2.890625j), control2=(4.46875-3.609375j), end=(4.46875-4.71875j))
; 		 CubicBezier(start=(4.46875-4.71875j), control1=(4.46875-5.84375j), control2=(3.515625-6.65625j), end=(2.390625-6.65625j))
; 		 CubicBezier(start=(2.390625-6.65625j), control1=(1.4375-6.65625j), control2=(0.5-5.9375j), end=(0.5-4.859375j))
; 		 CubicBezier(start=(0.5-4.859375j), control1=(0.5-4.609375j), control2=(0.59375-4.296875j), end=(1.015625-4.296875j))
; 		 CubicBezier(start=(1.015625-4.296875j), control1=(1.3125-4.296875j), control2=(1.546875-4.5j), end=(1.546875-4.8125j))
; 		 CubicBezier(start=(1.546875-4.8125j), control1=(1.546875-5.078125j), control2=(1.34375-5.328125j), end=(1.03125-5.328125j))
; 		 CubicBezier(start=(1.03125-5.328125j), control1=(0.984375-5.328125j), control2=(0.9375-5.328125j), end=(0.890625-5.328125j))
; 		 Line(start=(0.890625-5.328125j), end=(0.890625-5.328125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.78125-0.1875j), control1=(1.796875-0.140625j), control2=(1.8125-0.078125j), end=(1.8125-0.015625j))
; 		 CubicBezier(start=(1.8125-0.015625j), control1=(1.8125+0.65625j), control2=(1.5+1.234375j), end=(1.0625+1.71875j))
; 		 CubicBezier(start=(1.0625+1.71875j), control1=(1.03125+1.75j), control2=(1.015625+1.78125j), end=(1.015625+1.8125j))
; 		 CubicBezier(start=(1.015625+1.8125j), control1=(1.015625+1.859375j), control2=(1.0625+1.90625j), end=(1.109375+1.90625j))
; 		 CubicBezier(start=(1.109375+1.90625j), control1=(1.25+1.90625j), control2=(1.53125+1.53125j), end=(1.65625+1.3125j))
; 		 CubicBezier(start=(1.65625+1.3125j), control1=(1.890625+0.90625j), control2=(2.03125+0.4375j), end=(2.03125-0.03125j))
; 		 CubicBezier(start=(2.03125-0.03125j), control1=(2.03125-0.484375j), control2=(1.890625-1.0625j), end=(1.375-1.0625j))
; 		 CubicBezier(start=(1.375-1.0625j), control1=(1.078125-1.0625j), control2=(0.859375-0.84375j), end=(0.859375-0.53125j))
; 		 CubicBezier(start=(0.859375-0.53125j), control1=(0.859375-0.203125j), control2=(1.109375-0.015625j), end=(1.390625-0.015625j))
; 		 CubicBezier(start=(1.390625-0.015625j), control1=(1.53125-0.015625j), control2=(1.6875-0.0625j), end=(1.78125-0.1875j))
; 		 Line(start=(1.78125-0.1875j), end=(1.78125-0.1875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(3.65625-3.15625j), control1=(3.65625-2.296875j), control2=(3.65625-1.328125j), end=(3.09375-0.625j))
; 		 CubicBezier(start=(3.09375-0.625j), control1=(2.8125-0.296875j), control2=(2.46875-0.0625j), end=(2.03125-0.0625j))
; 		 CubicBezier(start=(2.03125-0.0625j), control1=(1.6875-0.0625j), control2=(1.296875-0.15625j), end=(1.0625-0.421875j))
; 		 CubicBezier(start=(1.0625-0.421875j), control1=(1.328125-0.421875j), control2=(1.578125-0.5625j), end=(1.578125-0.875j))
; 		 CubicBezier(start=(1.578125-0.875j), control1=(1.578125-1.109375j), control2=(1.421875-1.34375j), end=(1.140625-1.34375j))
; 		 CubicBezier(start=(1.140625-1.34375j), control1=(0.90625-1.34375j), control2=(0.703125-1.1875j), end=(0.671875-0.953125j))
; 		 Line(start=(0.671875-0.953125j), end=(0.671875-0.859375j))
; 		 CubicBezier(start=(0.671875-0.859375j), control1=(0.671875-0.09375j), control2=(1.375+0.203125j), end=(2.046875+0.203125j))
; 		 CubicBezier(start=(2.046875+0.203125j), control1=(3.78125+0.203125j), control2=(4.5625-1.796875j), end=(4.5625-3.34375j))
; 		 CubicBezier(start=(4.5625-3.34375j), control1=(4.5625-4.5625j), control2=(4.265625-6.65625j), end=(2.5-6.65625j))
; 		 CubicBezier(start=(2.5-6.65625j), control1=(1.609375-6.65625j), control2=(0.828125-6.015625j), end=(0.53125-5.203125j))
; 		 CubicBezier(start=(0.53125-5.203125j), control1=(0.453125-4.9375j), control2=(0.421875-4.671875j), end=(0.421875-4.390625j))
; 		 CubicBezier(start=(0.421875-4.390625j), control1=(0.421875-3.3125j), control2=(1.171875-2.1875j), end=(2.359375-2.1875j))
; 		 CubicBezier(start=(2.359375-2.1875j), control1=(2.9375-2.1875j), control2=(3.5-2.578125j), end=(3.65625-3.15625j))
; 		 Line(start=(3.65625-3.15625j), end=(3.65625-3.15625j))
; 		 Line(start=(3.640625-4.421875j), end=(3.640625-4.0625j))
; 		 CubicBezier(start=(3.640625-4.0625j), control1=(3.59375-3.359375j), control2=(3.296875-2.40625j), end=(2.421875-2.40625j))
; 		 CubicBezier(start=(2.421875-2.40625j), control1=(1.390625-2.40625j), control2=(1.3125-3.71875j), end=(1.3125-4.421875j))
; 		 CubicBezier(start=(1.3125-4.421875j), control1=(1.3125-5.09375j), control2=(1.34375-5.90625j), end=(2-6.28125j))
; 		 CubicBezier(start=(2-6.28125j), control1=(2.15625-6.359375j), control2=(2.328125-6.40625j), end=(2.5-6.40625j))
; 		 CubicBezier(start=(2.5-6.40625j), control1=(3.53125-6.40625j), control2=(3.640625-4.984375j), end=(3.640625-4.421875j))
; 		 Line(start=(3.640625-4.421875j), end=(3.640625-4.421875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.265625-4.296875j), end=(0.265625-3.984375j))
; 		 Line(start=(0.265625-3.984375j), end=(0.453125-3.984375j))
; 		 CubicBezier(start=(0.453125-3.984375j), control1=(0.75-3.984375j), control2=(1.046875-3.953125j), end=(1.046875-3.5625j))
; 		 Line(start=(1.046875-3.5625j), end=(1.046875+1.21875j))
; 		 CubicBezier(start=(1.046875+1.21875j), control1=(1.046875+1.578125j), control2=(0.78125+1.609375j), end=(0.5+1.609375j))
; 		 Line(start=(0.5+1.609375j), end=(0.265625+1.609375j))
; 		 Line(start=(0.265625+1.609375j), end=(0.265625+1.921875j))
; 		 Line(start=(0.265625+1.921875j), end=(2.515625+1.921875j))
; 		 Line(start=(2.515625+1.921875j), end=(2.515625+1.609375j))
; 		 Line(start=(2.515625+1.609375j), end=(2.28125+1.609375j))
; 		 CubicBezier(start=(2.28125+1.609375j), control1=(1.984375+1.609375j), control2=(1.75+1.578125j), end=(1.75+1.21875j))
; 		 Line(start=(1.75+1.21875j), end=(1.75-0.515625j))
; 		 CubicBezier(start=(1.75-0.515625j), control1=(1.984375-0.09375j), control2=(2.546875+0.09375j), end=(2.96875+0.09375j))
; 		 CubicBezier(start=(2.96875+0.09375j), control1=(4.203125+0.09375j), control2=(5.1875-0.953125j), end=(5.1875-2.15625j))
; 		 CubicBezier(start=(5.1875-2.15625j), control1=(5.1875-3.359375j), control2=(4.265625-4.40625j), end=(3.09375-4.40625j))
; 		 CubicBezier(start=(3.09375-4.40625j), control1=(2.546875-4.40625j), control2=(2.078125-4.171875j), end=(1.71875-3.765625j))
; 		 Line(start=(1.71875-3.765625j), end=(1.71875-4.40625j))
; 		 Line(start=(1.71875-4.40625j), end=(0.265625-4.296875j))
; 		 CubicBezier(start=(4.34375-1.984375j), control1=(4.296875-1.25j), control2=(3.90625-0.203125j), end=(3-0.125j))
; 		 Line(start=(3-0.125j), end=(2.9375-0.125j))
; 		 CubicBezier(start=(2.9375-0.125j), control1=(2.515625-0.125j), control2=(2.125-0.359375j), end=(1.890625-0.71875j))
; 		 CubicBezier(start=(1.890625-0.71875j), control1=(1.796875-0.828125j), control2=(1.75-0.921875j), end=(1.75-1.0625j))
; 		 Line(start=(1.75-1.0625j), end=(1.75-2.734375j))
; 		 CubicBezier(start=(1.75-2.734375j), control1=(1.75-2.875j), control2=(1.734375-3j), end=(1.734375-3.125j))
; 		 CubicBezier(start=(1.734375-3.125j), control1=(1.734375-3.65625j), control2=(2.40625-4.15625j), end=(3-4.15625j))
; 		 CubicBezier(start=(3-4.15625j), control1=(4-4.15625j), control2=(4.359375-2.84375j), end=(4.359375-2.140625j))
; 		 CubicBezier(start=(4.359375-2.140625j), control1=(4.359375-2.09375j), control2=(4.359375-2.03125j), end=(4.34375-1.984375j))
; 		 Line(start=(4.34375-1.984375j), end=(4.34375-1.984375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.109375-2.3125j), end=(3.984375-2.3125j))
; 		 CubicBezier(start=(3.984375-2.3125j), control1=(4.09375-2.3125j), control2=(4.140625-2.375j), end=(4.140625-2.5j))
; 		 CubicBezier(start=(4.140625-2.5j), control1=(4.140625-3.546875j), control2=(3.5-4.46875j), end=(2.375-4.46875j))
; 		 CubicBezier(start=(2.375-4.46875j), control1=(1.15625-4.46875j), control2=(0.28125-3.375j), end=(0.28125-2.1875j))
; 		 CubicBezier(start=(0.28125-2.1875j), control1=(0.28125-1.28125j), control2=(0.78125-0.453125j), end=(1.671875-0.0625j))
; 		 CubicBezier(start=(1.671875-0.0625j), control1=(1.890625+0.046875j), control2=(2.15625+0.09375j), end=(2.40625+0.09375j))
; 		 Line(start=(2.40625+0.09375j), end=(2.4375+0.09375j))
; 		 CubicBezier(start=(2.4375+0.09375j), control1=(3.203125+0.09375j), control2=(3.84375-0.328125j), end=(4.109375-1.09375j))
; 		 CubicBezier(start=(4.109375-1.09375j), control1=(4.125-1.125j), control2=(4.125-1.171875j), end=(4.125-1.203125j))
; 		 CubicBezier(start=(4.125-1.203125j), control1=(4.125-1.265625j), control2=(4.09375-1.3125j), end=(4.015625-1.3125j))
; 		 CubicBezier(start=(4.015625-1.3125j), control1=(3.875-1.3125j), control2=(3.8125-0.984375j), end=(3.75-0.875j))
; 		 CubicBezier(start=(3.75-0.875j), control1=(3.5-0.4375j), control2=(3.015625-0.15625j), end=(2.5-0.15625j))
; 		 CubicBezier(start=(2.5-0.15625j), control1=(2.140625-0.15625j), control2=(1.8125-0.359375j), end=(1.546875-0.625j))
; 		 CubicBezier(start=(1.546875-0.625j), control1=(1.140625-1.09375j), control2=(1.109375-1.734375j), end=(1.109375-2.3125j))
; 		 Line(start=(1.109375-2.3125j), end=(1.109375-2.3125j))
; 		 CubicBezier(start=(1.125-2.515625j), control1=(1.125-3.296875j), control2=(1.53125-4.25j), end=(2.359375-4.25j))
; 		 Line(start=(2.359375-4.25j), end=(2.40625-4.25j))
; 		 CubicBezier(start=(2.40625-4.25j), control1=(3.375-4.15625j), control2=(3.375-3.125j), end=(3.46875-2.515625j))
; 		 Line(start=(3.46875-2.515625j), end=(1.125-2.515625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(3.578125-3.859375j), control1=(3.3125-3.84375j), control2=(3.109375-3.671875j), end=(3.109375-3.40625j))
; 		 CubicBezier(start=(3.109375-3.40625j), control1=(3.109375-3.125j), control2=(3.296875-2.9375j), end=(3.5625-2.9375j))
; 		 CubicBezier(start=(3.5625-2.9375j), control1=(3.875-2.9375j), control2=(4.03125-3.15625j), end=(4.03125-3.4375j))
; 		 Line(start=(4.03125-3.4375j), end=(4.03125-3.5j))
; 		 CubicBezier(start=(4.03125-3.5j), control1=(3.921875-4.234375j), control2=(3.078125-4.46875j), end=(2.5-4.46875j))
; 		 CubicBezier(start=(2.5-4.46875j), control1=(1.28125-4.46875j), control2=(0.328125-3.40625j), end=(0.328125-2.15625j))
; 		 CubicBezier(start=(0.328125-2.15625j), control1=(0.328125-1.03125j), control2=(1.203125+0.09375j), end=(2.5+0.09375j))
; 		 CubicBezier(start=(2.5+0.09375j), control1=(3.15625+0.09375j), control2=(3.796875-0.265625j), end=(4.0625-0.953125j))
; 		 CubicBezier(start=(4.0625-0.953125j), control1=(4.09375-1.015625j), control2=(4.140625-1.109375j), end=(4.140625-1.171875j))
; 		 CubicBezier(start=(4.140625-1.171875j), control1=(4.140625-1.25j), control2=(4.09375-1.296875j), end=(4.015625-1.296875j))
; 		 CubicBezier(start=(4.015625-1.296875j), control1=(3.875-1.296875j), control2=(3.78125-0.921875j), end=(3.703125-0.78125j))
; 		 CubicBezier(start=(3.703125-0.78125j), control1=(3.46875-0.40625j), control2=(3.0625-0.15625j), end=(2.609375-0.15625j))
; 		 CubicBezier(start=(2.609375-0.15625j), control1=(1.546875-0.15625j), control2=(1.21875-1.21875j), end=(1.171875-2j))
; 		 Line(start=(1.171875-2j), end=(1.171875-2.125j))
; 		 CubicBezier(start=(1.171875-2.125j), control1=(1.171875-2.921875j), control2=(1.359375-4.109375j), end=(2.453125-4.21875j))
; 		 Line(start=(2.453125-4.21875j), end=(2.53125-4.21875j))
; 		 CubicBezier(start=(2.53125-4.21875j), control1=(2.90625-4.21875j), control2=(3.296875-4.109375j), end=(3.578125-3.859375j))
; 		 Line(start=(3.578125-3.859375j), end=(3.578125-3.859375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.484375-6.140625j), control1=(1.484375-5.453125j), control2=(1.203125-4.203125j), end=(0.171875-4.203125j))
; 		 Line(start=(0.171875-4.203125j), end=(0.171875-3.984375j))
; 		 Line(start=(0.171875-3.984375j), end=(1.03125-3.984375j))
; 		 Line(start=(1.03125-3.984375j), end=(1.03125-1.421875j))
; 		 CubicBezier(start=(1.03125-1.421875j), control1=(1.03125-1.09375j), control2=(1.0625-0.75j), end=(1.21875-0.46875j))
; 		 CubicBezier(start=(1.21875-0.46875j), control1=(1.4375-0.0625j), control2=(1.90625+0.09375j), end=(2.34375+0.09375j))
; 		 CubicBezier(start=(2.34375+0.09375j), control1=(3.15625+0.09375j), control2=(3.3125-0.8125j), end=(3.3125-1.453125j))
; 		 Line(start=(3.3125-1.453125j), end=(3.3125-1.8125j))
; 		 Line(start=(3.3125-1.8125j), end=(3.078125-1.8125j))
; 		 CubicBezier(start=(3.078125-1.8125j), control1=(3.078125-1.671875j), control2=(3.078125-1.515625j), end=(3.078125-1.359375j))
; 		 CubicBezier(start=(3.078125-1.359375j), control1=(3.078125-0.921875j), control2=(2.984375-0.15625j), end=(2.390625-0.15625j))
; 		 CubicBezier(start=(2.390625-0.15625j), control1=(1.828125-0.15625j), control2=(1.734375-0.84375j), end=(1.734375-1.28125j))
; 		 Line(start=(1.734375-1.28125j), end=(1.734375-3.984375j))
; 		 Line(start=(1.734375-3.984375j), end=(3.15625-3.984375j))
; 		 Line(start=(3.15625-3.984375j), end=(3.15625-4.296875j))
; 		 Line(start=(3.15625-4.296875j), end=(1.734375-4.296875j))
; 		 Line(start=(1.734375-4.296875j), end=(1.734375-6.140625j))
; 		 Line(start=(1.734375-6.140625j), end=(1.484375-6.140625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.265625-4.296875j), end=(0.265625-3.984375j))
; 		 Line(start=(0.265625-3.984375j), end=(0.421875-3.984375j))
; 		 CubicBezier(start=(0.421875-3.984375j), control1=(0.75-3.984375j), control2=(1.046875-3.953125j), end=(1.046875-3.484375j))
; 		 Line(start=(1.046875-3.484375j), end=(1.046875-0.734375j))
; 		 CubicBezier(start=(1.046875-0.734375j), control1=(1.046875-0.328125j), control2=(0.765625-0.3125j), end=(0.328125-0.3125j))
; 		 Line(start=(0.328125-0.3125j), end=(0.265625-0.3125j))
; 		 Line(start=(0.265625-0.3125j), end=(0.265625-0.015625j))
; 		 Line(start=(0.265625-0.015625j), end=(2.6875-0.015625j))
; 		 Line(start=(2.6875-0.015625j), end=(2.6875-0.3125j))
; 		 CubicBezier(start=(2.6875-0.3125j), control1=(2.5625-0.3125j), control2=(2.421875-0.3125j), end=(2.296875-0.3125j))
; 		 CubicBezier(start=(2.296875-0.3125j), control1=(1.984375-0.3125j), control2=(1.71875-0.359375j), end=(1.71875-0.734375j))
; 		 Line(start=(1.71875-0.734375j), end=(1.71875-2.046875j))
; 		 CubicBezier(start=(1.71875-2.046875j), control1=(1.71875-2.90625j), control2=(1.90625-4.1875j), end=(2.890625-4.1875j))
; 		 CubicBezier(start=(2.890625-4.1875j), control1=(2.9375-4.1875j), control2=(2.953125-4.1875j), end=(3-4.171875j))
; 		 CubicBezier(start=(3-4.171875j), control1=(2.84375-4.109375j), control2=(2.765625-3.953125j), end=(2.765625-3.78125j))
; 		 CubicBezier(start=(2.765625-3.78125j), control1=(2.765625-3.5625j), control2=(2.921875-3.40625j), end=(3.15625-3.359375j))
; 		 CubicBezier(start=(3.15625-3.359375j), control1=(3.4375-3.359375j), control2=(3.625-3.53125j), end=(3.625-3.78125j))
; 		 CubicBezier(start=(3.625-3.78125j), control1=(3.625-4.1875j), control2=(3.25-4.40625j), end=(2.890625-4.40625j))
; 		 CubicBezier(start=(2.890625-4.40625j), control1=(2.296875-4.40625j), control2=(1.78125-3.875j), end=(1.671875-3.34375j))
; 		 Line(start=(1.671875-3.34375j), end=(1.671875-4.40625j))
; 		 Line(start=(1.671875-4.40625j), end=(0.265625-4.296875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(2.34375-4.46875j), control1=(1.09375-4.34375j), control2=(0.28125-3.28125j), end=(0.28125-2.125j))
; 		 CubicBezier(start=(0.28125-2.125j), control1=(0.28125-1j), control2=(1.171875+0.09375j), end=(2.5+0.09375j))
; 		 CubicBezier(start=(2.5+0.09375j), control1=(3.6875+0.09375j), control2=(4.6875-0.875j), end=(4.6875-2.140625j))
; 		 CubicBezier(start=(4.6875-2.140625j), control1=(4.6875-3.3125j), control2=(3.796875-4.46875j), end=(2.46875-4.46875j))
; 		 CubicBezier(start=(2.46875-4.46875j), control1=(2.4375-4.46875j), control2=(2.375-4.46875j), end=(2.34375-4.46875j))
; 		 Line(start=(2.34375-4.46875j), end=(2.34375-4.46875j))
; 		 Line(start=(1.109375-1.890625j), end=(1.109375-2.328125j))
; 		 CubicBezier(start=(1.109375-2.328125j), control1=(1.109375-3.09375j), control2=(1.359375-4.25j), end=(2.484375-4.25j))
; 		 CubicBezier(start=(2.484375-4.25j), control1=(3.296875-4.25j), control2=(3.75-3.5625j), end=(3.84375-2.8125j))
; 		 CubicBezier(start=(3.84375-2.8125j), control1=(3.859375-2.59375j), control2=(3.859375-2.375j), end=(3.859375-2.15625j))
; 		 CubicBezier(start=(3.859375-2.15625j), control1=(3.859375-1.515625j), control2=(3.78125-0.703125j), end=(3.15625-0.34375j))
; 		 CubicBezier(start=(3.15625-0.34375j), control1=(2.953125-0.203125j), control2=(2.734375-0.15625j), end=(2.5-0.15625j))
; 		 CubicBezier(start=(2.5-0.15625j), control1=(1.78125-0.15625j), control2=(1.265625-0.71875j), end=(1.15625-1.5j))
; 		 CubicBezier(start=(1.15625-1.5j), control1=(1.140625-1.625j), control2=(1.140625-1.765625j), end=(1.109375-1.890625j))
; 		 Line(start=(1.109375-1.890625j), end=(1.109375-1.890625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.75-4.46875j), control1=(1.390625-4.421875j), control2=(1-4.359375j), end=(0.703125-4.109375j))
; 		 CubicBezier(start=(0.703125-4.109375j), control1=(0.46875-3.890625j), control2=(0.328125-3.5625j), end=(0.328125-3.234375j))
; 		 CubicBezier(start=(0.328125-3.234375j), control1=(0.328125-1.53125j), control2=(3.09375-2.5j), end=(3.09375-1j))
; 		 CubicBezier(start=(3.09375-1j), control1=(3.09375-0.40625j), control2=(2.546875-0.125j), end=(2-0.125j))
; 		 CubicBezier(start=(2-0.125j), control1=(1.25-0.125j), control2=(0.734375-0.65625j), end=(0.59375-1.515625j))
; 		 CubicBezier(start=(0.59375-1.515625j), control1=(0.578125-1.609375j), control2=(0.5625-1.6875j), end=(0.453125-1.6875j))
; 		 CubicBezier(start=(0.453125-1.6875j), control1=(0.375-1.6875j), control2=(0.328125-1.640625j), end=(0.328125-1.5625j))
; 		 Line(start=(0.328125-1.5625j), end=(0.328125+0.015625j))
; 		 CubicBezier(start=(0.328125+0.015625j), control1=(0.34375+0.0625j), control2=(0.375+0.09375j), end=(0.421875+0.09375j))
; 		 Line(start=(0.421875+0.09375j), end=(0.4375+0.09375j))
; 		 CubicBezier(start=(0.4375+0.09375j), control1=(0.59375+0.09375j), control2=(0.75-0.265625j), end=(0.875-0.296875j))
; 		 Line(start=(0.875-0.296875j), end=(0.890625-0.296875j))
; 		 CubicBezier(start=(0.890625-0.296875j), control1=(0.96875-0.296875j), control2=(1.234375-0.046875j), end=(1.453125+0.015625j))
; 		 CubicBezier(start=(1.453125+0.015625j), control1=(1.609375+0.078125j), control2=(1.796875+0.09375j), end=(1.96875+0.09375j))
; 		 CubicBezier(start=(1.96875+0.09375j), control1=(2.796875+0.09375j), control2=(3.59375-0.34375j), end=(3.59375-1.25j))
; 		 CubicBezier(start=(3.59375-1.25j), control1=(3.59375-1.90625j), control2=(3.109375-2.453125j), end=(2.453125-2.625j))
; 		 CubicBezier(start=(2.453125-2.625j), control1=(1.828125-2.796875j), control2=(0.8125-2.796875j), end=(0.8125-3.515625j))
; 		 CubicBezier(start=(0.8125-3.515625j), control1=(0.8125-4.125j), control2=(1.484375-4.28125j), end=(1.921875-4.28125j))
; 		 CubicBezier(start=(1.921875-4.28125j), control1=(2.40625-4.28125j), control2=(3.09375-4j), end=(3.09375-3.15625j))
; 		 CubicBezier(start=(3.09375-3.15625j), control1=(3.09375-3.0625j), control2=(3.09375-2.984375j), end=(3.203125-2.984375j))
; 		 CubicBezier(start=(3.203125-2.984375j), control1=(3.3125-2.984375j), control2=(3.34375-3.0625j), end=(3.34375-3.15625j))
; 		 CubicBezier(start=(3.34375-3.15625j), control1=(3.34375-3.203125j), control2=(3.34375-3.265625j), end=(3.34375-3.296875j))
; 		 Line(start=(3.34375-3.296875j), end=(3.34375-4.328125j))
; 		 CubicBezier(start=(3.34375-4.328125j), control1=(3.34375-4.390625j), control2=(3.3125-4.46875j), end=(3.234375-4.46875j))
; 		 CubicBezier(start=(3.234375-4.46875j), control1=(3.078125-4.46875j), control2=(2.96875-4.21875j), end=(2.859375-4.21875j))
; 		 Line(start=(2.859375-4.21875j), end=(2.84375-4.21875j))
; 		 CubicBezier(start=(2.84375-4.21875j), control1=(2.765625-4.21875j), control2=(2.609375-4.34375j), end=(2.5-4.390625j))
; 		 CubicBezier(start=(2.5-4.390625j), control1=(2.3125-4.453125j), control2=(2.109375-4.46875j), end=(1.921875-4.46875j))
; 		 CubicBezier(start=(1.921875-4.46875j), control1=(1.859375-4.46875j), control2=(1.8125-4.46875j), end=(1.75-4.46875j))
; 		 Line(start=(1.75-4.46875j), end=(1.75-4.46875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.1875-4.296875j), end=(0.1875-3.984375j))
; 		 Line(start=(0.1875-3.984375j), end=(0.34375-3.984375j))
; 		 CubicBezier(start=(0.34375-3.984375j), control1=(0.625-3.984375j), control2=(0.84375-3.953125j), end=(1.015625-3.578125j))
; 		 CubicBezier(start=(1.015625-3.578125j), control1=(1.46875-2.484375j), control2=(1.90625-1.375j), end=(2.375-0.28125j))
; 		 CubicBezier(start=(2.375-0.28125j), control1=(2.40625-0.203125j), control2=(2.484375-0.09375j), end=(2.484375+0j))
; 		 CubicBezier(start=(2.484375+0j), control1=(2.484375+0.015625j), control2=(2.46875+0.03125j), end=(2.46875+0.046875j))
; 		 CubicBezier(start=(2.46875+0.046875j), control1=(2.40625+0.25j), control2=(2.296875+0.453125j), end=(2.21875+0.640625j))
; 		 CubicBezier(start=(2.21875+0.640625j), control1=(2.015625+1.140625j), control2=(1.71875+1.8125j), end=(1.109375+1.8125j))
; 		 CubicBezier(start=(1.109375+1.8125j), control1=(0.953125+1.8125j), control2=(0.75+1.75j), end=(0.640625+1.640625j))
; 		 CubicBezier(start=(0.640625+1.640625j), control1=(0.828125+1.609375j), control2=(1.03125+1.5j), end=(1.03125+1.21875j))
; 		 CubicBezier(start=(1.03125+1.21875j), control1=(1.03125+1j), control2=(0.875+0.8125j), end=(0.65625+0.796875j))
; 		 CubicBezier(start=(0.65625+0.796875j), control1=(0.34375+0.796875j), control2=(0.1875+0.984375j), end=(0.1875+1.21875j))
; 		 CubicBezier(start=(0.1875+1.21875j), control1=(0.1875+1.71875j), control2=(0.65625+2.03125j), end=(1.109375+2.03125j))
; 		 CubicBezier(start=(1.109375+2.03125j), control1=(1.828125+2.03125j), control2=(2.21875+1.296875j), end=(2.46875+0.703125j))
; 		 Line(start=(2.46875+0.703125j), end=(2.765625-0.015625j))
; 		 CubicBezier(start=(2.765625-0.015625j), control1=(3.109375-0.875j), control2=(3.46875-1.71875j), end=(3.828125-2.578125j))
; 		 Line(start=(3.828125-2.578125j), end=(4.0625-3.15625j))
; 		 CubicBezier(start=(4.0625-3.15625j), control1=(4.234375-3.625j), control2=(4.5-3.984375j), end=(5.0625-3.984375j))
; 		 Line(start=(5.0625-3.984375j), end=(5.0625-4.296875j))
; 		 Line(start=(5.0625-4.296875j), end=(3.453125-4.296875j))
; 		 Line(start=(3.453125-4.296875j), end=(3.453125-3.984375j))
; 		 CubicBezier(start=(3.453125-3.984375j), control1=(3.6875-3.984375j), control2=(3.921875-3.828125j), end=(3.921875-3.578125j))
; 		 CubicBezier(start=(3.921875-3.578125j), control1=(3.921875-3.453125j), control2=(3.859375-3.3125j), end=(3.8125-3.203125j))
; 		 CubicBezier(start=(3.8125-3.203125j), control1=(3.515625-2.515625j), control2=(3.265625-1.828125j), end=(2.953125-1.15625j))
; 		 CubicBezier(start=(2.953125-1.15625j), control1=(2.9375-1.0625j), control2=(2.875-0.984375j), end=(2.859375-0.890625j))
; 		 Line(start=(2.859375-0.890625j), end=(2.84375-0.890625j))
; 		 CubicBezier(start=(2.84375-0.890625j), control1=(2.78125-1.109375j), control2=(2.671875-1.328125j), end=(2.578125-1.546875j))
; 		 CubicBezier(start=(2.578125-1.546875j), control1=(2.328125-2.1875j), control2=(2.015625-2.84375j), end=(1.78125-3.515625j))
; 		 CubicBezier(start=(1.78125-3.515625j), control1=(1.75-3.578125j), control2=(1.703125-3.65625j), end=(1.703125-3.734375j))
; 		 Line(start=(1.703125-3.734375j), end=(1.703125-3.765625j))
; 		 CubicBezier(start=(1.703125-3.765625j), control1=(1.765625-3.984375j), control2=(2.046875-3.984375j), end=(2.25-3.984375j))
; 		 Line(start=(2.25-3.984375j), end=(2.25-4.296875j))
; 		 Line(start=(2.25-4.296875j), end=(0.1875-4.296875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.375-4.296875j), end=(0.375-3.984375j))
; 		 Line(start=(0.375-3.984375j), end=(0.5625-3.984375j))
; 		 CubicBezier(start=(0.5625-3.984375j), control1=(0.84375-3.984375j), control2=(1.109375-3.953125j), end=(1.109375-3.484375j))
; 		 Line(start=(1.109375-3.484375j), end=(1.109375-0.734375j))
; 		 CubicBezier(start=(1.109375-0.734375j), control1=(1.109375-0.375j), control2=(0.921875-0.3125j), end=(0.328125-0.3125j))
; 		 Line(start=(0.328125-0.3125j), end=(0.328125-0.015625j))
; 		 Line(start=(0.328125-0.015625j), end=(2.46875-0.015625j))
; 		 Line(start=(2.46875-0.015625j), end=(2.46875-0.3125j))
; 		 Line(start=(2.46875-0.3125j), end=(2.265625-0.3125j))
; 		 CubicBezier(start=(2.265625-0.3125j), control1=(2.015625-0.3125j), control2=(1.78125-0.34375j), end=(1.78125-0.671875j))
; 		 Line(start=(1.78125-0.671875j), end=(1.78125-4.40625j))
; 		 Line(start=(1.78125-4.40625j), end=(0.375-4.296875j))
; 		 CubicBezier(start=(1.203125-6.671875j), control1=(0.953125-6.640625j), control2=(0.75-6.421875j), end=(0.75-6.15625j))
; 		 CubicBezier(start=(0.75-6.15625j), control1=(0.75-5.859375j), control2=(1-5.625j), end=(1.28125-5.625j))
; 		 CubicBezier(start=(1.28125-5.625j), control1=(1.5625-5.625j), control2=(1.8125-5.84375j), end=(1.8125-6.15625j))
; 		 CubicBezier(start=(1.8125-6.15625j), control1=(1.8125-6.4375j), control2=(1.5625-6.671875j), end=(1.28125-6.671875j))
; 		 CubicBezier(start=(1.28125-6.671875j), control1=(1.25-6.671875j), control2=(1.234375-6.671875j), end=(1.203125-6.671875j))
; 		 Line(start=(1.203125-6.671875j), end=(1.203125-6.671875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.109375-3.796875j), control1=(1.359375-4.109375j), control2=(1.796875-4.25j), end=(2.171875-4.25j))
; 		 CubicBezier(start=(2.171875-4.25j), control1=(2.90625-4.25j), control2=(3.234375-3.578125j), end=(3.234375-2.875j))
; 		 Line(start=(3.234375-2.875j), end=(3.234375-2.609375j))
; 		 CubicBezier(start=(3.234375-2.609375j), control1=(2.09375-2.609375j), control2=(0.40625-2.25j), end=(0.40625-0.953125j))
; 		 Line(start=(0.40625-0.953125j), end=(0.40625-0.875j))
; 		 CubicBezier(start=(0.40625-0.875j), control1=(0.484375-0.09375j), control2=(1.453125+0.09375j), end=(2.03125+0.09375j))
; 		 CubicBezier(start=(2.03125+0.09375j), control1=(2.53125+0.09375j), control2=(3.1875-0.234375j), end=(3.3125-0.75j))
; 		 CubicBezier(start=(3.3125-0.75j), control1=(3.375-0.3125j), control2=(3.65625+0.046875j), end=(4.09375+0.046875j))
; 		 CubicBezier(start=(4.09375+0.046875j), control1=(4.5+0.046875j), control2=(4.859375-0.28125j), end=(4.921875-0.734375j))
; 		 Line(start=(4.921875-0.734375j), end=(4.921875-1.453125j))
; 		 Line(start=(4.921875-1.453125j), end=(4.671875-1.453125j))
; 		 Line(start=(4.671875-1.453125j), end=(4.671875-0.953125j))
; 		 CubicBezier(start=(4.671875-0.953125j), control1=(4.671875-0.671875j), control2=(4.609375-0.265625j), end=(4.3125-0.265625j))
; 		 CubicBezier(start=(4.3125-0.265625j), control1=(3.984375-0.265625j), control2=(3.9375-0.65625j), end=(3.9375-0.921875j))
; 		 Line(start=(3.9375-0.921875j), end=(3.9375-2.59375j))
; 		 CubicBezier(start=(3.9375-2.59375j), control1=(3.9375-2.71875j), control2=(3.953125-2.84375j), end=(3.953125-2.96875j))
; 		 CubicBezier(start=(3.953125-2.96875j), control1=(3.953125-3.9375j), control2=(3.046875-4.46875j), end=(2.21875-4.46875j))
; 		 CubicBezier(start=(2.21875-4.46875j), control1=(1.578125-4.46875j), control2=(0.703125-4.15625j), end=(0.703125-3.359375j))
; 		 CubicBezier(start=(0.703125-3.359375j), control1=(0.703125-3.078125j), control2=(0.90625-2.875j), end=(1.171875-2.875j))
; 		 CubicBezier(start=(1.171875-2.875j), control1=(1.453125-2.875j), control2=(1.609375-3.09375j), end=(1.609375-3.34375j))
; 		 CubicBezier(start=(1.609375-3.34375j), control1=(1.609375-3.625j), control2=(1.390625-3.796875j), end=(1.109375-3.796875j))
; 		 Line(start=(1.109375-3.796875j), end=(1.109375-3.796875j))
; 		 Line(start=(3.234375-2.40625j), end=(3.234375-1.390625j))
; 		 CubicBezier(start=(3.234375-1.390625j), control1=(3.234375-0.703125j), control2=(2.765625-0.15625j), end=(2.109375-0.125j))
; 		 Line(start=(2.109375-0.125j), end=(2.078125-0.125j))
; 		 CubicBezier(start=(2.078125-0.125j), control1=(1.609375-0.125j), control2=(1.1875-0.484375j), end=(1.1875-0.96875j))
; 		 Line(start=(1.1875-0.96875j), end=(1.1875-1.015625j))
; 		 CubicBezier(start=(1.1875-1.015625j), control1=(1.25-2.03125j), control2=(2.375-2.375j), end=(3.234375-2.40625j))
; 		 Line(start=(3.234375-2.40625j), end=(3.234375-2.40625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.3125-4.296875j), end=(0.3125-3.984375j))
; 		 Line(start=(0.3125-3.984375j), end=(0.46875-3.984375j))
; 		 CubicBezier(start=(0.46875-3.984375j), control1=(0.796875-3.984375j), control2=(1.09375-3.953125j), end=(1.09375-3.484375j))
; 		 Line(start=(1.09375-3.484375j), end=(1.09375-0.734375j))
; 		 CubicBezier(start=(1.09375-0.734375j), control1=(1.09375-0.328125j), control2=(0.8125-0.3125j), end=(0.375-0.3125j))
; 		 Line(start=(0.375-0.3125j), end=(0.3125-0.3125j))
; 		 Line(start=(0.3125-0.3125j), end=(0.3125-0.015625j))
; 		 Line(start=(0.3125-0.015625j), end=(2.578125-0.015625j))
; 		 Line(start=(2.578125-0.015625j), end=(2.578125-0.3125j))
; 		 Line(start=(2.578125-0.3125j), end=(2.3125-0.3125j))
; 		 CubicBezier(start=(2.3125-0.3125j), control1=(2.03125-0.3125j), control2=(1.796875-0.359375j), end=(1.796875-0.734375j))
; 		 Line(start=(1.796875-0.734375j), end=(1.796875-2.546875j))
; 		 CubicBezier(start=(1.796875-2.546875j), control1=(1.796875-3.28125j), control2=(2.1875-4.1875j), end=(3.15625-4.1875j))
; 		 CubicBezier(start=(3.15625-4.1875j), control1=(3.78125-4.1875j), control2=(3.859375-3.53125j), end=(3.859375-3.078125j))
; 		 Line(start=(3.859375-3.078125j), end=(3.859375-0.703125j))
; 		 CubicBezier(start=(3.859375-0.703125j), control1=(3.859375-0.34375j), control2=(3.5625-0.3125j), end=(3.234375-0.3125j))
; 		 Line(start=(3.234375-0.3125j), end=(3.078125-0.3125j))
; 		 Line(start=(3.078125-0.3125j), end=(3.078125-0.015625j))
; 		 Line(start=(3.078125-0.015625j), end=(5.328125-0.015625j))
; 		 Line(start=(5.328125-0.015625j), end=(5.328125-0.3125j))
; 		 Line(start=(5.328125-0.3125j), end=(5.078125-0.3125j))
; 		 CubicBezier(start=(5.078125-0.3125j), control1=(4.796875-0.3125j), control2=(4.5625-0.359375j), end=(4.5625-0.734375j))
; 		 Line(start=(4.5625-0.734375j), end=(4.5625-2.546875j))
; 		 CubicBezier(start=(4.5625-2.546875j), control1=(4.5625-3.28125j), control2=(4.953125-4.1875j), end=(5.90625-4.1875j))
; 		 CubicBezier(start=(5.90625-4.1875j), control1=(6.546875-4.1875j), control2=(6.625-3.53125j), end=(6.625-3.078125j))
; 		 Line(start=(6.625-3.078125j), end=(6.625-0.703125j))
; 		 CubicBezier(start=(6.625-0.703125j), control1=(6.625-0.34375j), control2=(6.3125-0.3125j), end=(5.984375-0.3125j))
; 		 Line(start=(5.984375-0.3125j), end=(5.84375-0.3125j))
; 		 Line(start=(5.84375-0.3125j), end=(5.84375-0.015625j))
; 		 Line(start=(5.84375-0.015625j), end=(8.09375-0.015625j))
; 		 Line(start=(8.09375-0.015625j), end=(8.09375-0.3125j))
; 		 Line(start=(8.09375-0.3125j), end=(7.890625-0.3125j))
; 		 CubicBezier(start=(7.890625-0.3125j), control1=(7.59375-0.3125j), control2=(7.3125-0.34375j), end=(7.3125-0.703125j))
; 		 Line(start=(7.3125-0.703125j), end=(7.3125-2.953125j))
; 		 CubicBezier(start=(7.3125-2.953125j), control1=(7.3125-3.3125j), control2=(7.296875-3.65625j), end=(7.0625-3.984375j))
; 		 CubicBezier(start=(7.0625-3.984375j), control1=(6.796875-4.3125j), control2=(6.375-4.40625j), end=(5.96875-4.40625j))
; 		 CubicBezier(start=(5.96875-4.40625j), control1=(5.328125-4.40625j), control2=(4.796875-4.03125j), end=(4.53125-3.453125j))
; 		 CubicBezier(start=(4.53125-3.453125j), control1=(4.34375-4.15625j), control2=(3.890625-4.40625j), end=(3.203125-4.40625j))
; 		 CubicBezier(start=(3.203125-4.40625j), control1=(2.5625-4.40625j), control2=(1.953125-4j), end=(1.75-3.390625j))
; 		 Line(start=(1.75-3.390625j), end=(1.734375-4.40625j))
; 		 Line(start=(1.734375-4.40625j), end=(0.3125-4.296875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.3125-6.8125j), end=(0.3125-6.515625j))
; 		 Line(start=(0.3125-6.515625j), end=(0.46875-6.515625j))
; 		 CubicBezier(start=(0.46875-6.515625j), control1=(0.84375-6.515625j), control2=(1.09375-6.46875j), end=(1.09375-5.984375j))
; 		 Line(start=(1.09375-5.984375j), end=(1.09375-0.953125j))
; 		 CubicBezier(start=(1.09375-0.953125j), control1=(1.09375-0.890625j), control2=(1.109375-0.8125j), end=(1.109375-0.734375j))
; 		 CubicBezier(start=(1.109375-0.734375j), control1=(1.109375-0.359375j), control2=(0.84375-0.3125j), end=(0.5625-0.3125j))
; 		 Line(start=(0.5625-0.3125j), end=(0.3125-0.3125j))
; 		 Line(start=(0.3125-0.3125j), end=(0.3125-0.015625j))
; 		 Line(start=(0.3125-0.015625j), end=(2.578125-0.015625j))
; 		 Line(start=(2.578125-0.015625j), end=(2.578125-0.3125j))
; 		 Line(start=(2.578125-0.3125j), end=(2.3125-0.3125j))
; 		 CubicBezier(start=(2.3125-0.3125j), control1=(2.03125-0.3125j), control2=(1.796875-0.359375j), end=(1.796875-0.734375j))
; 		 Line(start=(1.796875-0.734375j), end=(1.796875-2.546875j))
; 		 CubicBezier(start=(1.796875-2.546875j), control1=(1.796875-3.265625j), control2=(2.15625-4.1875j), end=(3.15625-4.1875j))
; 		 CubicBezier(start=(3.15625-4.1875j), control1=(3.78125-4.1875j), control2=(3.84375-3.5j), end=(3.84375-3.078125j))
; 		 Line(start=(3.84375-3.078125j), end=(3.84375-0.6875j))
; 		 CubicBezier(start=(3.84375-0.6875j), control1=(3.84375-0.34375j), control2=(3.5625-0.3125j), end=(3.25-0.3125j))
; 		 Line(start=(3.25-0.3125j), end=(3.078125-0.3125j))
; 		 Line(start=(3.078125-0.3125j), end=(3.078125-0.015625j))
; 		 Line(start=(3.078125-0.015625j), end=(5.328125-0.015625j))
; 		 Line(start=(5.328125-0.015625j), end=(5.328125-0.3125j))
; 		 Line(start=(5.328125-0.3125j), end=(5.078125-0.3125j))
; 		 CubicBezier(start=(5.078125-0.3125j), control1=(4.8125-0.3125j), control2=(4.546875-0.359375j), end=(4.546875-0.703125j))
; 		 Line(start=(4.546875-0.703125j), end=(4.546875-2.875j))
; 		 CubicBezier(start=(4.546875-2.875j), control1=(4.546875-3.203125j), control2=(4.53125-3.53125j), end=(4.390625-3.84375j))
; 		 CubicBezier(start=(4.390625-3.84375j), control1=(4.140625-4.28125j), control2=(3.65625-4.40625j), end=(3.1875-4.40625j))
; 		 CubicBezier(start=(3.1875-4.40625j), control1=(2.609375-4.40625j), control2=(1.96875-4.015625j), end=(1.78125-3.453125j))
; 		 Line(start=(1.78125-3.453125j), end=(1.765625-6.921875j))
; 		 Line(start=(1.765625-6.921875j), end=(0.3125-6.8125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(3.03125-6.8125j), end=(3.03125-6.515625j))
; 		 Line(start=(3.03125-6.515625j), end=(3.1875-6.515625j))
; 		 CubicBezier(start=(3.1875-6.515625j), control1=(3.53125-6.515625j), control2=(3.8125-6.46875j), end=(3.8125-5.890625j))
; 		 Line(start=(3.8125-5.890625j), end=(3.796875-3.796875j))
; 		 CubicBezier(start=(3.796875-3.796875j), control1=(3.5625-4.203125j), control2=(2.984375-4.40625j), end=(2.53125-4.40625j))
; 		 CubicBezier(start=(2.53125-4.40625j), control1=(1.3125-4.40625j), control2=(0.328125-3.34375j), end=(0.328125-2.140625j))
; 		 CubicBezier(start=(0.328125-2.140625j), control1=(0.328125-1.078125j), control2=(1.140625+0.09375j), end=(2.46875+0.09375j))
; 		 CubicBezier(start=(2.46875+0.09375j), control1=(2.984375+0.09375j), control2=(3.453125-0.15625j), end=(3.78125-0.546875j))
; 		 Line(start=(3.78125-0.546875j), end=(3.78125+0.09375j))
; 		 Line(start=(3.78125+0.09375j), end=(5.25-0.015625j))
; 		 Line(start=(5.25-0.015625j), end=(5.25-0.3125j))
; 		 Line(start=(5.25-0.3125j), end=(5.078125-0.3125j))
; 		 CubicBezier(start=(5.078125-0.3125j), control1=(4.78125-0.3125j), control2=(4.46875-0.34375j), end=(4.46875-0.8125j))
; 		 Line(start=(4.46875-0.8125j), end=(4.46875-6.921875j))
; 		 Line(start=(4.46875-6.921875j), end=(3.03125-6.8125j))
; 		 Line(start=(1.171875-1.90625j), end=(1.171875-2.046875j))
; 		 CubicBezier(start=(1.171875-2.046875j), control1=(1.171875-2.796875j), control2=(1.25-3.53125j), end=(1.9375-4j))
; 		 CubicBezier(start=(1.9375-4j), control1=(2.140625-4.109375j), control2=(2.375-4.1875j), end=(2.609375-4.1875j))
; 		 CubicBezier(start=(2.609375-4.1875j), control1=(3.09375-4.1875j), control2=(3.78125-3.78125j), end=(3.78125-3.15625j))
; 		 CubicBezier(start=(3.78125-3.15625j), control1=(3.78125-3.03125j), control2=(3.78125-2.90625j), end=(3.78125-2.765625j))
; 		 Line(start=(3.78125-2.765625j), end=(3.78125-1.109375j))
; 		 CubicBezier(start=(3.78125-1.109375j), control1=(3.78125-0.984375j), control2=(3.734375-0.890625j), end=(3.65625-0.796875j))
; 		 CubicBezier(start=(3.65625-0.796875j), control1=(3.40625-0.40625j), control2=(2.96875-0.125j), end=(2.5-0.125j))
; 		 CubicBezier(start=(2.5-0.125j), control1=(1.78125-0.125j), control2=(1.3125-0.78125j), end=(1.203125-1.46875j))
; 		 CubicBezier(start=(1.203125-1.46875j), control1=(1.1875-1.609375j), control2=(1.1875-1.765625j), end=(1.171875-1.90625j))
; 		 Line(start=(1.171875-1.90625j), end=(1.171875-1.90625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.109375-4.296875j), end=(0.328125-4.296875j))
; 		 Line(start=(0.328125-4.296875j), end=(0.328125-3.984375j))
; 		 Line(start=(0.328125-3.984375j), end=(1.109375-3.984375j))
; 		 Line(start=(1.109375-3.984375j), end=(1.109375-0.6875j))
; 		 CubicBezier(start=(1.109375-0.6875j), control1=(1.109375-0.34375j), control2=(0.8125-0.3125j), end=(0.515625-0.3125j))
; 		 Line(start=(0.515625-0.3125j), end=(0.34375-0.3125j))
; 		 Line(start=(0.34375-0.3125j), end=(0.34375-0.015625j))
; 		 Line(start=(0.34375-0.015625j), end=(2.734375-0.015625j))
; 		 Line(start=(2.734375-0.015625j), end=(2.734375-0.3125j))
; 		 Line(start=(2.734375-0.3125j), end=(2.421875-0.3125j))
; 		 CubicBezier(start=(2.421875-0.3125j), control1=(2.203125-0.3125j), control2=(1.96875-0.328125j), end=(1.859375-0.4375j))
; 		 CubicBezier(start=(1.859375-0.4375j), control1=(1.78125-0.53125j), control2=(1.78125-0.65625j), end=(1.78125-0.78125j))
; 		 Line(start=(1.78125-0.78125j), end=(1.78125-3.984375j))
; 		 Line(start=(1.78125-3.984375j), end=(2.90625-3.984375j))
; 		 Line(start=(2.90625-3.984375j), end=(2.90625-4.296875j))
; 		 Line(start=(2.90625-4.296875j), end=(1.75-4.296875j))
; 		 Line(start=(1.75-4.296875j), end=(1.75-5.609375j))
; 		 CubicBezier(start=(1.75-5.609375j), control1=(1.75-6.15625j), control2=(2.125-6.8125j), end=(2.65625-6.8125j))
; 		 CubicBezier(start=(2.65625-6.8125j), control1=(2.765625-6.8125j), control2=(2.90625-6.796875j), end=(2.984375-6.734375j))
; 		 CubicBezier(start=(2.984375-6.734375j), control1=(2.796875-6.6875j), control2=(2.6875-6.515625j), end=(2.6875-6.3125j))
; 		 CubicBezier(start=(2.6875-6.3125j), control1=(2.6875-6.0625j), control2=(2.875-5.890625j), end=(3.125-5.890625j))
; 		 CubicBezier(start=(3.125-5.890625j), control1=(3.375-5.890625j), control2=(3.5625-6.09375j), end=(3.5625-6.3125j))
; 		 CubicBezier(start=(3.5625-6.3125j), control1=(3.5625-6.78125j), control2=(3.078125-7.03125j), end=(2.65625-7.03125j))
; 		 CubicBezier(start=(2.65625-7.03125j), control1=(1.75-7.03125j), control2=(1.109375-6.328125j), end=(1.109375-5.359375j))
; 		 Line(start=(1.109375-5.359375j), end=(1.109375-4.296875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.328125-6.8125j), end=(0.328125-6.515625j))
; 		 Line(start=(0.328125-6.515625j), end=(0.484375-6.515625j))
; 		 CubicBezier(start=(0.484375-6.515625j), control1=(0.84375-6.515625j), control2=(1.109375-6.46875j), end=(1.109375-5.984375j))
; 		 Line(start=(1.109375-5.984375j), end=(1.109375-0.953125j))
; 		 CubicBezier(start=(1.109375-0.953125j), control1=(1.109375-0.890625j), control2=(1.109375-0.8125j), end=(1.109375-0.734375j))
; 		 CubicBezier(start=(1.109375-0.734375j), control1=(1.109375-0.359375j), control2=(0.84375-0.3125j), end=(0.5625-0.3125j))
; 		 Line(start=(0.5625-0.3125j), end=(0.328125-0.3125j))
; 		 Line(start=(0.328125-0.3125j), end=(0.328125-0.015625j))
; 		 Line(start=(0.328125-0.015625j), end=(2.546875-0.015625j))
; 		 Line(start=(2.546875-0.015625j), end=(2.546875-0.3125j))
; 		 Line(start=(2.546875-0.3125j), end=(2.296875-0.3125j))
; 		 CubicBezier(start=(2.296875-0.3125j), control1=(2.015625-0.3125j), control2=(1.78125-0.359375j), end=(1.78125-0.75j))
; 		 Line(start=(1.78125-0.75j), end=(1.78125-6.921875j))
; 		 Line(start=(1.78125-6.921875j), end=(0.328125-6.8125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.3125-4.296875j), end=(0.3125-3.984375j))
; 		 Line(start=(0.3125-3.984375j), end=(0.46875-3.984375j))
; 		 CubicBezier(start=(0.46875-3.984375j), control1=(0.796875-3.984375j), control2=(1.09375-3.953125j), end=(1.09375-3.484375j))
; 		 Line(start=(1.09375-3.484375j), end=(1.09375-0.734375j))
; 		 CubicBezier(start=(1.09375-0.734375j), control1=(1.09375-0.328125j), control2=(0.8125-0.3125j), end=(0.375-0.3125j))
; 		 Line(start=(0.375-0.3125j), end=(0.3125-0.3125j))
; 		 Line(start=(0.3125-0.3125j), end=(0.3125-0.015625j))
; 		 Line(start=(0.3125-0.015625j), end=(2.578125-0.015625j))
; 		 Line(start=(2.578125-0.015625j), end=(2.578125-0.3125j))
; 		 Line(start=(2.578125-0.3125j), end=(2.3125-0.3125j))
; 		 CubicBezier(start=(2.3125-0.3125j), control1=(2.03125-0.3125j), control2=(1.796875-0.359375j), end=(1.796875-0.734375j))
; 		 Line(start=(1.796875-0.734375j), end=(1.796875-2.546875j))
; 		 CubicBezier(start=(1.796875-2.546875j), control1=(1.796875-3.265625j), control2=(2.15625-4.1875j), end=(3.15625-4.1875j))
; 		 CubicBezier(start=(3.15625-4.1875j), control1=(3.78125-4.1875j), control2=(3.84375-3.5j), end=(3.84375-3.078125j))
; 		 Line(start=(3.84375-3.078125j), end=(3.84375-0.6875j))
; 		 CubicBezier(start=(3.84375-0.6875j), control1=(3.84375-0.34375j), control2=(3.5625-0.3125j), end=(3.25-0.3125j))
; 		 Line(start=(3.25-0.3125j), end=(3.078125-0.3125j))
; 		 Line(start=(3.078125-0.3125j), end=(3.078125-0.015625j))
; 		 Line(start=(3.078125-0.015625j), end=(5.328125-0.015625j))
; 		 Line(start=(5.328125-0.015625j), end=(5.328125-0.3125j))
; 		 Line(start=(5.328125-0.3125j), end=(5.078125-0.3125j))
; 		 CubicBezier(start=(5.078125-0.3125j), control1=(4.8125-0.3125j), control2=(4.546875-0.359375j), end=(4.546875-0.703125j))
; 		 Line(start=(4.546875-0.703125j), end=(4.546875-2.875j))
; 		 CubicBezier(start=(4.546875-2.875j), control1=(4.546875-3.203125j), control2=(4.53125-3.546875j), end=(4.375-3.84375j))
; 		 CubicBezier(start=(4.375-3.84375j), control1=(4.140625-4.28125j), control2=(3.640625-4.40625j), end=(3.1875-4.40625j))
; 		 CubicBezier(start=(3.1875-4.40625j), control1=(2.578125-4.40625j), control2=(1.9375-3.984375j), end=(1.75-3.390625j))
; 		 Line(start=(1.75-3.390625j), end=(1.734375-4.40625j))
; 		 Line(start=(1.734375-4.40625j), end=(0.3125-4.296875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.3125-4.296875j), end=(0.3125-3.984375j))
; 		 Line(start=(0.3125-3.984375j), end=(0.453125-3.984375j))
; 		 CubicBezier(start=(0.453125-3.984375j), control1=(0.734375-3.984375j), control2=(1.03125-3.96875j), end=(1.09375-3.671875j))
; 		 CubicBezier(start=(1.09375-3.671875j), control1=(1.09375-3.5625j), control2=(1.09375-3.453125j), end=(1.09375-3.34375j))
; 		 Line(start=(1.09375-3.34375j), end=(1.09375-1.359375j))
; 		 CubicBezier(start=(1.09375-1.359375j), control1=(1.09375-1.03125j), control2=(1.109375-0.734375j), end=(1.28125-0.4375j))
; 		 CubicBezier(start=(1.28125-0.4375j), control1=(1.5625-0.015625j), control2=(2.125+0.109375j), end=(2.59375+0.109375j))
; 		 CubicBezier(start=(2.59375+0.109375j), control1=(3.046875+0.109375j), control2=(3.453125-0.09375j), end=(3.703125-0.453125j))
; 		 CubicBezier(start=(3.703125-0.453125j), control1=(3.765625-0.5625j), control2=(3.84375-0.65625j), end=(3.875-0.78125j))
; 		 Line(start=(3.875-0.78125j), end=(3.875+0.09375j))
; 		 Line(start=(3.875+0.09375j), end=(5.328125-0.015625j))
; 		 Line(start=(5.328125-0.015625j), end=(5.328125-0.3125j))
; 		 Line(start=(5.328125-0.3125j), end=(5.171875-0.3125j))
; 		 CubicBezier(start=(5.171875-0.3125j), control1=(4.84375-0.3125j), control2=(4.546875-0.34375j), end=(4.546875-0.796875j))
; 		 Line(start=(4.546875-0.796875j), end=(4.546875-4.40625j))
; 		 Line(start=(4.546875-4.40625j), end=(3.078125-4.296875j))
; 		 Line(start=(3.078125-4.296875j), end=(3.078125-3.984375j))
; 		 Line(start=(3.078125-3.984375j), end=(3.21875-3.984375j))
; 		 CubicBezier(start=(3.21875-3.984375j), control1=(3.546875-3.984375j), control2=(3.84375-3.953125j), end=(3.84375-3.484375j))
; 		 Line(start=(3.84375-3.484375j), end=(3.84375-1.6875j))
; 		 CubicBezier(start=(3.84375-1.6875j), control1=(3.84375-0.953125j), control2=(3.46875-0.125j), end=(2.640625-0.125j))
; 		 CubicBezier(start=(2.640625-0.125j), control1=(2.078125-0.125j), control2=(1.796875-0.265625j), end=(1.796875-1.328125j))
; 		 Line(start=(1.796875-1.328125j), end=(1.796875-4.40625j))
; 		 Line(start=(1.796875-4.40625j), end=(0.3125-4.296875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.265625-6.8125j), end=(0.265625-6.515625j))
; 		 Line(start=(0.265625-6.515625j), end=(0.4375-6.515625j))
; 		 CubicBezier(start=(0.4375-6.515625j), control1=(0.75-6.515625j), control2=(1.046875-6.484375j), end=(1.046875-6.015625j))
; 		 Line(start=(1.046875-6.015625j), end=(1.046875-0.015625j))
; 		 Line(start=(1.046875-0.015625j), end=(1.28125-0.03125j))
; 		 Line(start=(1.28125-0.03125j), end=(1.421875-0.203125j))
; 		 Line(start=(1.421875-0.203125j), end=(1.65625-0.625j))
; 		 Line(start=(1.65625-0.625j), end=(1.671875-0.625j))
; 		 CubicBezier(start=(1.671875-0.625j), control1=(1.90625-0.140625j), control2=(2.5+0.09375j), end=(3+0.09375j))
; 		 CubicBezier(start=(3+0.09375j), control1=(4.234375+0.09375j), control2=(5.1875-0.984375j), end=(5.1875-2.15625j))
; 		 CubicBezier(start=(5.1875-2.15625j), control1=(5.1875-3.3125j), control2=(4.3125-4.40625j), end=(3.046875-4.40625j))
; 		 CubicBezier(start=(3.046875-4.40625j), control1=(2.515625-4.40625j), control2=(2.0625-4.15625j), end=(1.71875-3.765625j))
; 		 Line(start=(1.71875-3.765625j), end=(1.71875-6.921875j))
; 		 Line(start=(1.71875-6.921875j), end=(0.265625-6.8125j))
; 		 CubicBezier(start=(4.34375-1.90625j), control1=(4.3125-1.4375j), control2=(4.25-1.015625j), end=(3.953125-0.640625j))
; 		 CubicBezier(start=(3.953125-0.640625j), control1=(3.703125-0.328125j), control2=(3.328125-0.125j), end=(2.9375-0.125j))
; 		 CubicBezier(start=(2.9375-0.125j), control1=(2.5-0.125j), control2=(2.109375-0.375j), end=(1.890625-0.734375j))
; 		 CubicBezier(start=(1.890625-0.734375j), control1=(1.796875-0.84375j), control2=(1.75-0.921875j), end=(1.75-1.078125j))
; 		 Line(start=(1.75-1.078125j), end=(1.75-2.734375j))
; 		 CubicBezier(start=(1.75-2.734375j), control1=(1.75-2.875j), control2=(1.734375-3j), end=(1.734375-3.125j))
; 		 CubicBezier(start=(1.734375-3.125j), control1=(1.734375-3.734375j), control2=(2.46875-4.1875j), end=(3.015625-4.1875j))
; 		 CubicBezier(start=(3.015625-4.1875j), control1=(4.09375-4.1875j), control2=(4.359375-2.9375j), end=(4.359375-2.140625j))
; 		 CubicBezier(start=(4.359375-2.140625j), control1=(4.359375-2.0625j), control2=(4.359375-2j), end=(4.34375-1.90625j))
; 		 Line(start=(4.34375-1.90625j), end=(4.34375-1.90625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.1875-4.296875j), end=(0.1875-3.984375j))
; 		 Line(start=(0.1875-3.984375j), end=(0.375-3.984375j))
; 		 CubicBezier(start=(0.375-3.984375j), control1=(0.875-3.984375j), control2=(0.921875-3.78125j), end=(1.21875-3.03125j))
; 		 Line(start=(1.21875-3.03125j), end=(2.03125-1.03125j))
; 		 CubicBezier(start=(2.03125-1.03125j), control1=(2.1875-0.640625j), control2=(2.390625+0.09375j), end=(2.625+0.09375j))
; 		 CubicBezier(start=(2.625+0.09375j), control1=(2.859375+0.09375j), control2=(3.046875-0.625j), end=(3.203125-0.984375j))
; 		 Line(start=(3.203125-0.984375j), end=(4.03125-3.046875j))
; 		 CubicBezier(start=(4.03125-3.046875j), control1=(4.28125-3.671875j), control2=(4.5-3.984375j), end=(5.0625-3.984375j))
; 		 Line(start=(5.0625-3.984375j), end=(5.0625-4.296875j))
; 		 Line(start=(5.0625-4.296875j), end=(3.453125-4.296875j))
; 		 Line(start=(3.453125-4.296875j), end=(3.453125-3.984375j))
; 		 CubicBezier(start=(3.453125-3.984375j), control1=(3.671875-3.984375j), control2=(3.921875-3.8125j), end=(3.921875-3.578125j))
; 		 CubicBezier(start=(3.921875-3.578125j), control1=(3.921875-3.453125j), control2=(3.875-3.3125j), end=(3.8125-3.1875j))
; 		 CubicBezier(start=(3.8125-3.1875j), control1=(3.53125-2.46875j), control2=(3.25-1.78125j), end=(2.953125-1.078125j))
; 		 CubicBezier(start=(2.953125-1.078125j), control1=(2.9375-0.984375j), control2=(2.875-0.890625j), end=(2.859375-0.796875j))
; 		 Line(start=(2.859375-0.796875j), end=(2.84375-0.796875j))
; 		 CubicBezier(start=(2.84375-0.796875j), control1=(2.78125-1.03125j), control2=(2.671875-1.25j), end=(2.578125-1.484375j))
; 		 Line(start=(2.578125-1.484375j), end=(1.765625-3.5j))
; 		 CubicBezier(start=(1.765625-3.5j), control1=(1.734375-3.578125j), control2=(1.6875-3.65625j), end=(1.6875-3.734375j))
; 		 Line(start=(1.6875-3.734375j), end=(1.6875-3.78125j))
; 		 CubicBezier(start=(1.6875-3.78125j), control1=(1.75-3.984375j), control2=(2.046875-3.984375j), end=(2.25-3.984375j))
; 		 Line(start=(2.25-3.984375j), end=(2.25-4.296875j))
; 		 Line(start=(2.25-4.296875j), end=(0.1875-4.296875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.1875-0.234375j), end=(1.1875-0.21875j))
; 		 CubicBezier(start=(1.1875-0.21875j), control1=(0.71875-0.125j), control2=(0.28125+0.3125j), end=(0.28125+0.78125j))
; 		 CubicBezier(start=(0.28125+0.78125j), control1=(0.28125+1.71875j), control2=(1.71875+2.046875j), end=(2.484375+2.046875j))
; 		 CubicBezier(start=(2.484375+2.046875j), control1=(3.3125+2.046875j), control2=(4.6875+1.75j), end=(4.6875+0.71875j))
; 		 CubicBezier(start=(4.6875+0.71875j), control1=(4.6875+0.0625j), control2=(4.234375-0.375j), end=(3.640625-0.546875j))
; 		 CubicBezier(start=(3.640625-0.546875j), control1=(3.28125-0.65625j), control2=(2.90625-0.671875j), end=(2.53125-0.671875j))
; 		 Line(start=(2.53125-0.671875j), end=(1.625-0.671875j))
; 		 CubicBezier(start=(1.625-0.671875j), control1=(1.3125-0.703125j), control2=(1.0625-1.015625j), end=(1.0625-1.328125j))
; 		 Line(start=(1.0625-1.328125j), end=(1.0625-1.390625j))
; 		 CubicBezier(start=(1.0625-1.390625j), control1=(1.0625-1.515625j), control2=(1.140625-1.78125j), end=(1.234375-1.78125j))
; 		 CubicBezier(start=(1.234375-1.78125j), control1=(1.359375-1.75j), control2=(1.5-1.640625j), end=(1.65625-1.578125j))
; 		 CubicBezier(start=(1.65625-1.578125j), control1=(1.828125-1.53125j), control2=(2.03125-1.5j), end=(2.21875-1.5j))
; 		 CubicBezier(start=(2.21875-1.5j), control1=(3.046875-1.5j), control2=(3.84375-2.109375j), end=(3.84375-2.953125j))
; 		 CubicBezier(start=(3.84375-2.953125j), control1=(3.84375-3.296875j), control2=(3.703125-3.6875j), end=(3.421875-3.90625j))
; 		 Line(start=(3.421875-3.90625j), end=(3.421875-3.921875j))
; 		 CubicBezier(start=(3.421875-3.921875j), control1=(3.703125-4.109375j), control2=(3.953125-4.296875j), end=(4.3125-4.296875j))
; 		 CubicBezier(start=(4.3125-4.296875j), control1=(4.34375-4.296875j), control2=(4.375-4.296875j), end=(4.40625-4.28125j))
; 		 CubicBezier(start=(4.40625-4.28125j), control1=(4.3125-4.234375j), control2=(4.25-4.15625j), end=(4.25-4.03125j))
; 		 CubicBezier(start=(4.25-4.03125j), control1=(4.25-3.875j), control2=(4.390625-3.734375j), end=(4.546875-3.734375j))
; 		 CubicBezier(start=(4.546875-3.734375j), control1=(4.71875-3.734375j), control2=(4.828125-3.875j), end=(4.828125-4.03125j))
; 		 CubicBezier(start=(4.828125-4.03125j), control1=(4.828125-4.296875j), control2=(4.609375-4.515625j), end=(4.34375-4.515625j))
; 		 CubicBezier(start=(4.34375-4.515625j), control1=(3.984375-4.515625j), control2=(3.65625-4.375j), end=(3.390625-4.15625j))
; 		 CubicBezier(start=(3.390625-4.15625j), control1=(3.359375-4.125j), control2=(3.3125-4.09375j), end=(3.28125-4.078125j))
; 		 Line(start=(3.28125-4.078125j), end=(3.265625-4.078125j))
; 		 CubicBezier(start=(3.265625-4.078125j), control1=(3.203125-4.078125j), control2=(3.125-4.15625j), end=(3.078125-4.1875j))
; 		 CubicBezier(start=(3.078125-4.1875j), control1=(2.828125-4.328125j), control2=(2.515625-4.40625j), end=(2.21875-4.40625j))
; 		 CubicBezier(start=(2.21875-4.40625j), control1=(1.4375-4.40625j), control2=(0.59375-3.859375j), end=(0.59375-2.953125j))
; 		 CubicBezier(start=(0.59375-2.953125j), control1=(0.59375-2.625j), control2=(0.71875-2.328125j), end=(0.921875-2.0625j))
; 		 CubicBezier(start=(0.921875-2.0625j), control1=(0.953125-2.03125j), control2=(1.03125-1.96875j), end=(1.03125-1.921875j))
; 		 CubicBezier(start=(1.03125-1.921875j), control1=(1.03125-1.875j), control2=(0.96875-1.828125j), end=(0.953125-1.78125j))
; 		 CubicBezier(start=(0.953125-1.78125j), control1=(0.8125-1.578125j), control2=(0.75-1.328125j), end=(0.75-1.09375j))
; 		 CubicBezier(start=(0.75-1.09375j), control1=(0.75-0.78125j), control2=(0.890625-0.375j), end=(1.1875-0.234375j))
; 		 Line(start=(1.1875-0.234375j), end=(1.1875-0.234375j))
; 		 Line(start=(0.8125+0.84375j), end=(0.8125+0.75j))
; 		 CubicBezier(start=(0.8125+0.75j), control1=(0.8125+0.234375j), control2=(1.265625-0.078125j), end=(1.796875-0.078125j))
; 		 Line(start=(1.796875-0.078125j), end=(2.578125-0.078125j))
; 		 CubicBezier(start=(2.578125-0.078125j), control1=(3.203125-0.078125j), control2=(4.171875+0.015625j), end=(4.171875+0.78125j))
; 		 CubicBezier(start=(4.171875+0.78125j), control1=(4.171875+1.5j), control2=(3.09375+1.8125j), end=(2.484375+1.8125j))
; 		 CubicBezier(start=(2.484375+1.8125j), control1=(1.828125+1.8125j), control2=(0.90625+1.515625j), end=(0.8125+0.84375j))
; 		 Line(start=(0.8125+0.84375j), end=(0.8125+0.84375j))
; 		 Line(start=(1.34375-2.75j), end=(1.34375-2.875j))
; 		 CubicBezier(start=(1.34375-2.875j), control1=(1.34375-3.421875j), control2=(1.46875-4.171875j), end=(2.21875-4.171875j))
; 		 CubicBezier(start=(2.21875-4.171875j), control1=(2.828125-4.171875j), control2=(3.078125-3.59375j), end=(3.078125-2.953125j))
; 		 CubicBezier(start=(3.078125-2.953125j), control1=(3.078125-2.546875j), control2=(3-1.734375j), end=(2.21875-1.734375j))
; 		 CubicBezier(start=(2.21875-1.734375j), control1=(1.96875-1.734375j), control2=(1.71875-1.859375j), end=(1.546875-2.078125j))
; 		 CubicBezier(start=(1.546875-2.078125j), control1=(1.390625-2.265625j), control2=(1.375-2.515625j), end=(1.34375-2.75j))
; 		 Line(start=(1.34375-2.75j), end=(1.34375-2.75j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.171875-4.296875j), end=(0.171875-3.984375j))
; 		 Line(start=(0.171875-3.984375j), end=(0.328125-3.984375j))
; 		 CubicBezier(start=(0.328125-3.984375j), control1=(0.84375-3.984375j), control2=(0.875-3.765625j), end=(1.109375-3.0625j))
; 		 CubicBezier(start=(1.109375-3.0625j), control1=(1.359375-2.375j), control2=(1.625-1.6875j), end=(1.859375-1j))
; 		 Line(start=(1.859375-1j), end=(2.15625-0.15625j))
; 		 CubicBezier(start=(2.15625-0.15625j), control1=(2.203125-0.046875j), control2=(2.234375+0.09375j), end=(2.375+0.09375j))
; 		 Line(start=(2.375+0.09375j), end=(2.390625+0.09375j))
; 		 CubicBezier(start=(2.390625+0.09375j), control1=(2.609375+0.09375j), control2=(2.75-0.5625j), end=(2.875-0.890625j))
; 		 CubicBezier(start=(2.875-0.890625j), control1=(3.0625-1.421875j), control2=(3.25-1.953125j), end=(3.453125-2.484375j))
; 		 CubicBezier(start=(3.453125-2.484375j), control1=(3.5-2.625j), control2=(3.5625-2.765625j), end=(3.59375-2.90625j))
; 		 CubicBezier(start=(3.59375-2.90625j), control1=(3.625-2.765625j), control2=(3.6875-2.625j), end=(3.734375-2.484375j))
; 		 Line(start=(3.734375-2.484375j), end=(4.03125-1.671875j))
; 		 CubicBezier(start=(4.03125-1.671875j), control1=(4.203125-1.15625j), control2=(4.40625-0.640625j), end=(4.578125-0.125j))
; 		 CubicBezier(start=(4.578125-0.125j), control1=(4.625-0.015625j), control2=(4.671875+0.09375j), end=(4.8125+0.09375j))
; 		 CubicBezier(start=(4.8125+0.09375j), control1=(5.03125+0.09375j), control2=(5.1875-0.609375j), end=(5.3125-0.96875j))
; 		 Line(start=(5.3125-0.96875j), end=(5.875-2.546875j))
; 		 CubicBezier(start=(5.875-2.546875j), control1=(6.109375-3.1875j), control2=(6.25-3.984375j), end=(7.015625-3.984375j))
; 		 Line(start=(7.015625-3.984375j), end=(7.015625-4.296875j))
; 		 Line(start=(7.015625-4.296875j), end=(5.359375-4.296875j))
; 		 Line(start=(5.359375-4.296875j), end=(5.359375-3.984375j))
; 		 CubicBezier(start=(5.359375-3.984375j), control1=(5.625-3.984375j), control2=(5.90625-3.84375j), end=(5.9375-3.5625j))
; 		 CubicBezier(start=(5.9375-3.5625j), control1=(5.9375-3.28125j), control2=(5.765625-2.953125j), end=(5.671875-2.703125j))
; 		 CubicBezier(start=(5.671875-2.703125j), control1=(5.515625-2.265625j), control2=(5.359375-1.84375j), end=(5.21875-1.421875j))
; 		 CubicBezier(start=(5.21875-1.421875j), control1=(5.140625-1.203125j), control2=(5.03125-0.984375j), end=(4.984375-0.765625j))
; 		 Line(start=(4.984375-0.765625j), end=(4.96875-0.765625j))
; 		 CubicBezier(start=(4.96875-0.765625j), control1=(4.921875-1j), control2=(4.796875-1.3125j), end=(4.6875-1.5625j))
; 		 CubicBezier(start=(4.6875-1.5625j), control1=(4.46875-2.203125j), control2=(4.234375-2.84375j), end=(4-3.5j))
; 		 CubicBezier(start=(4-3.5j), control1=(3.984375-3.5625j), control2=(3.953125-3.65625j), end=(3.953125-3.734375j))
; 		 Line(start=(3.953125-3.734375j), end=(3.953125-3.765625j))
; 		 CubicBezier(start=(3.953125-3.765625j), control1=(4-3.984375j), control2=(4.328125-3.984375j), end=(4.53125-3.984375j))
; 		 Line(start=(4.53125-3.984375j), end=(4.53125-4.296875j))
; 		 Line(start=(4.53125-4.296875j), end=(2.59375-4.296875j))
; 		 Line(start=(2.59375-4.296875j), end=(2.59375-3.984375j))
; 		 Line(start=(2.59375-3.984375j), end=(2.765625-3.984375j))
; 		 CubicBezier(start=(2.765625-3.984375j), control1=(3.15625-3.984375j), control2=(3.25-3.84375j), end=(3.375-3.5j))
; 		 CubicBezier(start=(3.375-3.5j), control1=(3.40625-3.421875j), control2=(3.453125-3.34375j), end=(3.453125-3.265625j))
; 		 CubicBezier(start=(3.453125-3.265625j), control1=(3.4375-3.171875j), control2=(3.390625-3.078125j), end=(3.359375-2.984375j))
; 		 Line(start=(3.359375-2.984375j), end=(2.796875-1.40625j))
; 		 CubicBezier(start=(2.796875-1.40625j), control1=(2.734375-1.234375j), control2=(2.6875-1.0625j), end=(2.609375-0.90625j))
; 		 CubicBezier(start=(2.609375-0.90625j), control1=(2.5-1.171875j), control2=(2.40625-1.453125j), end=(2.3125-1.71875j))
; 		 CubicBezier(start=(2.3125-1.71875j), control1=(2.15625-2.15625j), control2=(2-2.578125j), end=(1.859375-3.015625j))
; 		 Line(start=(1.859375-3.015625j), end=(1.671875-3.515625j))
; 		 CubicBezier(start=(1.671875-3.515625j), control1=(1.65625-3.578125j), control2=(1.609375-3.65625j), end=(1.609375-3.71875j))
; 		 CubicBezier(start=(1.609375-3.71875j), control1=(1.609375-3.984375j), control2=(1.984375-3.984375j), end=(2.203125-3.984375j))
; 		 Line(start=(2.203125-3.984375j), end=(2.203125-4.296875j))
; 		 Line(start=(2.203125-4.296875j), end=(0.171875-4.296875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.3125-1.0625j), control1=(1.0625-1.03125j), control2=(0.859375-0.796875j), end=(0.859375-0.546875j))
; 		 CubicBezier(start=(0.859375-0.546875j), control1=(0.859375-0.296875j), control2=(1.0625-0.015625j), end=(1.359375-0.015625j))
; 		 CubicBezier(start=(1.359375-0.015625j), control1=(1.640625-0.015625j), control2=(1.90625-0.21875j), end=(1.90625-0.53125j))
; 		 CubicBezier(start=(1.90625-0.53125j), control1=(1.90625-0.78125j), control2=(1.71875-1.0625j), end=(1.390625-1.0625j))
; 		 CubicBezier(start=(1.390625-1.0625j), control1=(1.359375-1.0625j), control2=(1.328125-1.0625j), end=(1.3125-1.0625j))
; 		 Line(start=(1.3125-1.0625j), end=(1.3125-1.0625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.3125-0.3125j), end=(0.3125-0.015625j))
; 		 Line(start=(0.3125-0.015625j), end=(2.46875-0.015625j))
; 		 Line(start=(2.46875-0.015625j), end=(2.46875-0.3125j))
; 		 CubicBezier(start=(2.46875-0.3125j), control1=(2.15625-0.3125j), control2=(1.75-0.46875j), end=(1.71875-0.8125j))
; 		 CubicBezier(start=(1.71875-0.8125j), control1=(1.71875-0.921875j), control2=(1.78125-1j), end=(1.796875-1.09375j))
; 		 CubicBezier(start=(1.796875-1.09375j), control1=(1.90625-1.421875j), control2=(2.015625-1.75j), end=(2.140625-2.078125j))
; 		 CubicBezier(start=(2.140625-2.078125j), control1=(2.171875-2.15625j), control2=(2.171875-2.28125j), end=(2.3125-2.28125j))
; 		 Line(start=(2.3125-2.28125j), end=(4.578125-2.28125j))
; 		 CubicBezier(start=(4.578125-2.28125j), control1=(4.6875-2.28125j), control2=(4.703125-2.171875j), end=(4.75-2.078125j))
; 		 Line(start=(4.75-2.078125j), end=(4.953125-1.453125j))
; 		 CubicBezier(start=(4.953125-1.453125j), control1=(5.046875-1.203125j), control2=(5.234375-0.8125j), end=(5.234375-0.609375j))
; 		 Line(start=(5.234375-0.609375j), end=(5.234375-0.5625j))
; 		 CubicBezier(start=(5.234375-0.5625j), control1=(5.140625-0.328125j), control2=(4.75-0.3125j), end=(4.46875-0.3125j))
; 		 Line(start=(4.46875-0.3125j), end=(4.40625-0.3125j))
; 		 Line(start=(4.40625-0.3125j), end=(4.40625-0.015625j))
; 		 Line(start=(4.40625-0.015625j), end=(7.140625-0.015625j))
; 		 Line(start=(7.140625-0.015625j), end=(7.140625-0.3125j))
; 		 Line(start=(7.140625-0.3125j), end=(6.859375-0.3125j))
; 		 CubicBezier(start=(6.859375-0.3125j), control1=(6.625-0.3125j), control2=(6.390625-0.34375j), end=(6.265625-0.453125j))
; 		 CubicBezier(start=(6.265625-0.453125j), control1=(6.15625-0.578125j), control2=(6.125-0.75j), end=(6.0625-0.890625j))
; 		 CubicBezier(start=(6.0625-0.890625j), control1=(5.546875-2.375j), control2=(5.046875-3.875j), end=(4.515625-5.359375j))
; 		 CubicBezier(start=(4.515625-5.359375j), control1=(4.34375-5.875j), control2=(4.09375-6.4375j), end=(3.96875-6.953125j))
; 		 CubicBezier(start=(3.96875-6.953125j), control1=(3.921875-7.09375j), control2=(3.875-7.140625j), end=(3.734375-7.140625j))
; 		 CubicBezier(start=(3.734375-7.140625j), control1=(3.53125-7.140625j), control2=(3.484375-6.9375j), end=(3.421875-6.734375j))
; 		 CubicBezier(start=(3.421875-6.734375j), control1=(2.875-5.046875j), control2=(2.25-3.390625j), end=(1.6875-1.71875j))
; 		 Line(start=(1.6875-1.71875j), end=(1.46875-1.109375j))
; 		 CubicBezier(start=(1.46875-1.109375j), control1=(1.421875-0.9375j), control2=(1.359375-0.75j), end=(1.21875-0.625j))
; 		 CubicBezier(start=(1.21875-0.625j), control1=(0.984375-0.375j), control2=(0.640625-0.3125j), end=(0.3125-0.3125j))
; 		 Line(start=(0.3125-0.3125j), end=(0.3125-0.3125j))
; 		 Line(start=(2.328125-2.59375j), end=(3.4375-5.828125j))
; 		 Line(start=(3.4375-5.828125j), end=(4.5625-2.59375j))
; 		 Line(start=(4.5625-2.59375j), end=(2.328125-2.59375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.046875-4.296875j), end=(0.265625-4.296875j))
; 		 Line(start=(0.265625-4.296875j), end=(0.265625-3.984375j))
; 		 Line(start=(0.265625-3.984375j), end=(1.046875-3.984375j))
; 		 Line(start=(1.046875-3.984375j), end=(1.046875-0.6875j))
; 		 CubicBezier(start=(1.046875-0.6875j), control1=(1.046875-0.34375j), control2=(0.75-0.3125j), end=(0.453125-0.3125j))
; 		 Line(start=(0.453125-0.3125j), end=(0.265625-0.3125j))
; 		 Line(start=(0.265625-0.3125j), end=(0.265625-0.015625j))
; 		 Line(start=(0.265625-0.015625j), end=(2.5-0.015625j))
; 		 Line(start=(2.5-0.015625j), end=(2.5-0.3125j))
; 		 Line(start=(2.5-0.3125j), end=(2.25-0.3125j))
; 		 CubicBezier(start=(2.25-0.3125j), control1=(1.96875-0.3125j), control2=(1.71875-0.34375j), end=(1.71875-0.6875j))
; 		 Line(start=(1.71875-0.6875j), end=(1.71875-3.984375j))
; 		 Line(start=(1.71875-3.984375j), end=(3.3125-3.984375j))
; 		 CubicBezier(start=(3.3125-3.984375j), control1=(3.59375-3.984375j), control2=(3.8125-3.9375j), end=(3.8125-3.40625j))
; 		 CubicBezier(start=(3.8125-3.40625j), control1=(3.8125-3.296875j), control2=(3.8125-3.203125j), end=(3.8125-3.09375j))
; 		 Line(start=(3.8125-3.09375j), end=(3.8125-0.734375j))
; 		 CubicBezier(start=(3.8125-0.734375j), control1=(3.8125-0.328125j), control2=(3.53125-0.3125j), end=(3.09375-0.3125j))
; 		 Line(start=(3.09375-0.3125j), end=(3.03125-0.3125j))
; 		 Line(start=(3.03125-0.3125j), end=(3.03125-0.015625j))
; 		 Line(start=(3.03125-0.015625j), end=(5.25-0.015625j))
; 		 Line(start=(5.25-0.015625j), end=(5.25-0.3125j))
; 		 Line(start=(5.25-0.3125j), end=(5-0.3125j))
; 		 CubicBezier(start=(5-0.3125j), control1=(4.71875-0.3125j), control2=(4.46875-0.359375j), end=(4.46875-0.71875j))
; 		 Line(start=(4.46875-0.71875j), end=(4.46875-4.421875j))
; 		 Line(start=(4.46875-4.421875j), end=(2.96875-4.296875j))
; 		 Line(start=(2.96875-4.296875j), end=(1.6875-4.296875j))
; 		 Line(start=(1.6875-4.296875j), end=(1.6875-5.3125j))
; 		 CubicBezier(start=(1.6875-5.3125j), control1=(1.6875-5.546875j), control2=(1.6875-5.765625j), end=(1.796875-5.984375j))
; 		 CubicBezier(start=(1.796875-5.984375j), control1=(2.015625-6.5j), control2=(2.609375-6.8125j), end=(3.15625-6.8125j))
; 		 CubicBezier(start=(3.15625-6.8125j), control1=(3.484375-6.8125j), control2=(3.78125-6.71875j), end=(4.015625-6.484375j))
; 		 CubicBezier(start=(4.015625-6.484375j), control1=(3.765625-6.484375j), control2=(3.53125-6.3125j), end=(3.53125-6.046875j))
; 		 CubicBezier(start=(3.53125-6.046875j), control1=(3.53125-5.796875j), control2=(3.734375-5.578125j), end=(4-5.578125j))
; 		 CubicBezier(start=(4-5.578125j), control1=(4.234375-5.578125j), control2=(4.4375-5.734375j), end=(4.453125-5.984375j))
; 		 Line(start=(4.453125-5.984375j), end=(4.453125-6.046875j))
; 		 CubicBezier(start=(4.453125-6.046875j), control1=(4.453125-6.734375j), control2=(3.734375-7.03125j), end=(3.15625-7.03125j))
; 		 CubicBezier(start=(3.15625-7.03125j), control1=(2.40625-7.03125j), control2=(1.5-6.71875j), end=(1.15625-5.984375j))
; 		 CubicBezier(start=(1.15625-5.984375j), control1=(1.0625-5.78125j), control2=(1.046875-5.5625j), end=(1.046875-5.328125j))
; 		 Line(start=(1.046875-5.328125j), end=(1.046875-4.296875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.828125-6.078125j), control1=(1.828125-5.984375j), control2=(1.828125-5.890625j), end=(1.828125-5.796875j))
; 		 CubicBezier(start=(1.828125-5.796875j), control1=(1.796875-5.1875j), control2=(1.5625-4.640625j), end=(1.125-4.21875j))
; 		 CubicBezier(start=(1.125-4.21875j), control1=(1.09375-4.171875j), control2=(1.015625-4.125j), end=(1.015625-4.0625j))
; 		 Line(start=(1.015625-4.0625j), end=(1.015625-4.03125j))
; 		 CubicBezier(start=(1.015625-4.03125j), control1=(1.015625-3.984375j), control2=(1.09375-3.953125j), end=(1.109375-3.953125j))
; 		 CubicBezier(start=(1.109375-3.953125j), control1=(1.265625-3.953125j), control2=(1.53125-4.3125j), end=(1.65625-4.5j))
; 		 CubicBezier(start=(1.65625-4.5j), control1=(1.890625-4.890625j), control2=(2.0625-5.375j), end=(2.0625-5.859375j))
; 		 CubicBezier(start=(2.0625-5.859375j), control1=(2.0625-6.25j), control2=(1.9375-6.875j), end=(1.453125-6.921875j))
; 		 CubicBezier(start=(1.453125-6.921875j), control1=(1.078125-6.921875j), control2=(0.859375-6.734375j), end=(0.859375-6.40625j))
; 		 CubicBezier(start=(0.859375-6.40625j), control1=(0.859375-6.09375j), control2=(1.078125-5.875j), end=(1.390625-5.875j))
; 		 CubicBezier(start=(1.390625-5.875j), control1=(1.5625-5.875j), control2=(1.703125-5.9375j), end=(1.828125-6.078125j))
; 		 Line(start=(1.828125-6.078125j), end=(1.828125-6.078125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(3.78125-0.625j), end=(3.78125+1.21875j))
; 		 CubicBezier(start=(3.78125+1.21875j), control1=(3.78125+1.59375j), control2=(3.53125+1.609375j), end=(3.015625+1.609375j))
; 		 Line(start=(3.015625+1.609375j), end=(3.015625+1.921875j))
; 		 Line(start=(3.015625+1.921875j), end=(5.265625+1.921875j))
; 		 Line(start=(5.265625+1.921875j), end=(5.265625+1.609375j))
; 		 Line(start=(5.265625+1.609375j), end=(5.03125+1.609375j))
; 		 CubicBezier(start=(5.03125+1.609375j), control1=(4.75+1.609375j), control2=(4.484375+1.578125j), end=(4.484375+1.203125j))
; 		 Line(start=(4.484375+1.203125j), end=(4.484375-4.40625j))
; 		 Line(start=(4.484375-4.40625j), end=(4.28125-4.390625j))
; 		 Line(start=(4.28125-4.390625j), end=(4.140625-4.140625j))
; 		 Line(start=(4.140625-4.140625j), end=(3.875-3.5j))
; 		 CubicBezier(start=(3.875-3.5j), control1=(3.625-4.0625j), control2=(3.109375-4.40625j), end=(2.5-4.40625j))
; 		 CubicBezier(start=(2.5-4.40625j), control1=(1.25-4.40625j), control2=(0.328125-3.296875j), end=(0.328125-2.140625j))
; 		 CubicBezier(start=(0.328125-2.140625j), control1=(0.328125-1.015625j), control2=(1.21875+0.09375j), end=(2.4375+0.09375j))
; 		 CubicBezier(start=(2.4375+0.09375j), control1=(2.90625+0.09375j), control2=(3.53125-0.15625j), end=(3.78125-0.625j))
; 		 Line(start=(3.78125-0.625j), end=(3.78125-0.625j))
; 		 Line(start=(1.171875-1.984375j), end=(1.171875-2.09375j))
; 		 CubicBezier(start=(1.171875-2.09375j), control1=(1.171875-2.890625j), control2=(1.453125-3.84375j), end=(2.359375-4.125j))
; 		 CubicBezier(start=(2.359375-4.125j), control1=(2.4375-4.140625j), control2=(2.515625-4.15625j), end=(2.609375-4.15625j))
; 		 CubicBezier(start=(2.609375-4.15625j), control1=(3.265625-4.15625j), control2=(3.71875-3.515625j), end=(3.8125-2.9375j))
; 		 Line(start=(3.8125-2.9375j), end=(3.8125-1.328125j))
; 		 CubicBezier(start=(3.8125-1.328125j), control1=(3.8125-0.734375j), control2=(3.1875-0.265625j), end=(2.796875-0.15625j))
; 		 CubicBezier(start=(2.796875-0.15625j), control1=(2.6875-0.125j), control2=(2.578125-0.125j), end=(2.46875-0.125j))
; 		 CubicBezier(start=(2.46875-0.125j), control1=(1.828125-0.140625j), control2=(1.390625-0.84375j), end=(1.234375-1.46875j))
; 		 CubicBezier(start=(1.234375-1.46875j), control1=(1.203125-1.640625j), control2=(1.1875-1.8125j), end=(1.171875-1.984375j))
; 		 Line(start=(1.171875-1.984375j), end=(1.171875-1.984375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.296875-6.8125j), end=(0.296875-6.515625j))
; 		 CubicBezier(start=(0.296875-6.515625j), control1=(0.421875-6.515625j), control2=(0.5625-6.515625j), end=(0.703125-6.515625j))
; 		 CubicBezier(start=(0.703125-6.515625j), control1=(1.046875-6.515625j), control2=(1.359375-6.46875j), end=(1.359375-6.09375j))
; 		 Line(start=(1.359375-6.09375j), end=(1.359375-0.75j))
; 		 CubicBezier(start=(1.359375-0.75j), control1=(1.359375-0.375j), control2=(1.03125-0.3125j), end=(0.703125-0.3125j))
; 		 CubicBezier(start=(0.703125-0.3125j), control1=(0.5625-0.3125j), control2=(0.40625-0.3125j), end=(0.296875-0.3125j))
; 		 Line(start=(0.296875-0.3125j), end=(0.296875-0.015625j))
; 		 Line(start=(0.296875-0.015625j), end=(3.3125-0.015625j))
; 		 Line(start=(3.3125-0.015625j), end=(3.3125-0.3125j))
; 		 CubicBezier(start=(3.3125-0.3125j), control1=(3.1875-0.3125j), control2=(3.046875-0.3125j), end=(2.90625-0.3125j))
; 		 CubicBezier(start=(2.90625-0.3125j), control1=(2.5625-0.3125j), control2=(2.25-0.375j), end=(2.25-0.75j))
; 		 Line(start=(2.25-0.75j), end=(2.25-6.09375j))
; 		 CubicBezier(start=(2.25-6.09375j), control1=(2.25-6.46875j), control2=(2.578125-6.515625j), end=(2.921875-6.515625j))
; 		 CubicBezier(start=(2.921875-6.515625j), control1=(3.046875-6.515625j), control2=(3.1875-6.515625j), end=(3.3125-6.515625j))
; 		 Line(start=(3.3125-6.515625j), end=(3.3125-6.8125j))
; 		 Line(start=(3.3125-6.8125j), end=(0.296875-6.8125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.046875-4.296875j), end=(0.265625-4.296875j))
; 		 Line(start=(0.265625-4.296875j), end=(0.265625-3.984375j))
; 		 Line(start=(0.265625-3.984375j), end=(1.046875-3.984375j))
; 		 Line(start=(1.046875-3.984375j), end=(1.046875-0.6875j))
; 		 CubicBezier(start=(1.046875-0.6875j), control1=(1.046875-0.34375j), control2=(0.75-0.3125j), end=(0.453125-0.3125j))
; 		 Line(start=(0.453125-0.3125j), end=(0.265625-0.3125j))
; 		 Line(start=(0.265625-0.3125j), end=(0.265625-0.015625j))
; 		 Line(start=(0.265625-0.015625j), end=(2.5-0.015625j))
; 		 Line(start=(2.5-0.015625j), end=(2.5-0.3125j))
; 		 Line(start=(2.5-0.3125j), end=(2.25-0.3125j))
; 		 CubicBezier(start=(2.25-0.3125j), control1=(1.96875-0.3125j), control2=(1.71875-0.34375j), end=(1.71875-0.6875j))
; 		 Line(start=(1.71875-0.6875j), end=(1.71875-3.984375j))
; 		 Line(start=(1.71875-3.984375j), end=(3.8125-3.984375j))
; 		 Line(start=(3.8125-3.984375j), end=(3.8125-0.6875j))
; 		 CubicBezier(start=(3.8125-0.6875j), control1=(3.8125-0.34375j), control2=(3.515625-0.3125j), end=(3.203125-0.3125j))
; 		 Line(start=(3.203125-0.3125j), end=(3.03125-0.3125j))
; 		 Line(start=(3.03125-0.3125j), end=(3.03125-0.015625j))
; 		 Line(start=(3.03125-0.015625j), end=(5.4375-0.015625j))
; 		 Line(start=(5.4375-0.015625j), end=(5.4375-0.3125j))
; 		 Line(start=(5.4375-0.3125j), end=(5.125-0.3125j))
; 		 CubicBezier(start=(5.125-0.3125j), control1=(4.90625-0.3125j), control2=(4.671875-0.328125j), end=(4.5625-0.4375j))
; 		 CubicBezier(start=(4.5625-0.4375j), control1=(4.46875-0.53125j), control2=(4.46875-0.65625j), end=(4.46875-0.78125j))
; 		 Line(start=(4.46875-0.78125j), end=(4.46875-3.984375j))
; 		 Line(start=(4.46875-3.984375j), end=(5.625-3.984375j))
; 		 Line(start=(5.625-3.984375j), end=(5.625-4.296875j))
; 		 Line(start=(5.625-4.296875j), end=(4.453125-4.296875j))
; 		 Line(start=(4.453125-4.296875j), end=(4.453125-5.25j))
; 		 CubicBezier(start=(4.453125-5.25j), control1=(4.453125-5.859375j), control2=(4.578125-6.734375j), end=(5.296875-6.8125j))
; 		 CubicBezier(start=(5.296875-6.8125j), control1=(5.4375-6.8125j), control2=(5.5625-6.8125j), end=(5.6875-6.75j))
; 		 CubicBezier(start=(5.6875-6.75j), control1=(5.515625-6.671875j), control2=(5.390625-6.546875j), end=(5.390625-6.34375j))
; 		 CubicBezier(start=(5.390625-6.34375j), control1=(5.390625-6.078125j), control2=(5.578125-5.890625j), end=(5.828125-5.890625j))
; 		 CubicBezier(start=(5.828125-5.890625j), control1=(6.125-5.890625j), control2=(6.265625-6.109375j), end=(6.265625-6.34375j))
; 		 CubicBezier(start=(6.265625-6.34375j), control1=(6.265625-6.84375j), control2=(5.734375-7.03125j), end=(5.328125-7.03125j))
; 		 CubicBezier(start=(5.328125-7.03125j), control1=(5-7.03125j), control2=(4.6875-6.90625j), end=(4.4375-6.734375j))
; 		 CubicBezier(start=(4.4375-6.734375j), control1=(4.390625-6.703125j), control2=(4.34375-6.65625j), end=(4.296875-6.65625j))
; 		 CubicBezier(start=(4.296875-6.65625j), control1=(4.25-6.65625j), control2=(4.109375-6.796875j), end=(4-6.859375j))
; 		 CubicBezier(start=(4-6.859375j), control1=(3.78125-6.96875j), control2=(3.53125-7.03125j), end=(3.296875-7.03125j))
; 		 Line(start=(3.296875-7.03125j), end=(3.1875-7.03125j))
; 		 CubicBezier(start=(3.1875-7.03125j), control1=(2.25-7.03125j), control2=(1.046875-6.59375j), end=(1.046875-5.359375j))
; 		 Line(start=(1.046875-5.359375j), end=(1.046875-4.296875j))
; 		 CubicBezier(start=(3.84375-5.78125j), control1=(3.8125-5.6875j), control2=(3.8125-5.578125j), end=(3.8125-5.484375j))
; 		 Line(start=(3.8125-5.484375j), end=(3.8125-4.296875j))
; 		 Line(start=(3.8125-4.296875j), end=(1.6875-4.296875j))
; 		 Line(start=(1.6875-4.296875j), end=(1.6875-5.34375j))
; 		 CubicBezier(start=(1.6875-5.34375j), control1=(1.6875-6.265625j), control2=(2.359375-6.8125j), end=(3.171875-6.8125j))
; 		 CubicBezier(start=(3.171875-6.8125j), control1=(3.453125-6.8125j), control2=(3.671875-6.734375j), end=(3.921875-6.625j))
; 		 CubicBezier(start=(3.921875-6.625j), control1=(3.71875-6.5625j), control2=(3.5625-6.4375j), end=(3.5625-6.203125j))
; 		 CubicBezier(start=(3.5625-6.203125j), control1=(3.5625-6.015625j), control2=(3.65625-5.84375j), end=(3.84375-5.78125j))
; 		 Line(start=(3.84375-5.78125j), end=(3.84375-5.78125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(3.703125-7.03125j), control1=(2.8125-6.90625j), control2=(2.0625-6.625j), end=(1.46875-5.921875j))
; 		 CubicBezier(start=(1.46875-5.921875j), control1=(0.875-5.21875j), control2=(0.546875-4.296875j), end=(0.546875-3.375j))
; 		 CubicBezier(start=(0.546875-3.375j), control1=(0.546875-1.671875j), control2=(1.78125+0.203125j), end=(3.890625+0.203125j))
; 		 CubicBezier(start=(3.890625+0.203125j), control1=(5.84375+0.203125j), control2=(7.203125-1.578125j), end=(7.203125-3.40625j))
; 		 CubicBezier(start=(7.203125-3.40625j), control1=(7.203125-5.234375j), control2=(5.859375-7.046875j), end=(3.84375-7.046875j))
; 		 CubicBezier(start=(3.84375-7.046875j), control1=(3.796875-7.046875j), control2=(3.75-7.03125j), end=(3.703125-7.03125j))
; 		 Line(start=(3.703125-7.03125j), end=(3.703125-7.03125j))
; 		 Line(start=(1.578125-3.265625j), end=(1.578125-3.4375j))
; 		 CubicBezier(start=(1.578125-3.4375j), control1=(1.578125-4.578125j), control2=(1.8125-5.890625j), end=(2.953125-6.546875j))
; 		 CubicBezier(start=(2.953125-6.546875j), control1=(3.234375-6.703125j), control2=(3.546875-6.78125j), end=(3.875-6.78125j))
; 		 CubicBezier(start=(3.875-6.78125j), control1=(4.71875-6.78125j), control2=(5.4375-6.1875j), end=(5.828125-5.390625j))
; 		 CubicBezier(start=(5.828125-5.390625j), control1=(6.09375-4.84375j), control2=(6.171875-4.15625j), end=(6.171875-3.578125j))
; 		 CubicBezier(start=(6.171875-3.578125j), control1=(6.171875-2.265625j), control2=(5.859375-0.625j), end=(4.328125-0.109375j))
; 		 CubicBezier(start=(4.328125-0.109375j), control1=(4.1875-0.0625j), control2=(4.03125-0.046875j), end=(3.890625-0.046875j))
; 		 CubicBezier(start=(3.890625-0.046875j), control1=(2.453125-0.046875j), control2=(1.765625-1.53125j), end=(1.609375-2.734375j))
; 		 CubicBezier(start=(1.609375-2.734375j), control1=(1.59375-2.921875j), control2=(1.59375-3.09375j), end=(1.578125-3.265625j))
; 		 Line(start=(1.578125-3.265625j), end=(1.578125-3.265625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.265625-6.8125j), end=(0.265625-6.515625j))
; 		 Line(start=(0.265625-6.515625j), end=(0.4375-6.515625j))
; 		 CubicBezier(start=(0.4375-6.515625j), control1=(0.75-6.515625j), control2=(1.046875-6.46875j), end=(1.046875-5.984375j))
; 		 Line(start=(1.046875-5.984375j), end=(1.046875-0.953125j))
; 		 CubicBezier(start=(1.046875-0.953125j), control1=(1.046875-0.890625j), control2=(1.0625-0.8125j), end=(1.0625-0.734375j))
; 		 CubicBezier(start=(1.0625-0.734375j), control1=(1.0625-0.359375j), control2=(0.78125-0.3125j), end=(0.515625-0.3125j))
; 		 Line(start=(0.515625-0.3125j), end=(0.265625-0.3125j))
; 		 Line(start=(0.265625-0.3125j), end=(0.265625-0.015625j))
; 		 Line(start=(0.265625-0.015625j), end=(2.46875-0.015625j))
; 		 Line(start=(2.46875-0.015625j), end=(2.46875-0.3125j))
; 		 Line(start=(2.46875-0.3125j), end=(2.234375-0.3125j))
; 		 CubicBezier(start=(2.234375-0.3125j), control1=(1.9375-0.3125j), control2=(1.6875-0.359375j), end=(1.6875-0.734375j))
; 		 Line(start=(1.6875-0.734375j), end=(1.6875-1.671875j))
; 		 CubicBezier(start=(1.6875-1.671875j), control1=(1.6875-1.875j), control2=(2.078125-2.15625j), end=(2.328125-2.328125j))
; 		 CubicBezier(start=(2.328125-2.328125j), control1=(2.40625-2.15625j), control2=(2.546875-2.015625j), end=(2.65625-1.859375j))
; 		 CubicBezier(start=(2.65625-1.859375j), control1=(2.890625-1.53125j), control2=(3.125-1.203125j), end=(3.34375-0.875j))
; 		 CubicBezier(start=(3.34375-0.875j), control1=(3.40625-0.78125j), control2=(3.484375-0.65625j), end=(3.484375-0.53125j))
; 		 CubicBezier(start=(3.484375-0.53125j), control1=(3.484375-0.359375j), control2=(3.296875-0.3125j), end=(3.140625-0.3125j))
; 		 Line(start=(3.140625-0.3125j), end=(3.140625-0.015625j))
; 		 Line(start=(3.140625-0.015625j), end=(5.09375-0.015625j))
; 		 Line(start=(5.09375-0.015625j), end=(5.09375-0.3125j))
; 		 CubicBezier(start=(5.09375-0.3125j), control1=(4.921875-0.3125j), control2=(4.734375-0.3125j), end=(4.578125-0.40625j))
; 		 CubicBezier(start=(4.578125-0.40625j), control1=(4.34375-0.53125j), control2=(4.1875-0.765625j), end=(4.046875-0.984375j))
; 		 CubicBezier(start=(4.046875-0.984375j), control1=(3.796875-1.328125j), control2=(3.546875-1.6875j), end=(3.296875-2.03125j))
; 		 Line(start=(3.296875-2.03125j), end=(2.9375-2.546875j))
; 		 CubicBezier(start=(2.9375-2.546875j), control1=(2.890625-2.59375j), control2=(2.828125-2.6875j), end=(2.828125-2.71875j))
; 		 Line(start=(2.828125-2.71875j), end=(2.828125-2.734375j))
; 		 CubicBezier(start=(2.828125-2.734375j), control1=(2.828125-2.8125j), control2=(3.125-3.03125j), end=(3.265625-3.140625j))
; 		 CubicBezier(start=(3.265625-3.140625j), control1=(3.71875-3.53125j), control2=(4.234375-3.984375j), end=(4.875-3.984375j))
; 		 Line(start=(4.875-3.984375j), end=(4.875-4.296875j))
; 		 Line(start=(4.875-4.296875j), end=(3.0625-4.296875j))
; 		 Line(start=(3.0625-4.296875j), end=(3.0625-3.984375j))
; 		 CubicBezier(start=(3.0625-3.984375j), control1=(3.203125-3.984375j), control2=(3.34375-3.875j), end=(3.34375-3.734375j))
; 		 CubicBezier(start=(3.34375-3.734375j), control1=(3.34375-3.4375j), control2=(2.875-3.140625j), end=(2.609375-2.921875j))
; 		 CubicBezier(start=(2.609375-2.921875j), control1=(2.296875-2.65625j), control2=(2.015625-2.40625j), end=(1.71875-2.15625j))
; 		 Line(start=(1.71875-2.15625j), end=(1.71875-6.921875j))
; 		 Line(start=(1.71875-6.921875j), end=(0.265625-6.8125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.328125-6.78125j), end=(0.328125-6.484375j))
; 		 CubicBezier(start=(0.328125-6.484375j), control1=(0.453125-6.484375j), control2=(0.59375-6.484375j), end=(0.703125-6.484375j))
; 		 CubicBezier(start=(0.703125-6.484375j), control1=(1.0625-6.484375j), control2=(1.359375-6.4375j), end=(1.359375-6.046875j))
; 		 Line(start=(1.359375-6.046875j), end=(1.359375-0.75j))
; 		 CubicBezier(start=(1.359375-0.75j), control1=(1.359375-0.375j), control2=(1.03125-0.3125j), end=(0.6875-0.3125j))
; 		 CubicBezier(start=(0.6875-0.3125j), control1=(0.5625-0.3125j), control2=(0.4375-0.3125j), end=(0.328125-0.3125j))
; 		 Line(start=(0.328125-0.3125j), end=(0.328125-0.015625j))
; 		 Line(start=(0.328125-0.015625j), end=(3.515625-0.015625j))
; 		 Line(start=(3.515625-0.015625j), end=(3.515625-0.3125j))
; 		 CubicBezier(start=(3.515625-0.3125j), control1=(3.375-0.3125j), control2=(3.203125-0.3125j), end=(3.046875-0.3125j))
; 		 CubicBezier(start=(3.046875-0.3125j), control1=(2.546875-0.3125j), control2=(2.25-0.34375j), end=(2.25-0.8125j))
; 		 Line(start=(2.25-0.8125j), end=(2.25-3.25j))
; 		 Line(start=(2.25-3.25j), end=(3.078125-3.25j))
; 		 CubicBezier(start=(3.078125-3.25j), control1=(3.421875-3.25j), control2=(3.828125-3.234375j), end=(4.015625-2.9375j))
; 		 CubicBezier(start=(4.015625-2.9375j), control1=(4.15625-2.71875j), control2=(4.171875-2.4375j), end=(4.171875-2.1875j))
; 		 Line(start=(4.171875-2.1875j), end=(4.171875-2.078125j))
; 		 Line(start=(4.171875-2.078125j), end=(4.421875-2.078125j))
; 		 Line(start=(4.421875-2.078125j), end=(4.421875-4.71875j))
; 		 Line(start=(4.421875-4.71875j), end=(4.171875-4.71875j))
; 		 Line(start=(4.171875-4.71875j), end=(4.171875-4.625j))
; 		 CubicBezier(start=(4.171875-4.625j), control1=(4.171875-4.109375j), control2=(4.0625-3.65625j), end=(3.453125-3.578125j))
; 		 CubicBezier(start=(3.453125-3.578125j), control1=(3.28125-3.5625j), control2=(3.09375-3.5625j), end=(2.90625-3.5625j))
; 		 Line(start=(2.90625-3.5625j), end=(2.25-3.5625j))
; 		 Line(start=(2.25-3.5625j), end=(2.25-6.125j))
; 		 CubicBezier(start=(2.25-6.125j), control1=(2.25-6.453125j), control2=(2.4375-6.484375j), end=(2.65625-6.484375j))
; 		 Line(start=(2.65625-6.484375j), end=(3.8125-6.484375j))
; 		 CubicBezier(start=(3.8125-6.484375j), control1=(4.34375-6.484375j), control2=(4.96875-6.453125j), end=(5.34375-6.078125j))
; 		 CubicBezier(start=(5.34375-6.078125j), control1=(5.71875-5.671875j), control2=(5.765625-5.0625j), end=(5.828125-4.546875j))
; 		 Line(start=(5.828125-4.546875j), end=(6.078125-4.546875j))
; 		 Line(start=(6.078125-4.546875j), end=(5.796875-6.78125j))
; 		 Line(start=(5.796875-6.78125j), end=(0.328125-6.78125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.171875-4.296875j), end=(0.171875-3.984375j))
; 		 Line(start=(0.171875-3.984375j), end=(0.375-3.984375j))
; 		 CubicBezier(start=(0.375-3.984375j), control1=(0.890625-3.984375j), control2=(1.03125-3.78125j), end=(1.25-3.484375j))
; 		 Line(start=(1.25-3.484375j), end=(2.1875-2.265625j))
; 		 CubicBezier(start=(2.1875-2.265625j), control1=(2.234375-2.21875j), control2=(2.3125-2.140625j), end=(2.3125-2.078125j))
; 		 CubicBezier(start=(2.3125-2.078125j), control1=(2.296875-2.03125j), control2=(2.21875-1.953125j), end=(2.1875-1.90625j))
; 		 CubicBezier(start=(2.1875-1.90625j), control1=(1.59375-1.21875j), control2=(1.15625-0.3125j), end=(0.125-0.3125j))
; 		 Line(start=(0.125-0.3125j), end=(0.125-0.015625j))
; 		 Line(start=(0.125-0.015625j), end=(1.90625-0.015625j))
; 		 Line(start=(1.90625-0.015625j), end=(1.90625-0.3125j))
; 		 CubicBezier(start=(1.90625-0.3125j), control1=(1.71875-0.3125j), control2=(1.609375-0.5j), end=(1.609375-0.640625j))
; 		 CubicBezier(start=(1.609375-0.640625j), control1=(1.609375-0.84375j), control2=(1.765625-0.984375j), end=(1.890625-1.125j))
; 		 CubicBezier(start=(1.890625-1.125j), control1=(2-1.28125j), control2=(2.125-1.4375j), end=(2.25-1.578125j))
; 		 CubicBezier(start=(2.25-1.578125j), control1=(2.328125-1.6875j), control2=(2.421875-1.78125j), end=(2.484375-1.890625j))
; 		 CubicBezier(start=(2.484375-1.890625j), control1=(2.734375-1.609375j), control2=(2.9375-1.296875j), end=(3.15625-1j))
; 		 Line(start=(3.15625-1j), end=(3.34375-0.75j))
; 		 CubicBezier(start=(3.34375-0.75j), control1=(3.390625-0.703125j), control2=(3.453125-0.640625j), end=(3.453125-0.5625j))
; 		 CubicBezier(start=(3.453125-0.5625j), control1=(3.453125-0.40625j), control2=(3.234375-0.3125j), end=(3.09375-0.3125j))
; 		 Line(start=(3.09375-0.3125j), end=(3.09375-0.015625j))
; 		 Line(start=(3.09375-0.015625j), end=(5.140625-0.015625j))
; 		 Line(start=(5.140625-0.015625j), end=(5.140625-0.3125j))
; 		 Line(start=(5.140625-0.3125j), end=(4.96875-0.3125j))
; 		 CubicBezier(start=(4.96875-0.3125j), control1=(4.796875-0.3125j), control2=(4.609375-0.328125j), end=(4.453125-0.40625j))
; 		 CubicBezier(start=(4.453125-0.40625j), control1=(4.28125-0.5j), control2=(4.15625-0.6875j), end=(4.03125-0.84375j))
; 		 CubicBezier(start=(4.03125-0.84375j), control1=(3.703125-1.28125j), control2=(3.359375-1.71875j), end=(3.03125-2.15625j))
; 		 Line(start=(3.03125-2.15625j), end=(2.9375-2.265625j))
; 		 CubicBezier(start=(2.9375-2.265625j), control1=(2.921875-2.296875j), control2=(2.90625-2.328125j), end=(2.890625-2.359375j))
; 		 CubicBezier(start=(2.890625-2.359375j), control1=(2.890625-2.40625j), control2=(2.953125-2.46875j), end=(2.984375-2.515625j))
; 		 Line(start=(2.984375-2.515625j), end=(3.296875-2.890625j))
; 		 CubicBezier(start=(3.296875-2.890625j), control1=(3.484375-3.109375j), control2=(3.640625-3.34375j), end=(3.84375-3.546875j))
; 		 CubicBezier(start=(3.84375-3.546875j), control1=(4.15625-3.828125j), control2=(4.546875-3.984375j), end=(4.96875-3.984375j))
; 		 Line(start=(4.96875-3.984375j), end=(4.96875-4.296875j))
; 		 Line(start=(4.96875-4.296875j), end=(3.1875-4.296875j))
; 		 Line(start=(3.1875-4.296875j), end=(3.1875-3.984375j))
; 		 CubicBezier(start=(3.1875-3.984375j), control1=(3.390625-3.9375j), control2=(3.46875-3.84375j), end=(3.46875-3.671875j))
; 		 CubicBezier(start=(3.46875-3.671875j), control1=(3.46875-3.390625j), control2=(3.15625-3.125j), end=(2.984375-2.90625j))
; 		 CubicBezier(start=(2.984375-2.90625j), control1=(2.90625-2.8125j), control2=(2.765625-2.609375j), end=(2.71875-2.609375j))
; 		 CubicBezier(start=(2.71875-2.609375j), control1=(2.65625-2.609375j), control2=(2.515625-2.828125j), end=(2.421875-2.9375j))
; 		 CubicBezier(start=(2.421875-2.9375j), control1=(2.203125-3.21875j), control2=(1.859375-3.578125j), end=(1.859375-3.75j))
; 		 CubicBezier(start=(1.859375-3.75j), control1=(1.859375-3.890625j), control2=(2.078125-3.984375j), end=(2.203125-3.984375j))
; 		 Line(start=(2.203125-3.984375j), end=(2.203125-4.296875j))
; 		 Line(start=(2.203125-4.296875j), end=(0.171875-4.296875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(0.8125-1.203125j), control1=(0.875-1.171875j), control2=(0.921875-1.171875j), end=(0.984375-1.171875j))
; 		 CubicBezier(start=(0.984375-1.171875j), control1=(1.28125-1.171875j), control2=(1.484375-1.375j), end=(1.484375-1.671875j))
; 		 CubicBezier(start=(1.484375-1.671875j), control1=(1.484375-1.9375j), control2=(1.25-2.140625j), end=(1-2.140625j))
; 		 CubicBezier(start=(1-2.140625j), control1=(0.734375-2.140625j), control2=(0.5-1.953125j), end=(0.5-1.640625j))
; 		 CubicBezier(start=(0.5-1.640625j), control1=(0.5-0.609375j), control2=(1.3125+0.203125j), end=(2.328125+0.203125j))
; 		 CubicBezier(start=(2.328125+0.203125j), control1=(3.53125+0.203125j), control2=(4.46875-0.828125j), end=(4.46875-2.03125j))
; 		 CubicBezier(start=(4.46875-2.03125j), control1=(4.46875-3.046875j), control2=(3.78125-4.203125j), end=(2.59375-4.203125j))
; 		 CubicBezier(start=(2.59375-4.203125j), control1=(2.109375-4.203125j), control2=(1.671875-4.015625j), end=(1.3125-3.6875j))
; 		 Line(start=(1.3125-3.6875j), end=(1.3125-5.625j))
; 		 CubicBezier(start=(1.3125-5.625j), control1=(1.5625-5.578125j), control2=(1.796875-5.515625j), end=(2.046875-5.5j))
; 		 Line(start=(2.046875-5.5j), end=(2.15625-5.5j))
; 		 CubicBezier(start=(2.15625-5.5j), control1=(2.859375-5.5j), control2=(3.515625-5.796875j), end=(3.984375-6.34375j))
; 		 CubicBezier(start=(3.984375-6.34375j), control1=(4.03125-6.40625j), control2=(4.09375-6.46875j), end=(4.09375-6.546875j))
; 		 Line(start=(4.09375-6.546875j), end=(4.09375-6.5625j))
; 		 CubicBezier(start=(4.09375-6.5625j), control1=(4.078125-6.609375j), control2=(4.03125-6.65625j), end=(3.984375-6.65625j))
; 		 CubicBezier(start=(3.984375-6.65625j), control1=(3.875-6.65625j), control2=(3.640625-6.515625j), end=(3.453125-6.453125j))
; 		 CubicBezier(start=(3.453125-6.453125j), control1=(3.171875-6.375j), control2=(2.859375-6.328125j), end=(2.546875-6.328125j))
; 		 CubicBezier(start=(2.546875-6.328125j), control1=(2.109375-6.328125j), control2=(1.703125-6.4375j), end=(1.28125-6.578125j))
; 		 CubicBezier(start=(1.28125-6.578125j), control1=(1.21875-6.59375j), control2=(1.171875-6.625j), end=(1.125-6.640625j))
; 		 Line(start=(1.125-6.640625j), end=(1.109375-6.640625j))
; 		 CubicBezier(start=(1.109375-6.640625j), control1=(1.0625-6.640625j), control2=(1.015625-6.59375j), end=(1-6.546875j))
; 		 Line(start=(1-6.546875j), end=(1-3.34375j))
; 		 CubicBezier(start=(1-3.34375j), control1=(1-3.265625j), control2=(1.03125-3.1875j), end=(1.140625-3.1875j))
; 		 CubicBezier(start=(1.140625-3.1875j), control1=(1.25-3.1875j), control2=(1.359375-3.40625j), end=(1.421875-3.453125j))
; 		 CubicBezier(start=(1.421875-3.453125j), control1=(1.6875-3.78125j), control2=(2.125-3.984375j), end=(2.5625-3.984375j))
; 		 Line(start=(2.5625-3.984375j), end=(2.59375-3.984375j))
; 		 CubicBezier(start=(2.59375-3.984375j), control1=(3.453125-3.953125j), control2=(3.59375-2.84375j), end=(3.59375-2.078125j))
; 		 CubicBezier(start=(3.59375-2.078125j), control1=(3.59375-1.40625j), control2=(3.546875-0.5625j), end=(2.78125-0.171875j))
; 		 CubicBezier(start=(2.78125-0.171875j), control1=(2.625-0.09375j), control2=(2.4375-0.0625j), end=(2.265625-0.0625j))
; 		 CubicBezier(start=(2.265625-0.0625j), control1=(1.625-0.0625j), control2=(0.984375-0.5625j), end=(0.8125-1.203125j))
; 		 Line(start=(0.8125-1.203125j), end=(0.8125-1.203125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.859375-3.359375j), end=(1.859375-3.34375j))
; 		 CubicBezier(start=(1.859375-3.34375j), control1=(1.125-3.078125j), control2=(0.421875-2.328125j), end=(0.421875-1.515625j))
; 		 Line(start=(0.421875-1.515625j), end=(0.421875-1.40625j))
; 		 CubicBezier(start=(0.421875-1.40625j), control1=(0.484375-0.421875j), control2=(1.515625+0.203125j), end=(2.46875+0.203125j))
; 		 CubicBezier(start=(2.46875+0.203125j), control1=(3.421875+0.203125j), control2=(4.5625-0.453125j), end=(4.5625-1.640625j))
; 		 CubicBezier(start=(4.5625-1.640625j), control1=(4.5625-2.40625j), control2=(4.171875-2.953125j), end=(3.4375-3.390625j))
; 		 CubicBezier(start=(3.4375-3.390625j), control1=(3.3125-3.46875j), control2=(3.203125-3.5625j), end=(3.078125-3.625j))
; 		 CubicBezier(start=(3.078125-3.625j), control1=(3.671875-4j), control2=(4.28125-4.390625j), end=(4.28125-5.1875j))
; 		 CubicBezier(start=(4.28125-5.1875j), control1=(4.28125-6.046875j), control2=(3.4375-6.65625j), end=(2.5-6.65625j))
; 		 CubicBezier(start=(2.5-6.65625j), control1=(1.609375-6.65625j), control2=(0.6875-6.03125j), end=(0.6875-4.96875j))
; 		 CubicBezier(start=(0.6875-4.96875j), control1=(0.6875-4.21875j), control2=(1.234375-3.703125j), end=(1.859375-3.359375j))
; 		 Line(start=(1.859375-3.359375j), end=(1.859375-3.359375j))
; 		 CubicBezier(start=(0.921875-1.40625j), control1=(0.921875-1.453125j), control2=(0.90625-1.484375j), end=(0.90625-1.53125j))
; 		 CubicBezier(start=(0.90625-1.53125j), control1=(0.90625-2.21875j), control2=(1.40625-2.765625j), end=(1.953125-3.125j))
; 		 CubicBezier(start=(1.953125-3.125j), control1=(2-3.140625j), control2=(2.046875-3.1875j), end=(2.09375-3.1875j))
; 		 CubicBezier(start=(2.09375-3.1875j), control1=(2.1875-3.1875j), control2=(2.5-2.953125j), end=(2.671875-2.828125j))
; 		 CubicBezier(start=(2.671875-2.828125j), control1=(3.0625-2.578125j), control2=(4.0625-2.109375j), end=(4.0625-1.40625j))
; 		 CubicBezier(start=(4.0625-1.40625j), control1=(4.0625-0.5625j), control2=(3.3125-0.0625j), end=(2.515625-0.0625j))
; 		 CubicBezier(start=(2.515625-0.0625j), control1=(1.796875-0.0625j), control2=(1.03125-0.53125j), end=(0.921875-1.40625j))
; 		 Line(start=(0.921875-1.40625j), end=(0.921875-1.40625j))
; 		 Line(start=(1.140625-5.28125j), end=(1.140625-5.375j))
; 		 CubicBezier(start=(1.140625-5.375j), control1=(1.140625-6.046875j), control2=(1.90625-6.40625j), end=(2.453125-6.40625j))
; 		 CubicBezier(start=(2.453125-6.40625j), control1=(3.109375-6.40625j), control2=(3.71875-6.015625j), end=(3.84375-5.265625j))
; 		 CubicBezier(start=(3.84375-5.265625j), control1=(3.84375-5.21875j), control2=(3.84375-5.1875j), end=(3.84375-5.15625j))
; 		 CubicBezier(start=(3.84375-5.15625j), control1=(3.84375-4.578125j), control2=(3.40625-4.140625j), end=(2.96875-3.84375j))
; 		 CubicBezier(start=(2.96875-3.84375j), control1=(2.9375-3.828125j), control2=(2.90625-3.78125j), end=(2.84375-3.78125j))
; 		 CubicBezier(start=(2.84375-3.78125j), control1=(2.765625-3.78125j), control2=(2.5-4j), end=(2.359375-4.09375j))
; 		 CubicBezier(start=(2.359375-4.09375j), control1=(1.84375-4.421875j), control2=(1.203125-4.703125j), end=(1.140625-5.28125j))
; 		 Line(start=(1.140625-5.28125j), end=(1.140625-5.28125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.328125-6.78125j), end=(0.328125-6.484375j))
; 		 CubicBezier(start=(0.328125-6.484375j), control1=(0.453125-6.484375j), control2=(0.59375-6.484375j), end=(0.703125-6.484375j))
; 		 CubicBezier(start=(0.703125-6.484375j), control1=(1.0625-6.484375j), control2=(1.359375-6.4375j), end=(1.359375-6.046875j))
; 		 Line(start=(1.359375-6.046875j), end=(1.359375-0.75j))
; 		 CubicBezier(start=(1.359375-0.75j), control1=(1.359375-0.375j), control2=(1.03125-0.3125j), end=(0.6875-0.3125j))
; 		 CubicBezier(start=(0.6875-0.3125j), control1=(0.5625-0.3125j), control2=(0.4375-0.3125j), end=(0.328125-0.3125j))
; 		 Line(start=(0.328125-0.3125j), end=(0.328125-0.015625j))
; 		 Line(start=(0.328125-0.015625j), end=(6.078125-0.015625j))
; 		 Line(start=(6.078125-0.015625j), end=(6.484375-2.578125j))
; 		 Line(start=(6.484375-2.578125j), end=(6.234375-2.578125j))
; 		 CubicBezier(start=(6.234375-2.578125j), control1=(6.09375-1.671875j), control2=(5.96875-0.671875j), end=(4.890625-0.40625j))
; 		 CubicBezier(start=(4.890625-0.40625j), control1=(4.578125-0.328125j), control2=(4.25-0.3125j), end=(3.9375-0.3125j))
; 		 Line(start=(3.9375-0.3125j), end=(2.65625-0.3125j))
; 		 CubicBezier(start=(2.65625-0.3125j), control1=(2.46875-0.3125j), control2=(2.25-0.34375j), end=(2.25-0.640625j))
; 		 Line(start=(2.25-0.640625j), end=(2.25-3.375j))
; 		 Line(start=(2.25-3.375j), end=(3.15625-3.375j))
; 		 CubicBezier(start=(3.15625-3.375j), control1=(3.515625-3.375j), control2=(3.890625-3.34375j), end=(4.09375-3.046875j))
; 		 CubicBezier(start=(4.09375-3.046875j), control1=(4.203125-2.828125j), control2=(4.21875-2.5625j), end=(4.21875-2.3125j))
; 		 Line(start=(4.21875-2.3125j), end=(4.21875-2.21875j))
; 		 Line(start=(4.21875-2.21875j), end=(4.46875-2.21875j))
; 		 Line(start=(4.46875-2.21875j), end=(4.46875-4.859375j))
; 		 Line(start=(4.46875-4.859375j), end=(4.21875-4.859375j))
; 		 Line(start=(4.21875-4.859375j), end=(4.21875-4.75j))
; 		 CubicBezier(start=(4.21875-4.75j), control1=(4.21875-4.21875j), control2=(4.09375-3.78125j), end=(3.515625-3.703125j))
; 		 CubicBezier(start=(3.515625-3.703125j), control1=(3.3125-3.6875j), control2=(3.125-3.6875j), end=(2.9375-3.6875j))
; 		 Line(start=(2.9375-3.6875j), end=(2.25-3.6875j))
; 		 Line(start=(2.25-3.6875j), end=(2.25-6.125j))
; 		 CubicBezier(start=(2.25-6.125j), control1=(2.25-6.46875j), control2=(2.453125-6.484375j), end=(2.65625-6.484375j))
; 		 Line(start=(2.65625-6.484375j), end=(3.859375-6.484375j))
; 		 CubicBezier(start=(3.859375-6.484375j), control1=(4.453125-6.484375j), control2=(5.171875-6.484375j), end=(5.5625-5.953125j))
; 		 CubicBezier(start=(5.5625-5.953125j), control1=(5.859375-5.546875j), control2=(5.90625-5.03125j), end=(5.96875-4.546875j))
; 		 Line(start=(5.96875-4.546875j), end=(6.21875-4.546875j))
; 		 Line(start=(6.21875-4.546875j), end=(5.9375-6.78125j))
; 		 Line(start=(5.9375-6.78125j), end=(0.328125-6.78125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.546875-6.765625j), end=(0.359375-4.515625j))
; 		 Line(start=(0.359375-4.515625j), end=(0.609375-4.515625j))
; 		 CubicBezier(start=(0.609375-4.515625j), control1=(0.640625-5.03125j), control2=(0.671875-5.734375j), end=(0.984375-6.078125j))
; 		 CubicBezier(start=(0.984375-6.078125j), control1=(1.328125-6.4375j), control2=(1.890625-6.453125j), end=(2.359375-6.453125j))
; 		 Line(start=(2.359375-6.453125j), end=(2.609375-6.453125j))
; 		 CubicBezier(start=(2.609375-6.453125j), control1=(2.875-6.453125j), control2=(3.15625-6.453125j), end=(3.15625-6.125j))
; 		 Line(start=(3.15625-6.125j), end=(3.15625-0.765625j))
; 		 CubicBezier(start=(3.15625-0.765625j), control1=(3.15625-0.375j), control2=(2.75-0.3125j), end=(2.390625-0.3125j))
; 		 CubicBezier(start=(2.390625-0.3125j), control1=(2.265625-0.3125j), control2=(2.140625-0.3125j), end=(2.03125-0.3125j))
; 		 Line(start=(2.03125-0.3125j), end=(1.703125-0.3125j))
; 		 Line(start=(1.703125-0.3125j), end=(1.703125-0.015625j))
; 		 Line(start=(1.703125-0.015625j), end=(5.484375-0.015625j))
; 		 Line(start=(5.484375-0.015625j), end=(5.484375-0.3125j))
; 		 Line(start=(5.484375-0.3125j), end=(4.8125-0.3125j))
; 		 CubicBezier(start=(4.8125-0.3125j), control1=(4.421875-0.3125j), control2=(4.03125-0.359375j), end=(4.03125-0.765625j))
; 		 Line(start=(4.03125-0.765625j), end=(4.03125-6.125j))
; 		 CubicBezier(start=(4.03125-6.125j), control1=(4.03125-6.40625j), control2=(4.203125-6.453125j), end=(4.578125-6.453125j))
; 		 Line(start=(4.578125-6.453125j), end=(4.828125-6.453125j))
; 		 CubicBezier(start=(4.828125-6.453125j), control1=(5.296875-6.453125j), control2=(5.859375-6.4375j), end=(6.203125-6.078125j))
; 		 CubicBezier(start=(6.203125-6.078125j), control1=(6.515625-5.734375j), control2=(6.546875-5.03125j), end=(6.578125-4.515625j))
; 		 Line(start=(6.578125-4.515625j), end=(6.828125-4.515625j))
; 		 Line(start=(6.828125-4.515625j), end=(6.640625-6.765625j))
; 		 Line(start=(6.640625-6.765625j), end=(0.546875-6.765625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.375-6.8125j), end=(0.375-6.515625j))
; 		 CubicBezier(start=(0.375-6.515625j), control1=(0.5-6.515625j), control2=(0.625-6.515625j), end=(0.75-6.515625j))
; 		 CubicBezier(start=(0.75-6.515625j), control1=(1.109375-6.515625j), control2=(1.390625-6.46875j), end=(1.390625-6.078125j))
; 		 Line(start=(1.390625-6.078125j), end=(1.390625-1.03125j))
; 		 CubicBezier(start=(1.390625-1.03125j), control1=(1.390625-0.375j), control2=(0.796875-0.3125j), end=(0.375-0.3125j))
; 		 Line(start=(0.375-0.3125j), end=(0.375-0.015625j))
; 		 Line(start=(0.375-0.015625j), end=(2.703125-0.015625j))
; 		 Line(start=(2.703125-0.015625j), end=(2.703125-0.3125j))
; 		 CubicBezier(start=(2.703125-0.3125j), control1=(2.25-0.3125j), control2=(1.671875-0.375j), end=(1.671875-1.0625j))
; 		 CubicBezier(start=(1.671875-1.0625j), control1=(1.671875-1.15625j), control2=(1.671875-1.25j), end=(1.671875-1.34375j))
; 		 Line(start=(1.671875-1.34375j), end=(1.6875-6.453125j))
; 		 CubicBezier(start=(1.6875-6.453125j), control1=(1.734375-6.234375j), control2=(1.828125-6.046875j), end=(1.90625-5.84375j))
; 		 CubicBezier(start=(1.90625-5.84375j), control1=(2.609375-4j), control2=(3.328125-2.15625j), end=(4.046875-0.34375j))
; 		 CubicBezier(start=(4.046875-0.34375j), control1=(4.09375-0.234375j), control2=(4.140625-0.015625j), end=(4.28125-0.015625j))
; 		 Line(start=(4.28125-0.015625j), end=(4.296875-0.015625j))
; 		 CubicBezier(start=(4.296875-0.015625j), control1=(4.453125-0.03125j), control2=(4.484375-0.265625j), end=(4.53125-0.375j))
; 		 CubicBezier(start=(4.53125-0.375j), control1=(5.25-2.25j), control2=(6-4.125j), end=(6.703125-6j))
; 		 CubicBezier(start=(6.703125-6j), control1=(6.78125-6.171875j), control2=(6.859375-6.34375j), end=(6.90625-6.515625j))
; 		 Line(start=(6.90625-6.515625j), end=(6.921875-0.765625j))
; 		 CubicBezier(start=(6.921875-0.765625j), control1=(6.921875-0.375j), control2=(6.59375-0.3125j), end=(6.28125-0.3125j))
; 		 CubicBezier(start=(6.28125-0.3125j), control1=(6.140625-0.3125j), control2=(6.015625-0.3125j), end=(5.890625-0.3125j))
; 		 Line(start=(5.890625-0.3125j), end=(5.890625-0.015625j))
; 		 Line(start=(5.890625-0.015625j), end=(8.75-0.015625j))
; 		 Line(start=(8.75-0.015625j), end=(8.75-0.3125j))
; 		 CubicBezier(start=(8.75-0.3125j), control1=(8.640625-0.3125j), control2=(8.5-0.3125j), end=(8.390625-0.3125j))
; 		 CubicBezier(start=(8.390625-0.3125j), control1=(8.03125-0.3125j), control2=(7.71875-0.359375j), end=(7.71875-0.75j))
; 		 Line(start=(7.71875-0.75j), end=(7.71875-6.078125j))
; 		 CubicBezier(start=(7.71875-6.078125j), control1=(7.71875-6.453125j), control2=(8.046875-6.515625j), end=(8.390625-6.515625j))
; 		 CubicBezier(start=(8.390625-6.515625j), control1=(8.515625-6.515625j), control2=(8.640625-6.515625j), end=(8.75-6.515625j))
; 		 Line(start=(8.75-6.515625j), end=(8.75-6.8125j))
; 		 Line(start=(8.75-6.8125j), end=(7.015625-6.8125j))
; 		 Line(start=(7.015625-6.8125j), end=(6.828125-6.78125j))
; 		 Line(start=(6.828125-6.78125j), end=(6.734375-6.625j))
; 		 Line(start=(6.734375-6.625j), end=(4.5625-1.015625j))
; 		 Line(start=(4.5625-1.015625j), end=(2.390625-6.625j))
; 		 Line(start=(2.390625-6.625j), end=(2.296875-6.78125j))
; 		 Line(start=(2.296875-6.78125j), end=(2.109375-6.8125j))
; 		 Line(start=(2.109375-6.8125j), end=(0.375-6.8125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.171875-6.8125j), end=(0.171875-6.515625j))
; 		 Line(start=(0.171875-6.515625j), end=(0.390625-6.515625j))
; 		 CubicBezier(start=(0.390625-6.515625j), control1=(1.015625-6.515625j), control2=(1.0625-6.21875j), end=(1.171875-5.84375j))
; 		 CubicBezier(start=(1.171875-5.84375j), control1=(1.796875-3.953125j), control2=(2.390625-2.078125j), end=(3.015625-0.171875j))
; 		 CubicBezier(start=(3.015625-0.171875j), control1=(3.0625-0.046875j), control2=(3.09375+0.203125j), end=(3.28125+0.203125j))
; 		 Line(start=(3.28125+0.203125j), end=(3.296875+0.203125j))
; 		 CubicBezier(start=(3.296875+0.203125j), control1=(3.484375+0.171875j), control2=(3.515625-0.140625j), end=(3.546875-0.21875j))
; 		 CubicBezier(start=(3.546875-0.21875j), control1=(4.046875-1.75j), control2=(4.53125-3.28125j), end=(5.03125-4.796875j))
; 		 CubicBezier(start=(5.03125-4.796875j), control1=(5.0625-4.890625j), control2=(5.09375-4.96875j), end=(5.109375-5.046875j))
; 		 Line(start=(5.109375-5.046875j), end=(5.125-5.046875j))
; 		 CubicBezier(start=(5.125-5.046875j), control1=(5.171875-4.8125j), control2=(5.28125-4.578125j), end=(5.34375-4.359375j))
; 		 CubicBezier(start=(5.34375-4.359375j), control1=(5.796875-2.984375j), control2=(6.234375-1.59375j), end=(6.6875-0.21875j))
; 		 Line(start=(6.6875-0.21875j), end=(6.78125+0.046875j))
; 		 CubicBezier(start=(6.78125+0.046875j), control1=(6.8125+0.125j), control2=(6.84375+0.1875j), end=(6.921875+0.203125j))
; 		 Line(start=(6.921875+0.203125j), end=(6.953125+0.203125j))
; 		 CubicBezier(start=(6.953125+0.203125j), control1=(7.140625+0.203125j), control2=(7.171875-0.03125j), end=(7.21875-0.171875j))
; 		 CubicBezier(start=(7.21875-0.171875j), control1=(7.84375-2.015625j), control2=(8.40625-3.875j), end=(9.03125-5.71875j))
; 		 CubicBezier(start=(9.03125-5.71875j), control1=(9.15625-6.125j), control2=(9.359375-6.515625j), end=(10.0625-6.515625j))
; 		 Line(start=(10.0625-6.515625j), end=(10.0625-6.8125j))
; 		 Line(start=(10.0625-6.8125j), end=(7.96875-6.8125j))
; 		 Line(start=(7.96875-6.8125j), end=(7.96875-6.515625j))
; 		 CubicBezier(start=(7.96875-6.515625j), control1=(8.3125-6.515625j), control2=(8.796875-6.375j), end=(8.828125-5.984375j))
; 		 CubicBezier(start=(8.828125-5.984375j), control1=(8.828125-5.9375j), control2=(8.8125-5.859375j), end=(8.796875-5.796875j))
; 		 Line(start=(8.796875-5.796875j), end=(8.703125-5.546875j))
; 		 CubicBezier(start=(8.703125-5.546875j), control1=(8.53125-4.984375j), control2=(8.34375-4.421875j), end=(8.15625-3.859375j))
; 		 Line(start=(8.15625-3.859375j), end=(7.5625-2.015625j))
; 		 CubicBezier(start=(7.5625-2.015625j), control1=(7.46875-1.734375j), control2=(7.359375-1.46875j), end=(7.28125-1.1875j))
; 		 CubicBezier(start=(7.28125-1.1875j), control1=(7.140625-1.53125j), control2=(7.046875-1.90625j), end=(6.9375-2.25j))
; 		 CubicBezier(start=(6.9375-2.25j), control1=(6.546875-3.453125j), control2=(6.171875-4.65625j), end=(5.765625-5.859375j))
; 		 CubicBezier(start=(5.765625-5.859375j), control1=(5.734375-5.96875j), control2=(5.65625-6.09375j), end=(5.65625-6.203125j))
; 		 CubicBezier(start=(5.65625-6.203125j), control1=(5.65625-6.515625j), control2=(6.21875-6.515625j), end=(6.5-6.515625j))
; 		 Line(start=(6.5-6.515625j), end=(6.5-6.8125j))
; 		 Line(start=(6.5-6.8125j), end=(3.875-6.8125j))
; 		 Line(start=(3.875-6.8125j), end=(3.875-6.515625j))
; 		 Line(start=(3.875-6.515625j), end=(4.140625-6.515625j))
; 		 CubicBezier(start=(4.140625-6.515625j), control1=(4.578125-6.515625j), control2=(4.671875-6.40625j), end=(4.90625-5.71875j))
; 		 CubicBezier(start=(4.90625-5.71875j), control1=(4.921875-5.65625j), control2=(4.96875-5.546875j), end=(4.96875-5.46875j))
; 		 Line(start=(4.96875-5.46875j), end=(4.96875-5.4375j))
; 		 CubicBezier(start=(4.96875-5.4375j), control1=(4.96875-5.3125j), control2=(4.875-5.125j), end=(4.828125-4.984375j))
; 		 CubicBezier(start=(4.828125-4.984375j), control1=(4.5-4.0625j), control2=(4.21875-3.125j), end=(3.921875-2.1875j))
; 		 CubicBezier(start=(3.921875-2.1875j), control1=(3.8125-1.859375j), control2=(3.6875-1.53125j), end=(3.59375-1.1875j))
; 		 CubicBezier(start=(3.59375-1.1875j), control1=(3.5625-1.3125j), control2=(3.515625-1.421875j), end=(3.484375-1.53125j))
; 		 Line(start=(3.484375-1.53125j), end=(2.40625-4.859375j))
; 		 CubicBezier(start=(2.40625-4.859375j), control1=(2.25-5.28125j), control2=(2.140625-5.6875j), end=(2-6.09375j))
; 		 CubicBezier(start=(2-6.09375j), control1=(1.984375-6.125j), control2=(1.96875-6.1875j), end=(1.96875-6.21875j))
; 		 Line(start=(1.96875-6.21875j), end=(1.96875-6.25j))
; 		 CubicBezier(start=(1.96875-6.25j), control1=(2.046875-6.515625j), control2=(2.515625-6.515625j), end=(2.8125-6.515625j))
; 		 Line(start=(2.8125-6.515625j), end=(2.8125-6.8125j))
; 		 Line(start=(2.8125-6.8125j), end=(0.171875-6.8125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.28125-1.65625j), end=(2.9375-1.65625j))
; 		 Line(start=(2.9375-1.65625j), end=(2.9375-0.75j))
; 		 CubicBezier(start=(2.9375-0.75j), control1=(2.9375-0.34375j), control2=(2.625-0.3125j), end=(2.109375-0.3125j))
; 		 Line(start=(2.109375-0.3125j), end=(1.96875-0.3125j))
; 		 Line(start=(1.96875-0.3125j), end=(1.96875-0.015625j))
; 		 Line(start=(1.96875-0.015625j), end=(4.671875-0.015625j))
; 		 Line(start=(4.671875-0.015625j), end=(4.671875-0.3125j))
; 		 CubicBezier(start=(4.671875-0.3125j), control1=(4.546875-0.3125j), control2=(4.421875-0.3125j), end=(4.296875-0.3125j))
; 		 CubicBezier(start=(4.296875-0.3125j), control1=(3.984375-0.3125j), control2=(3.703125-0.375j), end=(3.703125-0.75j))
; 		 Line(start=(3.703125-0.75j), end=(3.703125-1.65625j))
; 		 Line(start=(3.703125-1.65625j), end=(4.6875-1.65625j))
; 		 Line(start=(4.6875-1.65625j), end=(4.6875-1.96875j))
; 		 Line(start=(4.6875-1.96875j), end=(3.703125-1.96875j))
; 		 Line(start=(3.703125-1.96875j), end=(3.703125-6.4375j))
; 		 CubicBezier(start=(3.703125-6.4375j), control1=(3.703125-6.46875j), control2=(3.71875-6.515625j), end=(3.71875-6.5625j))
; 		 CubicBezier(start=(3.71875-6.5625j), control1=(3.71875-6.671875j), control2=(3.6875-6.765625j), end=(3.5625-6.765625j))
; 		 CubicBezier(start=(3.5625-6.765625j), control1=(3.40625-6.765625j), control2=(3.375-6.6875j), end=(3.3125-6.578125j))
; 		 CubicBezier(start=(3.3125-6.578125j), control1=(2.375-5.15625j), control2=(1.4375-3.734375j), end=(0.515625-2.328125j))
; 		 CubicBezier(start=(0.515625-2.328125j), control1=(0.390625-2.140625j), control2=(0.265625-2j), end=(0.265625-1.828125j))
; 		 CubicBezier(start=(0.265625-1.828125j), control1=(0.265625-1.78125j), control2=(0.28125-1.71875j), end=(0.28125-1.65625j))
; 		 Line(start=(0.28125-1.65625j), end=(0.28125-1.65625j))
; 		 Line(start=(0.5625-1.96875j), end=(2.984375-5.65625j))
; 		 Line(start=(2.984375-5.65625j), end=(2.984375-1.96875j))
; 		 Line(start=(2.984375-1.96875j), end=(0.5625-1.96875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.09375-5.78125j), control1=(1.421875-6.25j), control2=(1.96875-6.40625j), end=(2.5-6.40625j))
; 		 CubicBezier(start=(2.5-6.40625j), control1=(3.09375-6.40625j), control2=(3.359375-5.796875j), end=(3.359375-5.296875j))
; 		 CubicBezier(start=(3.359375-5.296875j), control1=(3.359375-4.671875j), control2=(3.15625-3.859375j), end=(2.484375-3.65625j))
; 		 CubicBezier(start=(2.484375-3.65625j), control1=(2.25-3.59375j), control2=(1.671875-3.671875j), end=(1.671875-3.484375j))
; 		 Line(start=(1.671875-3.484375j), end=(1.671875-3.453125j))
; 		 CubicBezier(start=(1.671875-3.453125j), control1=(1.671875-3.375j), control2=(1.75-3.375j), end=(1.828125-3.375j))
; 		 Line(start=(1.828125-3.375j), end=(1.9375-3.375j))
; 		 CubicBezier(start=(1.9375-3.375j), control1=(2.046875-3.375j), control2=(2.171875-3.375j), end=(2.296875-3.375j))
; 		 CubicBezier(start=(2.296875-3.375j), control1=(3.265625-3.375j), control2=(3.53125-2.453125j), end=(3.53125-1.734375j))
; 		 Line(start=(3.53125-1.734375j), end=(3.53125-1.578125j))
; 		 CubicBezier(start=(3.53125-1.578125j), control1=(3.515625-0.953125j), control2=(3.234375-0.0625j), end=(2.375-0.0625j))
; 		 CubicBezier(start=(2.375-0.0625j), control1=(1.828125-0.0625j), control2=(1.171875-0.328125j), end=(0.875-0.84375j))
; 		 CubicBezier(start=(0.875-0.84375j), control1=(0.90625-0.84375j), control2=(0.9375-0.828125j), end=(0.984375-0.828125j))
; 		 CubicBezier(start=(0.984375-0.828125j), control1=(1.296875-0.828125j), control2=(1.53125-1.078125j), end=(1.53125-1.390625j))
; 		 CubicBezier(start=(1.53125-1.390625j), control1=(1.53125-1.671875j), control2=(1.328125-1.890625j), end=(1.0625-1.9375j))
; 		 Line(start=(1.0625-1.9375j), end=(1-1.9375j))
; 		 CubicBezier(start=(1-1.9375j), control1=(0.59375-1.9375j), control2=(0.421875-1.65625j), end=(0.421875-1.34375j))
; 		 CubicBezier(start=(0.421875-1.34375j), control1=(0.421875-0.296875j), control2=(1.546875+0.203125j), end=(2.421875+0.203125j))
; 		 CubicBezier(start=(2.421875+0.203125j), control1=(3.5+0.203125j), control2=(4.5625-0.578125j), end=(4.5625-1.71875j))
; 		 CubicBezier(start=(4.5625-1.71875j), control1=(4.5625-2.65625j), control2=(3.765625-3.359375j), end=(2.875-3.515625j))
; 		 CubicBezier(start=(2.875-3.515625j), control1=(3.609375-3.78125j), control2=(4.28125-4.390625j), end=(4.28125-5.25j))
; 		 CubicBezier(start=(4.28125-5.25j), control1=(4.28125-6.125j), control2=(3.375-6.65625j), end=(2.4375-6.65625j))
; 		 CubicBezier(start=(2.4375-6.65625j), control1=(1.671875-6.65625j), control2=(0.6875-6.234375j), end=(0.6875-5.28125j))
; 		 CubicBezier(start=(0.6875-5.28125j), control1=(0.6875-4.96875j), control2=(0.921875-4.78125j), end=(1.203125-4.78125j))
; 		 CubicBezier(start=(1.203125-4.78125j), control1=(1.46875-4.78125j), control2=(1.6875-4.96875j), end=(1.6875-5.28125j))
; 		 CubicBezier(start=(1.6875-5.28125j), control1=(1.6875-5.578125j), control2=(1.46875-5.78125j), end=(1.171875-5.78125j))
; 		 Line(start=(1.171875-5.78125j), end=(1.09375-5.78125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.875-6.75j), end=(0.546875-4.6875j))
; 		 Line(start=(0.546875-4.6875j), end=(0.796875-4.6875j))
; 		 CubicBezier(start=(0.796875-4.6875j), control1=(0.875-5.109375j), control2=(0.859375-5.625j), end=(1.28125-5.625j))
; 		 Line(start=(1.28125-5.625j), end=(1.546875-5.625j))
; 		 CubicBezier(start=(1.546875-5.625j), control1=(1.78125-5.625j), control2=(2.015625-5.65625j), end=(2.25-5.65625j))
; 		 Line(start=(2.25-5.65625j), end=(4.09375-5.65625j))
; 		 CubicBezier(start=(4.09375-5.65625j), control1=(3.5625-4.9375j), control2=(3-4.203125j), end=(2.578125-3.421875j))
; 		 CubicBezier(start=(2.578125-3.421875j), control1=(2.09375-2.5j), control2=(1.75-1.359375j), end=(1.75-0.3125j))
; 		 CubicBezier(start=(1.75-0.3125j), control1=(1.75-0.015625j), control2=(1.9375+0.203125j), end=(2.21875+0.203125j))
; 		 Line(start=(2.21875+0.203125j), end=(2.25+0.203125j))
; 		 CubicBezier(start=(2.25+0.203125j), control1=(2.5+0.203125j), control2=(2.671875-0.015625j), end=(2.671875-0.296875j))
; 		 Line(start=(2.671875-0.296875j), end=(2.671875-0.484375j))
; 		 CubicBezier(start=(2.671875-0.484375j), control1=(2.671875-1.625j), control2=(2.6875-3.046875j), end=(3.34375-4.09375j))
; 		 CubicBezier(start=(3.34375-4.09375j), control1=(3.46875-4.28125j), control2=(3.609375-4.46875j), end=(3.75-4.671875j))
; 		 Line(start=(3.75-4.671875j), end=(4.6875-5.984375j))
; 		 CubicBezier(start=(4.6875-5.984375j), control1=(4.765625-6.09375j), control2=(4.828125-6.140625j), end=(4.828125-6.3125j))
; 		 Line(start=(4.828125-6.3125j), end=(4.828125-6.4375j))
; 		 CubicBezier(start=(4.828125-6.4375j), control1=(4-6.4375j), control2=(3.375-6.40625j), end=(2.328125-6.40625j))
; 		 CubicBezier(start=(2.328125-6.40625j), control1=(1.75-6.40625j), control2=(1.15625-6.453125j), end=(1.125-6.75j))
; 		 Line(start=(1.125-6.75j), end=(0.875-6.75j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.328125-3.296875j), end=(1.3125-3.296875j))
; 		 CubicBezier(start=(1.3125-3.296875j), control1=(1.3125-4.1875j), control2=(1.359375-5.203125j), end=(2-5.890625j))
; 		 CubicBezier(start=(2-5.890625j), control1=(2.28125-6.1875j), control2=(2.640625-6.40625j), end=(3.078125-6.40625j))
; 		 CubicBezier(start=(3.078125-6.40625j), control1=(3.40625-6.40625j), control2=(3.734375-6.265625j), end=(3.9375-6j))
; 		 CubicBezier(start=(3.9375-6j), control1=(3.90625-6.015625j), control2=(3.875-6.015625j), end=(3.84375-6.015625j))
; 		 CubicBezier(start=(3.84375-6.015625j), control1=(3.578125-6.015625j), control2=(3.390625-5.8125j), end=(3.390625-5.546875j))
; 		 CubicBezier(start=(3.390625-5.546875j), control1=(3.390625-5.296875j), control2=(3.59375-5.09375j), end=(3.84375-5.09375j))
; 		 CubicBezier(start=(3.84375-5.09375j), control1=(4.109375-5.09375j), control2=(4.3125-5.28125j), end=(4.3125-5.609375j))
; 		 CubicBezier(start=(4.3125-5.609375j), control1=(4.3125-6.34375j), control2=(3.625-6.65625j), end=(3.03125-6.65625j))
; 		 CubicBezier(start=(3.03125-6.65625j), control1=(1.359375-6.65625j), control2=(0.421875-4.765625j), end=(0.421875-3.265625j))
; 		 CubicBezier(start=(0.421875-3.265625j), control1=(0.421875-1.890625j), control2=(0.703125+0.203125j), end=(2.546875+0.203125j))
; 		 CubicBezier(start=(2.546875+0.203125j), control1=(3.765625+0.203125j), control2=(4.5625-0.921875j), end=(4.5625-2.046875j))
; 		 CubicBezier(start=(4.5625-2.046875j), control1=(4.5625-3.09375j), control2=(3.84375-4.25j), end=(2.65625-4.25j))
; 		 CubicBezier(start=(2.65625-4.25j), control1=(2.078125-4.25j), control2=(1.5-3.953125j), end=(1.328125-3.296875j))
; 		 Line(start=(1.328125-3.296875j), end=(1.328125-3.296875j))
; 		 Line(start=(1.328125-2.125j), end=(1.328125-2.21875j))
; 		 CubicBezier(start=(1.328125-2.21875j), control1=(1.328125-2.90625j), control2=(1.578125-4.03125j), end=(2.5625-4.03125j))
; 		 CubicBezier(start=(2.5625-4.03125j), control1=(3.65625-4.03125j), control2=(3.65625-2.71875j), end=(3.65625-2.109375j))
; 		 Line(start=(3.65625-2.109375j), end=(3.65625-1.921875j))
; 		 CubicBezier(start=(3.65625-1.921875j), control1=(3.65625-1.25j), control2=(3.59375-0.0625j), end=(2.515625-0.0625j))
; 		 CubicBezier(start=(2.515625-0.0625j), control1=(1.65625-0.0625j), control2=(1.390625-1.03125j), end=(1.34375-1.8125j))
; 		 CubicBezier(start=(1.34375-1.8125j), control1=(1.34375-1.90625j), control2=(1.34375-2.03125j), end=(1.328125-2.125j))
; 		 Line(start=(1.328125-2.125j), end=(1.328125-2.125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.109375-2.4375j), end=(0.109375-1.859375j))
; 		 Line(start=(0.109375-1.859375j), end=(2.765625-1.859375j))
; 		 Line(start=(2.765625-1.859375j), end=(2.765625-2.4375j))
; 		 Line(start=(2.765625-2.4375j), end=(0.109375-2.4375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.234375-8.5j), end=(1.234375-7.9375j))
; 		 CubicBezier(start=(1.234375-7.9375j), control1=(1.921875-7.9375j), control2=(2.625-7.984375j), end=(3.28125-8.203125j))
; 		 Line(start=(3.28125-8.203125j), end=(3.28125-0.59375j))
; 		 Line(start=(3.28125-0.59375j), end=(1.3125-0.59375j))
; 		 Line(start=(1.3125-0.59375j), end=(1.3125-0.015625j))
; 		 Line(start=(1.3125-0.015625j), end=(6.78125-0.015625j))
; 		 Line(start=(6.78125-0.015625j), end=(6.78125-0.59375j))
; 		 Line(start=(6.78125-0.59375j), end=(4.84375-0.59375j))
; 		 Line(start=(4.84375-0.59375j), end=(4.84375-8.859375j))
; 		 CubicBezier(start=(4.84375-8.859375j), control1=(4.84375-8.921875j), control2=(4.859375-9.015625j), end=(4.859375-9.109375j))
; 		 CubicBezier(start=(4.859375-9.109375j), control1=(4.859375-9.3125j), control2=(4.796875-9.421875j), end=(4.578125-9.421875j))
; 		 CubicBezier(start=(4.578125-9.421875j), control1=(4.296875-9.421875j), control2=(4.015625-9.09375j), end=(3.71875-8.953125j))
; 		 CubicBezier(start=(3.71875-8.953125j), control1=(2.9375-8.59375j), control2=(2.078125-8.5j), end=(1.234375-8.5j))
; 		 Line(start=(1.234375-8.5j), end=(1.234375-8.5j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.4375-9.859375j), end=(0.4375-9.28125j))
; 		 Line(start=(0.4375-9.28125j), end=(2.015625-9.28125j))
; 		 Line(start=(2.015625-9.28125j), end=(2.015625-0.59375j))
; 		 Line(start=(2.015625-0.59375j), end=(0.4375-0.59375j))
; 		 Line(start=(0.4375-0.59375j), end=(0.4375-0.015625j))
; 		 Line(start=(0.4375-0.015625j), end=(5.375-0.015625j))
; 		 Line(start=(5.375-0.015625j), end=(5.375-0.59375j))
; 		 Line(start=(5.375-0.59375j), end=(3.796875-0.59375j))
; 		 Line(start=(3.796875-0.59375j), end=(3.796875-9.28125j))
; 		 Line(start=(3.796875-9.28125j), end=(5.375-9.28125j))
; 		 Line(start=(5.375-9.28125j), end=(5.375-9.859375j))
; 		 Line(start=(5.375-9.859375j), end=(0.4375-9.859375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.546875-6.359375j), end=(0.546875-5.78125j))
; 		 Line(start=(0.546875-5.78125j), end=(0.828125-5.78125j))
; 		 CubicBezier(start=(0.828125-5.78125j), control1=(1.203125-5.78125j), control2=(1.546875-5.734375j), end=(1.546875-5.296875j))
; 		 Line(start=(1.546875-5.296875j), end=(1.546875-0.59375j))
; 		 Line(start=(1.546875-0.59375j), end=(0.546875-0.59375j))
; 		 Line(start=(0.546875-0.59375j), end=(0.546875-0.015625j))
; 		 Line(start=(0.546875-0.015625j), end=(4.0625-0.015625j))
; 		 Line(start=(4.0625-0.015625j), end=(4.0625-0.59375j))
; 		 Line(start=(4.0625-0.59375j), end=(3.0625-0.59375j))
; 		 Line(start=(3.0625-0.59375j), end=(3.0625-3.5j))
; 		 CubicBezier(start=(3.0625-3.5j), control1=(3.0625-4.65625j), control2=(3.65625-6.03125j), end=(5.046875-6.03125j))
; 		 CubicBezier(start=(5.046875-6.03125j), control1=(5.875-6.03125j), control2=(5.921875-5.09375j), end=(5.921875-4.28125j))
; 		 Line(start=(5.921875-4.28125j), end=(5.921875-0.59375j))
; 		 Line(start=(5.921875-0.59375j), end=(4.921875-0.59375j))
; 		 Line(start=(4.921875-0.59375j), end=(4.921875-0.015625j))
; 		 Line(start=(4.921875-0.015625j), end=(8.4375-0.015625j))
; 		 Line(start=(8.4375-0.015625j), end=(8.4375-0.59375j))
; 		 Line(start=(8.4375-0.59375j), end=(7.4375-0.59375j))
; 		 Line(start=(7.4375-0.59375j), end=(7.4375-4.3125j))
; 		 CubicBezier(start=(7.4375-4.3125j), control1=(7.4375-4.8125j), control2=(7.40625-5.296875j), end=(7.125-5.71875j))
; 		 CubicBezier(start=(7.125-5.71875j), control1=(6.734375-6.359375j), control2=(5.953125-6.46875j), end=(5.28125-6.46875j))
; 		 CubicBezier(start=(5.28125-6.46875j), control1=(4.25-6.46875j), control2=(3.234375-5.8125j), end=(2.953125-4.8125j))
; 		 Line(start=(2.953125-4.8125j), end=(2.9375-6.46875j))
; 		 Line(start=(2.9375-6.46875j), end=(1.34375-6.46875j))
; 		 Line(start=(1.34375-6.46875j), end=(0.546875-6.359375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(2.390625-9.125j), control1=(2.390625-8j), control2=(1.828125-6.25j), end=(0.28125-6.25j))
; 		 Line(start=(0.28125-6.25j), end=(0.28125-5.8125j))
; 		 Line(start=(0.28125-5.8125j), end=(1.4375-5.8125j))
; 		 Line(start=(1.4375-5.8125j), end=(1.4375-1.9375j))
; 		 CubicBezier(start=(1.4375-1.9375j), control1=(1.4375-1.5j), control2=(1.46875-1.078125j), end=(1.703125-0.703125j))
; 		 CubicBezier(start=(1.703125-0.703125j), control1=(2.125-0.078125j), control2=(2.921875+0.078125j), end=(3.59375+0.078125j))
; 		 CubicBezier(start=(3.59375+0.078125j), control1=(4.71875+0.078125j), control2=(5.25-0.859375j), end=(5.25-1.984375j))
; 		 Line(start=(5.25-1.984375j), end=(5.25-2.546875j))
; 		 Line(start=(5.25-2.546875j), end=(4.703125-2.546875j))
; 		 CubicBezier(start=(4.703125-2.546875j), control1=(4.703125-2.359375j), control2=(4.71875-2.15625j), end=(4.71875-1.9375j))
; 		 CubicBezier(start=(4.71875-1.9375j), control1=(4.71875-1.359375j), control2=(4.578125-0.421875j), end=(3.796875-0.421875j))
; 		 CubicBezier(start=(3.796875-0.421875j), control1=(3.078125-0.421875j), control2=(2.9375-1.171875j), end=(2.9375-1.84375j))
; 		 Line(start=(2.9375-1.84375j), end=(2.9375-5.8125j))
; 		 Line(start=(2.9375-5.8125j), end=(4.984375-5.8125j))
; 		 Line(start=(4.984375-5.8125j), end=(4.984375-6.390625j))
; 		 Line(start=(4.984375-6.390625j), end=(2.9375-6.390625j))
; 		 Line(start=(2.9375-6.390625j), end=(2.9375-9.125j))
; 		 Line(start=(2.9375-9.125j), end=(2.390625-9.125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.453125-6.359375j), end=(0.453125-5.78125j))
; 		 Line(start=(0.453125-5.78125j), end=(0.75-5.78125j))
; 		 CubicBezier(start=(0.75-5.78125j), control1=(1.125-5.78125j), control2=(1.46875-5.734375j), end=(1.46875-5.296875j))
; 		 Line(start=(1.46875-5.296875j), end=(1.46875-0.59375j))
; 		 Line(start=(1.46875-0.59375j), end=(0.453125-0.59375j))
; 		 Line(start=(0.453125-0.59375j), end=(0.453125-0.015625j))
; 		 Line(start=(0.453125-0.015625j), end=(4.15625-0.015625j))
; 		 Line(start=(4.15625-0.015625j), end=(4.15625-0.59375j))
; 		 Line(start=(4.15625-0.59375j), end=(2.90625-0.59375j))
; 		 Line(start=(2.90625-0.59375j), end=(2.90625-2.65625j))
; 		 CubicBezier(start=(2.90625-2.65625j), control1=(2.90625-3.9375j), control2=(2.953125-6.03125j), end=(4.65625-6.03125j))
; 		 CubicBezier(start=(4.65625-6.03125j), control1=(4.46875-5.84375j), control2=(4.359375-5.65625j), end=(4.359375-5.390625j))
; 		 CubicBezier(start=(4.359375-5.390625j), control1=(4.359375-4.875j), control2=(4.734375-4.578125j), end=(5.15625-4.578125j))
; 		 CubicBezier(start=(5.15625-4.578125j), control1=(5.640625-4.578125j), control2=(5.953125-4.953125j), end=(5.953125-5.359375j))
; 		 CubicBezier(start=(5.953125-5.359375j), control1=(5.953125-6.078125j), control2=(5.203125-6.46875j), end=(4.578125-6.46875j))
; 		 CubicBezier(start=(4.578125-6.46875j), control1=(3.6875-6.46875j), control2=(2.9375-5.65625j), end=(2.8125-4.796875j))
; 		 Line(start=(2.8125-4.796875j), end=(2.796875-6.46875j))
; 		 Line(start=(2.796875-6.46875j), end=(1.25-6.390625j))
; 		 Line(start=(1.25-6.390625j), end=(0.453125-6.359375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(3.671875-6.515625j), control1=(1.953125-6.328125j), control2=(0.4375-5.375j), end=(0.4375-3.15625j))
; 		 CubicBezier(start=(0.4375-3.15625j), control1=(0.4375-2.1875j), control2=(0.796875-1.234375j), end=(1.59375-0.625j))
; 		 CubicBezier(start=(1.59375-0.625j), control1=(2.25-0.125j), control2=(3.109375+0.078125j), end=(3.9375+0.078125j))
; 		 CubicBezier(start=(3.9375+0.078125j), control1=(5.6875+0.078125j), control2=(7.4375-0.890625j), end=(7.4375-3.1875j))
; 		 CubicBezier(start=(7.4375-3.1875j), control1=(7.4375-3.96875j), control2=(7.1875-4.8125j), end=(6.65625-5.40625j))
; 		 CubicBezier(start=(6.65625-5.40625j), control1=(5.953125-6.21875j), control2=(4.875-6.515625j), end=(3.84375-6.515625j))
; 		 Line(start=(3.84375-6.515625j), end=(3.671875-6.515625j))
; 		 Line(start=(2.078125-2.71875j), end=(2.078125-3.46875j))
; 		 CubicBezier(start=(2.078125-3.46875j), control1=(2.078125-4.203125j), control2=(2.09375-5.046875j), end=(2.65625-5.59375j))
; 		 CubicBezier(start=(2.65625-5.59375j), control1=(2.984375-5.921875j), control2=(3.453125-6.078125j), end=(3.921875-6.078125j))
; 		 CubicBezier(start=(3.921875-6.078125j), control1=(4.96875-6.078125j), control2=(5.65625-5.390625j), end=(5.765625-4.296875j))
; 		 CubicBezier(start=(5.765625-4.296875j), control1=(5.796875-4.015625j), control2=(5.8125-3.734375j), end=(5.8125-3.453125j))
; 		 Line(start=(5.8125-3.453125j), end=(5.8125-3.109375j))
; 		 CubicBezier(start=(5.8125-3.109375j), control1=(5.8125-2.40625j), control2=(5.78125-1.59375j), end=(5.34375-1.046875j))
; 		 CubicBezier(start=(5.34375-1.046875j), control1=(5-0.609375j), control2=(4.46875-0.421875j), end=(3.9375-0.421875j))
; 		 CubicBezier(start=(3.9375-0.421875j), control1=(3.40625-0.421875j), control2=(2.84375-0.625j), end=(2.515625-1.078125j))
; 		 CubicBezier(start=(2.515625-1.078125j), control1=(2.140625-1.546875j), control2=(2.140625-2.15625j), end=(2.078125-2.71875j))
; 		 Line(start=(2.078125-2.71875j), end=(2.078125-2.71875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(4.828125-9.859375j), end=(4.828125-9.28125j))
; 		 Line(start=(4.828125-9.28125j), end=(5.1875-9.28125j))
; 		 CubicBezier(start=(5.1875-9.28125j), control1=(5.53125-9.28125j), control2=(5.84375-9.234375j), end=(5.84375-8.796875j))
; 		 Line(start=(5.84375-8.796875j), end=(5.84375-5.765625j))
; 		 CubicBezier(start=(5.84375-5.765625j), control1=(5.296875-6.21875j), control2=(4.625-6.46875j), end=(3.921875-6.46875j))
; 		 CubicBezier(start=(3.921875-6.46875j), control1=(2.140625-6.46875j), control2=(0.53125-5.328125j), end=(0.53125-3.203125j))
; 		 CubicBezier(start=(0.53125-3.203125j), control1=(0.53125-2.453125j), control2=(0.71875-1.703125j), end=(1.171875-1.09375j))
; 		 CubicBezier(start=(1.171875-1.09375j), control1=(1.765625-0.328125j), control2=(2.734375+0.078125j), end=(3.703125+0.078125j))
; 		 CubicBezier(start=(3.703125+0.078125j), control1=(4.453125+0.078125j), control2=(5.21875-0.171875j), end=(5.78125-0.703125j))
; 		 Line(start=(5.78125-0.703125j), end=(5.78125+0.078125j))
; 		 Line(start=(5.78125+0.078125j), end=(8.296875+0.078125j))
; 		 Line(start=(8.296875+0.078125j), end=(8.296875-0.59375j))
; 		 CubicBezier(start=(8.296875-0.59375j), control1=(8.171875-0.59375j), control2=(8.03125-0.578125j), end=(7.90625-0.578125j))
; 		 CubicBezier(start=(7.90625-0.578125j), control1=(7.59375-0.578125j), control2=(7.28125-0.640625j), end=(7.28125-1.09375j))
; 		 Line(start=(7.28125-1.09375j), end=(7.28125-9.96875j))
; 		 Line(start=(7.28125-9.96875j), end=(5.671875-9.96875j))
; 		 Line(start=(5.671875-9.96875j), end=(4.828125-9.859375j))
; 		 Line(start=(2.171875-2.625j), end=(2.171875-3.359375j))
; 		 CubicBezier(start=(2.171875-3.359375j), control1=(2.171875-4.4375j), control2=(2.375-6.03125j), end=(4.09375-6.03125j))
; 		 CubicBezier(start=(4.09375-6.03125j), control1=(4.703125-6.03125j), control2=(5.21875-5.765625j), end=(5.65625-5.28125j))
; 		 CubicBezier(start=(5.65625-5.28125j), control1=(5.703125-5.21875j), control2=(5.765625-5.15625j), end=(5.78125-5.0625j))
; 		 Line(start=(5.78125-5.0625j), end=(5.78125-1.625j))
; 		 CubicBezier(start=(5.78125-1.625j), control1=(5.78125-1.53125j), control2=(5.78125-1.453125j), end=(5.71875-1.359375j))
; 		 CubicBezier(start=(5.71875-1.359375j), control1=(5.328125-0.78125j), control2=(4.640625-0.375j), end=(3.9375-0.375j))
; 		 CubicBezier(start=(3.9375-0.375j), control1=(2.921875-0.375j), control2=(2.328125-1.1875j), end=(2.1875-2.203125j))
; 		 CubicBezier(start=(2.1875-2.203125j), control1=(2.1875-2.359375j), control2=(2.1875-2.484375j), end=(2.171875-2.625j))
; 		 Line(start=(2.171875-2.625j), end=(2.171875-2.625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.546875-6.359375j), end=(0.546875-5.78125j))
; 		 Line(start=(0.546875-5.78125j), end=(0.90625-5.78125j))
; 		 CubicBezier(start=(0.90625-5.78125j), control1=(1.171875-5.78125j), control2=(1.515625-5.75j), end=(1.546875-5.4375j))
; 		 Line(start=(1.546875-5.4375j), end=(1.546875-1.984375j))
; 		 CubicBezier(start=(1.546875-1.984375j), control1=(1.546875-1.546875j), control2=(1.578125-1.109375j), end=(1.8125-0.71875j))
; 		 CubicBezier(start=(1.8125-0.71875j), control1=(2.21875+0.015625j), control2=(3.234375+0.078125j), end=(3.9375+0.078125j))
; 		 CubicBezier(start=(3.9375+0.078125j), control1=(4.8125+0.078125j), control2=(5.6875-0.3125j), end=(5.96875-1.15625j))
; 		 Line(start=(5.96875-1.15625j), end=(5.984375+0.078125j))
; 		 Line(start=(5.984375+0.078125j), end=(8.4375+0.078125j))
; 		 Line(start=(8.4375+0.078125j), end=(8.4375-0.59375j))
; 		 Line(start=(8.4375-0.59375j), end=(8.0625-0.59375j))
; 		 CubicBezier(start=(8.0625-0.59375j), control1=(7.734375-0.59375j), control2=(7.4375-0.609375j), end=(7.4375-1.0625j))
; 		 Line(start=(7.4375-1.0625j), end=(7.4375-6.46875j))
; 		 Line(start=(7.4375-6.46875j), end=(5.765625-6.46875j))
; 		 Line(start=(5.765625-6.46875j), end=(4.921875-6.359375j))
; 		 Line(start=(4.921875-6.359375j), end=(4.921875-5.78125j))
; 		 Line(start=(4.921875-5.78125j), end=(5.234375-5.78125j))
; 		 CubicBezier(start=(5.234375-5.78125j), control1=(5.609375-5.78125j), control2=(5.921875-5.734375j), end=(5.921875-5.328125j))
; 		 Line(start=(5.921875-5.328125j), end=(5.921875-2.15625j))
; 		 CubicBezier(start=(5.921875-2.15625j), control1=(5.84375-1.25j), control2=(5.25-0.375j), end=(4.203125-0.375j))
; 		 CubicBezier(start=(4.203125-0.375j), control1=(3.640625-0.375j), control2=(3.046875-0.375j), end=(3.046875-1.46875j))
; 		 CubicBezier(start=(3.046875-1.46875j), control1=(3.046875-1.671875j), control2=(3.0625-1.90625j), end=(3.0625-2.09375j))
; 		 Line(start=(3.0625-2.09375j), end=(3.0625-6.46875j))
; 		 Line(start=(3.0625-6.46875j), end=(1.390625-6.46875j))
; 		 Line(start=(1.390625-6.46875j), end=(0.546875-6.359375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(5.15625-5.90625j), control1=(4.921875-5.734375j), control2=(4.78125-5.53125j), end=(4.78125-5.234375j))
; 		 CubicBezier(start=(4.78125-5.234375j), control1=(4.78125-4.828125j), control2=(5.0625-4.421875j), end=(5.53125-4.421875j))
; 		 Line(start=(5.53125-4.421875j), end=(5.625-4.421875j))
; 		 CubicBezier(start=(5.625-4.421875j), control1=(6.125-4.421875j), control2=(6.390625-4.828125j), end=(6.390625-5.21875j))
; 		 CubicBezier(start=(6.390625-5.21875j), control1=(6.390625-6.5j), control2=(4.546875-6.515625j), end=(4.015625-6.515625j))
; 		 Line(start=(4.015625-6.515625j), end=(3.984375-6.515625j))
; 		 CubicBezier(start=(3.984375-6.515625j), control1=(3.15625-6.515625j), control2=(2.3125-6.3125j), end=(1.65625-5.78125j))
; 		 CubicBezier(start=(1.65625-5.78125j), control1=(0.90625-5.15625j), control2=(0.515625-4.1875j), end=(0.515625-3.21875j))
; 		 CubicBezier(start=(0.515625-3.21875j), control1=(0.515625-1.234375j), control2=(1.984375+0.078125j), end=(3.9375+0.078125j))
; 		 CubicBezier(start=(3.9375+0.078125j), control1=(4.9375+0.078125j), control2=(5.984375-0.359375j), end=(6.421875-1.3125j))
; 		 CubicBezier(start=(6.421875-1.3125j), control1=(6.46875-1.421875j), control2=(6.5625-1.5625j), end=(6.5625-1.671875j))
; 		 CubicBezier(start=(6.5625-1.671875j), control1=(6.5625-1.8125j), control2=(6.4375-1.84375j), end=(6.34375-1.84375j))
; 		 Line(start=(6.34375-1.84375j), end=(6.234375-1.84375j))
; 		 CubicBezier(start=(6.234375-1.84375j), control1=(5.96875-1.84375j), control2=(6-1.625j), end=(5.890625-1.453125j))
; 		 CubicBezier(start=(5.890625-1.453125j), control1=(5.578125-0.765625j), control2=(4.9375-0.421875j), end=(4.25-0.421875j))
; 		 CubicBezier(start=(4.25-0.421875j), control1=(2.828125-0.421875j), control2=(2.171875-1.578125j), end=(2.171875-3.203125j))
; 		 CubicBezier(start=(2.171875-3.203125j), control1=(2.171875-4.34375j), control2=(2.3125-6.03125j), end=(4.109375-6.03125j))
; 		 CubicBezier(start=(4.109375-6.03125j), control1=(4.453125-6.03125j), control2=(4.8125-5.96875j), end=(5.15625-5.90625j))
; 		 Line(start=(5.15625-5.90625j), end=(5.15625-5.90625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.625-6.359375j), end=(0.625-5.78125j))
; 		 CubicBezier(start=(0.625-5.78125j), control1=(0.734375-5.78125j), control2=(0.84375-5.796875j), end=(0.96875-5.796875j))
; 		 CubicBezier(start=(0.96875-5.796875j), control1=(1.28125-5.796875j), control2=(1.578125-5.734375j), end=(1.578125-5.328125j))
; 		 Line(start=(1.578125-5.328125j), end=(1.578125-0.59375j))
; 		 Line(start=(1.578125-0.59375j), end=(0.59375-0.59375j))
; 		 Line(start=(0.59375-0.59375j), end=(0.59375-0.015625j))
; 		 Line(start=(0.59375-0.015625j), end=(3.9375-0.015625j))
; 		 Line(start=(3.9375-0.015625j), end=(3.9375-0.59375j))
; 		 Line(start=(3.9375-0.59375j), end=(3.03125-0.59375j))
; 		 Line(start=(3.03125-0.59375j), end=(3.03125-6.46875j))
; 		 Line(start=(3.03125-6.46875j), end=(1.4375-6.46875j))
; 		 Line(start=(1.4375-6.46875j), end=(0.625-6.359375j))
; 		 CubicBezier(start=(1.984375-9.984375j), control1=(1.5-9.890625j), control2=(1.140625-9.515625j), end=(1.140625-9.015625j))
; 		 CubicBezier(start=(1.140625-9.015625j), control1=(1.140625-8.515625j), control2=(1.515625-8.046875j), end=(2.125-8.046875j))
; 		 Line(start=(2.125-8.046875j), end=(2.21875-8.046875j))
; 		 CubicBezier(start=(2.21875-8.046875j), control1=(2.703125-8.140625j), control2=(3.0625-8.515625j), end=(3.0625-9.015625j))
; 		 CubicBezier(start=(3.0625-9.015625j), control1=(3.0625-9.515625j), control2=(2.6875-9.984375j), end=(2.078125-9.984375j))
; 		 Line(start=(2.078125-9.984375j), end=(1.984375-9.984375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.65625-7.953125j), control1=(2.03125-8.515625j), control2=(2.71875-8.84375j), end=(3.40625-8.84375j))
; 		 CubicBezier(start=(3.40625-8.84375j), control1=(4.625-8.84375j), control2=(5.328125-7.796875j), end=(5.328125-6.703125j))
; 		 Line(start=(5.328125-6.703125j), end=(5.328125-6.53125j))
; 		 CubicBezier(start=(5.328125-6.53125j), control1=(5.25-5.171875j), control2=(4.171875-4.0625j), end=(3.28125-3.140625j))
; 		 CubicBezier(start=(3.28125-3.140625j), control1=(2.5-2.328125j), control2=(1.65625-1.515625j), end=(0.9375-0.65625j))
; 		 CubicBezier(start=(0.9375-0.65625j), control1=(0.8125-0.546875j), control2=(0.78125-0.46875j), end=(0.78125-0.3125j))
; 		 Line(start=(0.78125-0.3125j), end=(0.78125-0.015625j))
; 		 Line(start=(0.78125-0.015625j), end=(6.703125-0.015625j))
; 		 Line(start=(6.703125-0.015625j), end=(7.078125-3.03125j))
; 		 Line(start=(7.078125-3.03125j), end=(6.546875-3.03125j))
; 		 CubicBezier(start=(6.546875-3.03125j), control1=(6.453125-2.4375j), control2=(6.46875-1.671875j), end=(5.90625-1.671875j))
; 		 CubicBezier(start=(5.90625-1.671875j), control1=(5.5625-1.640625j), control2=(5.21875-1.640625j), end=(4.875-1.640625j))
; 		 Line(start=(4.875-1.640625j), end=(2.46875-1.640625j))
; 		 CubicBezier(start=(2.46875-1.640625j), control1=(3.234375-2.296875j), control2=(3.984375-2.96875j), end=(4.78125-3.578125j))
; 		 CubicBezier(start=(4.78125-3.578125j), control1=(5.796875-4.359375j), control2=(7.078125-5.21875j), end=(7.078125-6.734375j))
; 		 CubicBezier(start=(7.078125-6.734375j), control1=(7.078125-8.625j), control2=(5.25-9.421875j), end=(3.765625-9.421875j))
; 		 CubicBezier(start=(3.765625-9.421875j), control1=(2.4375-9.421875j), control2=(0.78125-8.65625j), end=(0.78125-7.109375j))
; 		 CubicBezier(start=(0.78125-7.109375j), control1=(0.78125-6.515625j), control2=(1.21875-6.171875j), end=(1.6875-6.171875j))
; 		 CubicBezier(start=(1.6875-6.171875j), control1=(2.234375-6.171875j), control2=(2.578125-6.59375j), end=(2.578125-7.078125j))
; 		 CubicBezier(start=(2.578125-7.078125j), control1=(2.578125-7.609375j), control2=(2.1875-7.953125j), end=(1.65625-7.953125j))
; 		 Line(start=(1.65625-7.953125j), end=(1.65625-7.953125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.84375-9.734375j), end=(0.578125-6.390625j))
; 		 Line(start=(0.578125-6.390625j), end=(1.125-6.390625j))
; 		 CubicBezier(start=(1.125-6.390625j), control1=(1.171875-7.078125j), control2=(1.21875-7.953125j), end=(1.625-8.484375j))
; 		 CubicBezier(start=(1.625-8.484375j), control1=(2.125-9.140625j), control2=(2.96875-9.171875j), end=(3.734375-9.171875j))
; 		 Line(start=(3.734375-9.171875j), end=(4.59375-9.171875j))
; 		 Line(start=(4.59375-9.171875j), end=(4.59375-0.59375j))
; 		 Line(start=(4.59375-0.59375j), end=(2.453125-0.59375j))
; 		 Line(start=(2.453125-0.59375j), end=(2.453125-0.015625j))
; 		 Line(start=(2.453125-0.015625j), end=(8.515625-0.015625j))
; 		 Line(start=(8.515625-0.015625j), end=(8.515625-0.59375j))
; 		 Line(start=(8.515625-0.59375j), end=(6.390625-0.59375j))
; 		 Line(start=(6.390625-0.59375j), end=(6.390625-9.171875j))
; 		 Line(start=(6.390625-9.171875j), end=(7.25-9.171875j))
; 		 CubicBezier(start=(7.25-9.171875j), control1=(8.21875-9.171875j), control2=(9.375-9.109375j), end=(9.671875-7.6875j))
; 		 CubicBezier(start=(9.671875-7.6875j), control1=(9.765625-7.265625j), control2=(9.8125-6.8125j), end=(9.84375-6.390625j))
; 		 Line(start=(9.84375-6.390625j), end=(10.390625-6.390625j))
; 		 Line(start=(10.390625-6.390625j), end=(10.109375-9.734375j))
; 		 Line(start=(10.109375-9.734375j), end=(0.84375-9.734375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.546875-9.859375j), end=(0.546875-9.28125j))
; 		 Line(start=(0.546875-9.28125j), end=(0.8125-9.28125j))
; 		 CubicBezier(start=(0.8125-9.28125j), control1=(1.1875-9.28125j), control2=(1.546875-9.28125j), end=(1.546875-8.75j))
; 		 Line(start=(1.546875-8.75j), end=(1.546875-0.59375j))
; 		 Line(start=(1.546875-0.59375j), end=(0.546875-0.59375j))
; 		 Line(start=(0.546875-0.59375j), end=(0.546875-0.015625j))
; 		 Line(start=(0.546875-0.015625j), end=(4.0625-0.015625j))
; 		 Line(start=(4.0625-0.015625j), end=(4.0625-0.59375j))
; 		 Line(start=(4.0625-0.59375j), end=(3.0625-0.59375j))
; 		 Line(start=(3.0625-0.59375j), end=(3.0625-3.5j))
; 		 CubicBezier(start=(3.0625-3.5j), control1=(3.0625-4.65625j), control2=(3.65625-6.03125j), end=(5.046875-6.03125j))
; 		 CubicBezier(start=(5.046875-6.03125j), control1=(5.875-6.03125j), control2=(5.921875-5.09375j), end=(5.921875-4.28125j))
; 		 Line(start=(5.921875-4.28125j), end=(5.921875-0.59375j))
; 		 Line(start=(5.921875-0.59375j), end=(4.921875-0.59375j))
; 		 Line(start=(4.921875-0.59375j), end=(4.921875-0.015625j))
; 		 Line(start=(4.921875-0.015625j), end=(8.4375-0.015625j))
; 		 Line(start=(8.4375-0.015625j), end=(8.4375-0.59375j))
; 		 Line(start=(8.4375-0.59375j), end=(7.4375-0.59375j))
; 		 Line(start=(7.4375-0.59375j), end=(7.4375-4.265625j))
; 		 CubicBezier(start=(7.4375-4.265625j), control1=(7.4375-4.78125j), control2=(7.40625-5.3125j), end=(7.109375-5.75j))
; 		 CubicBezier(start=(7.109375-5.75j), control1=(6.734375-6.359375j), control2=(5.953125-6.46875j), end=(5.28125-6.46875j))
; 		 CubicBezier(start=(5.28125-6.46875j), control1=(4.359375-6.46875j), control2=(3.25-5.84375j), end=(3.015625-4.953125j))
; 		 Line(start=(3.015625-4.953125j), end=(3-9.96875j))
; 		 Line(start=(3-9.96875j), end=(1.375-9.96875j))
; 		 Line(start=(1.375-9.96875j), end=(0.546875-9.859375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(2.0625-3.234375j), end=(6.484375-3.234375j))
; 		 CubicBezier(start=(6.484375-3.234375j), control1=(6.671875-3.234375j), control2=(6.734375-3.328125j), end=(6.734375-3.5j))
; 		 CubicBezier(start=(6.734375-3.5j), control1=(6.734375-5.046875j), control2=(5.96875-6.53125j), end=(3.78125-6.53125j))
; 		 CubicBezier(start=(3.78125-6.53125j), control1=(3.015625-6.53125j), control2=(2.21875-6.3125j), end=(1.609375-5.84375j))
; 		 CubicBezier(start=(1.609375-5.84375j), control1=(0.8125-5.234375j), control2=(0.4375-4.25j), end=(0.4375-3.25j))
; 		 CubicBezier(start=(0.4375-3.25j), control1=(0.4375-1.203125j), control2=(2+0.078125j), end=(3.96875+0.078125j))
; 		 CubicBezier(start=(3.96875+0.078125j), control1=(5.046875+0.078125j), control2=(6.234375-0.375j), end=(6.6875-1.53125j))
; 		 CubicBezier(start=(6.6875-1.53125j), control1=(6.703125-1.59375j), control2=(6.734375-1.640625j), end=(6.734375-1.6875j))
; 		 CubicBezier(start=(6.734375-1.6875j), control1=(6.734375-1.828125j), control2=(6.59375-1.890625j), end=(6.4375-1.890625j))
; 		 CubicBezier(start=(6.4375-1.890625j), control1=(6.171875-1.890625j), control2=(6.09375-1.515625j), end=(5.984375-1.3125j))
; 		 CubicBezier(start=(5.984375-1.3125j), control1=(5.609375-0.734375j), control2=(4.875-0.421875j), end=(4.1875-0.421875j))
; 		 CubicBezier(start=(4.1875-0.421875j), control1=(3.4375-0.421875j), control2=(2.71875-0.765625j), end=(2.359375-1.453125j))
; 		 CubicBezier(start=(2.359375-1.453125j), control1=(2.078125-2j), control2=(2.0625-2.625j), end=(2.0625-3.234375j))
; 		 Line(start=(2.0625-3.234375j), end=(2.0625-3.234375j))
; 		 CubicBezier(start=(2.0625-3.609375j), control1=(2.0625-4.25j), control2=(2.140625-4.9375j), end=(2.53125-5.453125j))
; 		 CubicBezier(start=(2.53125-5.453125j), control1=(2.828125-5.84375j), control2=(3.3125-6.0625j), end=(3.78125-6.0625j))
; 		 CubicBezier(start=(3.78125-6.0625j), control1=(5.09375-6.0625j), control2=(5.421875-4.6875j), end=(5.421875-3.609375j))
; 		 Line(start=(5.421875-3.609375j), end=(2.0625-3.609375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.3125-6.390625j), end=(0.3125-5.8125j))
; 		 Line(start=(0.3125-5.8125j), end=(1.09375-5.8125j))
; 		 CubicBezier(start=(1.09375-5.8125j), control1=(1.296875-5.8125j), control2=(1.34375-5.515625j), end=(1.4375-5.328125j))
; 		 CubicBezier(start=(1.4375-5.328125j), control1=(2.203125-3.71875j), control2=(2.9375-2.078125j), end=(3.65625-0.4375j))
; 		 CubicBezier(start=(3.65625-0.4375j), control1=(3.734375-0.296875j), control2=(3.828125-0.125j), end=(3.828125+0j))
; 		 CubicBezier(start=(3.828125+0j), control1=(3.828125+0.171875j), control2=(3.59375+0.546875j), end=(3.484375+0.78125j))
; 		 CubicBezier(start=(3.484375+0.78125j), control1=(3.171875+1.484375j), control2=(2.734375+2.34375j), end=(1.84375+2.40625j))
; 		 Line(start=(1.84375+2.40625j), end=(1.78125+2.40625j))
; 		 CubicBezier(start=(1.78125+2.40625j), control1=(1.625+2.40625j), control2=(1.484375+2.359375j), end=(1.3125+2.328125j))
; 		 CubicBezier(start=(1.3125+2.328125j), control1=(1.546875+2.140625j), control2=(1.78125+2.015625j), end=(1.78125+1.625j))
; 		 CubicBezier(start=(1.78125+1.625j), control1=(1.78125+1.171875j), control2=(1.46875+0.875j), end=(0.953125+0.875j))
; 		 CubicBezier(start=(0.953125+0.875j), control1=(0.5+0.96875j), control2=(0.296875+1.265625j), end=(0.296875+1.640625j))
; 		 CubicBezier(start=(0.296875+1.640625j), control1=(0.296875+2.453125j), control2=(1.109375+2.859375j), end=(1.765625+2.859375j))
; 		 CubicBezier(start=(1.765625+2.859375j), control1=(3.03125+2.859375j), control2=(3.6875+1.703125j), end=(4.125+0.703125j))
; 		 CubicBezier(start=(4.125+0.703125j), control1=(4.984375-1.21875j), control2=(5.890625-3.125j), end=(6.734375-5.0625j))
; 		 Line(start=(6.734375-5.0625j), end=(6.90625-5.453125j))
; 		 CubicBezier(start=(6.90625-5.453125j), control1=(7.078125-5.796875j), control2=(7.46875-5.8125j), end=(7.828125-5.8125j))
; 		 Line(start=(7.828125-5.8125j), end=(7.984375-5.8125j))
; 		 Line(start=(7.984375-5.8125j), end=(7.984375-6.390625j))
; 		 Line(start=(7.984375-6.390625j), end=(5.6875-6.390625j))
; 		 Line(start=(5.6875-6.390625j), end=(5.6875-5.8125j))
; 		 CubicBezier(start=(5.6875-5.8125j), control1=(5.890625-5.8125j), control2=(6.296875-5.796875j), end=(6.375-5.65625j))
; 		 Line(start=(6.375-5.65625j), end=(6.375-5.609375j))
; 		 CubicBezier(start=(6.375-5.609375j), control1=(6.375-5.421875j), control2=(6.078125-4.953125j), end=(5.953125-4.640625j))
; 		 Line(start=(5.953125-4.640625j), end=(5.09375-2.75j))
; 		 CubicBezier(start=(5.09375-2.75j), control1=(4.953125-2.421875j), control2=(4.8125-2.09375j), end=(4.640625-1.78125j))
; 		 Line(start=(4.640625-1.78125j), end=(2.828125-5.8125j))
; 		 Line(start=(2.828125-5.8125j), end=(3.625-5.8125j))
; 		 Line(start=(3.625-5.8125j), end=(3.625-6.390625j))
; 		 Line(start=(3.625-6.390625j), end=(0.3125-6.390625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.765625-4.1875j), end=(0.765625-3.9375j))
; 		 CubicBezier(start=(0.765625-3.9375j), control1=(1.109375-3.9375j), control2=(1.4375-3.984375j), end=(1.765625-4.125j))
; 		 Line(start=(1.765625-4.125j), end=(1.765625-0.546875j))
; 		 CubicBezier(start=(1.765625-0.546875j), control1=(1.765625-0.296875j), control2=(1.578125-0.28125j), end=(1.421875-0.265625j))
; 		 CubicBezier(start=(1.421875-0.265625j), control1=(1.21875-0.265625j), control2=(1.015625-0.265625j), end=(0.8125-0.265625j))
; 		 Line(start=(0.8125-0.265625j), end=(0.8125+0j))
; 		 Line(start=(0.8125+0j), end=(2.125+0j))
; 		 Line(start=(2.125+0j), end=(2.28125-0.03125j))
; 		 Line(start=(2.28125-0.03125j), end=(2.953125-0.03125j))
; 		 Line(start=(2.953125-0.03125j), end=(3.296875+0j))
; 		 Line(start=(3.296875+0j), end=(3.296875-0.265625j))
; 		 CubicBezier(start=(3.296875-0.265625j), control1=(3.171875-0.265625j), control2=(3.03125-0.25j), end=(2.90625-0.25j))
; 		 CubicBezier(start=(2.90625-0.25j), control1=(2.609375-0.25j), control2=(2.34375-0.296875j), end=(2.34375-0.546875j))
; 		 Line(start=(2.34375-0.546875j), end=(2.34375-4.5j))
; 		 CubicBezier(start=(2.34375-4.5j), control1=(2.34375-4.578125j), control2=(2.296875-4.640625j), end=(2.203125-4.640625j))
; 		 CubicBezier(start=(2.203125-4.640625j), control1=(2.09375-4.640625j), control2=(2-4.515625j), end=(1.90625-4.453125j))
; 		 CubicBezier(start=(1.90625-4.453125j), control1=(1.5625-4.25j), control2=(1.15625-4.1875j), end=(0.765625-4.1875j))
; 		 Line(start=(0.765625-4.1875j), end=(0.765625-4.1875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(0.765625-3.734375j), control1=(0.96875-4.125j), control2=(1.328125-4.390625j), end=(1.796875-4.390625j))
; 		 CubicBezier(start=(1.796875-4.390625j), control1=(2.390625-4.390625j), control2=(2.84375-3.90625j), end=(2.84375-3.265625j))
; 		 CubicBezier(start=(2.84375-3.265625j), control1=(2.84375-2.546875j), control2=(2.265625-2j), end=(1.78125-1.53125j))
; 		 CubicBezier(start=(1.78125-1.53125j), control1=(1.40625-1.15625j), control2=(1.015625-0.78125j), end=(0.65625-0.421875j))
; 		 Line(start=(0.65625-0.421875j), end=(0.515625-0.296875j))
; 		 CubicBezier(start=(0.515625-0.296875j), control1=(0.46875-0.234375j), control2=(0.4375-0.203125j), end=(0.4375-0.125j))
; 		 CubicBezier(start=(0.4375-0.125j), control1=(0.4375-0.078125j), control2=(0.4375-0.046875j), end=(0.4375+0j))
; 		 Line(start=(0.4375+0j), end=(3.3125+0j))
; 		 Line(start=(3.3125+0j), end=(3.515625-1.28125j))
; 		 Line(start=(3.515625-1.28125j), end=(3.28125-1.28125j))
; 		 CubicBezier(start=(3.28125-1.28125j), control1=(3.25-1.078125j), control2=(3.21875-0.734375j), end=(3.09375-0.65625j))
; 		 CubicBezier(start=(3.09375-0.65625j), control1=(3.046875-0.625j), control2=(2.9375-0.609375j), end=(2.8125-0.609375j))
; 		 Line(start=(2.8125-0.609375j), end=(2.578125-0.609375j))
; 		 CubicBezier(start=(2.578125-0.609375j), control1=(2.328125-0.609375j), control2=(2.09375-0.609375j), end=(1.859375-0.609375j))
; 		 Line(start=(1.859375-0.609375j), end=(1.140625-0.609375j))
; 		 CubicBezier(start=(1.140625-0.609375j), control1=(1.59375-1.03125j), control2=(2.0625-1.421875j), end=(2.5625-1.796875j))
; 		 CubicBezier(start=(2.5625-1.796875j), control1=(3.015625-2.15625j), control2=(3.515625-2.609375j), end=(3.515625-3.265625j))
; 		 CubicBezier(start=(3.515625-3.265625j), control1=(3.515625-4.21875j), control2=(2.640625-4.640625j), end=(1.890625-4.640625j))
; 		 CubicBezier(start=(1.890625-4.640625j), control1=(1.203125-4.640625j), control2=(0.4375-4.15625j), end=(0.4375-3.390625j))
; 		 CubicBezier(start=(0.4375-3.390625j), control1=(0.4375-3.15625j), control2=(0.59375-3j), end=(0.8125-3j))
; 		 CubicBezier(start=(0.8125-3j), control1=(1.015625-3j), control2=(1.171875-3.15625j), end=(1.171875-3.359375j))
; 		 CubicBezier(start=(1.171875-3.359375j), control1=(1.171875-3.609375j), control2=(0.96875-3.734375j), end=(0.765625-3.734375j))
; 		 Line(start=(0.765625-3.734375j), end=(0.765625-3.734375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.71875-3.609375j), end=(0.71875-3.359375j))
; 		 CubicBezier(start=(0.71875-3.359375j), control1=(1.046875-3.359375j), control2=(1.34375-3.40625j), end=(1.640625-3.53125j))
; 		 Line(start=(1.640625-3.53125j), end=(1.640625-0.484375j))
; 		 CubicBezier(start=(1.640625-0.484375j), control1=(1.640625-0.265625j), control2=(1.453125-0.265625j), end=(1.328125-0.25j))
; 		 CubicBezier(start=(1.328125-0.25j), control1=(1.140625-0.25j), control2=(0.953125-0.25j), end=(0.765625-0.25j))
; 		 Line(start=(0.765625-0.25j), end=(0.765625+0j))
; 		 Line(start=(0.765625+0j), end=(1.296875+0j))
; 		 Line(start=(1.296875+0j), end=(1.578125-0.03125j))
; 		 Line(start=(1.578125-0.03125j), end=(2.71875-0.03125j))
; 		 Line(start=(2.71875-0.03125j), end=(3.015625+0j))
; 		 Line(start=(3.015625+0j), end=(3.015625-0.25j))
; 		 CubicBezier(start=(3.015625-0.25j), control1=(2.90625-0.25j), control2=(2.765625-0.234375j), end=(2.640625-0.234375j))
; 		 CubicBezier(start=(2.640625-0.234375j), control1=(2.375-0.234375j), control2=(2.140625-0.265625j), end=(2.140625-0.484375j))
; 		 Line(start=(2.140625-0.484375j), end=(2.140625-3.859375j))
; 		 CubicBezier(start=(2.140625-3.859375j), control1=(2.140625-3.9375j), control2=(2.109375-3.984375j), end=(2.015625-3.984375j))
; 		 CubicBezier(start=(2.015625-3.984375j), control1=(1.921875-3.984375j), control2=(1.84375-3.890625j), end=(1.75-3.84375j))
; 		 CubicBezier(start=(1.75-3.84375j), control1=(1.4375-3.640625j), control2=(1.078125-3.609375j), end=(0.71875-3.609375j))
; 		 Line(start=(0.71875-3.609375j), end=(0.71875-3.609375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(0.734375-3.21875j), control1=(0.9375-3.5625j), control2=(1.28125-3.75j), end=(1.65625-3.75j))
; 		 CubicBezier(start=(1.65625-3.75j), control1=(2.078125-3.75j), control2=(2.609375-3.453125j), end=(2.609375-2.8125j))
; 		 CubicBezier(start=(2.609375-2.8125j), control1=(2.609375-2.171875j), control2=(2.078125-1.703125j), end=(1.625-1.296875j))
; 		 CubicBezier(start=(1.625-1.296875j), control1=(1.296875-1j), control2=(0.953125-0.6875j), end=(0.625-0.390625j))
; 		 CubicBezier(start=(0.625-0.390625j), control1=(0.53125-0.296875j), control2=(0.421875-0.25j), end=(0.421875-0.109375j))
; 		 CubicBezier(start=(0.421875-0.109375j), control1=(0.421875-0.078125j), control2=(0.4375-0.03125j), end=(0.4375+0j))
; 		 Line(start=(0.4375+0j), end=(3.03125+0j))
; 		 Line(start=(3.03125+0j), end=(3.21875-1.125j))
; 		 Line(start=(3.21875-1.125j), end=(3-1.125j))
; 		 CubicBezier(start=(3-1.125j), control1=(2.96875-0.953125j), control2=(2.9375-0.6875j), end=(2.84375-0.59375j))
; 		 CubicBezier(start=(2.84375-0.59375j), control1=(2.8125-0.5625j), control2=(2.703125-0.546875j), end=(2.578125-0.546875j))
; 		 Line(start=(2.578125-0.546875j), end=(2.375-0.546875j))
; 		 CubicBezier(start=(2.375-0.546875j), control1=(1.953125-0.546875j), control2=(1.53125-0.546875j), end=(1.109375-0.546875j))
; 		 CubicBezier(start=(1.109375-0.546875j), control1=(1.515625-0.890625j), control2=(1.9375-1.21875j), end=(2.375-1.53125j))
; 		 CubicBezier(start=(2.375-1.53125j), control1=(2.78125-1.84375j), control2=(3.21875-2.203125j), end=(3.21875-2.796875j))
; 		 CubicBezier(start=(3.21875-2.796875j), control1=(3.21875-3.65625j), control2=(2.40625-3.984375j), end=(1.734375-3.984375j))
; 		 CubicBezier(start=(1.734375-3.984375j), control1=(1.140625-3.984375j), control2=(0.4375-3.609375j), end=(0.4375-2.921875j))
; 		 CubicBezier(start=(0.4375-2.921875j), control1=(0.4375-2.703125j), control2=(0.578125-2.578125j), end=(0.75-2.578125j))
; 		 CubicBezier(start=(0.75-2.578125j), control1=(0.9375-2.578125j), control2=(1.078125-2.71875j), end=(1.078125-2.90625j))
; 		 CubicBezier(start=(1.078125-2.90625j), control1=(1.078125-3.09375j), control2=(0.921875-3.21875j), end=(0.734375-3.21875j))
; 		 Line(start=(0.734375-3.21875j), end=(0.734375-3.21875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.46875-5.390625j), end=(0.296875-3.578125j))
; 		 Line(start=(0.296875-3.578125j), end=(0.546875-3.578125j))
; 		 CubicBezier(start=(0.546875-3.578125j), control1=(0.59375-4j), control2=(0.609375-4.5625j), end=(0.875-4.84375j))
; 		 CubicBezier(start=(0.875-4.84375j), control1=(1.140625-5.109375j), control2=(1.609375-5.140625j), end=(1.953125-5.140625j))
; 		 Line(start=(1.953125-5.140625j), end=(2.265625-5.140625j))
; 		 CubicBezier(start=(2.265625-5.140625j), control1=(2.46875-5.140625j), control2=(2.6875-5.125j), end=(2.6875-4.875j))
; 		 Line(start=(2.6875-4.875j), end=(2.6875-0.625j))
; 		 CubicBezier(start=(2.6875-0.625j), control1=(2.6875-0.296875j), control2=(2.359375-0.265625j), end=(2.0625-0.265625j))
; 		 Line(start=(2.0625-0.265625j), end=(1.546875-0.265625j))
; 		 Line(start=(1.546875-0.265625j), end=(1.546875-0.015625j))
; 		 Line(start=(1.546875-0.015625j), end=(4.546875-0.015625j))
; 		 Line(start=(4.546875-0.015625j), end=(4.546875-0.265625j))
; 		 Line(start=(4.546875-0.265625j), end=(4.046875-0.265625j))
; 		 CubicBezier(start=(4.046875-0.265625j), control1=(3.71875-0.265625j), control2=(3.421875-0.296875j), end=(3.421875-0.625j))
; 		 Line(start=(3.421875-0.625j), end=(3.421875-4.875j))
; 		 CubicBezier(start=(3.421875-4.875j), control1=(3.421875-5.109375j), control2=(3.609375-5.140625j), end=(3.875-5.140625j))
; 		 Line(start=(3.875-5.140625j), end=(4.1875-5.140625j))
; 		 CubicBezier(start=(4.1875-5.140625j), control1=(4.6875-5.140625j), control2=(5.265625-5.078125j), end=(5.4375-4.40625j))
; 		 CubicBezier(start=(5.4375-4.40625j), control1=(5.5-4.140625j), control2=(5.53125-3.859375j), end=(5.546875-3.578125j))
; 		 Line(start=(5.546875-3.578125j), end=(5.796875-3.578125j))
; 		 Line(start=(5.796875-3.578125j), end=(5.640625-5.390625j))
; 		 Line(start=(5.640625-5.390625j), end=(0.46875-5.390625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.3125-5.453125j), end=(0.3125-5.1875j))
; 		 Line(start=(0.3125-5.1875j), end=(0.46875-5.1875j))
; 		 CubicBezier(start=(0.46875-5.1875j), control1=(0.71875-5.1875j), control2=(0.921875-5.15625j), end=(0.921875-4.796875j))
; 		 Line(start=(0.921875-4.796875j), end=(0.921875-0.78125j))
; 		 CubicBezier(start=(0.921875-0.78125j), control1=(0.921875-0.734375j), control2=(0.9375-0.671875j), end=(0.9375-0.609375j))
; 		 CubicBezier(start=(0.9375-0.609375j), control1=(0.9375-0.28125j), control2=(0.6875-0.265625j), end=(0.359375-0.265625j))
; 		 Line(start=(0.359375-0.265625j), end=(0.3125-0.265625j))
; 		 Line(start=(0.3125-0.265625j), end=(0.3125-0.015625j))
; 		 Line(start=(0.3125-0.015625j), end=(0.8125-0.03125j))
; 		 Line(start=(0.8125-0.03125j), end=(1.640625-0.03125j))
; 		 Line(start=(1.640625-0.03125j), end=(2.140625-0.015625j))
; 		 Line(start=(2.140625-0.015625j), end=(2.140625-0.265625j))
; 		 Line(start=(2.140625-0.265625j), end=(1.96875-0.265625j))
; 		 CubicBezier(start=(1.96875-0.265625j), control1=(1.734375-0.265625j), control2=(1.53125-0.296875j), end=(1.53125-0.59375j))
; 		 Line(start=(1.53125-0.59375j), end=(1.53125-2.0625j))
; 		 CubicBezier(start=(1.53125-2.0625j), control1=(1.53125-2.65625j), control2=(1.921875-3.296875j), end=(2.6875-3.296875j))
; 		 CubicBezier(start=(2.6875-3.296875j), control1=(3.140625-3.296875j), control2=(3.265625-2.890625j), end=(3.265625-2.421875j))
; 		 Line(start=(3.265625-2.421875j), end=(3.265625-0.578125j))
; 		 CubicBezier(start=(3.265625-0.578125j), control1=(3.265625-0.296875j), control2=(3.046875-0.265625j), end=(2.828125-0.265625j))
; 		 Line(start=(2.828125-0.265625j), end=(2.65625-0.265625j))
; 		 Line(start=(2.65625-0.265625j), end=(2.65625-0.015625j))
; 		 Line(start=(2.65625-0.015625j), end=(3.15625-0.03125j))
; 		 Line(start=(3.15625-0.03125j), end=(4-0.03125j))
; 		 Line(start=(4-0.03125j), end=(4.484375-0.015625j))
; 		 Line(start=(4.484375-0.015625j), end=(4.484375-0.265625j))
; 		 Line(start=(4.484375-0.265625j), end=(4.328125-0.265625j))
; 		 CubicBezier(start=(4.328125-0.265625j), control1=(4.109375-0.265625j), control2=(3.875-0.296875j), end=(3.875-0.578125j))
; 		 Line(start=(3.875-0.578125j), end=(3.875-2.296875j))
; 		 CubicBezier(start=(3.875-2.296875j), control1=(3.875-2.546875j), control2=(3.875-2.796875j), end=(3.765625-3.015625j))
; 		 CubicBezier(start=(3.765625-3.015625j), control1=(3.578125-3.390625j), control2=(3.15625-3.515625j), end=(2.765625-3.515625j))
; 		 CubicBezier(start=(2.765625-3.515625j), control1=(2.296875-3.515625j), control2=(1.71875-3.28125j), end=(1.515625-2.8125j))
; 		 Line(start=(1.515625-2.8125j), end=(1.515625-5.53125j))
; 		 Line(start=(1.515625-5.53125j), end=(0.3125-5.453125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.9375-1.828125j), end=(3.375-1.828125j))
; 		 CubicBezier(start=(3.375-1.828125j), control1=(3.46875-1.828125j), control2=(3.515625-1.875j), end=(3.515625-1.96875j))
; 		 CubicBezier(start=(3.515625-1.96875j), control1=(3.515625-2.375j), control2=(3.40625-2.84375j), end=(3.109375-3.125j))
; 		 CubicBezier(start=(3.109375-3.125j), control1=(2.828125-3.421875j), control2=(2.4375-3.546875j), end=(2.03125-3.546875j))
; 		 CubicBezier(start=(2.03125-3.546875j), control1=(1.09375-3.546875j), control2=(0.234375-2.84375j), end=(0.234375-1.765625j))
; 		 CubicBezier(start=(0.234375-1.765625j), control1=(0.234375-0.859375j), control2=(0.953125+0.078125j), end=(2.109375+0.078125j))
; 		 CubicBezier(start=(2.109375+0.078125j), control1=(2.703125+0.078125j), control2=(3.34375-0.296875j), end=(3.5-0.890625j))
; 		 CubicBezier(start=(3.5-0.890625j), control1=(3.515625-0.90625j), control2=(3.515625-0.9375j), end=(3.515625-0.953125j))
; 		 CubicBezier(start=(3.515625-0.953125j), control1=(3.515625-1.015625j), control2=(3.46875-1.078125j), end=(3.40625-1.078125j))
; 		 CubicBezier(start=(3.40625-1.078125j), control1=(3.25-1.078125j), control2=(3.234375-0.828125j), end=(3.15625-0.71875j))
; 		 CubicBezier(start=(3.15625-0.71875j), control1=(2.953125-0.390625j), control2=(2.5625-0.1875j), end=(2.171875-0.1875j))
; 		 CubicBezier(start=(2.171875-0.1875j), control1=(1.75-0.1875j), control2=(1.34375-0.40625j), end=(1.125-0.796875j))
; 		 CubicBezier(start=(1.125-0.796875j), control1=(0.96875-1.109375j), control2=(0.9375-1.46875j), end=(0.9375-1.828125j))
; 		 Line(start=(0.9375-1.828125j), end=(0.9375-1.828125j))
; 		 CubicBezier(start=(0.953125-2.046875j), control1=(1.03125-2.5j), control2=(1.109375-2.984375j), end=(1.578125-3.21875j))
; 		 CubicBezier(start=(1.578125-3.21875j), control1=(1.71875-3.296875j), control2=(1.859375-3.328125j), end=(2.015625-3.328125j))
; 		 CubicBezier(start=(2.015625-3.328125j), control1=(2.53125-3.328125j), control2=(2.875-2.90625j), end=(2.953125-2.328125j))
; 		 CubicBezier(start=(2.953125-2.328125j), control1=(2.96875-2.234375j), control2=(2.96875-2.140625j), end=(2.984375-2.046875j))
; 		 Line(start=(2.984375-2.046875j), end=(0.953125-2.046875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.34375-5.453125j), end=(0.34375-5.1875j))
; 		 Line(start=(0.34375-5.1875j), end=(0.625-5.1875j))
; 		 CubicBezier(start=(0.625-5.1875j), control1=(0.96875-5.1875j), control2=(1.15625-5.15625j), end=(1.15625-4.828125j))
; 		 Line(start=(1.15625-4.828125j), end=(1.15625-2.296875j))
; 		 CubicBezier(start=(1.15625-2.296875j), control1=(1.15625-2.171875j), control2=(1.140625-2.046875j), end=(1.140625-1.921875j))
; 		 CubicBezier(start=(1.140625-1.921875j), control1=(1.140625-0.78125j), control2=(1.984375+0.15625j), end=(3.34375+0.15625j))
; 		 CubicBezier(start=(3.34375+0.15625j), control1=(4.28125+0.15625j), control2=(5.03125-0.640625j), end=(5.15625-1.53125j))
; 		 CubicBezier(start=(5.15625-1.53125j), control1=(5.171875-1.671875j), control2=(5.171875-1.8125j), end=(5.171875-1.9375j))
; 		 Line(start=(5.171875-1.9375j), end=(5.171875-4.578125j))
; 		 CubicBezier(start=(5.171875-4.578125j), control1=(5.171875-5.125j), control2=(5.625-5.1875j), end=(5.984375-5.1875j))
; 		 Line(start=(5.984375-5.1875j), end=(5.984375-5.453125j))
; 		 Line(start=(5.984375-5.453125j), end=(5.484375-5.421875j))
; 		 Line(start=(5.484375-5.421875j), end=(4.640625-5.421875j))
; 		 Line(start=(4.640625-5.421875j), end=(4.109375-5.453125j))
; 		 Line(start=(4.109375-5.453125j), end=(4.109375-5.1875j))
; 		 CubicBezier(start=(4.109375-5.1875j), control1=(4.484375-5.1875j), control2=(4.921875-5.125j), end=(4.921875-4.5625j))
; 		 Line(start=(4.921875-4.5625j), end=(4.921875-1.96875j))
; 		 CubicBezier(start=(4.921875-1.96875j), control1=(4.921875-1.203125j), control2=(4.640625-0.546875j), end=(3.84375-0.203125j))
; 		 CubicBezier(start=(3.84375-0.203125j), control1=(3.65625-0.140625j), control2=(3.453125-0.109375j), end=(3.265625-0.109375j))
; 		 CubicBezier(start=(3.265625-0.109375j), control1=(2.890625-0.109375j), control2=(2.5-0.28125j), end=(2.265625-0.578125j))
; 		 CubicBezier(start=(2.265625-0.578125j), control1=(1.890625-1.03125j), control2=(1.890625-1.609375j), end=(1.890625-2.15625j))
; 		 Line(start=(1.890625-2.15625j), end=(1.890625-4.84375j))
; 		 CubicBezier(start=(1.890625-4.84375j), control1=(1.890625-5.140625j), control2=(2.15625-5.203125j), end=(2.421875-5.203125j))
; 		 CubicBezier(start=(2.421875-5.203125j), control1=(2.515625-5.203125j), control2=(2.609375-5.1875j), end=(2.6875-5.1875j))
; 		 Line(start=(2.6875-5.1875j), end=(2.6875-5.453125j))
; 		 Line(start=(2.6875-5.453125j), end=(0.34375-5.453125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.3125-3.421875j), end=(0.3125-3.15625j))
; 		 Line(start=(0.3125-3.15625j), end=(0.46875-3.15625j))
; 		 CubicBezier(start=(0.46875-3.15625j), control1=(0.703125-3.15625j), control2=(0.921875-3.125j), end=(0.921875-2.765625j))
; 		 Line(start=(0.921875-2.765625j), end=(0.921875-0.609375j))
; 		 CubicBezier(start=(0.921875-0.609375j), control1=(0.921875-0.28125j), control2=(0.71875-0.265625j), end=(0.3125-0.265625j))
; 		 Line(start=(0.3125-0.265625j), end=(0.3125-0.015625j))
; 		 Line(start=(0.3125-0.015625j), end=(0.8125-0.03125j))
; 		 Line(start=(0.8125-0.03125j), end=(1.640625-0.03125j))
; 		 Line(start=(1.640625-0.03125j), end=(2.140625-0.015625j))
; 		 Line(start=(2.140625-0.015625j), end=(2.140625-0.265625j))
; 		 Line(start=(2.140625-0.265625j), end=(1.96875-0.265625j))
; 		 CubicBezier(start=(1.96875-0.265625j), control1=(1.734375-0.265625j), control2=(1.53125-0.296875j), end=(1.53125-0.59375j))
; 		 Line(start=(1.53125-0.59375j), end=(1.53125-2.0625j))
; 		 CubicBezier(start=(1.53125-2.0625j), control1=(1.53125-2.65625j), control2=(1.921875-3.296875j), end=(2.6875-3.296875j))
; 		 CubicBezier(start=(2.6875-3.296875j), control1=(3.140625-3.296875j), control2=(3.265625-2.890625j), end=(3.265625-2.421875j))
; 		 Line(start=(3.265625-2.421875j), end=(3.265625-0.578125j))
; 		 CubicBezier(start=(3.265625-0.578125j), control1=(3.265625-0.296875j), control2=(3.046875-0.265625j), end=(2.828125-0.265625j))
; 		 Line(start=(2.828125-0.265625j), end=(2.65625-0.265625j))
; 		 Line(start=(2.65625-0.265625j), end=(2.65625-0.015625j))
; 		 Line(start=(2.65625-0.015625j), end=(3.15625-0.03125j))
; 		 Line(start=(3.15625-0.03125j), end=(4-0.03125j))
; 		 Line(start=(4-0.03125j), end=(4.484375-0.015625j))
; 		 Line(start=(4.484375-0.015625j), end=(4.484375-0.265625j))
; 		 Line(start=(4.484375-0.265625j), end=(4.328125-0.265625j))
; 		 CubicBezier(start=(4.328125-0.265625j), control1=(4.09375-0.265625j), control2=(3.875-0.296875j), end=(3.875-0.578125j))
; 		 Line(start=(3.875-0.578125j), end=(3.875-2.28125j))
; 		 CubicBezier(start=(3.875-2.28125j), control1=(3.875-2.546875j), control2=(3.875-2.8125j), end=(3.75-3.0625j))
; 		 CubicBezier(start=(3.75-3.0625j), control1=(3.546875-3.40625j), control2=(3.125-3.515625j), end=(2.75-3.515625j))
; 		 CubicBezier(start=(2.75-3.515625j), control1=(2.265625-3.515625j), control2=(1.671875-3.25j), end=(1.484375-2.75j))
; 		 Line(start=(1.484375-2.75j), end=(1.484375-3.515625j))
; 		 Line(start=(1.484375-3.515625j), end=(1.1875-3.484375j))
; 		 Line(start=(1.1875-3.484375j), end=(0.3125-3.421875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.359375-3.421875j), end=(0.359375-3.15625j))
; 		 Line(start=(0.359375-3.15625j), end=(0.484375-3.15625j))
; 		 CubicBezier(start=(0.484375-3.15625j), control1=(0.734375-3.15625j), control2=(0.9375-3.109375j), end=(0.9375-2.765625j))
; 		 Line(start=(0.9375-2.765625j), end=(0.9375-0.609375j))
; 		 CubicBezier(start=(0.9375-0.609375j), control1=(0.9375-0.28125j), control2=(0.71875-0.265625j), end=(0.328125-0.265625j))
; 		 Line(start=(0.328125-0.265625j), end=(0.328125-0.015625j))
; 		 Line(start=(0.328125-0.015625j), end=(0.8125-0.03125j))
; 		 Line(start=(0.8125-0.03125j), end=(1.625-0.03125j))
; 		 Line(start=(1.625-0.03125j), end=(2.0625-0.015625j))
; 		 Line(start=(2.0625-0.015625j), end=(2.0625-0.265625j))
; 		 Line(start=(2.0625-0.265625j), end=(1.953125-0.265625j))
; 		 CubicBezier(start=(1.953125-0.265625j), control1=(1.71875-0.265625j), control2=(1.515625-0.28125j), end=(1.515625-0.5625j))
; 		 Line(start=(1.515625-0.5625j), end=(1.515625-3.515625j))
; 		 Line(start=(1.515625-3.515625j), end=(1.234375-3.484375j))
; 		 Line(start=(1.234375-3.484375j), end=(0.359375-3.421875j))
; 		 CubicBezier(start=(1.046875-5.34375j), control1=(0.8125-5.3125j), control2=(0.65625-5.109375j), end=(0.65625-4.90625j))
; 		 CubicBezier(start=(0.65625-4.90625j), control1=(0.65625-4.6875j), control2=(0.84375-4.46875j), end=(1.09375-4.46875j))
; 		 CubicBezier(start=(1.09375-4.46875j), control1=(1.359375-4.46875j), control2=(1.546875-4.6875j), end=(1.546875-4.921875j))
; 		 CubicBezier(start=(1.546875-4.921875j), control1=(1.546875-5.125j), control2=(1.375-5.359375j), end=(1.109375-5.359375j))
; 		 CubicBezier(start=(1.109375-5.359375j), control1=(1.09375-5.359375j), control2=(1.0625-5.34375j), end=(1.046875-5.34375j))
; 		 Line(start=(1.046875-5.34375j), end=(1.046875-5.34375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.171875-3.4375j), end=(0.171875-3.171875j))
; 		 Line(start=(0.171875-3.171875j), end=(0.296875-3.171875j))
; 		 CubicBezier(start=(0.296875-3.171875j), control1=(0.734375-3.171875j), control2=(0.765625-2.96875j), end=(1.015625-2.421875j))
; 		 CubicBezier(start=(1.015625-2.421875j), control1=(1.34375-1.671875j), control2=(1.671875-0.90625j), end=(2-0.15625j))
; 		 CubicBezier(start=(2-0.15625j), control1=(2.046875-0.046875j), control2=(2.09375+0.078125j), end=(2.234375+0.078125j))
; 		 CubicBezier(start=(2.234375+0.078125j), control1=(2.359375+0.078125j), control2=(2.421875-0.03125j), end=(2.46875-0.140625j))
; 		 CubicBezier(start=(2.46875-0.140625j), control1=(2.765625-0.90625j), control2=(3.125-1.703125j), end=(3.46875-2.46875j))
; 		 CubicBezier(start=(3.46875-2.46875j), control1=(3.65625-2.875j), control2=(3.84375-3.171875j), end=(4.28125-3.171875j))
; 		 Line(start=(4.28125-3.171875j), end=(4.28125-3.4375j))
; 		 Line(start=(4.28125-3.4375j), end=(3.96875-3.40625j))
; 		 Line(start=(3.96875-3.40625j), end=(3.375-3.40625j))
; 		 Line(start=(3.375-3.40625j), end=(2.96875-3.4375j))
; 		 Line(start=(2.96875-3.4375j), end=(2.96875-3.171875j))
; 		 CubicBezier(start=(2.96875-3.171875j), control1=(3.140625-3.15625j), control2=(3.296875-3.0625j), end=(3.328125-2.890625j))
; 		 Line(start=(3.328125-2.890625j), end=(3.328125-2.859375j))
; 		 CubicBezier(start=(3.328125-2.859375j), control1=(3.328125-2.671875j), control2=(3.203125-2.46875j), end=(3.125-2.296875j))
; 		 CubicBezier(start=(3.125-2.296875j), control1=(2.9375-1.890625j), control2=(2.765625-1.484375j), end=(2.59375-1.09375j))
; 		 CubicBezier(start=(2.59375-1.09375j), control1=(2.546875-0.953125j), control2=(2.484375-0.8125j), end=(2.421875-0.671875j))
; 		 CubicBezier(start=(2.421875-0.671875j), control1=(2.125-1.375j), control2=(1.8125-2.078125j), end=(1.5-2.78125j))
; 		 CubicBezier(start=(1.5-2.78125j), control1=(1.46875-2.84375j), control2=(1.4375-2.90625j), end=(1.4375-2.96875j))
; 		 Line(start=(1.4375-2.96875j), end=(1.4375-3j))
; 		 CubicBezier(start=(1.4375-3j), control1=(1.46875-3.15625j), control2=(1.703125-3.171875j), end=(1.859375-3.171875j))
; 		 Line(start=(1.859375-3.171875j), end=(1.859375-3.4375j))
; 		 Line(start=(1.859375-3.4375j), end=(1.359375-3.40625j))
; 		 Line(start=(1.359375-3.40625j), end=(0.578125-3.40625j))
; 		 Line(start=(0.578125-3.40625j), end=(0.171875-3.4375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.265625-3.421875j), end=(0.265625-3.15625j))
; 		 Line(start=(0.265625-3.15625j), end=(0.390625-3.15625j))
; 		 CubicBezier(start=(0.390625-3.15625j), control1=(0.640625-3.15625j), control2=(0.875-3.140625j), end=(0.875-2.765625j))
; 		 Line(start=(0.875-2.765625j), end=(0.875-0.609375j))
; 		 CubicBezier(start=(0.875-0.609375j), control1=(0.875-0.28125j), control2=(0.671875-0.265625j), end=(0.265625-0.265625j))
; 		 Line(start=(0.265625-0.265625j), end=(0.265625-0.015625j))
; 		 Line(start=(0.265625-0.015625j), end=(0.75-0.03125j))
; 		 Line(start=(0.75-0.03125j), end=(1.65625-0.03125j))
; 		 Line(start=(1.65625-0.03125j), end=(2.21875-0.015625j))
; 		 Line(start=(2.21875-0.015625j), end=(2.21875-0.265625j))
; 		 CubicBezier(start=(2.21875-0.265625j), control1=(2.140625-0.265625j), control2=(2.046875-0.265625j), end=(1.9375-0.265625j))
; 		 CubicBezier(start=(1.9375-0.265625j), control1=(1.6875-0.265625j), control2=(1.46875-0.296875j), end=(1.46875-0.59375j))
; 		 Line(start=(1.46875-0.59375j), end=(1.46875-1.921875j))
; 		 CubicBezier(start=(1.46875-1.921875j), control1=(1.46875-2.5j), control2=(1.78125-3.296875j), end=(2.46875-3.296875j))
; 		 CubicBezier(start=(2.46875-3.296875j), control1=(2.390625-3.21875j), control2=(2.34375-3.109375j), end=(2.34375-3j))
; 		 CubicBezier(start=(2.34375-3j), control1=(2.34375-2.78125j), control2=(2.53125-2.640625j), end=(2.703125-2.640625j))
; 		 CubicBezier(start=(2.703125-2.640625j), control1=(2.90625-2.640625j), control2=(3.0625-2.8125j), end=(3.0625-3j))
; 		 CubicBezier(start=(3.0625-3j), control1=(3.0625-3.34375j), control2=(2.71875-3.515625j), end=(2.4375-3.515625j))
; 		 CubicBezier(start=(2.4375-3.515625j), control1=(1.96875-3.515625j), control2=(1.5-3.09375j), end=(1.421875-2.671875j))
; 		 Line(start=(1.421875-2.671875j), end=(1.40625-3.515625j))
; 		 Line(start=(1.40625-3.515625j), end=(1.015625-3.484375j))
; 		 Line(start=(1.015625-3.484375j), end=(0.265625-3.421875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(0.75-0.265625j), control1=(1-0.046875j), control2=(1.3125+0.078125j), end=(1.65625+0.078125j))
; 		 CubicBezier(start=(1.65625+0.078125j), control1=(2.28125+0.078125j), control2=(3.046875-0.15625j), end=(3.046875-1.046875j))
; 		 Line(start=(3.046875-1.046875j), end=(3.046875-1.109375j))
; 		 CubicBezier(start=(3.046875-1.109375j), control1=(3.015625-1.59375j), control2=(2.625-1.9375j), end=(2.1875-2.078125j))
; 		 CubicBezier(start=(2.1875-2.078125j), control1=(1.734375-2.21875j), control2=(0.703125-2.171875j), end=(0.703125-2.78125j))
; 		 CubicBezier(start=(0.703125-2.78125j), control1=(0.703125-3.25j), control2=(1.3125-3.359375j), end=(1.65625-3.359375j))
; 		 CubicBezier(start=(1.65625-3.359375j), control1=(2.140625-3.359375j), control2=(2.53125-3.109375j), end=(2.59375-2.546875j))
; 		 CubicBezier(start=(2.59375-2.546875j), control1=(2.59375-2.484375j), control2=(2.59375-2.40625j), end=(2.703125-2.40625j))
; 		 CubicBezier(start=(2.703125-2.40625j), control1=(2.8125-2.40625j), control2=(2.84375-2.484375j), end=(2.84375-2.578125j))
; 		 CubicBezier(start=(2.84375-2.578125j), control1=(2.84375-2.625j), control2=(2.84375-2.671875j), end=(2.84375-2.71875j))
; 		 Line(start=(2.84375-2.71875j), end=(2.84375-3.4375j))
; 		 CubicBezier(start=(2.84375-3.4375j), control1=(2.84375-3.5j), control2=(2.796875-3.546875j), end=(2.71875-3.546875j))
; 		 CubicBezier(start=(2.71875-3.546875j), control1=(2.578125-3.546875j), control2=(2.515625-3.359375j), end=(2.421875-3.359375j))
; 		 Line(start=(2.421875-3.359375j), end=(2.40625-3.359375j))
; 		 CubicBezier(start=(2.40625-3.359375j), control1=(2.28125-3.40625j), control2=(2.171875-3.484375j), end=(2.03125-3.515625j))
; 		 CubicBezier(start=(2.03125-3.515625j), control1=(1.90625-3.53125j), control2=(1.796875-3.546875j), end=(1.671875-3.546875j))
; 		 CubicBezier(start=(1.671875-3.546875j), control1=(1.09375-3.546875j), control2=(0.28125-3.390625j), end=(0.28125-2.546875j))
; 		 Line(start=(0.28125-2.546875j), end=(0.28125-2.484375j))
; 		 CubicBezier(start=(0.28125-2.484375j), control1=(0.296875-2.203125j), control2=(0.484375-2j), end=(0.703125-1.828125j))
; 		 CubicBezier(start=(0.703125-1.828125j), control1=(1.328125-1.421875j), control2=(2.53125-1.6875j), end=(2.625-0.875j))
; 		 CubicBezier(start=(2.625-0.875j), control1=(2.625-0.3125j), control2=(2.125-0.15625j), end=(1.6875-0.15625j))
; 		 CubicBezier(start=(1.6875-0.15625j), control1=(1.078125-0.15625j), control2=(0.65625-0.578125j), end=(0.5625-1.125j))
; 		 CubicBezier(start=(0.5625-1.125j), control1=(0.53125-1.21875j), control2=(0.53125-1.328125j), end=(0.40625-1.328125j))
; 		 CubicBezier(start=(0.40625-1.328125j), control1=(0.28125-1.328125j), control2=(0.28125-1.25j), end=(0.28125-1.171875j))
; 		 Line(start=(0.28125-1.171875j), end=(0.28125-0.203125j))
; 		 CubicBezier(start=(0.28125-0.203125j), control1=(0.28125-0.171875j), control2=(0.265625-0.125j), end=(0.265625-0.09375j))
; 		 CubicBezier(start=(0.265625-0.09375j), control1=(0.265625-0.015625j), control2=(0.296875+0.078125j), end=(0.375+0.078125j))
; 		 CubicBezier(start=(0.375+0.078125j), control1=(0.453125+0.078125j), control2=(0.53125-0.015625j), end=(0.5625-0.0625j))
; 		 Line(start=(0.5625-0.0625j), end=(0.609375-0.109375j))
; 		 CubicBezier(start=(0.609375-0.109375j), control1=(0.65625-0.171875j), control2=(0.703125-0.21875j), end=(0.75-0.265625j))
; 		 Line(start=(0.75-0.265625j), end=(0.75-0.265625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.234375-4.90625j), control1=(1.234375-4.34375j), control2=(0.96875-3.390625j), end=(0.15625-3.390625j))
; 		 Line(start=(0.15625-3.390625j), end=(0.15625-3.171875j))
; 		 Line(start=(0.15625-3.171875j), end=(0.875-3.171875j))
; 		 Line(start=(0.875-3.171875j), end=(0.875-1.109375j))
; 		 CubicBezier(start=(0.875-1.109375j), control1=(0.875-0.859375j), control2=(0.890625-0.609375j), end=(1.015625-0.375j))
; 		 CubicBezier(start=(1.015625-0.375j), control1=(1.21875-0.0625j), control2=(1.609375+0.078125j), end=(1.96875+0.078125j))
; 		 CubicBezier(start=(1.96875+0.078125j), control1=(2.640625+0.078125j), control2=(2.8125-0.640625j), end=(2.8125-1.15625j))
; 		 Line(start=(2.8125-1.15625j), end=(2.8125-1.453125j))
; 		 Line(start=(2.8125-1.453125j), end=(2.5625-1.453125j))
; 		 CubicBezier(start=(2.5625-1.453125j), control1=(2.5625-1.34375j), control2=(2.578125-1.234375j), end=(2.578125-1.109375j))
; 		 CubicBezier(start=(2.578125-1.109375j), control1=(2.578125-0.765625j), control2=(2.484375-0.171875j), end=(2.015625-0.171875j))
; 		 CubicBezier(start=(2.015625-0.171875j), control1=(1.578125-0.171875j), control2=(1.484375-0.640625j), end=(1.484375-1.03125j))
; 		 Line(start=(1.484375-1.03125j), end=(1.484375-3.171875j))
; 		 Line(start=(1.484375-3.171875j), end=(2.671875-3.171875j))
; 		 Line(start=(2.671875-3.171875j), end=(2.671875-3.4375j))
; 		 Line(start=(2.671875-3.4375j), end=(1.484375-3.4375j))
; 		 Line(start=(1.484375-3.4375j), end=(1.484375-4.90625j))
; 		 Line(start=(1.484375-4.90625j), end=(1.234375-4.90625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.171875-3.4375j), end=(0.171875-3.171875j))
; 		 Line(start=(0.171875-3.171875j), end=(0.296875-3.171875j))
; 		 CubicBezier(start=(0.296875-3.171875j), control1=(0.5625-3.171875j), control2=(0.703125-3.109375j), end=(0.84375-2.828125j))
; 		 Line(start=(0.84375-2.828125j), end=(1.015625-2.421875j))
; 		 CubicBezier(start=(1.015625-2.421875j), control1=(1.34375-1.703125j), control2=(1.671875-0.96875j), end=(1.984375-0.25j))
; 		 Line(start=(1.984375-0.25j), end=(2.0625-0.09375j))
; 		 CubicBezier(start=(2.0625-0.09375j), control1=(2.078125-0.078125j), control2=(2.078125-0.046875j), end=(2.078125-0.015625j))
; 		 Line(start=(2.078125-0.015625j), end=(2.078125+0.015625j))
; 		 CubicBezier(start=(2.078125+0.015625j), control1=(2.078125+0.0625j), control2=(2.046875+0.109375j), end=(2.015625+0.15625j))
; 		 Line(start=(2.015625+0.15625j), end=(1.890625+0.4375j))
; 		 CubicBezier(start=(1.890625+0.4375j), control1=(1.71875+0.84375j), control2=(1.46875+1.390625j), end=(0.9375+1.390625j))
; 		 CubicBezier(start=(0.9375+1.390625j), control1=(0.828125+1.390625j), control2=(0.703125+1.34375j), end=(0.609375+1.296875j))
; 		 CubicBezier(start=(0.609375+1.296875j), control1=(0.734375+1.25j), control2=(0.859375+1.15625j), end=(0.859375+0.953125j))
; 		 CubicBezier(start=(0.859375+0.953125j), control1=(0.859375+0.75j), control2=(0.6875+0.625j), end=(0.515625+0.625j))
; 		 CubicBezier(start=(0.515625+0.625j), control1=(0.3125+0.625j), control2=(0.171875+0.78125j), end=(0.171875+0.96875j))
; 		 CubicBezier(start=(0.171875+0.96875j), control1=(0.171875+1.390625j), control2=(0.59375+1.625j), end=(0.9375+1.625j))
; 		 CubicBezier(start=(0.9375+1.625j), control1=(1.5625+1.625j), control2=(1.90625+1.03125j), end=(2.140625+0.515625j))
; 		 Line(start=(2.140625+0.515625j), end=(2.34375+0.03125j))
; 		 CubicBezier(start=(2.34375+0.03125j), control1=(2.71875-0.8125j), control2=(3.078125-1.65625j), end=(3.46875-2.46875j))
; 		 CubicBezier(start=(3.46875-2.46875j), control1=(3.609375-2.78125j), control2=(3.765625-3.171875j), end=(4.28125-3.171875j))
; 		 Line(start=(4.28125-3.171875j), end=(4.28125-3.4375j))
; 		 Line(start=(4.28125-3.4375j), end=(3.953125-3.40625j))
; 		 Line(start=(3.953125-3.40625j), end=(3.375-3.40625j))
; 		 Line(start=(3.375-3.40625j), end=(2.96875-3.4375j))
; 		 Line(start=(2.96875-3.4375j), end=(2.96875-3.171875j))
; 		 CubicBezier(start=(2.96875-3.171875j), control1=(3.203125-3.171875j), control2=(3.3125-3j), end=(3.3125-2.84375j))
; 		 CubicBezier(start=(3.3125-2.84375j), control1=(3.3125-2.6875j), control2=(3.21875-2.546875j), end=(3.15625-2.40625j))
; 		 Line(start=(3.15625-2.40625j), end=(2.59375-1.140625j))
; 		 CubicBezier(start=(2.59375-1.140625j), control1=(2.53125-1j), control2=(2.484375-0.875j), end=(2.421875-0.734375j))
; 		 CubicBezier(start=(2.421875-0.734375j), control1=(2.34375-0.921875j), control2=(2.25-1.109375j), end=(2.15625-1.3125j))
; 		 Line(start=(2.15625-1.3125j), end=(1.5-2.78125j))
; 		 CubicBezier(start=(1.5-2.78125j), control1=(1.484375-2.84375j), control2=(1.453125-2.90625j), end=(1.453125-2.96875j))
; 		 Line(start=(1.453125-2.96875j), end=(1.453125-3j))
; 		 CubicBezier(start=(1.453125-3j), control1=(1.46875-3.171875j), control2=(1.71875-3.171875j), end=(1.859375-3.171875j))
; 		 Line(start=(1.859375-3.171875j), end=(1.859375-3.4375j))
; 		 Line(start=(1.859375-3.4375j), end=(1.359375-3.40625j))
; 		 Line(start=(1.359375-3.40625j), end=(0.59375-3.40625j))
; 		 Line(start=(0.59375-3.40625j), end=(0.171875-3.4375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.984375-3.546875j), control1=(1.078125-3.453125j), control2=(0.234375-2.796875j), end=(0.234375-1.671875j))
; 		 CubicBezier(start=(0.234375-1.671875j), control1=(0.234375-0.59375j), control2=(1.21875+0.078125j), end=(2.140625+0.078125j))
; 		 CubicBezier(start=(2.140625+0.078125j), control1=(3.0625+0.078125j), control2=(4-0.640625j), end=(4-1.6875j))
; 		 CubicBezier(start=(4-1.6875j), control1=(4-2.71875j), control2=(3.140625-3.546875j), end=(2.09375-3.546875j))
; 		 Line(start=(2.09375-3.546875j), end=(1.984375-3.546875j))
; 		 CubicBezier(start=(0.9375-1.546875j), control1=(0.9375-1.609375j), control2=(0.9375-1.6875j), end=(0.9375-1.75j))
; 		 CubicBezier(start=(0.9375-1.75j), control1=(0.9375-2.171875j), control2=(0.96875-2.71875j), end=(1.3125-3.03125j))
; 		 CubicBezier(start=(1.3125-3.03125j), control1=(1.53125-3.234375j), control2=(1.828125-3.328125j), end=(2.125-3.328125j))
; 		 CubicBezier(start=(2.125-3.328125j), control1=(2.9375-3.328125j), control2=(3.28125-2.65625j), end=(3.28125-1.828125j))
; 		 CubicBezier(start=(3.28125-1.828125j), control1=(3.28125-1.390625j), control2=(3.265625-0.90625j), end=(2.96875-0.546875j))
; 		 CubicBezier(start=(2.96875-0.546875j), control1=(2.75-0.296875j), control2=(2.4375-0.171875j), end=(2.109375-0.171875j))
; 		 CubicBezier(start=(2.109375-0.171875j), control1=(1.515625-0.171875j), control2=(1.0625-0.59375j), end=(0.96875-1.21875j))
; 		 CubicBezier(start=(0.96875-1.21875j), control1=(0.953125-1.328125j), control2=(0.953125-1.4375j), end=(0.9375-1.546875j))
; 		 Line(start=(0.9375-1.546875j), end=(0.9375-1.546875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.9375-3.4375j), end=(0.28125-3.4375j))
; 		 Line(start=(0.28125-3.4375j), end=(0.28125-3.171875j))
; 		 Line(start=(0.28125-3.171875j), end=(0.9375-3.171875j))
; 		 Line(start=(0.9375-3.171875j), end=(0.9375-0.5625j))
; 		 CubicBezier(start=(0.9375-0.5625j), control1=(0.9375-0.28125j), control2=(0.6875-0.265625j), end=(0.4375-0.265625j))
; 		 Line(start=(0.4375-0.265625j), end=(0.328125-0.265625j))
; 		 Line(start=(0.328125-0.265625j), end=(0.328125-0.015625j))
; 		 Line(start=(0.328125-0.015625j), end=(0.8125-0.03125j))
; 		 Line(start=(0.8125-0.03125j), end=(1.71875-0.03125j))
; 		 Line(start=(1.71875-0.03125j), end=(2.28125-0.015625j))
; 		 Line(start=(2.28125-0.015625j), end=(2.28125-0.265625j))
; 		 Line(start=(2.28125-0.265625j), end=(2.03125-0.265625j))
; 		 CubicBezier(start=(2.03125-0.265625j), control1=(1.703125-0.265625j), control2=(1.515625-0.296875j), end=(1.515625-0.625j))
; 		 Line(start=(1.515625-0.625j), end=(1.515625-3.171875j))
; 		 Line(start=(1.515625-3.171875j), end=(2.484375-3.171875j))
; 		 Line(start=(2.484375-3.171875j), end=(2.484375-3.4375j))
; 		 Line(start=(2.484375-3.4375j), end=(1.5-3.4375j))
; 		 Line(start=(1.5-3.4375j), end=(1.5-4.171875j))
; 		 CubicBezier(start=(1.5-4.171875j), control1=(1.5-4.6875j), control2=(1.65625-5.390625j), end=(2.25-5.390625j))
; 		 CubicBezier(start=(2.25-5.390625j), control1=(2.3125-5.390625j), control2=(2.40625-5.375j), end=(2.46875-5.359375j))
; 		 CubicBezier(start=(2.46875-5.359375j), control1=(2.34375-5.296875j), control2=(2.28125-5.15625j), end=(2.28125-5.03125j))
; 		 CubicBezier(start=(2.28125-5.03125j), control1=(2.28125-4.84375j), control2=(2.4375-4.671875j), end=(2.640625-4.671875j))
; 		 CubicBezier(start=(2.640625-4.671875j), control1=(2.875-4.671875j), control2=(3.015625-4.859375j), end=(3.015625-5.046875j))
; 		 CubicBezier(start=(3.015625-5.046875j), control1=(3.015625-5.4375j), control2=(2.59375-5.625j), end=(2.265625-5.625j))
; 		 CubicBezier(start=(2.265625-5.625j), control1=(1.515625-5.625j), control2=(0.9375-5.078125j), end=(0.9375-4.265625j))
; 		 Line(start=(0.9375-4.265625j), end=(0.9375-3.4375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.484375-0.125j), control1=(1.484375+0.390625j), control2=(1.328125+0.890625j), end=(0.9375+1.28125j))
; 		 CubicBezier(start=(0.9375+1.28125j), control1=(0.890625+1.3125j), control2=(0.84375+1.359375j), end=(0.84375+1.421875j))
; 		 Line(start=(0.84375+1.421875j), end=(0.84375+1.4375j))
; 		 CubicBezier(start=(0.84375+1.4375j), control1=(0.859375+1.5j), control2=(0.90625+1.53125j), end=(0.953125+1.53125j))
; 		 CubicBezier(start=(0.953125+1.53125j), control1=(1.078125+1.53125j), control2=(1.28125+1.234375j), end=(1.375+1.09375j))
; 		 CubicBezier(start=(1.375+1.09375j), control1=(1.578125+0.765625j), control2=(1.71875+0.359375j), end=(1.71875-0.03125j))
; 		 CubicBezier(start=(1.71875-0.03125j), control1=(1.71875-0.390625j), control2=(1.625-0.890625j), end=(1.125-0.890625j))
; 		 CubicBezier(start=(1.125-0.890625j), control1=(0.875-0.859375j), control2=(0.734375-0.671875j), end=(0.734375-0.453125j))
; 		 CubicBezier(start=(0.734375-0.453125j), control1=(0.734375-0.1875j), control2=(0.9375-0.015625j), end=(1.171875-0.015625j))
; 		 CubicBezier(start=(1.171875-0.015625j), control1=(1.296875-0.015625j), control2=(1.40625-0.0625j), end=(1.484375-0.125j))
; 		 Line(start=(1.484375-0.125j), end=(1.484375-0.125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.34375-5.453125j), end=(0.34375-5.1875j))
; 		 CubicBezier(start=(0.34375-5.1875j), control1=(0.453125-5.1875j), control2=(0.546875-5.203125j), end=(0.640625-5.203125j))
; 		 CubicBezier(start=(0.640625-5.203125j), control1=(0.921875-5.203125j), control2=(1.15625-5.15625j), end=(1.15625-4.84375j))
; 		 Line(start=(1.15625-4.84375j), end=(1.15625-0.625j))
; 		 CubicBezier(start=(1.15625-0.625j), control1=(1.15625-0.296875j), control2=(0.890625-0.265625j), end=(0.625-0.265625j))
; 		 CubicBezier(start=(0.625-0.265625j), control1=(0.53125-0.265625j), control2=(0.4375-0.265625j), end=(0.34375-0.265625j))
; 		 Line(start=(0.34375-0.265625j), end=(0.34375-0.015625j))
; 		 Line(start=(0.34375-0.015625j), end=(2.6875-0.015625j))
; 		 Line(start=(2.6875-0.015625j), end=(2.6875-0.265625j))
; 		 CubicBezier(start=(2.6875-0.265625j), control1=(2.59375-0.265625j), control2=(2.484375-0.265625j), end=(2.375-0.265625j))
; 		 CubicBezier(start=(2.375-0.265625j), control1=(2.125-0.265625j), control2=(1.890625-0.296875j), end=(1.890625-0.609375j))
; 		 Line(start=(1.890625-0.609375j), end=(1.890625-1.90625j))
; 		 CubicBezier(start=(1.890625-1.90625j), control1=(1.890625-2.046875j), control2=(2.09375-2.140625j), end=(2.203125-2.25j))
; 		 CubicBezier(start=(2.203125-2.25j), control1=(2.4375-2.46875j), control2=(2.6875-2.671875j), end=(2.921875-2.890625j))
; 		 CubicBezier(start=(2.921875-2.890625j), control1=(2.96875-2.8125j), control2=(3.015625-2.734375j), end=(3.078125-2.671875j))
; 		 Line(start=(3.078125-2.671875j), end=(3.359375-2.28125j))
; 		 CubicBezier(start=(3.359375-2.28125j), control1=(3.671875-1.859375j), control2=(3.96875-1.453125j), end=(4.28125-1.03125j))
; 		 Line(start=(4.28125-1.03125j), end=(4.421875-0.828125j))
; 		 CubicBezier(start=(4.421875-0.828125j), control1=(4.5-0.734375j), control2=(4.59375-0.609375j), end=(4.59375-0.5j))
; 		 CubicBezier(start=(4.59375-0.5j), control1=(4.59375-0.34375j), control2=(4.46875-0.265625j), end=(4.171875-0.265625j))
; 		 Line(start=(4.171875-0.265625j), end=(4.171875-0.015625j))
; 		 Line(start=(4.171875-0.015625j), end=(6.203125-0.015625j))
; 		 Line(start=(6.203125-0.015625j), end=(6.203125-0.265625j))
; 		 CubicBezier(start=(6.203125-0.265625j), control1=(5.8125-0.265625j), control2=(5.578125-0.359375j), end=(5.34375-0.6875j))
; 		 Line(start=(5.34375-0.6875j), end=(5.015625-1.140625j))
; 		 CubicBezier(start=(5.015625-1.140625j), control1=(4.46875-1.859375j), control2=(3.9375-2.609375j), end=(3.40625-3.328125j))
; 		 CubicBezier(start=(3.40625-3.328125j), control1=(3.453125-3.359375j), control2=(3.515625-3.421875j), end=(3.5625-3.453125j))
; 		 Line(start=(3.5625-3.453125j), end=(4.6875-4.46875j))
; 		 CubicBezier(start=(4.6875-4.46875j), control1=(5.109375-4.84375j), control2=(5.515625-5.1875j), end=(6.09375-5.1875j))
; 		 Line(start=(6.09375-5.1875j), end=(6.09375-5.453125j))
; 		 CubicBezier(start=(6.09375-5.453125j), control1=(5.8125-5.4375j), control2=(5.53125-5.421875j), end=(5.25-5.421875j))
; 		 Line(start=(5.25-5.421875j), end=(4.96875-5.421875j))
; 		 CubicBezier(start=(4.96875-5.421875j), control1=(4.765625-5.4375j), control2=(4.59375-5.453125j), end=(4.40625-5.453125j))
; 		 Line(start=(4.40625-5.453125j), end=(4.40625-5.1875j))
; 		 CubicBezier(start=(4.40625-5.1875j), control1=(4.59375-5.1875j), control2=(4.6875-5.0625j), end=(4.6875-4.9375j))
; 		 CubicBezier(start=(4.6875-4.9375j), control1=(4.6875-4.71875j), control2=(4.375-4.515625j), end=(4.234375-4.390625j))
; 		 CubicBezier(start=(4.234375-4.390625j), control1=(3.6875-3.90625j), control2=(3.140625-3.421875j), end=(2.59375-2.953125j))
; 		 CubicBezier(start=(2.59375-2.953125j), control1=(2.359375-2.734375j), control2=(2.140625-2.515625j), end=(1.890625-2.3125j))
; 		 Line(start=(1.890625-2.3125j), end=(1.890625-4.828125j))
; 		 CubicBezier(start=(1.890625-4.828125j), control1=(1.890625-5j), control2=(1.921875-5.140625j), end=(2.203125-5.171875j))
; 		 CubicBezier(start=(2.203125-5.171875j), control1=(2.359375-5.1875j), control2=(2.53125-5.1875j), end=(2.6875-5.1875j))
; 		 Line(start=(2.6875-5.1875j), end=(2.6875-5.453125j))
; 		 Line(start=(2.6875-5.453125j), end=(0.34375-5.453125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.171875-3.4375j), end=(0.171875-3.171875j))
; 		 Line(start=(0.171875-3.171875j), end=(0.265625-3.171875j))
; 		 CubicBezier(start=(0.265625-3.171875j), control1=(0.421875-3.171875j), control2=(0.578125-3.15625j), end=(0.703125-3.109375j))
; 		 CubicBezier(start=(0.703125-3.109375j), control1=(0.84375-3.03125j), control2=(0.953125-2.90625j), end=(1.046875-2.78125j))
; 		 Line(start=(1.046875-2.78125j), end=(1.546875-2.171875j))
; 		 CubicBezier(start=(1.546875-2.171875j), control1=(1.671875-2.03125j), control2=(1.9375-1.765625j), end=(1.9375-1.6875j))
; 		 CubicBezier(start=(1.9375-1.6875j), control1=(1.9375-1.609375j), control2=(1.6875-1.359375j), end=(1.5625-1.21875j))
; 		 CubicBezier(start=(1.5625-1.21875j), control1=(1.15625-0.765625j), control2=(0.8125-0.265625j), end=(0.140625-0.265625j))
; 		 Line(start=(0.140625-0.265625j), end=(0.140625-0.015625j))
; 		 Line(start=(0.140625-0.015625j), end=(0.484375-0.03125j))
; 		 Line(start=(0.484375-0.03125j), end=(1.140625-0.03125j))
; 		 Line(start=(1.140625-0.03125j), end=(1.578125-0.015625j))
; 		 Line(start=(1.578125-0.015625j), end=(1.578125-0.265625j))
; 		 CubicBezier(start=(1.578125-0.265625j), control1=(1.4375-0.296875j), control2=(1.390625-0.390625j), end=(1.390625-0.5j))
; 		 CubicBezier(start=(1.390625-0.5j), control1=(1.390625-0.65625j), control2=(1.53125-0.8125j), end=(1.640625-0.921875j))
; 		 CubicBezier(start=(1.640625-0.921875j), control1=(1.734375-1.03125j), control2=(1.828125-1.140625j), end=(1.9375-1.265625j))
; 		 CubicBezier(start=(1.9375-1.265625j), control1=(1.984375-1.34375j), control2=(2.0625-1.40625j), end=(2.109375-1.484375j))
; 		 CubicBezier(start=(2.109375-1.484375j), control1=(2.1875-1.421875j), control2=(2.234375-1.34375j), end=(2.296875-1.265625j))
; 		 Line(start=(2.296875-1.265625j), end=(2.8125-0.65625j))
; 		 CubicBezier(start=(2.8125-0.65625j), control1=(2.859375-0.59375j), control2=(2.921875-0.53125j), end=(2.921875-0.453125j))
; 		 CubicBezier(start=(2.921875-0.453125j), control1=(2.921875-0.34375j), control2=(2.78125-0.28125j), end=(2.6875-0.265625j))
; 		 Line(start=(2.6875-0.265625j), end=(2.6875-0.015625j))
; 		 Line(start=(2.6875-0.015625j), end=(3.171875-0.03125j))
; 		 Line(start=(3.171875-0.03125j), end=(3.9375-0.03125j))
; 		 Line(start=(3.9375-0.03125j), end=(4.328125-0.015625j))
; 		 Line(start=(4.328125-0.015625j), end=(4.328125-0.265625j))
; 		 Line(start=(4.328125-0.265625j), end=(4.25-0.265625j))
; 		 CubicBezier(start=(4.25-0.265625j), control1=(4.09375-0.265625j), control2=(3.9375-0.28125j), end=(3.796875-0.34375j))
; 		 CubicBezier(start=(3.796875-0.34375j), control1=(3.65625-0.421875j), control2=(3.546875-0.5625j), end=(3.4375-0.6875j))
; 		 CubicBezier(start=(3.4375-0.6875j), control1=(3.125-1.0625j), control2=(2.84375-1.46875j), end=(2.5-1.8125j))
; 		 CubicBezier(start=(2.5-1.8125j), control1=(2.5-1.828125j), control2=(2.484375-1.84375j), end=(2.484375-1.875j))
; 		 CubicBezier(start=(2.484375-1.875j), control1=(2.484375-1.9375j), control2=(2.703125-2.171875j), end=(2.828125-2.296875j))
; 		 CubicBezier(start=(2.828125-2.296875j), control1=(3.234375-2.78125j), control2=(3.578125-3.171875j), end=(4.1875-3.171875j))
; 		 Line(start=(4.1875-3.171875j), end=(4.1875-3.4375j))
; 		 Line(start=(4.1875-3.4375j), end=(3.84375-3.40625j))
; 		 Line(start=(3.84375-3.40625j), end=(3.171875-3.40625j))
; 		 Line(start=(3.171875-3.40625j), end=(2.734375-3.4375j))
; 		 Line(start=(2.734375-3.4375j), end=(2.734375-3.171875j))
; 		 CubicBezier(start=(2.734375-3.171875j), control1=(2.875-3.15625j), control2=(2.9375-3.0625j), end=(2.9375-2.953125j))
; 		 CubicBezier(start=(2.9375-2.953125j), control1=(2.9375-2.703125j), control2=(2.640625-2.484375j), end=(2.5-2.3125j))
; 		 CubicBezier(start=(2.5-2.3125j), control1=(2.4375-2.234375j), control2=(2.34375-2.09375j), end=(2.296875-2.09375j))
; 		 CubicBezier(start=(2.296875-2.09375j), control1=(2.265625-2.09375j), control2=(2.234375-2.140625j), end=(2.21875-2.15625j))
; 		 Line(start=(2.21875-2.15625j), end=(1.765625-2.703125j))
; 		 CubicBezier(start=(1.765625-2.703125j), control1=(1.703125-2.78125j), control2=(1.578125-2.890625j), end=(1.578125-2.984375j))
; 		 CubicBezier(start=(1.578125-2.984375j), control1=(1.578125-3.09375j), control2=(1.71875-3.15625j), end=(1.828125-3.171875j))
; 		 Line(start=(1.828125-3.171875j), end=(1.828125-3.4375j))
; 		 Line(start=(1.828125-3.4375j), end=(1.328125-3.40625j))
; 		 Line(start=(1.328125-3.40625j), end=(0.578125-3.40625j))
; 		 Line(start=(0.578125-3.40625j), end=(0.171875-3.4375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.328125-5.453125j), end=(0.328125-5.1875j))
; 		 Line(start=(0.328125-5.1875j), end=(0.4375-5.1875j))
; 		 CubicBezier(start=(0.4375-5.1875j), control1=(0.6875-5.1875j), control2=(0.9375-5.171875j), end=(0.9375-4.765625j))
; 		 Line(start=(0.9375-4.765625j), end=(0.9375-0.78125j))
; 		 CubicBezier(start=(0.9375-0.78125j), control1=(0.9375-0.71875j), control2=(0.9375-0.671875j), end=(0.9375-0.609375j))
; 		 CubicBezier(start=(0.9375-0.609375j), control1=(0.9375-0.28125j), control2=(0.6875-0.265625j), end=(0.40625-0.265625j))
; 		 Line(start=(0.40625-0.265625j), end=(0.328125-0.265625j))
; 		 Line(start=(0.328125-0.265625j), end=(0.328125-0.015625j))
; 		 Line(start=(0.328125-0.015625j), end=(0.8125-0.03125j))
; 		 Line(start=(0.8125-0.03125j), end=(1.640625-0.03125j))
; 		 Line(start=(1.640625-0.03125j), end=(2.125-0.015625j))
; 		 Line(start=(2.125-0.015625j), end=(2.125-0.265625j))
; 		 Line(start=(2.125-0.265625j), end=(1.9375-0.265625j))
; 		 CubicBezier(start=(1.9375-0.265625j), control1=(1.71875-0.265625j), control2=(1.515625-0.296875j), end=(1.515625-0.578125j))
; 		 Line(start=(1.515625-0.578125j), end=(1.515625-5.53125j))
; 		 Line(start=(1.515625-5.53125j), end=(0.328125-5.453125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(0.765625-4.25j), control1=(0.921875-4.734375j), control2=(1.421875-5.046875j), end=(1.921875-5.046875j))
; 		 CubicBezier(start=(1.921875-5.046875j), control1=(2.640625-5.046875j), control2=(3.046875-4.390625j), end=(3.046875-3.765625j))
; 		 Line(start=(3.046875-3.765625j), end=(3.046875-3.65625j))
; 		 CubicBezier(start=(3.046875-3.65625j), control1=(2.984375-2.859375j), control2=(2.390625-2.25j), end=(1.859375-1.703125j))
; 		 Line(start=(1.859375-1.703125j), end=(1.28125-1.109375j))
; 		 CubicBezier(start=(1.28125-1.109375j), control1=(1.03125-0.859375j), control2=(0.796875-0.59375j), end=(0.546875-0.34375j))
; 		 CubicBezier(start=(0.546875-0.34375j), control1=(0.484375-0.28125j), control2=(0.421875-0.234375j), end=(0.421875-0.109375j))
; 		 Line(start=(0.421875-0.109375j), end=(0.421875-0.015625j))
; 		 Line(start=(0.421875-0.015625j), end=(3.5625-0.015625j))
; 		 Line(start=(3.5625-0.015625j), end=(3.796875-1.4375j))
; 		 Line(start=(3.796875-1.4375j), end=(3.546875-1.4375j))
; 		 CubicBezier(start=(3.546875-1.4375j), control1=(3.515625-1.21875j), control2=(3.484375-0.84375j), end=(3.359375-0.71875j))
; 		 CubicBezier(start=(3.359375-0.71875j), control1=(3.296875-0.6875j), control2=(3.171875-0.671875j), end=(3.03125-0.671875j))
; 		 Line(start=(3.03125-0.671875j), end=(2.78125-0.671875j))
; 		 CubicBezier(start=(2.78125-0.671875j), control1=(2.515625-0.671875j), control2=(2.234375-0.671875j), end=(1.96875-0.671875j))
; 		 Line(start=(1.96875-0.671875j), end=(1.171875-0.671875j))
; 		 CubicBezier(start=(1.171875-0.671875j), control1=(1.671875-1.109375j), control2=(2.171875-1.578125j), end=(2.6875-2j))
; 		 CubicBezier(start=(2.6875-2j), control1=(3.234375-2.453125j), control2=(3.796875-2.96875j), end=(3.796875-3.765625j))
; 		 CubicBezier(start=(3.796875-3.765625j), control1=(3.796875-4.796875j), control2=(2.859375-5.3125j), end=(2.015625-5.3125j))
; 		 CubicBezier(start=(2.015625-5.3125j), control1=(1.21875-5.3125j), control2=(0.453125-4.734375j), end=(0.421875-3.90625j))
; 		 Line(start=(0.421875-3.90625j), end=(0.421875-3.859375j))
; 		 CubicBezier(start=(0.421875-3.859375j), control1=(0.421875-3.609375j), control2=(0.59375-3.4375j), end=(0.84375-3.4375j))
; 		 CubicBezier(start=(0.84375-3.4375j), control1=(1.078125-3.4375j), control2=(1.25-3.625j), end=(1.25-3.84375j))
; 		 CubicBezier(start=(1.25-3.84375j), control1=(1.25-4.125j), control2=(1.03125-4.25j), end=(0.765625-4.25j))
; 		 Line(start=(0.765625-4.25j), end=(0.765625-4.25j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.984375-5.3125j), control1=(0.484375-5.09375j), control2=(0.328125-3.671875j), end=(0.328125-2.484375j))
; 		 CubicBezier(start=(0.328125-2.484375j), control1=(0.328125-1.375j), control2=(0.609375+0.15625j), end=(2.125+0.15625j))
; 		 CubicBezier(start=(2.125+0.15625j), control1=(2.875+0.15625j), control2=(3.421875-0.296875j), end=(3.6875-0.984375j))
; 		 CubicBezier(start=(3.6875-0.984375j), control1=(3.875-1.5j), control2=(3.890625-2.078125j), end=(3.890625-2.640625j))
; 		 CubicBezier(start=(3.890625-2.640625j), control1=(3.890625-3.359375j), control2=(3.796875-4.25j), end=(3.3125-4.796875j))
; 		 CubicBezier(start=(3.3125-4.796875j), control1=(3.015625-5.109375j), control2=(2.578125-5.3125j), end=(2.140625-5.3125j))
; 		 CubicBezier(start=(2.140625-5.3125j), control1=(2.09375-5.3125j), control2=(2.046875-5.3125j), end=(1.984375-5.3125j))
; 		 Line(start=(1.984375-5.3125j), end=(1.984375-5.3125j))
; 		 CubicBezier(start=(1.03125-2.09375j), control1=(1.03125-2.15625j), control2=(1.03125-2.21875j), end=(1.03125-2.28125j))
; 		 CubicBezier(start=(1.03125-2.28125j), control1=(1.03125-2.765625j), control2=(1.03125-3.265625j), end=(1.0625-3.75j))
; 		 CubicBezier(start=(1.0625-3.75j), control1=(1.09375-4.203125j), control2=(1.15625-4.640625j), end=(1.609375-4.9375j))
; 		 CubicBezier(start=(1.609375-4.9375j), control1=(1.765625-5.03125j), control2=(1.9375-5.078125j), end=(2.109375-5.078125j))
; 		 CubicBezier(start=(2.109375-5.078125j), control1=(3.125-5.078125j), control2=(3.1875-3.890625j), end=(3.1875-3.078125j))
; 		 Line(start=(3.1875-3.078125j), end=(3.1875-2.203125j))
; 		 CubicBezier(start=(3.1875-2.203125j), control1=(3.1875-1.328125j), control2=(3.125-0.4375j), end=(2.46875-0.140625j))
; 		 CubicBezier(start=(2.46875-0.140625j), control1=(2.359375-0.09375j), control2=(2.234375-0.0625j), end=(2.125-0.0625j))
; 		 CubicBezier(start=(2.125-0.0625j), control1=(1.265625-0.0625j), control2=(1.078125-0.96875j), end=(1.046875-1.859375j))
; 		 CubicBezier(start=(1.046875-1.859375j), control1=(1.046875-1.9375j), control2=(1.046875-2.015625j), end=(1.03125-2.09375j))
; 		 Line(start=(1.03125-2.09375j), end=(1.03125-2.09375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(0.96875-4.625j), control1=(1.21875-4.953125j), control2=(1.65625-5.09375j), end=(2.046875-5.09375j))
; 		 CubicBezier(start=(2.046875-5.09375j), control1=(2.53125-5.09375j), control2=(2.875-4.765625j), end=(2.875-4.21875j))
; 		 CubicBezier(start=(2.875-4.21875j), control1=(2.875-3.703125j), control2=(2.640625-3.0625j), end=(2.09375-2.9375j))
; 		 CubicBezier(start=(2.09375-2.9375j), control1=(1.890625-2.90625j), control2=(1.4375-2.96875j), end=(1.40625-2.796875j))
; 		 CubicBezier(start=(1.40625-2.796875j), control1=(1.40625-2.6875j), control2=(1.484375-2.671875j), end=(1.5625-2.671875j))
; 		 Line(start=(1.5625-2.671875j), end=(1.65625-2.671875j))
; 		 CubicBezier(start=(1.65625-2.671875j), control1=(1.75-2.671875j), control2=(1.859375-2.671875j), end=(1.953125-2.671875j))
; 		 CubicBezier(start=(1.953125-2.671875j), control1=(2.8125-2.671875j), control2=(3.046875-1.953125j), end=(3.046875-1.359375j))
; 		 Line(start=(3.046875-1.359375j), end=(3.046875-1.3125j))
; 		 CubicBezier(start=(3.046875-1.3125j), control1=(3.03125-0.765625j), control2=(2.78125-0.140625j), end=(2.125-0.078125j))
; 		 Line(start=(2.125-0.078125j), end=(2.046875-0.078125j))
; 		 CubicBezier(start=(2.046875-0.078125j), control1=(1.5625-0.078125j), control2=(0.984375-0.28125j), end=(0.734375-0.671875j))
; 		 CubicBezier(start=(0.734375-0.671875j), control1=(1.03125-0.671875j), control2=(1.21875-0.859375j), end=(1.21875-1.125j))
; 		 CubicBezier(start=(1.21875-1.125j), control1=(1.21875-1.375j), control2=(1.015625-1.546875j), end=(0.78125-1.546875j))
; 		 CubicBezier(start=(0.78125-1.546875j), control1=(0.484375-1.546875j), control2=(0.34375-1.3125j), end=(0.34375-1.078125j))
; 		 CubicBezier(start=(0.34375-1.078125j), control1=(0.34375-0.171875j), control2=(1.390625+0.15625j), end=(2.078125+0.15625j))
; 		 CubicBezier(start=(2.078125+0.15625j), control1=(2.953125+0.15625j), control2=(3.875-0.4375j), end=(3.875-1.375j))
; 		 Line(start=(3.875-1.375j), end=(3.875-1.4375j))
; 		 CubicBezier(start=(3.875-1.4375j), control1=(3.84375-2.140625j), control2=(3.203125-2.71875j), end=(2.53125-2.8125j))
; 		 CubicBezier(start=(2.53125-2.8125j), control1=(3.09375-3.03125j), control2=(3.640625-3.46875j), end=(3.640625-4.21875j))
; 		 CubicBezier(start=(3.640625-4.21875j), control1=(3.640625-4.953125j), control2=(2.765625-5.3125j), end=(2.09375-5.3125j))
; 		 CubicBezier(start=(2.09375-5.3125j), control1=(1.453125-5.3125j), control2=(0.59375-4.984375j), end=(0.59375-4.234375j))
; 		 Line(start=(0.59375-4.234375j), end=(0.59375-4.1875j))
; 		 CubicBezier(start=(0.59375-4.1875j), control1=(0.609375-3.96875j), control2=(0.765625-3.8125j), end=(0.984375-3.8125j))
; 		 CubicBezier(start=(0.984375-3.8125j), control1=(1.21875-3.8125j), control2=(1.40625-4j), end=(1.40625-4.234375j))
; 		 CubicBezier(start=(1.40625-4.234375j), control1=(1.40625-4.46875j), control2=(1.203125-4.625j), end=(0.96875-4.625j))
; 		 Line(start=(0.96875-4.625j), end=(0.96875-4.625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.109375-0.890625j), control1=(0.875-0.859375j), control2=(0.734375-0.640625j), end=(0.734375-0.453125j))
; 		 CubicBezier(start=(0.734375-0.453125j), control1=(0.734375-0.234375j), control2=(0.90625-0.015625j), end=(1.15625-0.015625j))
; 		 CubicBezier(start=(1.15625-0.015625j), control1=(1.4375-0.015625j), control2=(1.625-0.21875j), end=(1.625-0.453125j))
; 		 CubicBezier(start=(1.625-0.453125j), control1=(1.625-0.671875j), control2=(1.4375-0.890625j), end=(1.1875-0.890625j))
; 		 CubicBezier(start=(1.1875-0.890625j), control1=(1.15625-0.890625j), control2=(1.140625-0.890625j), end=(1.109375-0.890625j))
; 		 Line(start=(1.109375-0.890625j), end=(1.109375-0.890625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.28125-0.265625j), end=(0.28125-0.015625j))
; 		 Line(start=(0.28125-0.015625j), end=(0.703125-0.03125j))
; 		 Line(start=(0.703125-0.03125j), end=(1.484375-0.03125j))
; 		 Line(start=(1.484375-0.03125j), end=(1.984375-0.015625j))
; 		 Line(start=(1.984375-0.015625j), end=(1.984375-0.265625j))
; 		 CubicBezier(start=(1.984375-0.265625j), control1=(1.765625-0.265625j), control2=(1.4375-0.375j), end=(1.4375-0.65625j))
; 		 CubicBezier(start=(1.4375-0.65625j), control1=(1.4375-0.75j), control2=(1.484375-0.859375j), end=(1.515625-0.953125j))
; 		 Line(start=(1.515625-0.953125j), end=(1.796875-1.6875j))
; 		 CubicBezier(start=(1.796875-1.6875j), control1=(1.828125-1.75j), control2=(1.828125-1.828125j), end=(1.9375-1.828125j))
; 		 Line(start=(1.9375-1.828125j), end=(3.9375-1.828125j))
; 		 CubicBezier(start=(3.9375-1.828125j), control1=(4.03125-1.828125j), control2=(4.046875-1.75j), end=(4.078125-1.671875j))
; 		 Line(start=(4.078125-1.671875j), end=(4.25-1.1875j))
; 		 CubicBezier(start=(4.25-1.1875j), control1=(4.3125-1.015625j), control2=(4.375-0.859375j), end=(4.453125-0.6875j))
; 		 CubicBezier(start=(4.453125-0.6875j), control1=(4.46875-0.640625j), control2=(4.5-0.578125j), end=(4.5-0.515625j))
; 		 CubicBezier(start=(4.5-0.515625j), control1=(4.5-0.28125j), control2=(4.09375-0.265625j), end=(3.859375-0.265625j))
; 		 Line(start=(3.859375-0.265625j), end=(3.859375-0.015625j))
; 		 Line(start=(3.859375-0.015625j), end=(6.046875-0.015625j))
; 		 Line(start=(6.046875-0.015625j), end=(6.046875-0.265625j))
; 		 Line(start=(6.046875-0.265625j), end=(5.859375-0.265625j))
; 		 CubicBezier(start=(5.859375-0.265625j), control1=(5.6875-0.265625j), control2=(5.484375-0.28125j), end=(5.359375-0.375j))
; 		 CubicBezier(start=(5.359375-0.375j), control1=(5.265625-0.484375j), control2=(5.21875-0.640625j), end=(5.171875-0.765625j))
; 		 CubicBezier(start=(5.171875-0.765625j), control1=(4.59375-2.3125j), control2=(4-3.828125j), end=(3.4375-5.375j))
; 		 CubicBezier(start=(3.4375-5.375j), control1=(3.375-5.515625j), control2=(3.34375-5.703125j), end=(3.15625-5.703125j))
; 		 CubicBezier(start=(3.15625-5.703125j), control1=(2.984375-5.703125j), control2=(2.953125-5.515625j), end=(2.90625-5.375j))
; 		 CubicBezier(start=(2.90625-5.375j), control1=(2.390625-4.046875j), control2=(1.890625-2.6875j), end=(1.375-1.34375j))
; 		 Line(start=(1.375-1.34375j), end=(1.234375-0.9375j))
; 		 CubicBezier(start=(1.234375-0.9375j), control1=(1.03125-0.40625j), control2=(0.71875-0.28125j), end=(0.28125-0.265625j))
; 		 Line(start=(0.28125-0.265625j), end=(0.28125-0.265625j))
; 		 Line(start=(1.953125-2.09375j), end=(2.9375-4.6875j))
; 		 Line(start=(2.9375-4.6875j), end=(3.90625-2.09375j))
; 		 Line(start=(3.90625-2.09375j), end=(1.953125-2.09375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.3125-3.421875j), end=(0.3125-3.15625j))
; 		 Line(start=(0.3125-3.15625j), end=(0.46875-3.15625j))
; 		 CubicBezier(start=(0.46875-3.15625j), control1=(0.703125-3.15625j), control2=(0.921875-3.125j), end=(0.921875-2.765625j))
; 		 Line(start=(0.921875-2.765625j), end=(0.921875-0.609375j))
; 		 CubicBezier(start=(0.921875-0.609375j), control1=(0.921875-0.28125j), control2=(0.71875-0.265625j), end=(0.3125-0.265625j))
; 		 Line(start=(0.3125-0.265625j), end=(0.3125-0.015625j))
; 		 Line(start=(0.3125-0.015625j), end=(0.8125-0.03125j))
; 		 Line(start=(0.8125-0.03125j), end=(1.640625-0.03125j))
; 		 Line(start=(1.640625-0.03125j), end=(2.140625-0.015625j))
; 		 Line(start=(2.140625-0.015625j), end=(2.140625-0.265625j))
; 		 Line(start=(2.140625-0.265625j), end=(1.96875-0.265625j))
; 		 CubicBezier(start=(1.96875-0.265625j), control1=(1.734375-0.265625j), control2=(1.53125-0.296875j), end=(1.53125-0.59375j))
; 		 Line(start=(1.53125-0.59375j), end=(1.53125-2.0625j))
; 		 CubicBezier(start=(1.53125-2.0625j), control1=(1.53125-2.640625j), control2=(1.90625-3.296875j), end=(2.6875-3.296875j))
; 		 CubicBezier(start=(2.6875-3.296875j), control1=(3.140625-3.296875j), control2=(3.28125-2.90625j), end=(3.28125-2.421875j))
; 		 Line(start=(3.28125-2.421875j), end=(3.28125-0.578125j))
; 		 CubicBezier(start=(3.28125-0.578125j), control1=(3.28125-0.296875j), control2=(3.0625-0.265625j), end=(2.828125-0.265625j))
; 		 Line(start=(2.828125-0.265625j), end=(2.671875-0.265625j))
; 		 Line(start=(2.671875-0.265625j), end=(2.671875-0.015625j))
; 		 Line(start=(2.671875-0.015625j), end=(3.171875-0.03125j))
; 		 Line(start=(3.171875-0.03125j), end=(4.015625-0.03125j))
; 		 Line(start=(4.015625-0.03125j), end=(4.5-0.015625j))
; 		 Line(start=(4.5-0.015625j), end=(4.5-0.265625j))
; 		 Line(start=(4.5-0.265625j), end=(4.328125-0.265625j))
; 		 CubicBezier(start=(4.328125-0.265625j), control1=(4.078125-0.265625j), control2=(3.890625-0.296875j), end=(3.890625-0.59375j))
; 		 Line(start=(3.890625-0.59375j), end=(3.890625-2.0625j))
; 		 CubicBezier(start=(3.890625-2.0625j), control1=(3.890625-2.640625j), control2=(4.265625-3.296875j), end=(5.046875-3.296875j))
; 		 CubicBezier(start=(5.046875-3.296875j), control1=(5.5-3.296875j), control2=(5.625-2.90625j), end=(5.625-2.421875j))
; 		 Line(start=(5.625-2.421875j), end=(5.625-0.578125j))
; 		 CubicBezier(start=(5.625-0.578125j), control1=(5.625-0.296875j), control2=(5.40625-0.265625j), end=(5.171875-0.265625j))
; 		 Line(start=(5.171875-0.265625j), end=(5.015625-0.265625j))
; 		 Line(start=(5.015625-0.265625j), end=(5.015625-0.015625j))
; 		 Line(start=(5.015625-0.015625j), end=(5.515625-0.03125j))
; 		 Line(start=(5.515625-0.03125j), end=(6.359375-0.03125j))
; 		 Line(start=(6.359375-0.03125j), end=(6.84375-0.015625j))
; 		 Line(start=(6.84375-0.015625j), end=(6.84375-0.265625j))
; 		 Line(start=(6.84375-0.265625j), end=(6.703125-0.265625j))
; 		 CubicBezier(start=(6.703125-0.265625j), control1=(6.46875-0.265625j), control2=(6.234375-0.296875j), end=(6.234375-0.578125j))
; 		 Line(start=(6.234375-0.578125j), end=(6.234375-2.28125j))
; 		 CubicBezier(start=(6.234375-2.28125j), control1=(6.234375-2.546875j), control2=(6.234375-2.8125j), end=(6.109375-3.046875j))
; 		 CubicBezier(start=(6.109375-3.046875j), control1=(5.921875-3.390625j), control2=(5.5-3.515625j), end=(5.125-3.515625j))
; 		 CubicBezier(start=(5.125-3.515625j), control1=(4.640625-3.515625j), control2=(4.046875-3.28125j), end=(3.859375-2.78125j))
; 		 Line(start=(3.859375-2.78125j), end=(3.84375-2.78125j))
; 		 CubicBezier(start=(3.84375-2.78125j), control1=(3.78125-3.328125j), control2=(3.21875-3.515625j), end=(2.734375-3.515625j))
; 		 CubicBezier(start=(2.734375-3.515625j), control1=(2.25-3.515625j), control2=(1.671875-3.234375j), end=(1.484375-2.75j))
; 		 Line(start=(1.484375-2.75j), end=(1.484375-3.515625j))
; 		 Line(start=(1.484375-3.515625j), end=(1.1875-3.484375j))
; 		 Line(start=(1.1875-3.484375j), end=(0.3125-3.421875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(2.921875-3.171875j), control1=(2.765625-3.109375j), control2=(2.671875-2.984375j), end=(2.671875-2.828125j))
; 		 CubicBezier(start=(2.671875-2.828125j), control1=(2.671875-2.59375j), control2=(2.84375-2.453125j), end=(3.09375-2.453125j))
; 		 CubicBezier(start=(3.09375-2.453125j), control1=(3.3125-2.453125j), control2=(3.421875-2.65625j), end=(3.421875-2.828125j))
; 		 CubicBezier(start=(3.421875-2.828125j), control1=(3.421875-3.40625j), control2=(2.734375-3.515625j), end=(2.25-3.546875j))
; 		 Line(start=(2.25-3.546875j), end=(2.15625-3.546875j))
; 		 CubicBezier(start=(2.15625-3.546875j), control1=(1.125-3.546875j), control2=(0.28125-2.75j), end=(0.28125-1.71875j))
; 		 CubicBezier(start=(0.28125-1.71875j), control1=(0.28125-0.734375j), control2=(1.109375+0.078125j), end=(2.125+0.078125j))
; 		 CubicBezier(start=(2.125+0.078125j), control1=(2.6875+0.078125j), control2=(3.234375-0.21875j), end=(3.453125-0.765625j))
; 		 CubicBezier(start=(3.453125-0.765625j), control1=(3.46875-0.828125j), control2=(3.515625-0.890625j), end=(3.515625-0.953125j))
; 		 CubicBezier(start=(3.515625-0.953125j), control1=(3.515625-1.03125j), control2=(3.46875-1.0625j), end=(3.40625-1.0625j))
; 		 CubicBezier(start=(3.40625-1.0625j), control1=(3.25-1.0625j), control2=(3.28125-0.890625j), end=(3.171875-0.75j))
; 		 CubicBezier(start=(3.171875-0.75j), control1=(2.984375-0.390625j), control2=(2.625-0.171875j), end=(2.21875-0.171875j))
; 		 CubicBezier(start=(2.21875-0.171875j), control1=(1.390625-0.171875j), control2=(0.984375-0.953125j), end=(0.984375-1.765625j))
; 		 CubicBezier(start=(0.984375-1.765625j), control1=(0.984375-2.40625j), control2=(1.25-3.3125j), end=(2.171875-3.3125j))
; 		 CubicBezier(start=(2.171875-3.3125j), control1=(2.390625-3.3125j), control2=(2.703125-3.296875j), end=(2.921875-3.171875j))
; 		 Line(start=(2.921875-3.171875j), end=(2.921875-3.171875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.265625-5.453125j), end=(0.265625-5.1875j))
; 		 Line(start=(0.265625-5.1875j), end=(0.390625-5.1875j))
; 		 CubicBezier(start=(0.390625-5.1875j), control1=(0.640625-5.1875j), control2=(0.875-5.15625j), end=(0.875-4.796875j))
; 		 Line(start=(0.875-4.796875j), end=(0.875-0.015625j))
; 		 Line(start=(0.875-0.015625j), end=(1.109375-0.015625j))
; 		 Line(start=(1.109375-0.015625j), end=(1.40625-0.5j))
; 		 CubicBezier(start=(1.40625-0.5j), control1=(1.71875-0.15625j), control2=(2.0625+0.078125j), end=(2.53125+0.078125j))
; 		 CubicBezier(start=(2.53125+0.078125j), control1=(3.5+0.078125j), control2=(4.421875-0.6875j), end=(4.421875-1.71875j))
; 		 CubicBezier(start=(4.421875-1.71875j), control1=(4.421875-2.65625j), control2=(3.65625-3.515625j), end=(2.625-3.515625j))
; 		 CubicBezier(start=(2.625-3.515625j), control1=(2.171875-3.515625j), control2=(1.78125-3.34375j), end=(1.46875-3.03125j))
; 		 Line(start=(1.46875-3.03125j), end=(1.46875-5.53125j))
; 		 Line(start=(1.46875-5.53125j), end=(0.265625-5.453125j))
; 		 CubicBezier(start=(3.703125-1.546875j), control1=(3.671875-1.21875j), control2=(3.625-0.875j), end=(3.390625-0.59375j))
; 		 CubicBezier(start=(3.390625-0.59375j), control1=(3.15625-0.3125j), control2=(2.828125-0.15625j), end=(2.484375-0.15625j))
; 		 CubicBezier(start=(2.484375-0.15625j), control1=(2.140625-0.15625j), control2=(1.484375-0.46875j), end=(1.484375-0.921875j))
; 		 Line(start=(1.484375-0.921875j), end=(1.484375-2.1875j))
; 		 CubicBezier(start=(1.484375-2.1875j), control1=(1.484375-2.28125j), control2=(1.484375-2.390625j), end=(1.484375-2.484375j))
; 		 CubicBezier(start=(1.484375-2.484375j), control1=(1.484375-2.953125j), control2=(2.09375-3.296875j), end=(2.5625-3.296875j))
; 		 CubicBezier(start=(2.5625-3.296875j), control1=(3.453125-3.296875j), control2=(3.703125-2.359375j), end=(3.703125-1.71875j))
; 		 CubicBezier(start=(3.703125-1.71875j), control1=(3.703125-1.65625j), control2=(3.703125-1.609375j), end=(3.703125-1.546875j))
; 		 Line(start=(3.703125-1.546875j), end=(3.703125-1.546875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.265625-3.421875j), end=(0.265625-3.15625j))
; 		 Line(start=(0.265625-3.15625j), end=(0.390625-3.15625j))
; 		 CubicBezier(start=(0.390625-3.15625j), control1=(0.640625-3.15625j), control2=(0.875-3.140625j), end=(0.875-2.828125j))
; 		 Line(start=(0.875-2.828125j), end=(0.875+0.953125j))
; 		 CubicBezier(start=(0.875+0.953125j), control1=(0.875+1.265625j), control2=(0.65625+1.28125j), end=(0.265625+1.28125j))
; 		 Line(start=(0.265625+1.28125j), end=(0.265625+1.53125j))
; 		 Line(start=(0.265625+1.53125j), end=(0.75+1.515625j))
; 		 Line(start=(0.75+1.515625j), end=(1.59375+1.515625j))
; 		 Line(start=(1.59375+1.515625j), end=(2.09375+1.53125j))
; 		 Line(start=(2.09375+1.53125j), end=(2.09375+1.28125j))
; 		 Line(start=(2.09375+1.28125j), end=(1.90625+1.28125j))
; 		 CubicBezier(start=(1.90625+1.28125j), control1=(1.6875+1.28125j), control2=(1.484375+1.25j), end=(1.484375+0.953125j))
; 		 Line(start=(1.484375+0.953125j), end=(1.5-0.390625j))
; 		 CubicBezier(start=(1.5-0.390625j), control1=(1.671875-0.09375j), control2=(2.171875+0.078125j), end=(2.546875+0.078125j))
; 		 CubicBezier(start=(2.546875+0.078125j), control1=(3.515625+0.078125j), control2=(4.421875-0.703125j), end=(4.421875-1.71875j))
; 		 CubicBezier(start=(4.421875-1.71875j), control1=(4.421875-2.609375j), control2=(3.703125-3.515625j), end=(2.671875-3.515625j))
; 		 CubicBezier(start=(2.671875-3.515625j), control1=(2.21875-3.515625j), control2=(1.796875-3.359375j), end=(1.46875-3.03125j))
; 		 Line(start=(1.46875-3.03125j), end=(1.46875-3.515625j))
; 		 Line(start=(1.46875-3.515625j), end=(0.265625-3.421875j))
; 		 CubicBezier(start=(3.703125-1.59375j), control1=(3.671875-0.953125j), control2=(3.25-0.15625j), end=(2.46875-0.15625j))
; 		 CubicBezier(start=(2.46875-0.15625j), control1=(2.0625-0.15625j), control2=(1.484375-0.546875j), end=(1.484375-0.90625j))
; 		 Line(start=(1.484375-0.90625j), end=(1.484375-2.171875j))
; 		 CubicBezier(start=(1.484375-2.171875j), control1=(1.484375-2.265625j), control2=(1.484375-2.359375j), end=(1.484375-2.46875j))
; 		 CubicBezier(start=(1.484375-2.46875j), control1=(1.484375-2.921875j), control2=(2.078125-3.265625j), end=(2.546875-3.265625j))
; 		 CubicBezier(start=(2.546875-3.265625j), control1=(3.34375-3.265625j), control2=(3.703125-2.34375j), end=(3.703125-1.71875j))
; 		 CubicBezier(start=(3.703125-1.71875j), control1=(3.703125-1.6875j), control2=(3.703125-1.640625j), end=(3.703125-1.59375j))
; 		 Line(start=(3.703125-1.59375j), end=(3.703125-1.59375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.0625-3.125j), end=(1.0625-3.140625j))
; 		 CubicBezier(start=(1.0625-3.140625j), control1=(1.328125-3.265625j), control2=(1.546875-3.328125j), end=(1.84375-3.328125j))
; 		 CubicBezier(start=(1.84375-3.328125j), control1=(2.375-3.328125j), control2=(2.734375-2.921875j), end=(2.734375-2.3125j))
; 		 Line(start=(2.734375-2.3125j), end=(2.734375-2.125j))
; 		 CubicBezier(start=(2.734375-2.125j), control1=(1.75-2.125j), control2=(0.34375-1.8125j), end=(0.34375-0.8125j))
; 		 CubicBezier(start=(0.34375-0.8125j), control1=(0.34375-0.140625j), control2=(1.15625+0.078125j), end=(1.671875+0.078125j))
; 		 CubicBezier(start=(1.671875+0.078125j), control1=(2.125+0.078125j), control2=(2.609375-0.125j), end=(2.828125-0.578125j))
; 		 CubicBezier(start=(2.828125-0.578125j), control1=(2.828125-0.234375j), control2=(3.109375+0.03125j), end=(3.4375+0.03125j))
; 		 CubicBezier(start=(3.4375+0.03125j), control1=(3.84375+0.03125j), control2=(4.171875-0.265625j), end=(4.171875-0.6875j))
; 		 Line(start=(4.171875-0.6875j), end=(4.171875-1.15625j))
; 		 Line(start=(4.171875-1.15625j), end=(3.921875-1.15625j))
; 		 Line(start=(3.921875-1.15625j), end=(3.921875-0.765625j))
; 		 CubicBezier(start=(3.921875-0.765625j), control1=(3.921875-0.5625j), control2=(3.890625-0.265625j), end=(3.640625-0.265625j))
; 		 CubicBezier(start=(3.640625-0.265625j), control1=(3.390625-0.265625j), control2=(3.359375-0.5625j), end=(3.359375-0.734375j))
; 		 Line(start=(3.359375-0.734375j), end=(3.359375-2.078125j))
; 		 CubicBezier(start=(3.359375-2.078125j), control1=(3.359375-2.171875j), control2=(3.359375-2.28125j), end=(3.359375-2.375j))
; 		 CubicBezier(start=(3.359375-2.375j), control1=(3.359375-3.171875j), control2=(2.546875-3.546875j), end=(1.84375-3.546875j))
; 		 CubicBezier(start=(1.84375-3.546875j), control1=(1.421875-3.546875j), control2=(0.609375-3.421875j), end=(0.609375-2.78125j))
; 		 CubicBezier(start=(0.609375-2.78125j), control1=(0.609375-2.609375j), control2=(0.71875-2.40625j), end=(0.953125-2.40625j))
; 		 CubicBezier(start=(0.953125-2.40625j), control1=(1.21875-2.40625j), control2=(1.359375-2.5625j), end=(1.359375-2.765625j))
; 		 CubicBezier(start=(1.359375-2.765625j), control1=(1.359375-2.9375j), control2=(1.265625-3.09375j), end=(1.0625-3.125j))
; 		 Line(start=(1.0625-3.125j), end=(1.0625-3.125j))
; 		 Line(start=(2.734375-1.921875j), end=(2.734375-1.140625j))
; 		 CubicBezier(start=(2.734375-1.140625j), control1=(2.734375-0.578125j), control2=(2.3125-0.15625j), end=(1.765625-0.15625j))
; 		 CubicBezier(start=(1.765625-0.15625j), control1=(1.40625-0.15625j), control2=(1.03125-0.390625j), end=(1-0.765625j))
; 		 Line(start=(1-0.765625j), end=(1-0.8125j))
; 		 CubicBezier(start=(1-0.8125j), control1=(1-1.671875j), control2=(2.078125-1.875j), end=(2.734375-1.921875j))
; 		 Line(start=(2.734375-1.921875j), end=(2.734375-1.921875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(2.625-5.453125j), end=(2.625-5.1875j))
; 		 Line(start=(2.625-5.1875j), end=(2.703125-5.1875j))
; 		 CubicBezier(start=(2.703125-5.1875j), control1=(3.015625-5.1875j), control2=(3.234375-5.171875j), end=(3.234375-4.703125j))
; 		 Line(start=(3.234375-4.703125j), end=(3.234375-3.0625j))
; 		 CubicBezier(start=(3.234375-3.0625j), control1=(2.9375-3.34375j), control2=(2.578125-3.515625j), end=(2.15625-3.515625j))
; 		 CubicBezier(start=(2.15625-3.515625j), control1=(1.171875-3.515625j), control2=(0.28125-2.75j), end=(0.28125-1.71875j))
; 		 CubicBezier(start=(0.28125-1.71875j), control1=(0.28125-0.734375j), control2=(1.09375+0.078125j), end=(2.078125+0.078125j))
; 		 CubicBezier(start=(2.078125+0.078125j), control1=(2.5+0.078125j), control2=(2.921875-0.109375j), end=(3.203125-0.4375j))
; 		 Line(start=(3.203125-0.4375j), end=(3.203125+0.078125j))
; 		 Line(start=(3.203125+0.078125j), end=(4.421875-0.015625j))
; 		 Line(start=(4.421875-0.015625j), end=(4.421875-0.265625j))
; 		 Line(start=(4.421875-0.265625j), end=(4.296875-0.265625j))
; 		 CubicBezier(start=(4.296875-0.265625j), control1=(4.0625-0.265625j), control2=(3.8125-0.296875j), end=(3.8125-0.671875j))
; 		 Line(start=(3.8125-0.671875j), end=(3.8125-5.53125j))
; 		 Line(start=(3.8125-5.53125j), end=(2.625-5.453125j))
; 		 Line(start=(0.984375-1.546875j), end=(0.984375-1.65625j))
; 		 CubicBezier(start=(0.984375-1.65625j), control1=(0.984375-2.21875j), control2=(1.09375-2.890625j), end=(1.703125-3.171875j))
; 		 CubicBezier(start=(1.703125-3.171875j), control1=(1.859375-3.25j), control2=(2.046875-3.296875j), end=(2.203125-3.296875j))
; 		 CubicBezier(start=(2.203125-3.296875j), control1=(2.5625-3.296875j), control2=(2.953125-3.09375j), end=(3.140625-2.765625j))
; 		 CubicBezier(start=(3.140625-2.765625j), control1=(3.171875-2.734375j), control2=(3.203125-2.703125j), end=(3.203125-2.65625j))
; 		 Line(start=(3.203125-2.65625j), end=(3.203125-0.9375j))
; 		 CubicBezier(start=(3.203125-0.9375j), control1=(3.203125-0.53125j), control2=(2.546875-0.15625j), end=(2.140625-0.15625j))
; 		 CubicBezier(start=(2.140625-0.15625j), control1=(1.546875-0.15625j), control2=(1.125-0.625j), end=(1.03125-1.1875j))
; 		 CubicBezier(start=(1.03125-1.1875j), control1=(1.015625-1.3125j), control2=(1-1.421875j), end=(0.984375-1.546875j))
; 		 Line(start=(0.984375-1.546875j), end=(0.984375-1.546875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.34375-5.421875j), end=(0.34375-5.15625j))
; 		 CubicBezier(start=(0.34375-5.15625j), control1=(0.453125-5.15625j), control2=(0.546875-5.171875j), end=(0.640625-5.171875j))
; 		 CubicBezier(start=(0.640625-5.171875j), control1=(0.921875-5.171875j), control2=(1.15625-5.125j), end=(1.15625-4.828125j))
; 		 Line(start=(1.15625-4.828125j), end=(1.15625-0.625j))
; 		 CubicBezier(start=(1.15625-0.625j), control1=(1.15625-0.296875j), control2=(0.890625-0.265625j), end=(0.625-0.265625j))
; 		 CubicBezier(start=(0.625-0.265625j), control1=(0.53125-0.265625j), control2=(0.4375-0.265625j), end=(0.34375-0.265625j))
; 		 Line(start=(0.34375-0.265625j), end=(0.34375-0.015625j))
; 		 Line(start=(0.34375-0.015625j), end=(5.15625-0.015625j))
; 		 Line(start=(5.15625-0.015625j), end=(5.515625-2.078125j))
; 		 Line(start=(5.515625-2.078125j), end=(5.265625-2.078125j))
; 		 CubicBezier(start=(5.265625-2.078125j), control1=(5.171875-1.625j), control2=(5.109375-1.09375j), end=(4.8125-0.71875j))
; 		 CubicBezier(start=(4.8125-0.71875j), control1=(4.4375-0.28125j), control2=(3.828125-0.265625j), end=(3.296875-0.265625j))
; 		 Line(start=(3.296875-0.265625j), end=(2.203125-0.265625j))
; 		 CubicBezier(start=(2.203125-0.265625j), control1=(1.984375-0.265625j), control2=(1.890625-0.328125j), end=(1.890625-0.5625j))
; 		 Line(start=(1.890625-0.5625j), end=(1.890625-2.6875j))
; 		 Line(start=(1.890625-2.6875j), end=(2.671875-2.6875j))
; 		 CubicBezier(start=(2.671875-2.6875j), control1=(2.9375-2.6875j), control2=(3.234375-2.671875j), end=(3.390625-2.484375j))
; 		 CubicBezier(start=(3.390625-2.484375j), control1=(3.546875-2.296875j), control2=(3.546875-2.0625j), end=(3.546875-1.828125j))
; 		 Line(start=(3.546875-1.828125j), end=(3.546875-1.765625j))
; 		 Line(start=(3.546875-1.765625j), end=(3.796875-1.765625j))
; 		 Line(start=(3.796875-1.765625j), end=(3.796875-3.890625j))
; 		 Line(start=(3.796875-3.890625j), end=(3.546875-3.890625j))
; 		 Line(start=(3.546875-3.890625j), end=(3.546875-3.828125j))
; 		 CubicBezier(start=(3.546875-3.828125j), control1=(3.546875-3.59375j), control2=(3.546875-3.34375j), end=(3.390625-3.15625j))
; 		 CubicBezier(start=(3.390625-3.15625j), control1=(3.234375-2.96875j), control2=(2.9375-2.953125j), end=(2.671875-2.953125j))
; 		 Line(start=(2.671875-2.953125j), end=(1.890625-2.953125j))
; 		 Line(start=(1.890625-2.953125j), end=(1.890625-4.890625j))
; 		 CubicBezier(start=(1.890625-4.890625j), control1=(1.890625-5.15625j), control2=(2.0625-5.15625j), end=(2.234375-5.15625j))
; 		 Line(start=(2.234375-5.15625j), end=(3.28125-5.15625j))
; 		 CubicBezier(start=(3.28125-5.15625j), control1=(3.859375-5.15625j), control2=(4.59375-5.140625j), end=(4.859375-4.4375j))
; 		 CubicBezier(start=(4.859375-4.4375j), control1=(4.953125-4.171875j), control2=(4.984375-3.890625j), end=(5.03125-3.625j))
; 		 Line(start=(5.03125-3.625j), end=(5.28125-3.625j))
; 		 Line(start=(5.28125-3.625j), end=(5.046875-5.421875j))
; 		 Line(start=(5.046875-5.421875j), end=(0.34375-5.421875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(0.984375-0.390625j), control1=(1.515625+0.015625j), control2=(1.9375+0.15625j), end=(2.59375+0.15625j))
; 		 CubicBezier(start=(2.59375+0.15625j), control1=(3.46875+0.15625j), control2=(4.21875-0.578125j), end=(4.21875-1.484375j))
; 		 CubicBezier(start=(4.21875-1.484375j), control1=(4.21875-2.171875j), control2=(3.78125-2.8125j), end=(3.109375-3.03125j))
; 		 CubicBezier(start=(3.109375-3.03125j), control1=(2.796875-3.140625j), control2=(2.46875-3.1875j), end=(2.140625-3.265625j))
; 		 CubicBezier(start=(2.140625-3.265625j), control1=(1.9375-3.3125j), control2=(1.734375-3.34375j), end=(1.546875-3.4375j))
; 		 CubicBezier(start=(1.546875-3.4375j), control1=(1.21875-3.625j), control2=(0.96875-3.96875j), end=(0.96875-4.34375j))
; 		 CubicBezier(start=(0.96875-4.34375j), control1=(0.96875-4.984375j), control2=(1.578125-5.375j), end=(2.15625-5.375j))
; 		 CubicBezier(start=(2.15625-5.375j), control1=(2.984375-5.375j), control2=(3.625-4.78125j), end=(3.734375-3.8125j))
; 		 CubicBezier(start=(3.734375-3.8125j), control1=(3.734375-3.71875j), control2=(3.765625-3.640625j), end=(3.875-3.640625j))
; 		 CubicBezier(start=(3.875-3.640625j), control1=(4-3.640625j), control2=(4-3.71875j), end=(4-3.796875j))
; 		 Line(start=(4-3.796875j), end=(4-5.484375j))
; 		 CubicBezier(start=(4-5.484375j), control1=(4-5.53125j), control2=(3.96875-5.625j), end=(3.890625-5.625j))
; 		 CubicBezier(start=(3.890625-5.625j), control1=(3.78125-5.625j), control2=(3.6875-5.421875j), end=(3.625-5.3125j))
; 		 CubicBezier(start=(3.625-5.3125j), control1=(3.578125-5.234375j), control2=(3.515625-5.140625j), end=(3.46875-5.0625j))
; 		 CubicBezier(start=(3.46875-5.0625j), control1=(3.109375-5.421875j), control2=(2.671875-5.625j), end=(2.15625-5.625j))
; 		 CubicBezier(start=(2.15625-5.625j), control1=(1.375-5.625j), control2=(0.46875-5.09375j), end=(0.46875-4.09375j))
; 		 CubicBezier(start=(0.46875-4.09375j), control1=(0.46875-3.453125j), control2=(0.875-2.921875j), end=(1.453125-2.6875j))
; 		 CubicBezier(start=(1.453125-2.6875j), control1=(1.78125-2.5625j), control2=(2.15625-2.5j), end=(2.5-2.421875j))
; 		 CubicBezier(start=(2.5-2.421875j), control1=(2.6875-2.375j), control2=(2.890625-2.34375j), end=(3.078125-2.25j))
; 		 CubicBezier(start=(3.078125-2.25j), control1=(3.4375-2.078125j), control2=(3.71875-1.6875j), end=(3.71875-1.28125j))
; 		 CubicBezier(start=(3.71875-1.28125j), control1=(3.71875-0.625j), control2=(3.21875-0.109375j), end=(2.546875-0.109375j))
; 		 CubicBezier(start=(2.546875-0.109375j), control1=(1.765625-0.109375j), control2=(0.90625-0.46875j), end=(0.734375-1.421875j))
; 		 CubicBezier(start=(0.734375-1.421875j), control1=(0.71875-1.484375j), control2=(0.71875-1.5625j), end=(0.71875-1.640625j))
; 		 Line(start=(0.71875-1.640625j), end=(0.71875-1.6875j))
; 		 CubicBezier(start=(0.71875-1.6875j), control1=(0.71875-1.765625j), control2=(0.671875-1.8125j), end=(0.59375-1.8125j))
; 		 CubicBezier(start=(0.59375-1.8125j), control1=(0.484375-1.8125j), control2=(0.46875-1.734375j), end=(0.46875-1.65625j))
; 		 Line(start=(0.46875-1.65625j), end=(0.46875+0.03125j))
; 		 CubicBezier(start=(0.46875+0.03125j), control1=(0.46875+0.078125j), control2=(0.484375+0.15625j), end=(0.578125+0.15625j))
; 		 CubicBezier(start=(0.578125+0.15625j), control1=(0.671875+0.15625j), control2=(0.890625-0.1875j), end=(0.984375-0.390625j))
; 		 Line(start=(0.984375-0.390625j), end=(0.984375-0.390625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(1.109375-3.421875j), control1=(0.875-3.390625j), control2=(0.734375-3.1875j), end=(0.734375-2.984375j))
; 		 CubicBezier(start=(0.734375-2.984375j), control1=(0.734375-2.765625j), control2=(0.90625-2.546875j), end=(1.15625-2.546875j))
; 		 CubicBezier(start=(1.15625-2.546875j), control1=(1.4375-2.546875j), control2=(1.625-2.765625j), end=(1.625-3j))
; 		 CubicBezier(start=(1.625-3j), control1=(1.625-3.21875j), control2=(1.4375-3.4375j), end=(1.1875-3.4375j))
; 		 CubicBezier(start=(1.1875-3.4375j), control1=(1.15625-3.4375j), control2=(1.140625-3.421875j), end=(1.109375-3.421875j))
; 		 Line(start=(1.109375-3.421875j), end=(1.109375-3.421875j))
; 		 CubicBezier(start=(1.109375-0.890625j), control1=(0.875-0.859375j), control2=(0.734375-0.640625j), end=(0.734375-0.453125j))
; 		 CubicBezier(start=(0.734375-0.453125j), control1=(0.734375-0.234375j), control2=(0.90625-0.015625j), end=(1.15625-0.015625j))
; 		 CubicBezier(start=(1.15625-0.015625j), control1=(1.4375-0.015625j), control2=(1.625-0.21875j), end=(1.625-0.453125j))
; 		 CubicBezier(start=(1.625-0.453125j), control1=(1.625-0.671875j), control2=(1.4375-0.890625j), end=(1.1875-0.890625j))
; 		 CubicBezier(start=(1.1875-0.890625j), control1=(1.15625-0.890625j), control2=(1.140625-0.890625j), end=(1.109375-0.890625j))
; 		 Line(start=(1.109375-0.890625j), end=(1.109375-0.890625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(3.609375-5.96875j), control1=(3.515625-5.9375j), control2=(3.453125-5.71875j), end=(3.40625-5.59375j))
; 		 CubicBezier(start=(3.40625-5.59375j), control1=(2.453125-3.21875j), control2=(1.515625-0.796875j), end=(0.578125+1.578125j))
; 		 CubicBezier(start=(0.578125+1.578125j), control1=(0.546875+1.65625j), control2=(0.46875+1.796875j), end=(0.46875+1.875j))
; 		 CubicBezier(start=(0.46875+1.875j), control1=(0.484375+1.921875j), control2=(0.515625+1.96875j), end=(0.578125+1.96875j))
; 		 Line(start=(0.578125+1.96875j), end=(0.59375+1.96875j))
; 		 CubicBezier(start=(0.59375+1.96875j), control1=(0.703125+1.953125j), control2=(0.765625+1.703125j), end=(0.828125+1.5625j))
; 		 CubicBezier(start=(0.828125+1.5625j), control1=(1.640625-0.453125j), control2=(2.40625-2.46875j), end=(3.21875-4.484375j))
; 		 CubicBezier(start=(3.21875-4.484375j), control1=(3.375-4.875j), control2=(3.515625-5.25j), end=(3.671875-5.640625j))
; 		 CubicBezier(start=(3.671875-5.640625j), control1=(3.703125-5.71875j), control2=(3.75-5.796875j), end=(3.75-5.875j))
; 		 CubicBezier(start=(3.75-5.875j), control1=(3.75-5.9375j), control2=(3.703125-5.984375j), end=(3.640625-5.984375j))
; 		 CubicBezier(start=(3.640625-5.984375j), control1=(3.640625-5.984375j), control2=(3.625-5.96875j), end=(3.609375-5.96875j))
; 		 Line(start=(3.609375-5.96875j), end=(3.609375-5.96875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.765625-4.796875j), end=(0.765625-4.53125j))
; 		 CubicBezier(start=(0.765625-4.53125j), control1=(1.15625-4.53125j), control2=(1.515625-4.59375j), end=(1.875-4.75j))
; 		 Line(start=(1.875-4.75j), end=(1.875-0.65625j))
; 		 CubicBezier(start=(1.875-0.65625j), control1=(1.875-0.265625j), control2=(1.546875-0.265625j), end=(1.09375-0.265625j))
; 		 Line(start=(1.09375-0.265625j), end=(0.8125-0.265625j))
; 		 Line(start=(0.8125-0.265625j), end=(0.8125-0.015625j))
; 		 Line(start=(0.8125-0.015625j), end=(3.5625-0.015625j))
; 		 Line(start=(3.5625-0.015625j), end=(3.5625-0.265625j))
; 		 CubicBezier(start=(3.5625-0.265625j), control1=(3.453125-0.265625j), control2=(3.328125-0.265625j), end=(3.203125-0.265625j))
; 		 CubicBezier(start=(3.203125-0.265625j), control1=(2.78125-0.265625j), control2=(2.515625-0.28125j), end=(2.515625-0.65625j))
; 		 Line(start=(2.515625-0.65625j), end=(2.515625-5.15625j))
; 		 CubicBezier(start=(2.515625-5.15625j), control1=(2.515625-5.234375j), control2=(2.484375-5.3125j), end=(2.375-5.3125j))
; 		 CubicBezier(start=(2.375-5.3125j), control1=(2.21875-5.3125j), control2=(2.140625-5.1875j), end=(2.03125-5.109375j))
; 		 CubicBezier(start=(2.03125-5.109375j), control1=(1.65625-4.875j), control2=(1.21875-4.796875j), end=(0.765625-4.796875j))
; 		 Line(start=(0.765625-4.796875j), end=(0.765625-4.796875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.3125-3.421875j), end=(0.3125-3.15625j))
; 		 Line(start=(0.3125-3.15625j), end=(0.421875-3.15625j))
; 		 CubicBezier(start=(0.421875-3.15625j), control1=(0.625-3.15625j), control2=(0.875-3.140625j), end=(0.90625-2.921875j))
; 		 CubicBezier(start=(0.90625-2.921875j), control1=(0.921875-2.84375j), control2=(0.921875-2.75j), end=(0.921875-2.671875j))
; 		 Line(start=(0.921875-2.671875j), end=(0.921875-1.09375j))
; 		 CubicBezier(start=(0.921875-1.09375j), control1=(0.921875-0.859375j), control2=(0.9375-0.625j), end=(1.046875-0.421875j))
; 		 CubicBezier(start=(1.046875-0.421875j), control1=(1.28125-0.03125j), control2=(1.78125+0.078125j), end=(2.171875+0.078125j))
; 		 CubicBezier(start=(2.171875+0.078125j), control1=(2.546875+0.078125j), control2=(2.890625-0.046875j), end=(3.109375-0.328125j))
; 		 CubicBezier(start=(3.109375-0.328125j), control1=(3.171875-0.390625j), control2=(3.25-0.484375j), end=(3.28125-0.578125j))
; 		 Line(start=(3.28125-0.578125j), end=(3.296875+0.078125j))
; 		 Line(start=(3.296875+0.078125j), end=(4.484375-0.015625j))
; 		 Line(start=(4.484375-0.015625j), end=(4.484375-0.265625j))
; 		 Line(start=(4.484375-0.265625j), end=(4.328125-0.265625j))
; 		 CubicBezier(start=(4.328125-0.265625j), control1=(4.109375-0.265625j), control2=(3.875-0.296875j), end=(3.875-0.65625j))
; 		 Line(start=(3.875-0.65625j), end=(3.875-3.515625j))
; 		 Line(start=(3.875-3.515625j), end=(2.65625-3.421875j))
; 		 Line(start=(2.65625-3.421875j), end=(2.65625-3.15625j))
; 		 Line(start=(2.65625-3.15625j), end=(2.75-3.15625j))
; 		 CubicBezier(start=(2.75-3.15625j), control1=(3.03125-3.15625j), control2=(3.265625-3.140625j), end=(3.265625-2.765625j))
; 		 Line(start=(3.265625-2.765625j), end=(3.265625-1.40625j))
; 		 CubicBezier(start=(3.265625-1.40625j), control1=(3.265625-0.765625j), control2=(2.921875-0.15625j), end=(2.234375-0.15625j))
; 		 CubicBezier(start=(2.234375-0.15625j), control1=(1.953125-0.15625j), control2=(1.59375-0.21875j), end=(1.546875-0.625j))
; 		 CubicBezier(start=(1.546875-0.625j), control1=(1.53125-0.765625j), control2=(1.53125-0.921875j), end=(1.53125-1.0625j))
; 		 Line(start=(1.53125-1.0625j), end=(1.53125-3.515625j))
; 		 Line(start=(1.53125-3.515625j), end=(0.3125-3.421875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.265625-5.453125j), end=(0.265625-5.1875j))
; 		 Line(start=(0.265625-5.1875j), end=(0.421875-5.1875j))
; 		 CubicBezier(start=(0.421875-5.1875j), control1=(0.671875-5.1875j), control2=(0.875-5.15625j), end=(0.875-4.796875j))
; 		 Line(start=(0.875-4.796875j), end=(0.875-0.78125j))
; 		 CubicBezier(start=(0.875-0.78125j), control1=(0.875-0.734375j), control2=(0.890625-0.671875j), end=(0.890625-0.609375j))
; 		 CubicBezier(start=(0.890625-0.609375j), control1=(0.890625-0.28125j), control2=(0.640625-0.265625j), end=(0.3125-0.265625j))
; 		 Line(start=(0.3125-0.265625j), end=(0.265625-0.265625j))
; 		 Line(start=(0.265625-0.265625j), end=(0.265625-0.015625j))
; 		 Line(start=(0.265625-0.015625j), end=(0.734375-0.03125j))
; 		 Line(start=(0.734375-0.03125j), end=(1.546875-0.03125j))
; 		 Line(start=(1.546875-0.03125j), end=(2.046875-0.015625j))
; 		 Line(start=(2.046875-0.015625j), end=(2.046875-0.265625j))
; 		 Line(start=(2.046875-0.265625j), end=(1.875-0.265625j))
; 		 CubicBezier(start=(1.875-0.265625j), control1=(1.640625-0.265625j), control2=(1.4375-0.296875j), end=(1.4375-0.59375j))
; 		 Line(start=(1.4375-0.59375j), end=(1.4375-1.3125j))
; 		 CubicBezier(start=(1.4375-1.3125j), control1=(1.4375-1.4375j), control2=(1.53125-1.46875j), end=(1.609375-1.546875j))
; 		 CubicBezier(start=(1.609375-1.546875j), control1=(1.734375-1.640625j), control2=(1.859375-1.75j), end=(1.984375-1.84375j))
; 		 CubicBezier(start=(1.984375-1.84375j), control1=(2.0625-1.703125j), control2=(2.171875-1.578125j), end=(2.265625-1.46875j))
; 		 CubicBezier(start=(2.265625-1.46875j), control1=(2.515625-1.140625j), control2=(2.984375-0.65625j), end=(2.984375-0.4375j))
; 		 CubicBezier(start=(2.984375-0.4375j), control1=(2.984375-0.3125j), control2=(2.875-0.265625j), end=(2.734375-0.265625j))
; 		 Line(start=(2.734375-0.265625j), end=(2.734375-0.015625j))
; 		 Line(start=(2.734375-0.015625j), end=(3.25-0.03125j))
; 		 Line(start=(3.25-0.03125j), end=(3.953125-0.03125j))
; 		 Line(start=(3.953125-0.03125j), end=(4.328125-0.015625j))
; 		 Line(start=(4.328125-0.015625j), end=(4.328125-0.265625j))
; 		 CubicBezier(start=(4.328125-0.265625j), control1=(3.84375-0.265625j), control2=(3.671875-0.53125j), end=(3.3125-1j))
; 		 CubicBezier(start=(3.3125-1j), control1=(3.015625-1.390625j), control2=(2.671875-1.78125j), end=(2.40625-2.1875j))
; 		 CubicBezier(start=(2.40625-2.1875j), control1=(2.53125-2.25j), control2=(2.65625-2.375j), end=(2.765625-2.46875j))
; 		 CubicBezier(start=(2.765625-2.46875j), control1=(3.15625-2.78125j), control2=(3.609375-3.171875j), end=(4.140625-3.171875j))
; 		 Line(start=(4.140625-3.171875j), end=(4.140625-3.4375j))
; 		 Line(start=(4.140625-3.4375j), end=(3.859375-3.40625j))
; 		 Line(start=(3.859375-3.40625j), end=(3.171875-3.40625j))
; 		 Line(start=(3.171875-3.40625j), end=(2.65625-3.4375j))
; 		 Line(start=(2.65625-3.4375j), end=(2.65625-3.171875j))
; 		 CubicBezier(start=(2.65625-3.171875j), control1=(2.75-3.171875j), control2=(2.84375-3.09375j), end=(2.84375-3j))
; 		 CubicBezier(start=(2.84375-3j), control1=(2.84375-2.75j), control2=(2.4375-2.515625j), end=(2.203125-2.34375j))
; 		 CubicBezier(start=(2.203125-2.34375j), control1=(1.953125-2.140625j), control2=(1.71875-1.9375j), end=(1.46875-1.75j))
; 		 Line(start=(1.46875-1.75j), end=(1.46875-5.53125j))
; 		 Line(start=(1.46875-5.53125j), end=(0.265625-5.453125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(1.109375-2.65625j), end=(1.09375-2.65625j))
; 		 CubicBezier(start=(1.09375-2.65625j), control1=(1.09375-3.40625j), control2=(1.171875-4.25j), end=(1.78125-4.78125j))
; 		 CubicBezier(start=(1.78125-4.78125j), control1=(2.015625-4.984375j), control2=(2.296875-5.09375j), end=(2.609375-5.09375j))
; 		 CubicBezier(start=(2.609375-5.09375j), control1=(2.859375-5.09375j), control2=(3.140625-5.015625j), end=(3.296875-4.796875j))
; 		 CubicBezier(start=(3.296875-4.796875j), control1=(3.078125-4.796875j), control2=(2.90625-4.640625j), end=(2.90625-4.421875j))
; 		 CubicBezier(start=(2.90625-4.421875j), control1=(2.90625-4.234375j), control2=(3.0625-4.046875j), end=(3.28125-4.046875j))
; 		 CubicBezier(start=(3.28125-4.046875j), control1=(3.46875-4.046875j), control2=(3.640625-4.171875j), end=(3.65625-4.375j))
; 		 Line(start=(3.65625-4.375j), end=(3.65625-4.453125j))
; 		 CubicBezier(start=(3.65625-4.453125j), control1=(3.65625-5.046875j), control2=(3.109375-5.3125j), end=(2.59375-5.3125j))
; 		 CubicBezier(start=(2.59375-5.3125j), control1=(1.21875-5.3125j), control2=(0.34375-3.890625j), end=(0.34375-2.609375j))
; 		 CubicBezier(start=(0.34375-2.609375j), control1=(0.34375-1.6875j), control2=(0.46875-0.5j), end=(1.421875+0j))
; 		 CubicBezier(start=(1.421875+0j), control1=(1.65625+0.109375j), control2=(1.890625+0.15625j), end=(2.15625+0.15625j))
; 		 CubicBezier(start=(2.15625+0.15625j), control1=(3.109375+0.15625j), control2=(3.875-0.640625j), end=(3.875-1.609375j))
; 		 CubicBezier(start=(3.875-1.609375j), control1=(3.875-2.53125j), control2=(3.21875-3.40625j), end=(2.21875-3.40625j))
; 		 CubicBezier(start=(2.21875-3.40625j), control1=(1.84375-3.40625j), control2=(1.5-3.25j), end=(1.28125-2.953125j))
; 		 CubicBezier(start=(1.28125-2.953125j), control1=(1.21875-2.859375j), control2=(1.140625-2.765625j), end=(1.109375-2.65625j))
; 		 Line(start=(1.109375-2.65625j), end=(1.109375-2.65625j))
; 		 Line(start=(1.125-1.640625j), end=(1.125-1.953125j))
; 		 CubicBezier(start=(1.125-1.953125j), control1=(1.171875-2.53125j), control2=(1.515625-3.171875j), end=(2.1875-3.171875j))
; 		 CubicBezier(start=(2.1875-3.171875j), control1=(2.84375-3.171875j), control2=(3.109375-2.53125j), end=(3.125-1.90625j))
; 		 Line(start=(3.125-1.90625j), end=(3.125-1.515625j))
; 		 CubicBezier(start=(3.125-1.515625j), control1=(3.125-1j), control2=(3.03125-0.078125j), end=(2.140625-0.078125j))
; 		 CubicBezier(start=(2.140625-0.078125j), control1=(1.328125-0.078125j), control2=(1.140625-0.96875j), end=(1.125-1.640625j))
; 		 Line(start=(1.125-1.640625j), end=(1.125-1.640625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(0.96875-0.171875j), control1=(0.609375-0.109375j), control2=(0.234375+0.21875j), end=(0.234375+0.609375j))
; 		 CubicBezier(start=(0.234375+0.609375j), control1=(0.234375+1.4375j), control2=(1.546875+1.625j), end=(2.109375+1.625j))
; 		 CubicBezier(start=(2.109375+1.625j), control1=(2.765625+1.625j), control2=(4+1.421875j), end=(4+0.59375j))
; 		 CubicBezier(start=(4+0.59375j), control1=(4-0.40625j), control2=(2.9375-0.5625j), end=(2.140625-0.5625j))
; 		 Line(start=(2.140625-0.5625j), end=(1.453125-0.5625j))
; 		 CubicBezier(start=(1.453125-0.5625j), control1=(1.140625-0.5625j), control2=(0.9375-0.8125j), end=(0.9375-1.09375j))
; 		 Line(start=(0.9375-1.09375j), end=(0.9375-1.125j))
; 		 CubicBezier(start=(0.9375-1.125j), control1=(0.953125-1.234375j), control2=(0.984375-1.375j), end=(1.0625-1.390625j))
; 		 CubicBezier(start=(1.0625-1.390625j), control1=(1.09375-1.390625j), control2=(1.171875-1.34375j), end=(1.21875-1.3125j))
; 		 CubicBezier(start=(1.21875-1.3125j), control1=(1.421875-1.21875j), control2=(1.65625-1.171875j), end=(1.875-1.171875j))
; 		 CubicBezier(start=(1.875-1.171875j), control1=(2.515625-1.171875j), control2=(3.25-1.59375j), end=(3.25-2.328125j))
; 		 CubicBezier(start=(3.25-2.328125j), control1=(3.25-2.5625j), control2=(3.171875-2.8125j), end=(3.03125-2.984375j))
; 		 CubicBezier(start=(3.03125-2.984375j), control1=(3-3.015625j), control2=(2.953125-3.0625j), end=(2.953125-3.09375j))
; 		 CubicBezier(start=(2.953125-3.09375j), control1=(2.953125-3.21875j), control2=(3.421875-3.375j), end=(3.6875-3.375j))
; 		 CubicBezier(start=(3.6875-3.375j), control1=(3.640625-3.328125j), control2=(3.609375-3.265625j), end=(3.609375-3.1875j))
; 		 CubicBezier(start=(3.609375-3.1875j), control1=(3.609375-3.046875j), control2=(3.71875-2.9375j), end=(3.859375-2.9375j))
; 		 CubicBezier(start=(3.859375-2.9375j), control1=(4-2.9375j), control2=(4.109375-3.046875j), end=(4.109375-3.1875j))
; 		 CubicBezier(start=(4.109375-3.1875j), control1=(4.109375-3.453125j), control2=(3.875-3.609375j), end=(3.640625-3.609375j))
; 		 CubicBezier(start=(3.640625-3.609375j), control1=(3.359375-3.609375j), control2=(3.03125-3.46875j), end=(2.859375-3.3125j))
; 		 CubicBezier(start=(2.859375-3.3125j), control1=(2.84375-3.296875j), control2=(2.8125-3.265625j), end=(2.765625-3.265625j))
; 		 Line(start=(2.765625-3.265625j), end=(2.75-3.265625j))
; 		 CubicBezier(start=(2.75-3.265625j), control1=(2.71875-3.265625j), control2=(2.65625-3.3125j), end=(2.59375-3.34375j))
; 		 CubicBezier(start=(2.59375-3.34375j), control1=(2.359375-3.453125j), control2=(2.140625-3.515625j), end=(1.890625-3.515625j))
; 		 CubicBezier(start=(1.890625-3.515625j), control1=(1.25-3.515625j), control2=(0.515625-3.09375j), end=(0.515625-2.34375j))
; 		 CubicBezier(start=(0.515625-2.34375j), control1=(0.515625-2.09375j), control2=(0.609375-1.828125j), end=(0.78125-1.640625j))
; 		 CubicBezier(start=(0.78125-1.640625j), control1=(0.8125-1.609375j), control2=(0.859375-1.5625j), end=(0.859375-1.53125j))
; 		 Line(start=(0.859375-1.53125j), end=(0.859375-1.515625j))
; 		 CubicBezier(start=(0.859375-1.515625j), control1=(0.859375-1.453125j), control2=(0.71875-1.296875j), end=(0.6875-1.171875j))
; 		 CubicBezier(start=(0.6875-1.171875j), control1=(0.65625-1.078125j), control2=(0.640625-0.984375j), end=(0.640625-0.890625j))
; 		 CubicBezier(start=(0.640625-0.890625j), control1=(0.640625-0.578125j), control2=(0.75-0.4375j), end=(0.96875-0.171875j))
; 		 Line(start=(0.96875-0.171875j), end=(0.96875-0.171875j))
; 		 CubicBezier(start=(0.703125+0.65625j), control1=(0.703125+0.625j), control2=(0.703125+0.609375j), end=(0.703125+0.59375j))
; 		 CubicBezier(start=(0.703125+0.59375j), control1=(0.703125+0.15625j), control2=(1.15625-0.03125j), end=(1.53125-0.03125j))
; 		 Line(start=(1.53125-0.03125j), end=(2.140625-0.03125j))
; 		 CubicBezier(start=(2.140625-0.03125j), control1=(2.734375-0.03125j), control2=(3.4375+0.015625j), end=(3.515625+0.5625j))
; 		 Line(start=(3.515625+0.5625j), end=(3.515625+0.609375j))
; 		 CubicBezier(start=(3.515625+0.609375j), control1=(3.515625+0.921875j), control2=(3.140625+1.171875j), end=(2.921875+1.25j))
; 		 CubicBezier(start=(2.921875+1.25j), control1=(2.65625+1.359375j), control2=(2.359375+1.390625j), end=(2.09375+1.390625j))
; 		 CubicBezier(start=(2.09375+1.390625j), control1=(1.578125+1.390625j), control2=(0.765625+1.171875j), end=(0.703125+0.65625j))
; 		 Line(start=(0.703125+0.65625j), end=(0.703125+0.65625j))
; 		 Line(start=(1.140625-2.203125j), end=(1.140625-2.296875j))
; 		 CubicBezier(start=(1.140625-2.296875j), control1=(1.140625-2.75j), control2=(1.28125-3.28125j), end=(1.875-3.28125j))
; 		 CubicBezier(start=(1.875-3.28125j), control1=(2.375-3.28125j), control2=(2.625-2.859375j), end=(2.625-2.3125j))
; 		 CubicBezier(start=(2.625-2.3125j), control1=(2.625-1.84375j), control2=(2.40625-1.40625j), end=(1.875-1.40625j))
; 		 CubicBezier(start=(1.875-1.40625j), control1=(1.671875-1.40625j), control2=(1.46875-1.5j), end=(1.328125-1.671875j))
; 		 CubicBezier(start=(1.328125-1.671875j), control1=(1.1875-1.828125j), control2=(1.15625-2.015625j), end=(1.140625-2.203125j))
; 		 Line(start=(1.140625-2.203125j), end=(1.140625-2.203125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 CubicBezier(start=(4.984375-4.921875j), control1=(4.515625-5.328125j), control2=(4.125-5.625j), end=(3.40625-5.625j))
; 		 CubicBezier(start=(3.40625-5.625j), control1=(1.859375-5.625j), control2=(0.546875-4.390625j), end=(0.46875-2.84375j))
; 		 Line(start=(0.46875-2.84375j), end=(0.46875-2.734375j))
; 		 CubicBezier(start=(0.46875-2.734375j), control1=(0.46875-1.078125j), control2=(1.859375+0.15625j), end=(3.421875+0.15625j))
; 		 CubicBezier(start=(3.421875+0.15625j), control1=(4.5+0.15625j), control2=(5.640625-0.65625j), end=(5.640625-1.859375j))
; 		 CubicBezier(start=(5.640625-1.859375j), control1=(5.640625-1.96875j), control2=(5.59375-2.03125j), end=(5.5-2.03125j))
; 		 CubicBezier(start=(5.5-2.03125j), control1=(5.421875-2.03125j), control2=(5.390625-1.96875j), end=(5.390625-1.890625j))
; 		 Line(start=(5.390625-1.890625j), end=(5.390625-1.828125j))
; 		 CubicBezier(start=(5.390625-1.828125j), control1=(5.296875-0.90625j), control2=(4.5625-0.109375j), end=(3.484375-0.109375j))
; 		 CubicBezier(start=(3.484375-0.109375j), control1=(2.84375-0.109375j), control2=(2.1875-0.4375j), end=(1.796875-0.953125j))
; 		 CubicBezier(start=(1.796875-0.953125j), control1=(1.4375-1.4375j), control2=(1.296875-2.078125j), end=(1.296875-2.6875j))
; 		 CubicBezier(start=(1.296875-2.6875j), control1=(1.296875-3.21875j), control2=(1.375-3.75j), end=(1.625-4.21875j))
; 		 CubicBezier(start=(1.625-4.21875j), control1=(1.953125-4.890625j), control2=(2.703125-5.359375j), end=(3.46875-5.359375j))
; 		 CubicBezier(start=(3.46875-5.359375j), control1=(4.53125-5.359375j), control2=(5.171875-4.484375j), end=(5.359375-3.546875j))
; 		 CubicBezier(start=(5.359375-3.546875j), control1=(5.375-3.453125j), control2=(5.359375-3.3125j), end=(5.5-3.3125j))
; 		 CubicBezier(start=(5.5-3.3125j), control1=(5.609375-3.3125j), control2=(5.640625-3.375j), end=(5.640625-3.453125j))
; 		 Line(start=(5.640625-3.453125j), end=(5.640625-5.46875j))
; 		 CubicBezier(start=(5.640625-5.46875j), control1=(5.640625-5.53125j), control2=(5.625-5.625j), end=(5.53125-5.625j))
; 		 CubicBezier(start=(5.53125-5.625j), control1=(5.375-5.625j), control2=(5-4.9375j), end=(4.984375-4.921875j))
; 		 Line(start=(4.984375-4.921875j), end=(4.984375-4.921875j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.359375-5.453125j), end=(0.359375-5.1875j))
; 		 CubicBezier(start=(0.359375-5.1875j), control1=(0.453125-5.1875j), control2=(0.5625-5.203125j), end=(0.65625-5.203125j))
; 		 CubicBezier(start=(0.65625-5.203125j), control1=(0.9375-5.203125j), control2=(1.15625-5.15625j), end=(1.15625-4.84375j))
; 		 Line(start=(1.15625-4.84375j), end=(1.15625-0.625j))
; 		 CubicBezier(start=(1.15625-0.625j), control1=(1.15625-0.296875j), control2=(0.90625-0.265625j), end=(0.625-0.265625j))
; 		 CubicBezier(start=(0.625-0.265625j), control1=(0.53125-0.265625j), control2=(0.453125-0.265625j), end=(0.359375-0.265625j))
; 		 Line(start=(0.359375-0.265625j), end=(0.359375-0.015625j))
; 		 Line(start=(0.359375-0.015625j), end=(2.671875-0.015625j))
; 		 Line(start=(2.671875-0.015625j), end=(2.671875-0.265625j))
; 		 CubicBezier(start=(2.671875-0.265625j), control1=(2.578125-0.265625j), control2=(2.484375-0.265625j), end=(2.375-0.265625j))
; 		 CubicBezier(start=(2.375-0.265625j), control1=(2.109375-0.265625j), control2=(1.875-0.3125j), end=(1.875-0.625j))
; 		 Line(start=(1.875-0.625j), end=(1.875-2.625j))
; 		 Line(start=(1.875-2.625j), end=(2.796875-2.625j))
; 		 CubicBezier(start=(2.796875-2.625j), control1=(3.21875-2.625j), control2=(3.625-2.515625j), end=(3.875-2.15625j))
; 		 CubicBezier(start=(3.875-2.15625j), control1=(4.015625-1.9375j), control2=(4.015625-1.703125j), end=(4.015625-1.4375j))
; 		 Line(start=(4.015625-1.4375j), end=(4.015625-1.15625j))
; 		 CubicBezier(start=(4.015625-1.15625j), control1=(4.015625-0.59375j), control2=(4.171875+0.15625j), end=(5.375+0.15625j))
; 		 CubicBezier(start=(5.375+0.15625j), control1=(5.84375+0.15625j), control2=(6.1875-0.25j), end=(6.1875-0.71875j))
; 		 CubicBezier(start=(6.1875-0.71875j), control1=(6.1875-0.78125j), control2=(6.1875-0.890625j), end=(6.0625-0.890625j))
; 		 CubicBezier(start=(6.0625-0.890625j), control1=(5.921875-0.890625j), control2=(5.9375-0.71875j), end=(5.921875-0.625j))
; 		 CubicBezier(start=(5.921875-0.625j), control1=(5.890625-0.34375j), control2=(5.6875-0.0625j), end=(5.390625-0.0625j))
; 		 CubicBezier(start=(5.390625-0.0625j), control1=(4.984375-0.0625j), control2=(4.90625-0.59375j), end=(4.859375-0.953125j))
; 		 CubicBezier(start=(4.859375-0.953125j), control1=(4.75-1.578125j), control2=(4.796875-2.171875j), end=(4.171875-2.546875j))
; 		 CubicBezier(start=(4.171875-2.546875j), control1=(4.046875-2.609375j), control2=(3.90625-2.6875j), end=(3.765625-2.71875j))
; 		 CubicBezier(start=(3.765625-2.71875j), control1=(4.328125-2.90625j), control2=(5.15625-3.25j), end=(5.15625-4.015625j))
; 		 CubicBezier(start=(5.15625-4.015625j), control1=(5.15625-5.015625j), control2=(3.875-5.40625j), end=(3.0625-5.453125j))
; 		 Line(start=(3.0625-5.453125j), end=(0.359375-5.453125j))
; 		 Line(start=(1.875-2.84375j), end=(1.875-4.890625j))
; 		 CubicBezier(start=(1.875-4.890625j), control1=(1.875-5.09375j), control2=(1.953125-5.1875j), end=(2.171875-5.1875j))
; 		 Line(start=(2.171875-5.1875j), end=(2.765625-5.1875j))
; 		 CubicBezier(start=(2.765625-5.1875j), control1=(3.4375-5.1875j), control2=(4.328125-5.046875j), end=(4.328125-4.03125j))
; 		 CubicBezier(start=(4.328125-4.03125j), control1=(4.328125-2.890625j), control2=(3.3125-2.84375j), end=(2.609375-2.84375j))
; 		 Line(start=(2.609375-2.84375j), end=(1.875-2.84375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(3.109375-2.5j), end=(3.125-2.5j))
; 		 CubicBezier(start=(3.125-2.5j), control1=(3.125-1.671875j), control2=(3.046875-0.578125j), end=(2.15625-0.15625j))
; 		 CubicBezier(start=(2.15625-0.15625j), control1=(2.015625-0.109375j), control2=(1.875-0.078125j), end=(1.734375-0.078125j))
; 		 CubicBezier(start=(1.734375-0.078125j), control1=(1.4375-0.078125j), control2=(1.15625-0.15625j), end=(0.9375-0.34375j))
; 		 CubicBezier(start=(0.9375-0.34375j), control1=(1.140625-0.34375j), control2=(1.3125-0.515625j), end=(1.3125-0.71875j))
; 		 CubicBezier(start=(1.3125-0.71875j), control1=(1.3125-0.921875j), control2=(1.15625-1.09375j), end=(0.9375-1.09375j))
; 		 CubicBezier(start=(0.9375-1.09375j), control1=(0.75-1.09375j), control2=(0.59375-0.96875j), end=(0.5625-0.765625j))
; 		 Line(start=(0.5625-0.765625j), end=(0.5625-0.703125j))
; 		 CubicBezier(start=(0.5625-0.703125j), control1=(0.5625-0.046875j), control2=(1.234375+0.15625j), end=(1.78125+0.15625j))
; 		 CubicBezier(start=(1.78125+0.15625j), control1=(2.4375+0.15625j), control2=(3-0.25j), end=(3.34375-0.765625j))
; 		 CubicBezier(start=(3.34375-0.765625j), control1=(3.734375-1.3125j), control2=(3.875-1.96875j), end=(3.875-2.640625j))
; 		 CubicBezier(start=(3.875-2.640625j), control1=(3.875-3.359375j), control2=(3.78125-4.1875j), end=(3.296875-4.765625j))
; 		 CubicBezier(start=(3.296875-4.765625j), control1=(3-5.125j), control2=(2.546875-5.3125j), end=(2.09375-5.3125j))
; 		 CubicBezier(start=(2.09375-5.3125j), control1=(1.15625-5.3125j), control2=(0.34375-4.515625j), end=(0.34375-3.515625j))
; 		 CubicBezier(start=(0.34375-3.515625j), control1=(0.34375-2.640625j), control2=(1.015625-1.75j), end=(1.984375-1.75j))
; 		 CubicBezier(start=(1.984375-1.75j), control1=(2.453125-1.75j), control2=(2.96875-2.046875j), end=(3.109375-2.5j))
; 		 Line(start=(3.109375-2.5j), end=(3.109375-2.5j))
; 		 CubicBezier(start=(3.09375-3.5625j), control1=(3.09375-3.484375j), control2=(3.109375-3.421875j), end=(3.109375-3.34375j))
; 		 CubicBezier(start=(3.109375-3.34375j), control1=(3.078125-2.765625j), control2=(2.8125-1.96875j), end=(2.0625-1.96875j))
; 		 CubicBezier(start=(2.0625-1.96875j), control1=(1.265625-1.96875j), control2=(1.09375-2.8125j), end=(1.09375-3.46875j))
; 		 Line(start=(1.09375-3.46875j), end=(1.09375-3.796875j))
; 		 CubicBezier(start=(1.09375-3.796875j), control1=(1.125-4.359375j), control2=(1.34375-5.09375j), end=(2.140625-5.09375j))
; 		 CubicBezier(start=(2.140625-5.09375j), control1=(2.953125-5.09375j), control2=(3.09375-4.0625j), end=(3.09375-3.5625j))
; 		 Line(start=(3.09375-3.5625j), end=(3.09375-3.5625j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.34375-5.453125j), end=(0.34375-5.1875j))
; 		 CubicBezier(start=(0.34375-5.1875j), control1=(0.453125-5.1875j), control2=(0.546875-5.203125j), end=(0.640625-5.203125j))
; 		 CubicBezier(start=(0.640625-5.203125j), control1=(0.921875-5.203125j), control2=(1.15625-5.15625j), end=(1.15625-4.84375j))
; 		 Line(start=(1.15625-4.84375j), end=(1.15625-0.75j))
; 		 CubicBezier(start=(1.15625-0.75j), control1=(1.15625-0.703125j), control2=(1.15625-0.640625j), end=(1.15625-0.59375j))
; 		 CubicBezier(start=(1.15625-0.59375j), control1=(1.15625-0.296875j), control2=(0.90625-0.265625j), end=(0.640625-0.265625j))
; 		 CubicBezier(start=(0.640625-0.265625j), control1=(0.53125-0.265625j), control2=(0.4375-0.265625j), end=(0.34375-0.265625j))
; 		 Line(start=(0.34375-0.265625j), end=(0.34375-0.015625j))
; 		 Line(start=(0.34375-0.015625j), end=(2.6875-0.015625j))
; 		 Line(start=(2.6875-0.015625j), end=(2.6875-0.265625j))
; 		 CubicBezier(start=(2.6875-0.265625j), control1=(2.59375-0.265625j), control2=(2.484375-0.265625j), end=(2.390625-0.265625j))
; 		 CubicBezier(start=(2.390625-0.265625j), control1=(2.125-0.265625j), control2=(1.890625-0.3125j), end=(1.890625-0.625j))
; 		 Line(start=(1.890625-0.625j), end=(1.890625-2.703125j))
; 		 Line(start=(1.890625-2.703125j), end=(4.453125-2.703125j))
; 		 Line(start=(4.453125-2.703125j), end=(4.453125-0.625j))
; 		 CubicBezier(start=(4.453125-0.625j), control1=(4.453125-0.296875j), control2=(4.171875-0.265625j), end=(3.859375-0.265625j))
; 		 Line(start=(3.859375-0.265625j), end=(3.640625-0.265625j))
; 		 Line(start=(3.640625-0.265625j), end=(3.640625-0.015625j))
; 		 Line(start=(3.640625-0.015625j), end=(5.984375-0.015625j))
; 		 Line(start=(5.984375-0.015625j), end=(5.984375-0.265625j))
; 		 CubicBezier(start=(5.984375-0.265625j), control1=(5.890625-0.265625j), control2=(5.796875-0.265625j), end=(5.703125-0.265625j))
; 		 CubicBezier(start=(5.703125-0.265625j), control1=(5.421875-0.265625j), control2=(5.171875-0.296875j), end=(5.171875-0.59375j))
; 		 CubicBezier(start=(5.171875-0.59375j), control1=(5.171875-0.640625j), control2=(5.171875-0.703125j), end=(5.171875-0.75j))
; 		 Line(start=(5.171875-0.75j), end=(5.171875-4.84375j))
; 		 CubicBezier(start=(5.171875-4.84375j), control1=(5.171875-5.15625j), control2=(5.4375-5.203125j), end=(5.71875-5.203125j))
; 		 CubicBezier(start=(5.71875-5.203125j), control1=(5.8125-5.203125j), control2=(5.90625-5.1875j), end=(5.984375-5.1875j))
; 		 Line(start=(5.984375-5.1875j), end=(5.984375-5.453125j))
; 		 Line(start=(5.984375-5.453125j), end=(3.640625-5.453125j))
; 		 Line(start=(3.640625-5.453125j), end=(3.640625-5.1875j))
; 		 CubicBezier(start=(3.640625-5.1875j), control1=(3.734375-5.1875j), control2=(3.84375-5.203125j), end=(3.9375-5.203125j))
; 		 CubicBezier(start=(3.9375-5.203125j), control1=(4.21875-5.203125j), control2=(4.453125-5.140625j), end=(4.453125-4.828125j))
; 		 Line(start=(4.453125-4.828125j), end=(4.453125-2.96875j))
; 		 Line(start=(4.453125-2.96875j), end=(1.890625-2.96875j))
; 		 Line(start=(1.890625-2.96875j), end=(1.890625-4.828125j))
; 		 CubicBezier(start=(1.890625-4.828125j), control1=(1.890625-5.15625j), control2=(2.15625-5.1875j), end=(2.46875-5.1875j))
; 		 Line(start=(2.46875-5.1875j), end=(2.6875-5.1875j))
; 		 Line(start=(2.6875-5.1875j), end=(2.6875-5.453125j))
; 		 Line(start=(2.6875-5.453125j), end=(0.34375-5.453125j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.359375-5.453125j), end=(0.359375-5.1875j))
; 		 CubicBezier(start=(0.359375-5.1875j), control1=(0.453125-5.1875j), control2=(0.5625-5.203125j), end=(0.65625-5.203125j))
; 		 CubicBezier(start=(0.65625-5.203125j), control1=(0.9375-5.203125j), control2=(1.15625-5.15625j), end=(1.15625-4.84375j))
; 		 Line(start=(1.15625-4.84375j), end=(1.15625-0.625j))
; 		 CubicBezier(start=(1.15625-0.625j), control1=(1.15625-0.296875j), control2=(0.90625-0.265625j), end=(0.625-0.265625j))
; 		 CubicBezier(start=(0.625-0.265625j), control1=(0.53125-0.265625j), control2=(0.453125-0.265625j), end=(0.359375-0.265625j))
; 		 Line(start=(0.359375-0.265625j), end=(0.359375-0.015625j))
; 		 Line(start=(0.359375-0.015625j), end=(2.703125-0.015625j))
; 		 Line(start=(2.703125-0.015625j), end=(2.703125-0.265625j))
; 		 CubicBezier(start=(2.703125-0.265625j), control1=(2.59375-0.265625j), control2=(2.5-0.265625j), end=(2.40625-0.265625j))
; 		 CubicBezier(start=(2.40625-0.265625j), control1=(2.125-0.265625j), control2=(1.890625-0.3125j), end=(1.890625-0.625j))
; 		 Line(start=(1.890625-0.625j), end=(1.890625-2.5j))
; 		 Line(start=(1.890625-2.5j), end=(3.46875-2.5j))
; 		 CubicBezier(start=(3.46875-2.5j), control1=(4.265625-2.546875j), control2=(5.203125-3.015625j), end=(5.28125-3.875j))
; 		 Line(start=(5.28125-3.875j), end=(5.28125-3.9375j))
; 		 CubicBezier(start=(5.28125-3.9375j), control1=(5.28125-4.984375j), control2=(4.109375-5.453125j), end=(3.15625-5.453125j))
; 		 CubicBezier(start=(3.15625-5.453125j), control1=(3-5.453125j), control2=(2.84375-5.453125j), end=(2.671875-5.453125j))
; 		 Line(start=(2.671875-5.453125j), end=(0.359375-5.453125j))
; 		 Line(start=(1.875-2.734375j), end=(1.875-4.9375j))
; 		 CubicBezier(start=(1.875-4.9375j), control1=(1.875-5.140625j), control2=(2.015625-5.1875j), end=(2.1875-5.1875j))
; 		 Line(start=(2.1875-5.1875j), end=(2.859375-5.1875j))
; 		 CubicBezier(start=(2.859375-5.1875j), control1=(3.5625-5.1875j), control2=(4.453125-5.140625j), end=(4.453125-3.9375j))
; 		 CubicBezier(start=(4.453125-3.9375j), control1=(4.453125-2.90625j), control2=(3.671875-2.734375j), end=(3-2.734375j))
; 		 Line(start=(3-2.734375j), end=(1.875-2.734375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.359375-5.453125j), end=(0.359375-5.1875j))
; 		 CubicBezier(start=(0.359375-5.1875j), control1=(0.453125-5.1875j), control2=(0.5625-5.203125j), end=(0.65625-5.203125j))
; 		 CubicBezier(start=(0.65625-5.203125j), control1=(0.9375-5.203125j), control2=(1.15625-5.15625j), end=(1.15625-4.84375j))
; 		 Line(start=(1.15625-4.84375j), end=(1.15625-0.625j))
; 		 CubicBezier(start=(1.15625-0.625j), control1=(1.15625-0.296875j), control2=(0.90625-0.265625j), end=(0.625-0.265625j))
; 		 CubicBezier(start=(0.625-0.265625j), control1=(0.53125-0.265625j), control2=(0.453125-0.265625j), end=(0.359375-0.265625j))
; 		 Line(start=(0.359375-0.265625j), end=(0.359375-0.015625j))
; 		 Line(start=(0.359375-0.015625j), end=(2.6875-0.015625j))
; 		 CubicBezier(start=(2.6875-0.015625j), control1=(2.875-0.015625j), control2=(3.046875+0j), end=(3.234375+0j))
; 		 CubicBezier(start=(3.234375+0j), control1=(4.59375+0j), control2=(5.984375-0.921875j), end=(5.984375-2.6875j))
; 		 CubicBezier(start=(5.984375-2.6875j), control1=(5.984375-3.984375j), control2=(5.078125-5.296875j), end=(3.640625-5.4375j))
; 		 CubicBezier(start=(3.640625-5.4375j), control1=(3.5-5.453125j), control2=(3.359375-5.453125j), end=(3.21875-5.453125j))
; 		 Line(start=(3.21875-5.453125j), end=(0.359375-5.453125j))
; 		 CubicBezier(start=(5.171875-2.375j), control1=(5.140625-2.09375j), control2=(5.125-1.8125j), end=(5.03125-1.53125j))
; 		 CubicBezier(start=(5.03125-1.53125j), control1=(4.828125-0.953125j), control2=(4.359375-0.515625j), end=(3.78125-0.34375j))
; 		 CubicBezier(start=(3.78125-0.34375j), control1=(3.5-0.265625j), control2=(3.21875-0.265625j), end=(2.921875-0.265625j))
; 		 Line(start=(2.921875-0.265625j), end=(2.203125-0.265625j))
; 		 CubicBezier(start=(2.203125-0.265625j), control1=(2.046875-0.265625j), control2=(1.859375-0.28125j), end=(1.859375-0.5625j))
; 		 CubicBezier(start=(1.859375-0.5625j), control1=(1.859375-0.609375j), control2=(1.875-0.671875j), end=(1.875-0.71875j))
; 		 Line(start=(1.875-0.71875j), end=(1.875-4.609375j))
; 		 CubicBezier(start=(1.875-4.609375j), control1=(1.875-4.671875j), control2=(1.859375-4.75j), end=(1.859375-4.828125j))
; 		 CubicBezier(start=(1.859375-4.828125j), control1=(1.859375-5.140625j), control2=(1.984375-5.1875j), end=(2.21875-5.1875j))
; 		 Line(start=(2.21875-5.1875j), end=(2.40625-5.1875j))
; 		 CubicBezier(start=(2.40625-5.1875j), control1=(2.578125-5.1875j), control2=(2.765625-5.203125j), end=(2.953125-5.203125j))
; 		 CubicBezier(start=(2.953125-5.203125j), control1=(3.625-5.203125j), control2=(4.375-5.0625j), end=(4.84375-4.28125j))
; 		 CubicBezier(start=(4.84375-4.28125j), control1=(5.09375-3.828125j), control2=(5.171875-3.28125j), end=(5.171875-2.765625j))
; 		 Line(start=(5.171875-2.765625j), end=(5.171875-2.5625j))
; 		 CubicBezier(start=(5.171875-2.5625j), control1=(5.171875-2.5j), control2=(5.171875-2.4375j), end=(5.171875-2.375j))
; 		 Line(start=(5.171875-2.375j), end=(5.171875-2.375j))
; 	Translate X 0 Y 0
; 	Translate X 0 Y 0
; 		 Line(start=(0.34375-5.421875j), end=(0.34375-5.15625j))
; 		 CubicBezier(start=(0.34375-5.15625j), control1=(0.453125-5.15625j), control2=(0.546875-5.171875j), end=(0.640625-5.171875j))
; 		 CubicBezier(start=(0.640625-5.171875j), control1=(0.921875-5.171875j), control2=(1.15625-5.125j), end=(1.15625-4.828125j))
; 		 Line(start=(1.15625-4.828125j), end=(1.15625-0.625j))
; 		 CubicBezier(start=(1.15625-0.625j), control1=(1.15625-0.296875j), control2=(0.890625-0.265625j), end=(0.625-0.265625j))
; 		 CubicBezier(start=(0.625-0.265625j), control1=(0.53125-0.265625j), control2=(0.4375-0.265625j), end=(0.34375-0.265625j))
; 		 Line(start=(0.34375-0.265625j), end=(0.34375-0.015625j))
; 		 Line(start=(0.34375-0.015625j), end=(2.890625-0.015625j))
; 		 Line(start=(2.890625-0.015625j), end=(2.890625-0.265625j))
; 		 CubicBezier(start=(2.890625-0.265625j), control1=(2.78125-0.265625j), control2=(2.65625-0.265625j), end=(2.53125-0.265625j))
; 		 CubicBezier(start=(2.53125-0.265625j), control1=(2.109375-0.265625j), control2=(1.890625-0.296875j), end=(1.890625-0.671875j))
; 		 Line(start=(1.890625-0.671875j), end=(1.890625-2.59375j))
; 		 Line(start=(1.890625-2.59375j), end=(2.640625-2.59375j))
; 		 CubicBezier(start=(2.640625-2.59375j), control1=(2.921875-2.59375j), control2=(3.25-2.5625j), end=(3.390625-2.328125j))
; 		 CubicBezier(start=(3.390625-2.328125j), control1=(3.5-2.171875j), control2=(3.515625-1.9375j), end=(3.515625-1.734375j))
; 		 Line(start=(3.515625-1.734375j), end=(3.515625-1.65625j))
; 		 Line(start=(3.515625-1.65625j), end=(3.765625-1.65625j))
; 		 Line(start=(3.765625-1.65625j), end=(3.765625-3.78125j))
; 		 Line(start=(3.765625-3.78125j), end=(3.515625-3.78125j))
; 		 Line(start=(3.515625-3.78125j), end=(3.515625-3.71875j))
; 		 CubicBezier(start=(3.515625-3.71875j), control1=(3.515625-3.265625j), control2=(3.421875-2.9375j), end=(2.921875-2.875j))
; 		 CubicBezier(start=(2.921875-2.875j), control1=(2.765625-2.859375j), control2=(2.609375-2.859375j), end=(2.46875-2.859375j))
; 		 Line(start=(2.46875-2.859375j), end=(1.890625-2.859375j))
; 		 Line(start=(1.890625-2.859375j), end=(1.890625-4.875j))
; 		 CubicBezier(start=(1.890625-4.875j), control1=(1.890625-5.140625j), control2=(2.046875-5.15625j), end=(2.21875-5.15625j))
; 		 Line(start=(2.21875-5.15625j), end=(3.203125-5.15625j))
; 		 CubicBezier(start=(3.203125-5.15625j), control1=(3.6875-5.15625j), control2=(4.296875-5.15625j), end=(4.609375-4.703125j))
; 		 CubicBezier(start=(4.609375-4.703125j), control1=(4.828125-4.40625j), control2=(4.875-3.984375j), end=(4.921875-3.625j))
; 		 Line(start=(4.921875-3.625j), end=(5.15625-3.625j))
; 		 Line(start=(5.15625-3.625j), end=(4.9375-5.421875j))
; 		 Line(start=(4.9375-5.421875j), end=(0.34375-5.421875j))
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 		 Line(start=(0.0011875-3.125e-05j), end=(470.26681-3.125e-05j))
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 		 Line(start=(0.0011875-0.001j), end=(188.10275-0.001j))
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
; 	Translate X -65.565441 Y -24.779501
G21 ; Set units to millimeters
G90 ; Set to absolute coordinates
G0 F60 ; Set feed rate

G28 Z                               ; home_z          
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G28 X Y                             ; home_xy         
G0 X000.50 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.93 Y261.79 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y261.79 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.23 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.23 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.26 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.14 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.18 Y260.01 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.34 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.40 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.99 Y260.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.99 Y260.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.49 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.97 Y261.88 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.42 Y261.53 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.16 Y261.66 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.93 Y260.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.48 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.55 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.18 Y260.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y261.69 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.68 Y261.82 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.08 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.14 Y261.28 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.19 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.19 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.66 Y261.67 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.42 Y260.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.92 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.69 Y260.34 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.79 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.79 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.63 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.60 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.51 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.19 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.02 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.77 Y260.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.01 Y261.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.33 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.65 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.81 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.24 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.74 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.66 Y261.66 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.41 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.68 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.71 Y260.01 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.95 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.86 Y260.66 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.22 Y260.09 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.68 Y260.28 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.78 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.36 Y260.63 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.49 Y261.37 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.81 Y261.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.90 Y261.60 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.99 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.45 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.38 Y261.98 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.83 Y261.78 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.44 Y261.35 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.30 Y260.78 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.30 Y260.78 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.44 Y260.28 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.90 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.64 Y260.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.05 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.20 Y261.14 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.20 Y261.14 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.02 Y261.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y261.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.93 Y261.18 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.76 Y260.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y260.01 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.74 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y261.90 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.41 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.93 Y261.27 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.66 Y261.67 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.43 Y260.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.92 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.66 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y260.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.37 Y260.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.80 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.67 Y260.58 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.38 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.38 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.95 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.20 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.41 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.83 Y261.20 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.92 Y261.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.21 Y261.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.68 Y261.53 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.64 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.46 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.39 Y260.88 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.84 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.84 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.34 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.39 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.80 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.83 Y261.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y261.06 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.13 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.35 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.49 Y260.31 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.38 Y259.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.38 Y259.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.02 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.28 Y260.89 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.28 Y260.89 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.14 Y261.40 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.75 Y261.76 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.75 Y261.76 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.24 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y262.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y262.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.69 Y263.10 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.17 Y263.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.17 Y263.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.78 Y263.01 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.05 Y262.34 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.15 Y262.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.15 Y263.35 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.91 Y263.01 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y263.40 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y263.40 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.52 Y263.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.25 Y262.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.25 Y262.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.46 Y261.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y261.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y261.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.61 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.02 Y261.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.05 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.85 Y260.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.35 Y260.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.35 Y260.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.52 Y260.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.52 Y260.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.34 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.25 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.25 Y259.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.51 Y261.72 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.51 Y259.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.19 Y259.06 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.09 Y259.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y259.36 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.37 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.37 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.88 Y260.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.22 Y260.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.35 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.35 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.19 Y261.66 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.71 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.43 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y261.78 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y262.08 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X002.03 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.85 Y260.37 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.33 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.33 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y260.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y260.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y261.34 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y261.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.38 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.38 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.88 Y261.69 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.03 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.48 Y261.15 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.84 Y261.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.90 Y261.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.71 Y261.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y262.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y262.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y261.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.27 Y261.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.16 Y261.08 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.16 Y261.08 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.27 Y260.57 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.59 Y260.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.13 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.60 Y260.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.89 Y260.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.75 Y260.43 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.18 Y260.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.52 Y260.64 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.48 Y261.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.48 Y261.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.63 Y261.79 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y262.07 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y262.07 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.62 Y261.40 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.63 Y261.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.48 Y261.23 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.86 Y261.56 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.62 Y261.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.15 Y262.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.15 Y262.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.62 Y261.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.30 Y261.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.31 Y260.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.66 Y260.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.19 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.19 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.88 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.78 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.19 Y260.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.90 Y260.14 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y261.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y261.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.63 Y261.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y262.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y262.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.73 Y261.71 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.67 Y262.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.57 Y262.41 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.08 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y260.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.63 Y260.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.06 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.06 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.52 Y260.67 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.43 Y260.66 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y260.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y260.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y260.63 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y260.63 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.44 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.44 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y262.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.51 Y261.71 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.51 Y260.33 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.18 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.75 Y260.34 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.75 Y261.01 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.75 Y261.01 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.80 Y261.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y261.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.31 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.75 Y261.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.75 Y261.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.74 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.74 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.18 Y262.08 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.08 Y262.15 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.58 Y261.95 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.27 Y261.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.16 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.16 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.27 Y260.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.61 Y260.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.66 Y260.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.00 Y260.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.12 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.12 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.00 Y261.57 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.67 Y261.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.13 Y262.15 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.47 Y261.07 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.55 Y261.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.91 Y262.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.14 Y262.08 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.60 Y261.85 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.79 Y261.34 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.80 Y261.08 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.61 Y260.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.61 Y260.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.13 Y260.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.13 Y260.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.68 Y260.28 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y260.76 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.37 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.90 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.90 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.39 Y260.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.64 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.64 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y261.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y261.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.72 Y261.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.33 Y261.66 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.33 Y261.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.87 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.87 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.44 Y261.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.53 Y261.47 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.53 Y262.10 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.89 Y262.15 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.40 Y262.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y261.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y261.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y261.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.47 Y260.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.47 Y260.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.90 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.90 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.46 Y260.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.26 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y260.82 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y260.01 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.15 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.56 Y261.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.56 Y261.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y260.81 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y260.14 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y259.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y259.10 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y259.36 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y259.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y259.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.11 Y259.66 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.47 Y260.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.68 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.89 Y261.57 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.89 Y261.57 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.33 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.33 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.67 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.78 Y261.56 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.57 Y261.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.35 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.14 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y261.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.91 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.08 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.05 Y260.94 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.27 Y260.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.27 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.05 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.67 Y262.89 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.67 Y260.43 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.28 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.48 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.76 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.38 Y260.14 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.85 Y260.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X003.13 Y261.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X003.23 Y261.64 Z000.00 F6000.00 ; move_abs_xy     
G0 X003.23 Y261.64 Z000.00 F6000.00 ; move_abs_xy     
G0 X003.16 Y262.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.93 Y262.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.57 Y263.08 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.06 Y263.30 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.44 Y263.32 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.28 Y263.32 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X002.86 Y261.38 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X002.75 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.48 Y260.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.04 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.98 Y260.39 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.98 Y262.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.18 Y263.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.79 Y263.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.79 Y263.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.32 Y263.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.65 Y262.64 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.82 Y262.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.87 Y261.65 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.52 Y261.71 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.52 Y260.33 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.06 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.21 Y262.08 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.51 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.51 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.77 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.77 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.77 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.45 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.43 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X002.02 Y260.32 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X002.02 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.02 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.58 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.58 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.01 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.01 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.01 Y262.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.01 Y262.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.40 Y263.34 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.80 Y263.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.44 Y263.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.44 Y263.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.96 Y263.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.96 Y263.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y263.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y263.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.85 Y263.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y262.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y262.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.77 Y262.64 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.77 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.77 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.75 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.75 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.75 Y262.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.75 Y262.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.95 Y263.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.45 Y263.34 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.47 Y261.68 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.01 Y262.08 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y262.08 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.52 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.52 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y261.14 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.30 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.22 Y260.41 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.90 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.90 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.53 Y260.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.54 Y260.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.90 Y259.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.93 Y259.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.25 Y260.35 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.77 Y260.47 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.77 Y261.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.70 Y261.78 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.02 Y262.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.02 Y262.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.40 Y261.75 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.52 Y260.68 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.98 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.95 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.52 Y260.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.52 Y260.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.52 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.52 Y261.71 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.52 Y260.33 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y261.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y261.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.91 Y261.76 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.40 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.40 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.78 Y261.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.78 Y261.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.78 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.37 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.04 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.04 Y261.43 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.04 Y261.43 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.91 Y261.95 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.91 Y261.95 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.41 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.41 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y261.64 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y262.08 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.77 Y262.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.77 Y261.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.77 Y261.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.31 Y261.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y261.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y261.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.30 Y260.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.62 Y260.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.12 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.12 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.76 Y260.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.76 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.35 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.02 Y260.37 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.02 Y263.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.02 Y263.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.45 Y263.32 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.50 Y261.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.63 Y261.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.20 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.20 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.77 Y261.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.76 Y261.35 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.76 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.76 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.14 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.14 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.52 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.52 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.52 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.18 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.77 Y260.32 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.77 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.32 Y261.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.32 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y262.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y262.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.17 Y263.34 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.56 Y263.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.18 Y263.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.18 Y263.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y262.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.52 Y262.95 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.52 Y260.33 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y263.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y263.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y263.32 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.57 Y260.12 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.13 Y259.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.13 Y259.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y259.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.14 Y259.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.14 Y259.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.80 Y259.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.16 Y259.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.16 Y259.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.67 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.67 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.13 Y260.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y260.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.47 Y260.62 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.01 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.52 Y260.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.73 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.73 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.54 Y261.90 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.01 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.51 Y261.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.30 Y261.43 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.30 Y261.43 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.93 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.32 Y259.63 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.84 Y260.07 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.27 Y260.07 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.96 Y259.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.96 Y259.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.66 Y259.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.14 Y259.10 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.14 Y259.10 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.63 Y259.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.32 Y259.59 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.59 Y261.45 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.01 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.44 Y261.41 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.44 Y261.41 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y260.81 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y260.81 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.59 Y261.33 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.52 Y262.95 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.52 Y260.33 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y261.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y261.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.91 Y261.76 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.40 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.40 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.78 Y261.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.78 Y261.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.78 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.37 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.04 Y260.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.04 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.04 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.92 Y261.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.43 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.43 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y261.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y261.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y263.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y263.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y263.32 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.18 Y262.29 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.12 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.31 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.05 Y262.08 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.05 Y260.26 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.60 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.78 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.33 Y260.25 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.33 Y262.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.61 Y262.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.15 Y261.89 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.26 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.20 Y262.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.20 Y262.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.18 Y262.29 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.37 Y262.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.37 Y260.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.11 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y261.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y261.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.76 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y261.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.05 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.58 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y262.35 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.78 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.30 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.29 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.34 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.55 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.55 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.35 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y261.51 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.35 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.64 Y261.39 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.16 Y261.27 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.29 Y260.77 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.77 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.37 Y260.55 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.37 Y261.19 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.37 Y260.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.11 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y261.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y261.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.76 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y260.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.95 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.95 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.20 Y261.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.20 Y261.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.20 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.94 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.68 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.42 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.42 Y261.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.98 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.49 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.49 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.04 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.04 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y261.14 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.73 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.08 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y260.77 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y260.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y260.28 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.65 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.65 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y260.26 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.30 Y261.01 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.73 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.24 Y261.11 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.08 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.69 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.39 Y260.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.94 Y260.80 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.64 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.11 Y261.08 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.11 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.62 Y260.67 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.04 Y260.34 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.04 Y260.34 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.67 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.67 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y260.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.11 Y260.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.11 Y260.01 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.67 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.19 Y260.43 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.19 Y260.43 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.85 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.85 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.26 Y261.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.63 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.46 Y262.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.46 Y260.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.14 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.05 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y260.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.74 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.74 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.74 Y260.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.33 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.02 Y260.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.02 Y262.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.33 Y262.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y262.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.74 Y262.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.74 Y261.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.74 Y261.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y261.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y261.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y262.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.05 Y262.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.14 Y262.31 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.37 Y261.19 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.37 Y260.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.11 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.37 Y261.19 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.37 Y260.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.11 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y261.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y261.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.76 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y261.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.36 Y260.78 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.32 Y260.78 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.37 Y260.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.79 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.79 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.29 Y261.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.55 Y260.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.36 Y260.37 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.36 Y260.78 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.36 Y260.85 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y260.85 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y260.85 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.36 Y260.85 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.59 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.37 Y259.44 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.09 Y260.57 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.98 Y260.57 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.66 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.54 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.22 Y260.67 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.22 Y262.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.90 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y261.35 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.16 Y260.78 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.18 Y260.67 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.99 Y261.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.99 Y261.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.99 Y260.67 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.99 Y260.67 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.18 Y260.67 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.34 Y261.94 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.79 Y262.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.79 Y262.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.13 Y261.77 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.13 Y261.77 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y261.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.20 Y260.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.20 Y260.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y260.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y260.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.26 Y260.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.14 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y259.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y259.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.51 Y260.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.51 Y260.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y261.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y261.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y261.79 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y261.79 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y262.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y262.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.23 Y261.80 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.74 Y262.02 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.74 Y260.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.32 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.38 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.97 Y260.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.97 Y262.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.80 Y262.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.31 Y262.03 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.29 Y262.28 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.18 Y261.61 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.74 Y261.94 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.38 Y261.94 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.95 Y261.31 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.69 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.59 Y260.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.87 Y260.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.91 Y260.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y261.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y261.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.55 Y262.03 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.28 Y261.76 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.74 Y262.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.74 Y262.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.20 Y261.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.20 Y261.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.05 Y261.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.74 Y260.74 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.16 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.39 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.39 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.48 Y260.57 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.96 Y260.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.40 Y260.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.40 Y260.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.48 Y261.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.48 Y261.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.27 Y262.07 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.79 Y262.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.79 Y262.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.16 Y261.64 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.41 Y261.09 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.45 Y261.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.69 Y262.03 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.43 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.01 Y262.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y262.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y262.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.24 Y261.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.14 Y261.10 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.14 Y261.10 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.18 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.39 Y260.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y259.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y259.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.48 Y260.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.51 Y260.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.34 Y261.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.42 Y261.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.42 Y260.77 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y261.37 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y261.37 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.23 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.23 Y260.66 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y260.08 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.42 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.49 Y262.08 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.06 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.35 Y261.36 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.35 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.34 Y260.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y260.47 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y260.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.80 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.80 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y260.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y260.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y261.36 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y262.08 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X002.04 Y260.44 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X002.39 Y261.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.39 Y261.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.30 Y261.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.04 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.61 Y262.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.31 Y262.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.74 Y262.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.33 Y261.79 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.18 Y261.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.18 Y261.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.32 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y260.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.32 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.32 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.84 Y260.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.33 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.03 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.24 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.72 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.38 Y260.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.26 Y261.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.26 Y261.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.38 Y261.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y262.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.27 Y262.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.27 Y262.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.83 Y262.14 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.18 Y261.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.31 Y261.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.31 Y261.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.07 Y260.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.94 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.94 Y261.52 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.28 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.81 Y261.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.63 Y261.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.63 Y261.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.80 Y260.67 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.72 Y260.68 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.84 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.29 Y261.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y261.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.69 Y261.57 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.72 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.72 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.28 Y260.51 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.84 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.37 Y261.14 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.37 Y260.46 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.86 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.28 Y260.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y260.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y261.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.87 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.26 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.26 Y259.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y259.35 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.74 Y259.35 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.48 Y259.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.48 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y261.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.85 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.85 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.33 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.31 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.38 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.76 Y261.40 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.27 Y261.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.27 Y261.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.27 Y260.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.27 Y260.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y260.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.38 Y260.67 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.34 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.82 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.32 Y261.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.32 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.35 Y260.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.35 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.38 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.38 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y261.43 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.27 Y262.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.27 Y261.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.33 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.30 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.48 Y260.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.48 Y262.35 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.38 Y260.70 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.64 Y261.36 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.27 Y261.07 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.39 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.63 Y261.14 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.14 Y260.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.14 Y260.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y259.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y259.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.10 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.51 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.01 Y261.23 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.42 Y261.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y261.74 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y262.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y262.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.23 Y261.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.23 Y261.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.63 Y261.14 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.29 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.65 Y261.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.90 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.35 Y260.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.35 Y260.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y260.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y260.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.30 Y260.48 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.37 Y261.79 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.82 Y262.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y262.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y261.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y261.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.98 Y261.30 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.23 Y262.38 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.70 Y262.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.33 Y261.76 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.18 Y261.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.18 Y261.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.31 Y260.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.68 Y260.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y259.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y259.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.86 Y260.11 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.24 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.38 Y261.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.38 Y261.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.24 Y261.74 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.86 Y262.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y262.38 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.49 Y261.17 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.59 Y261.76 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.98 Y262.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.27 Y262.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.94 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.94 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.07 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.07 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.94 Y260.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y260.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.01 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.74 Y260.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.91 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.35 Y262.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.35 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.54 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.99 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.99 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.72 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.72 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.52 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.56 Y262.35 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.57 Y260.37 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.57 Y260.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y261.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.00 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.00 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.35 Y261.19 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.35 Y260.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.88 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.56 Y260.70 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.88 Y261.41 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.78 Y262.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.22 Y261.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.14 Y261.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.14 Y261.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.26 Y260.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y259.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y259.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.43 Y260.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.51 Y261.11 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.51 Y261.11 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.39 Y261.85 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y262.25 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.39 Y260.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.41 Y261.60 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.82 Y262.18 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.21 Y261.79 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.15 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.80 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.46 Y260.34 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.40 Y260.88 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.05 Y262.53 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.60 Y262.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y262.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.40 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.34 Y260.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.34 Y260.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.40 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.62 Y259.66 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y259.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.97 Y259.34 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y259.78 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.56 Y260.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.52 Y260.82 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.52 Y260.82 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.56 Y261.39 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.72 Y261.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.05 Y262.44 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.45 Y262.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.45 Y260.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.14 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.02 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.02 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.15 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.08 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.87 Y260.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.87 Y260.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.10 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.73 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.73 Y261.15 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.47 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.47 Y261.64 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.73 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.73 Y262.09 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.30 Y262.20 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.83 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.83 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.99 Y261.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.06 Y261.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.98 Y262.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.98 Y262.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.14 Y262.30 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.68 Y260.59 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.04 Y260.10 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.04 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.62 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.04 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.71 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.94 Y260.80 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.65 Y261.36 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.65 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.07 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.72 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.05 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.35 Y261.20 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.35 Y259.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y259.35 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y259.35 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y259.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.00 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.51 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.72 Y260.74 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.72 Y260.74 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.52 Y261.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.02 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.02 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.56 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.45 Y260.67 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.13 Y260.07 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.97 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y260.37 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y260.37 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y260.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.00 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.00 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y260.74 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y262.36 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.58 Y261.89 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y261.37 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y260.82 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y260.82 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y260.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.59 Y259.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.33 Y259.36 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.37 Y259.28 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y259.74 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.89 Y260.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.95 Y260.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.95 Y260.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.88 Y261.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.66 Y262.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.26 Y262.50 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.38 Y261.72 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.38 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y260.89 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y260.89 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y260.89 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y260.89 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.76 Y261.40 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.76 Y261.40 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.18 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.89 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.63 Y260.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.63 Y261.75 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.38 Y261.72 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.38 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.92 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.47 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.47 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.47 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.19 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.02 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.72 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.72 Y261.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.02 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.19 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.47 Y261.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.47 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.47 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y261.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.92 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.43 Y261.59 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.87 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.64 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.46 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.23 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.69 Y261.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.08 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.48 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.01 Y261.11 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.65 Y261.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.03 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y260.14 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.86 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.40 Y260.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.40 Y260.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.05 Y261.07 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.05 Y261.07 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y261.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.34 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.72 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.72 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.24 Y261.40 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.32 Y261.34 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.32 Y261.94 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.72 Y261.98 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.15 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.23 Y260.44 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.84 Y260.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y260.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.23 Y260.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.62 Y261.68 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.62 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.27 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.18 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.67 Y261.88 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.18 Y261.47 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.11 Y260.89 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.11 Y260.89 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.31 Y260.11 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.21 Y260.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.21 Y260.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y261.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y261.82 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.06 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.06 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.34 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.25 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.63 Y261.79 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.63 Y261.79 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y261.33 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y261.33 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.67 Y260.67 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.67 Y260.67 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.18 Y260.11 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.14 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.19 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.79 Y260.63 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.26 Y261.33 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y261.33 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.67 Y261.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.67 Y261.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.14 Y261.37 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.51 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.30 Y259.51 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.03 Y260.89 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.87 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.19 Y260.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.58 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.12 Y260.28 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.18 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y261.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y261.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y261.47 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.67 Y260.62 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.03 Y261.15 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.68 Y260.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.68 Y260.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.37 Y261.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.37 Y261.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.56 Y261.77 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y261.81 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y261.25 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.30 Y261.01 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.30 Y259.66 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.07 Y259.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y259.46 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.84 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.46 Y260.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y260.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.87 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.23 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.85 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.49 Y260.88 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.85 Y261.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.85 Y261.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.23 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.65 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.12 Y260.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.17 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.67 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.67 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.08 Y260.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.08 Y260.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.47 Y260.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.69 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y260.31 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.44 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.32 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.67 Y261.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.68 Y261.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.98 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.98 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.32 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.14 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.71 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.15 Y260.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.74 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.33 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.33 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.69 Y261.19 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.42 Y261.73 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.05 Y261.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.29 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.29 Y260.40 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.66 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.93 Y260.41 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y260.36 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y261.73 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.30 Y260.98 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.30 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.07 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.49 Y260.58 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.82 Y261.18 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.66 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.08 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.08 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.32 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.32 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.15 Y261.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.66 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.70 Y261.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y261.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.79 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y260.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.89 Y260.10 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.33 Y260.42 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.09 Y260.91 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.48 Y260.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.87 Y260.28 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.56 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.17 Y260.43 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.56 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.01 Y260.35 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y260.35 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.69 Y260.74 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.69 Y260.74 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.23 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.54 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.87 Y260.89 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.29 Y261.01 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.48 Y260.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.67 Y260.08 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y259.43 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.70 Y259.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y260.33 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y260.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.05 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.98 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.61 Y261.20 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.91 Y260.81 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.91 Y260.74 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.39 Y260.64 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.11 Y260.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.11 Y260.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.12 Y260.84 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.63 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.63 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.20 Y260.95 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.91 Y260.39 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.60 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.34 Y260.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.91 Y260.68 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.98 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.89 Y261.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.87 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.67 Y261.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.87 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.87 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.65 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.28 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.06 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.06 Y260.83 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.68 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.28 Y260.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.90 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.90 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y261.69 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.89 Y261.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.87 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.81 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.90 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.50 Y260.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y260.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y261.95 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.08 Y261.66 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.07 Y261.07 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.26 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.26 Y261.95 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y260.58 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.55 Y261.13 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.71 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.34 Y260.41 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.77 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.75 Y261.98 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y261.51 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y261.69 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.72 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y261.95 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.98 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.89 Y261.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.87 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.81 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.90 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.49 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.94 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.38 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.28 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.28 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.75 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.37 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.30 Y261.70 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.30 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.85 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.46 Y260.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y260.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y261.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y261.95 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.49 Y260.88 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.85 Y261.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.85 Y261.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.23 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.34 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.57 Y260.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.90 Y260.28 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.14 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y260.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.05 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.08 Y259.78 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.70 Y259.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y259.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.32 Y259.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.32 Y259.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y260.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.63 Y260.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.83 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.63 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.17 Y260.83 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.73 Y260.02 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.18 Y259.78 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.18 Y259.78 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y259.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y259.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.23 Y259.76 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.52 Y260.28 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.66 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.98 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.32 Y260.44 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.13 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.62 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.05 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.09 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.70 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.65 Y260.64 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.29 Y260.64 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.24 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X002.02 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.71 Y260.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.49 Y260.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.27 Y261.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.97 Y261.90 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.72 Y261.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.48 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.66 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.97 Y261.64 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.97 Y261.64 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.66 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.30 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.30 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.07 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.08 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.08 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.48 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y261.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y261.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.89 Y261.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.89 Y261.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.33 Y261.69 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.07 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.07 Y259.66 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.85 Y259.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.49 Y259.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.27 Y259.66 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.27 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.71 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.09 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.69 Y259.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y260.59 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.67 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.74 Y261.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.83 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.70 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.35 Y260.41 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.38 Y261.72 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.38 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.08 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y261.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.08 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.30 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.30 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.07 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.53 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y261.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.49 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.90 Y261.98 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.30 Y261.51 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.08 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.48 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.48 Y261.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.90 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.05 Y261.98 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.41 Y261.67 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.41 Y261.67 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y260.95 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y260.95 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.27 Y260.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.59 Y260.10 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.59 Y260.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.91 Y260.47 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.03 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.03 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.91 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.58 Y261.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y261.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.45 Y260.97 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.52 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y261.85 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.64 Y261.52 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.64 Y261.52 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.74 Y261.01 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.74 Y261.01 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.64 Y260.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.22 Y260.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y260.01 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y260.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.45 Y260.77 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.30 Y261.69 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.30 Y260.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.07 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.89 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.44 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.92 Y260.89 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.38 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.38 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.49 Y260.61 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.49 Y261.95 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.38 Y261.71 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.38 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.99 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y260.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.25 Y260.59 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.25 Y261.33 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.64 Y261.01 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.64 Y261.73 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.72 Y261.28 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.64 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.64 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.62 Y260.54 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.04 Y260.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.04 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.54 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.87 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.45 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.40 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.90 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.62 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.05 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.14 Y260.46 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.66 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.66 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.57 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.57 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y261.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.37 Y261.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.37 Y261.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.61 Y261.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.12 Y261.79 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.28 Y261.85 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.28 Y260.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y260.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y260.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y260.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.64 Y260.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.23 Y260.34 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.52 Y260.94 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.12 Y260.43 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.40 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.97 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.87 Y261.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.21 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.21 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y261.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y261.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y261.40 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y261.40 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.52 Y260.95 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.26 Y260.43 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.55 Y260.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.75 Y260.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.15 Y260.40 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.15 Y260.40 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y260.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y260.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.26 Y260.40 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.32 Y261.52 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.69 Y261.81 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.69 Y261.81 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.38 Y261.71 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.38 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.72 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.72 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.83 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.76 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.38 Y260.11 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.64 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.64 Y260.95 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.26 Y260.63 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.26 Y261.37 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.64 Y261.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.64 Y261.73 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.57 Y261.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.76 Y261.28 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.68 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.68 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.15 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.10 Y261.27 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.89 Y261.73 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.89 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.48 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.55 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.14 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.14 Y261.73 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.93 Y261.27 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.87 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.87 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.39 Y261.72 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.39 Y260.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.11 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.47 Y260.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.48 Y261.82 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.54 Y261.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.74 Y261.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.94 Y260.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.14 Y260.10 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.11 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.48 Y260.64 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.69 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.89 Y261.69 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.95 Y261.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.95 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.66 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.47 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.18 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.18 Y261.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.90 Y261.87 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.67 Y261.87 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y261.65 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.50 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.68 Y260.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.85 Y260.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.00 Y260.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.21 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y261.35 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.51 Y261.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.70 Y260.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.89 Y260.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.04 Y260.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.21 Y260.57 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.38 Y261.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.55 Y261.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.84 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.25 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.46 Y261.57 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.30 Y261.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.30 Y261.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.13 Y260.57 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.96 Y260.64 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.79 Y261.14 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.63 Y261.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.83 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.36 Y261.41 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.11 Y260.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.98 Y260.43 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.68 Y261.37 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.79 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.05 Y261.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.08 Y260.47 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.83 Y260.47 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.56 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.32 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.05 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.05 Y261.82 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y261.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.54 Y261.26 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y260.66 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.16 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y261.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y261.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.16 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.95 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.70 Y261.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.65 Y260.95 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.00 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.00 Y260.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.67 Y260.02 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.12 Y260.38 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.68 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.68 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.21 Y261.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.21 Y261.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.69 Y261.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.69 Y261.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.25 Y261.91 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.15 Y261.32 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.64 Y261.60 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.16 Y261.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y261.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y260.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y260.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y260.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.75 Y260.14 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y260.66 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.94 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.36 Y261.82 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.66 Y261.81 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.37 Y260.93 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.56 Y261.66 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.22 Y261.58 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.86 Y261.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y261.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.31 Y261.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.22 Y260.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.72 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.72 Y259.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y260.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.75 Y261.20 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.37 Y260.63 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.72 Y261.14 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.72 Y261.14 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y260.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y260.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y260.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.38 Y260.51 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.03 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.78 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y260.69 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.03 Y260.69 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.35 Y262.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.93 Y262.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y262.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.37 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.37 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.91 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.91 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.37 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.37 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.37 Y262.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.05 Y262.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.35 Y262.40 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.57 Y262.62 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.57 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.52 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.07 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.07 Y262.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.52 Y262.78 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y262.78 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.44 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.44 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.15 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.00 Y261.47 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y261.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y261.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.67 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.67 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.67 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.39 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.38 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.10 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.10 Y261.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.01 Y261.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.49 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.49 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y261.36 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.67 Y262.58 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.54 Y262.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.08 Y261.76 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.41 Y261.64 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.41 Y260.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.48 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.48 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.33 Y260.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.07 Y260.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.07 Y260.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y261.64 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y261.64 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.41 Y261.64 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.41 Y261.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y261.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y261.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y262.58 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.41 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.41 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.13 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.17 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.89 Y261.36 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.31 Y261.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.68 Y261.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.29 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.79 Y261.35 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.04 Y261.84 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.40 Y261.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.89 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.89 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.45 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.45 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.11 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.11 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.59 Y260.07 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.95 Y260.37 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.10 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.10 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.88 Y261.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.88 Y261.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y261.84 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.59 Y260.98 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.75 Y261.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.11 Y261.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.63 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.64 Y260.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.51 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.65 Y262.48 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.65 Y261.63 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.65 Y261.63 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.11 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.11 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.44 Y261.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.33 Y260.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.33 Y260.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.05 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.05 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.63 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.63 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.34 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.05 Y260.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.05 Y262.81 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.61 Y260.95 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.70 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y261.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y261.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.60 Y261.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.63 Y261.43 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.63 Y260.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.61 Y260.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.11 Y260.11 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.11 Y260.11 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.62 Y260.62 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.44 Y261.53 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.44 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.11 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.11 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.68 Y260.33 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.69 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.38 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.10 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.10 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.67 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.67 Y260.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.67 Y260.61 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.19 Y260.11 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.19 Y260.11 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.41 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.80 Y261.47 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.13 Y261.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.12 Y261.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.47 Y261.63 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.47 Y261.63 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y260.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y260.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.27 Y260.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.62 Y260.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.11 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.11 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.81 Y260.37 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.66 Y260.41 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.20 Y260.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.20 Y260.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y260.34 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.61 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.61 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y261.70 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.45 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.45 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.17 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.11 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.47 Y262.24 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.96 Y262.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.96 Y262.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y261.89 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.50 Y261.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.30 Y261.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y260.89 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y260.89 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.26 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.22 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.89 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.89 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.00 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.38 Y260.46 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.70 Y260.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y260.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.35 Y261.01 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.35 Y261.01 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.78 Y261.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.00 Y261.90 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.00 Y261.90 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.69 Y262.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.06 Y262.66 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.06 Y262.66 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y262.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.22 Y262.01 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.24 Y262.75 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.16 Y261.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.32 Y261.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.46 Y262.39 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.46 Y262.39 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.05 Y262.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.30 Y262.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.30 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.30 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.69 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.69 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.40 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.40 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.80 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.80 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.80 Y262.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.05 Y262.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.73 Y262.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.93 Y261.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.85 Y262.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.85 Y262.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.24 Y262.75 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.44 Y262.47 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.44 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.15 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.15 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.00 Y261.47 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y261.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y261.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.67 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.67 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.67 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.39 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.38 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.10 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.10 Y261.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X002.01 Y261.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.49 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.49 Y261.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.85 Y261.40 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.85 Y261.40 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.85 Y262.81 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.58 Y260.91 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.83 Y260.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.90 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.72 Y261.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.07 Y261.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.07 Y261.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.45 Y261.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.45 Y261.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.26 Y260.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.61 Y260.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.12 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.12 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.89 Y260.43 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.69 Y260.37 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.18 Y260.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.18 Y260.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.67 Y260.41 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.67 Y260.41 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.91 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y261.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.07 Y261.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.53 Y261.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.53 Y261.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y261.02 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.41 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.62 Y261.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y260.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y260.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.98 Y259.78 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.52 Y259.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.08 Y259.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y259.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y259.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y259.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.16 Y259.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.41 Y260.34 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.66 Y260.88 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.90 Y261.43 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X002.25 Y261.80 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.61 Y261.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.68 Y261.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.44 Y260.78 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.31 Y260.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.80 Y261.64 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.02 Y261.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y261.80 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.50 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.50 Y260.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.66 Y260.15 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.66 Y261.27 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.80 Y260.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.50 Y260.43 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.12 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.93 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.32 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.72 Y260.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.72 Y260.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.99 Y260.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.99 Y260.92 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y261.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y261.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.46 Y261.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.46 Y260.14 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y260.14 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y261.09 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.74 Y260.79 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.46 Y260.37 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.91 Y260.79 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.49 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.49 Y261.12 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y260.82 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.13 Y261.52 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.08 Y261.01 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.76 Y261.38 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.76 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.44 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.28 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.97 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.97 Y261.38 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.64 Y261.01 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.59 Y261.52 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.59 Y261.52 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.13 Y261.52 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.26 Y261.35 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.26 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.43 Y260.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y260.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.92 Y260.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.92 Y260.16 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.43 Y260.79 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.43 Y261.56 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.26 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.95 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.57 Y261.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.07 Y260.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.07 Y260.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.99 Y260.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.84 Y260.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.27 Y260.58 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y261.36 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y260.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.32 Y260.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.94 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.94 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y260.43 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y260.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y261.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.39 Y261.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.39 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.39 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.08 Y260.06 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y260.61 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y261.37 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.26 Y260.78 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.26 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.43 Y260.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y260.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.92 Y260.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.92 Y260.16 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.26 Y260.78 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.26 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.43 Y260.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.43 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.29 Y260.68 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.56 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.98 Y260.70 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.68 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.42 Y260.78 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.25 Y260.78 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.25 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.41 Y260.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y260.93 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.47 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.86 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.62 Y260.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.20 Y260.78 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.47 Y261.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.08 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.20 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.74 Y260.25 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.35 Y261.38 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.04 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.25 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.25 Y260.31 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.42 Y260.29 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.42 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.29 Y260.68 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.56 Y260.07 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.66 Y259.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.98 Y260.70 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.56 Y261.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.07 Y260.47 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.07 Y260.47 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.13 Y260.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.13 Y260.48 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.59 Y261.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y260.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.26 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.26 Y260.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.43 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.43 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.64 Y261.59 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.26 Y261.20 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y261.37 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.18 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.75 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y260.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.96 Y260.94 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.20 Y261.24 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y260.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y260.65 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y261.36 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.44 Y260.34 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.04 Y260.07 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.26 Y261.34 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.26 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.43 Y260.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.43 Y261.56 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.54 Y261.42 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.86 Y261.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y261.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.52 Y260.48 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.12 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.01 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.76 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.07 Y261.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.07 Y261.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.57 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.12 Y261.10 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.56 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.17 Y261.20 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.09 Y260.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.19 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.04 Y260.28 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y261.35 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y261.43 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.90 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.90 Y260.62 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.70 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y260.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.30 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.55 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.86 Y260.38 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.10 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.59 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.59 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y260.39 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y260.41 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y260.79 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y260.79 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y261.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y261.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.59 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.59 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.17 Y261.20 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.55 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.11 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.09 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.71 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.21 Y260.87 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.97 Y261.52 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y261.52 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y260.95 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.39 Y260.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.55 Y260.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y261.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y261.32 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y260.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y260.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.55 Y260.59 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.26 Y260.78 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.26 Y260.17 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.43 Y260.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y260.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y260.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y260.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.10 Y260.58 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y260.93 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.59 Y260.68 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.59 Y260.16 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.61 Y261.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.08 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.08 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.63 Y260.05 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.28 Y260.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.28 Y260.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.61 Y260.93 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.25 Y261.35 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.25 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.25 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.25 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.74 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.41 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.41 Y261.56 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.72 Y260.93 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.05 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.25 Y260.80 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.25 Y259.73 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.72 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.25 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.25 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.75 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.05 Y260.45 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.70 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.72 Y260.92 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.05 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.77 Y260.60 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.10 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.47 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.95 Y260.67 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.52 Y261.00 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.28 Y260.23 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.77 Y260.54 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.61 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.08 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.08 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.59 Y259.98 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.08 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.08 Y261.56 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.28 Y260.47 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.48 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y261.36 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.56 Y260.59 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y260.16 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y260.76 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.07 Y260.50 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.07 Y261.10 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y260.83 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y261.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.37 Y261.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.49 Y261.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y261.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.42 Y261.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y261.53 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.73 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.19 Y260.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.19 Y260.42 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.88 Y260.86 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.61 Y261.52 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.05 Y261.08 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.61 Y261.59 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.13 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.13 Y261.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.41 Y260.76 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.05 Y260.36 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.72 Y260.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.72 Y260.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.21 Y260.40 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.96 Y261.58 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.76 Y261.07 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.56 Y260.57 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.36 Y260.06 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.16 Y259.55 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.23 Y259.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.46 Y260.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.68 Y260.70 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.91 Y261.27 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y261.34 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.23 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.01 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.71 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.09 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.09 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.92 Y260.40 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.63 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.43 Y260.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.43 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.25 Y261.35 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.25 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.41 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.41 Y261.56 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.31 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.50 Y261.35 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.73 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.27 Y261.25 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.74 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.74 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.40 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.61 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y260.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y260.45 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.63 Y260.96 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.32 Y260.55 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.62 Y260.90 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.88 Y260.43 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.60 Y260.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y260.02 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.32 Y260.46 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.07 Y259.83 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.60 Y259.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y259.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.13 Y259.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.13 Y259.83 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y260.16 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y260.33 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.92 Y260.66 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.15 Y260.66 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.41 Y261.39 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.96 Y261.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.96 Y261.59 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.39 Y261.36 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.13 Y260.80 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.13 Y260.77 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.38 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.97 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.97 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.59 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.52 Y260.52 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.98 Y260.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.98 Y260.03 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.51 Y260.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.37 Y260.76 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.46 Y261.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.98 Y261.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.98 Y261.51 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.51 Y261.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.59 Y260.97 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.59 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y261.37 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.75 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y260.74 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.13 Y260.33 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.52 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.37 Y260.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.18 Y260.72 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.06 Y260.77 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y261.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y261.13 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.86 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y260.80 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y261.38 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.78 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.22 Y261.14 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.22 Y261.14 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.74 Y260.80 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.88 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.61 Y260.04 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.50 Y259.96 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.94 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.94 Y260.22 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.09 Y260.75 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.93 Y261.34 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.59 Y261.50 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.99 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.56 Y260.49 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.88 Y260.94 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.58 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.58 Y260.56 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.31 Y260.98 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.31 Y261.07 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.60 Y261.44 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.87 Y261.01 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y261.37 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y260.76 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y260.76 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.76 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.76 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.69 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y260.21 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.46 Y261.37 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.69 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y261.36 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y260.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y260.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y260.84 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y261.36 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y261.37 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.98 Y260.71 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.49 Y261.09 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.49 Y261.11 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.89 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.75 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y260.77 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y261.39 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.81 Y261.46 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y261.11 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.26 Y261.11 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.85 Y260.77 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y261.37 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.76 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.91 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.44 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.69 Y260.76 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.69 Y260.76 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.51 Y261.27 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.03 Y261.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.91 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y261.54 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.42 Y260.43 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.07 Y260.10 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y260.20 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y261.30 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.83 Y261.47 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.37 Y261.21 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y261.36 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.33 Y260.18 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.82 Y260.00 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y260.19 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.53 Y260.73 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.06 Y260.47 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.06 Y261.07 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y260.81 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X000.53 Y261.38 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.46 Y261.02 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X001.39 Y261.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X001.39 Y261.53 Z000.00 F6000.00 ; move_abs_xy     
G0 X000.10 Y261.53 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X-18.50 Y266.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X114.22 Y266.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X-18.50 Y266.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           
G0 X034.58 Y266.99 Z000.00 F6000.00 ; move_abs_xy     
M400                                ; wait_on_actions 
M280 P0 S045.00                     ; servo_set       
G4 P200.00                          ; dwell           
G28 X Y                             ; home_xy         
M400                                ; wait_on_actions 
M280 P0 S135.00                     ; servo_set       
G4 P200.00                          ; dwell           

M84 ; disable motors

; END

